import clasiffier.DatabaseMng;
import clasiffier.DatabaseMng.TYPEDATA;
import ij.IJ;
import ij.ImagePlus;
import ij.Macro;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import ij.gui.Plot ;
import morpho.StatisticalMoments;
import morpho.StructElementListPixels;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.profe.plugins.TesisRGBPromedioMinimoPlugin;
import morpho.color.jvazqnog.profe.plugins.TesisRGBPromedioSinVentanasMinimoPlugin;
import utils.ParamMng ;
import utils.MiscelaneousArray ;


/**
 * <p>Implementacion de covarianza morfologica en color usando DESVIACION con ventana 
 * retorna una curva de distribución de orientacion,distancia y volumen .
 * Utiliza las orientaciones (grandos sexagesimales ):  0 , 45 , 90 , 135 
 *  </p>
 * @author nelsond
 * @param  CANT_DIST cantidad de distancias 
 * @param  INCREMENT_SIZE  tamanho del paso entre cada distancia    
 * @return retorna una curva de distribucion de orientacion.distancia mapeado a tamanho
 *  
 * */


public class O_CovarianceColorPromConVentan_  implements PlugInFilter {
	String macroOptions = null ;
	
	ImagePlus  imp  = null;
	
	/*parametros principales del algoritmo */
	
	/* cantidad de distancias a ser evaluadas */
    private int CANT_DIST = 20 ;
    
	/* paso entre las distancias si es para 2: 0, 2, 4, 6.. ; para 1: 0,1,2,3..    */
    private int INCREMENT_SIZE = 1;
    
    /* tamanhos  de rois para las ventanas   */
    int ROISIZE = 4 ;
    
    //directorio de salida 
    String dirOutput = "covarcolor/" ;
    
    
	public int setup(String arg, ImagePlus imp) {
		this.imp = imp ;			
		
		
		macroOptions = Macro.getOptions();
		
		//si no reciben parametros entonces mostrar dialogbox
		if (macroOptions == null) {
			int [] p =  ParamMng.dialogCovariance();
			  this.CANT_DIST = p[0] ; 
			  this.INCREMENT_SIZE =  p[1] ; 
		}
		
		return DOES_ALL;
	
	}
	
	public void run(ImageProcessor orig) {	
	
		
		//cantidad de tamanhos de elementos estructurantes  
		int CANT_ANGLES = 4; 
		
		//grafico
		double[] plotx = new double[ CANT_DIST * CANT_ANGLES ];
		double[] ploty = new double[ CANT_DIST * CANT_ANGLES ];
				
		//lista de distancias 
		int [] listDistances = new int[CANT_DIST ];
		MiscelaneousArray.setSerialArray(listDistances, 1 , INCREMENT_SIZE);
		
		//lista de angulos 
		int [] angles = {0, 45, 90, 135};
		
		//volumen  de imagen de entrada
		ImageProcessor currenOrigt16bits = imp.getProcessor().convertToShortProcessor();

		double volOrig = StatisticalMoments.calcVolumen( currenOrigt16bits );
		
		System.out.println("volOrig : " + volOrig );
		
		int auxi = 0 ; 
		
		
		//angulos X distancias 
		for(int a = 0; a< angles.length; a++){
			int angle = angles[ a ] ;
		
		for(int s  = 0; s < CANT_DIST ; s++ ){
			int distance = listDistances[s];
			
			
			//elemento estructurante de forma cuadrada para una direccion 
			Pixel [] se = StructElementListPixels.getSESquare( 0 , distance, angle);
						
			//erosion  (minimo)  
			String imgErod = imp.getShortTitle()+ "_covarconvent" + "_dist"+distance  + "_angle"+ angle ;
			
			TesisRGBPromedioMinimoPlugin   promMin  = new TesisRGBPromedioMinimoPlugin ();
			promMin.setup("",this.imp );
			promMin.setRois( ROISIZE ,  ROISIZE );
			promMin.setOutput( imgErod , dirOutput);
			promMin.setSE( se );
			promMin.run(orig);	
			
			
			
			//conversion a 16 bits 
			ImagePlus impFinal = IJ.openImage( dirOutput + imgErod + ".bmp" ); 
			
			ImageProcessor current16bits = impFinal.getProcessor().convertToShortProcessor();
			
			double volumen = StatisticalMoments.calcVolumen( current16bits );
			
			//double volumen = StatisticalMoments.calcVolumen( copy );
				
			System.out.println(distance + "\t" + volumen ) ;

			//plotx[auxi] = distance ;	
			plotx[auxi] = auxi ;	
			ploty[auxi] = volumen / volOrig ;

			auxi++;	 
			
			
			}
		
		}
		
		/*Generacion de archivos auxiliares de salida para el clasificador */
		//if (  macroOptions != null ){
		
			DatabaseMng dbmg = new DatabaseMng();
			dbmg.typedata= TYPEDATA.TRAIN;
			String params = "CANT_DIST" + CANT_DIST  + "_INCREMENT_SIZE"  +INCREMENT_SIZE + "_ROISIZE" + ROISIZE; 
			dbmg.inserTextureProp(imp.getTitle() ,  ploty ,macroOptions,-1, params );	
			 
			 
			 
		//}
		
		if ( macroOptions == null ) {	
			Plot covarPlot ;
			covarPlot = new Plot("","distancia","volumen",plotx, ploty); 
			covarPlot.show() ;
		}
		
		
	}
	
	

	

}


