import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.profe.plugins.RGBDeviationMax;
import morpho.color.jvazqnog.profe.plugins.RGBDeviationMin;
import morpho.color.jvazqnog.profe.plugins.RGBDeviationWWMax;
import morpho.color.jvazqnog.profe.plugins.RGBDeviationWWMin;
import morpho.color.jvazqnog.profe.plugins.TesisRGBPromedioSinVentanasMaximoPlugin;
import morpho.color.jvazqnog.profe.plugins.TesisRGBPromedioSinVentanasMinimoPlugin;

/**
 * Granulometria color, procesamiento vectorial fijando pesos CON DESVIACION SIN VENTANAS
 * 
 * */

public class GranulometryColorDeviationWW_ extends GranulometryBasic {
	
	
	
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		
		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 
		String currentcolorimg = "WW_" + imp.getShortTitle() + "_granDESV" + "_s" + sizeSE  ;
			
		//erosion  (minimo)  
		RGBDeviationWWMin  desvMin  = new RGBDeviationWWMin(currentcolorimg , dirOutput,
				"bmp", (ColorProcessor)  this.imp.getProcessor(), se );
		
		desvMin.run();
	
		//dilatacion maximo,  
		ImagePlus impEro = IJ.openImage( dirOutput + currentcolorimg + ".bmp" );
		//se reemplaza la imagen a color actual erosionada con su dilatacion 
		RGBDeviationWWMax  desvMax  = new RGBDeviationWWMax( currentcolorimg, dirOutput,
				"bmp", (ColorProcessor)  impEro.getProcessor(), se  );
		
		desvMax.run();
		
		//cargar en memoria la imagen sobreescrita 
		ImagePlus impFinal = IJ.openImage( dirOutput + currentcolorimg + ".bmp" ); 
		return impFinal.getProcessor();
		
	
	}

}



