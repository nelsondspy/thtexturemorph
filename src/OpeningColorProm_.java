import morpho.color.jvazqnog.profe.plugins.*;
import ij.IJ;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;


public class OpeningColorProm_ implements PlugInFilter {
	ImagePlus imp ;
	
	/**
	 * 
	 * */
	public int setup(String arg, ImagePlus imp) {
		this.imp = imp;
		
		return DOES_ALL ;	
	}
	
	public void run(ImageProcessor orig) {
		
		
		final String PATHOUTP = "testcolor/";
		
		final int SIZEES =  10 ;
		
		final int ROISIZE = 20;
		
		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 
		String imgErod = imp.getShortTitle() + "_ero_ROISIZE_"+ROISIZE ;
		String imgDil = imp.getShortTitle() + "_dil_ROISIZE_" + ROISIZE ;
		
		//erosion  (minimo)  
		TesisRGBPromedioMinimoPlugin  promMin  = new TesisRGBPromedioMinimoPlugin();
		promMin.setup("",this.imp );
		promMin.setRois(ROISIZE , ROISIZE );
		promMin.setOutput( imgErod, PATHOUTP  );
		promMin.setseEight( SIZEES );
		promMin.run(orig);

			 
		//dilatacion ( maximo )
		TesisRGBPromedioMaximoPlugin promMax = new TesisRGBPromedioMaximoPlugin();
		promMax.setup("",this.imp );
		promMax.setRois(ROISIZE , ROISIZE );
		promMax.setOutput( imgDil, PATHOUTP  );
		promMax.setseEight( SIZEES );
		promMax.run(orig);
		
		//apertura
		ImagePlus impOpening = IJ.openImage( PATHOUTP  + imgErod+".bmp" );  
		TesisRGBPromedioMaximoPlugin promMax2 = new TesisRGBPromedioMaximoPlugin();
		promMax2.setup("", impOpening );
		promMax2.setRois(ROISIZE , ROISIZE  );
		promMax2.setOutput( imgDil+"_apertura_ROISIZE_" + ROISIZE , PATHOUTP );
		promMax2.setseEight( SIZEES );
		promMax2.run( impOpening.getProcessor() );
		
		
	}
	
	
}
