import order.LabLexicograph;
import ij.ImagePlus;
import morpho.color.jvazqnog.models.Pixel;

/**
 * Covarianza morgologica Lab lexicografico 
 * */
public class CovarianceLabLex_ extends CovarianceBasicStack {
	
	
	/**
	 * covarianza M. usando distancias euclidea 
	 * */
	public ImagePlus applyMorphoOperation(int distance, int orientation , Pixel [] se  ){

		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 

		String imgErod = imp.getShortTitle()+ "_M=covarLex" + "_dist="+distance  + "_angl="+ orientation+ "_ROI=" + PRM.ROISIZE ;

		LabLexicograph lexLab = new LabLexicograph();
		lexLab.setImageProcessor(this.imp.getProcessor());

		//erosion  (minimo) 
		ImagePlus eroded = lexLab.filter( se, dirOutput , imgErod , LabLexicograph.filterType.MIN );
		return eroded ;
		
	}	

	@Override
	public String getColorspace() {
		return "Lab";
		
	}
	
}
