import morpho.color.jvazqnog.profe.plugins.*;
import ij.IJ;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;


public class OpeningColorSinVentanas_ implements PlugInFilter {
	ImagePlus imp ;
	
	/**
	 * 
	 * */
	public int setup(String arg, ImagePlus imp) {
		this.imp = imp;
		
		return DOES_ALL ;	
	}
	
	public void run(ImageProcessor orig) {
		
		
		final String PATHOUTP = "testcolor/";
		final int SIZEES =  10 ;
		
		//String imgErod = imp.getTitle() + "_ero";
		//String imgDil = imp.getTitle() + "_dil";
		
		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 
		String imgErod = imp.getShortTitle() + "_ero_sinvent" ;
		String imgDil = imp.getShortTitle() + "_dil_sinvent" ;
		
		
		//erosion  (minimo)  
		TesisRGBPromedioSinVentanasMinimoPlugin  promMin  = new TesisRGBPromedioSinVentanasMinimoPlugin();
		promMin.setup("",this.imp );
		promMin.setOutput( imgErod, PATHOUTP  );
		promMin.setseEight( SIZEES );
		promMin.run(orig);

			 
		//dilatacion ( maximo )
		TesisRGBPromedioSinVentanasMaximoPlugin promMax = new TesisRGBPromedioSinVentanasMaximoPlugin ();
		promMax.setup("",this.imp );
		promMax.setOutput( imgDil, PATHOUTP  );
		promMax.setseEight( SIZEES );
		promMax.run(orig);
		
		 
		//apertura
		
		ImagePlus impOpening = IJ.openImage( PATHOUTP  + imgErod+".bmp" );  
		TesisRGBPromedioSinVentanasMaximoPlugin promMax2 = new TesisRGBPromedioSinVentanasMaximoPlugin ();
		promMax2.setup("", impOpening );
		promMax2.setOutput( imgDil+"_apertura" , PATHOUTP );
		promMax2.setseEight( SIZEES );
		promMax2.run( impOpening.getProcessor() );
		

	}
	
	
}
