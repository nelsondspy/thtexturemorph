import ij.ImagePlus;
import ij.process.ImageProcessor;
import ij.plugin.filter.PlugInFilter;

import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.util.Various;
import order.Bitmixing;
import order.Lexicographical;
import order.Marginal;
import order.NormalEuclid;

public class DilateColor_ implements PlugInFilter {

	ImagePlus imp;
	
	public int setup(String arg, ImagePlus imp) {
		this.imp = imp;
		return DOES_ALL; 
		//return DOES_ALL;
	}
	
public void run(ImageProcessor orig) {
		
		String currentcolorimg =  this.imp.getShortTitle() + "_s" + 7  ;
	
		Marginal marginal = new Marginal();
		Lexicographical lexic = new Lexicographical();
		NormalEuclid normeuclid = new NormalEuclid();
		Bitmixing bitmix = new Bitmixing();


		
		
		Pixel[] se = Various.getShiftArray( 8 );
		
		//erosion  (minimo) 
		marginal.dilate(orig ,se, "" ,  currentcolorimg+"_marginal");
		lexic.dilate(orig ,se, "" ,  currentcolorimg+"_lexic" );
		normeuclid.dilate(orig ,se, "" ,  currentcolorimg+"_norma" );
		bitmix.dilate(orig ,se, "" ,  currentcolorimg+"_bitmix" );

	}
	
	

	
	
}
