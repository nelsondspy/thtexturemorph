import order.Bitmixing;
import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.StructElementListPixels;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.profe.plugins.RGBEntropyMin;

/**
 * Covarianza morgologica usando pesos ENTROPIA CON VENTANAS 
 * */
public class CovarianceColorBitMixing_ extends CovarianceBasic {
	
	
	/**
	 * covarianza M. usando Bitmixing  
	 * */
	public ImageProcessor applyMorphoOperation(int distance, int orientation , Pixel [] se  ){

		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 

		String imgErod = imp.getShortTitle()+ "_M=covarBitmix" + "_dist="+distance  + "_angl="+ orientation+ "_ROI=" + PRM.ROISIZE ;

		Bitmixing bitmix = new Bitmixing();
		bitmix.erode(this.imp.getProcessor(),se, dirOutput , imgErod );
		
		//
		ImagePlus impFinal = IJ.openImage( dirOutput + imgErod + ".bmp" ); 
		return impFinal.getProcessor();
		
	}	

	
	
}
