import java.io.File;
import java.util.ArrayList;

import ij.ImagePlus;
import ij.Macro;
import ij.gui.Plot;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import ij.process.ByteProcessor;
import ij.process.ShortProcessor ;
import ij.process.ByteBlitter;
import morpho.gray.*;
import utils.MiscelaneousArray;
import utils.ParamMng ;
import morpho.StatisticalMoments;
import clasiffier.DatabaseMng.TYPEDATA;
import clasiffier.pca.OtherReductionMethods;
import clasiffier.pca.RunPCAUnsupervisedFilter;
import clasiffier.DatabaseMng;
import clasiffier.ExportToARFF;



public class HybridCovarGranByte_ implements PlugInFilter {
	
	String macroOptions = null ;
	ImagePlus  imp  = null;
	
	
	
	
	/*Parametros del algoritmo */
	
	//longitud vector de distancias 
	private int CANT_DIST = 10  ;
	//pasos entre distancias  
	private int STEP_DISTANCE = 1 ;
	//longitud del vector de tamanhos
	private int CANT_SIZES = 15;
	//paso entre tamanhoss
	private int STEP_SIZES = 2 ;
	
	//lista de elementos estructurantes creados 
	ArrayList<int [][] > arraSEsize = new ArrayList<int [][]>();
	
	
	public int setup(String arg, ImagePlus imp) {
		
		this.imp = imp ;				
		
		macroOptions = Macro.getOptions();
		if (macroOptions == null  ){
			
			int[] p = ParamMng.dialogHybridCovarGran();
			
			CANT_DIST = p[0] ;
			STEP_DISTANCE = p[1] ;
			CANT_SIZES = p[2];
			STEP_SIZES = p[3] ;
		}
		
		return DOES_8G; 
		
	}

	
	public void run(ImageProcessor orig) {
		
		double [][] distriSizeOrient = new double [ CANT_DIST * 4 ][ CANT_SIZES ]; 
		ImageProcessor imDistr = new ShortProcessor( CANT_DIST * 4, CANT_SIZES );

		//lista de distancias 
		int [] listDistOrient = new int[CANT_DIST ];
		
		MiscelaneousArray.setSerialArray(listDistOrient , 1  ,STEP_DISTANCE);
		
		//lista de angulos 
		int [] listAngles = {0, 45, 90, 135};		
			
		//lista de tamanhos
		int [] listSizes = new int[CANT_SIZES];
		MiscelaneousArray.setSerialArray(listSizes, 0 , STEP_SIZES);
		
		//lista de elementos estructurantes 
		MiscelaneousArray.setArraySESizes(listSizes, arraSEsize);
		
		
	
		//volumen  de imagen de entrada
		double volOrig = StatisticalMoments.calcVolumen( orig );
				
		//System.out.println("volOrig : " + volOrig );
		
		int axSizeOrient = 0 ; 
		int tmp1 = 0 ;
		
		/* Orden de iteracion : orientacion X distancia X tamanho */
		
		//itera la orientacion 
		for(int a = 0 ; a < listAngles.length;  a++){
			int angle = listAngles[ a ] ;

		//itera la distancia 
		for(int d  = 0; d < CANT_DIST ; d++ ){
			int distance = listDistOrient[d];
			
				
				//itera el tamanho 
				for(int s = 0; s < listSizes.length ; s++ ){
					//int size = listSizes[s];
					int size = s ;
					
					ImageProcessor copy = orig.duplicate();
					
					//apertura 
					//int[][] se = StrucElementSizeDist.SESquare( distance ,angle , size);
					//Operator.erode(copy, se);		
					//Operator.dilate(copy, se);
					
					// distancia-angulo , tamanho  
					//int[][] se = StructElement.SESquare( size );
					
					int[][] se = arraSEsize.get(size);
					
					OperatorPairSE.erode(copy, se , distance , angle );
					OperatorPairSE.dilate(copy, se , distance , angle );
					
					/*
					String title = "im_copy: " + distance + ",angle :" + angle  ;
					ImagePlus imgse = new ImagePlus(title, copy);
					imgse.show();
					*/
									
					
					double volumenNorm = StatisticalMoments.calcVolumen( copy ) / volOrig;
					distriSizeOrient[axSizeOrient][s] = volumenNorm ;
					
					tmp1++;
					System.out.println( "tmp1:" + tmp1 );
					
					imDistr.set(axSizeOrient , s, (int)(volumenNorm * 65536) ) ;
				}	
				
				axSizeOrient ++; //incrementar en distancia x orientacion 
			}
		}
		
		//
		if ( macroOptions  == null ){
			ImagePlus imagePdist = new ImagePlus("Distribucion", imDistr);
			imagePdist.show();			
		}
		
		/*exportar a archivo */
		
		/* se trata de evitar el conflicto de nombre con otro hilo  */
		String filename = imp.getShortTitle() + "__D" + CANT_DIST + "paso" +
				STEP_DISTANCE + "_S" + CANT_SIZES + "paso" + STEP_SIZES +  ".arff" ;
		
		
		/* genera conflictos al tratar de sobre escribir */
		//String filename = "temph.arff";
		
		ExportToARFF arffile = new ExportToARFF("dist_distanorient_size", "v", 
				(CANT_SIZES ), 
				filename , 
				0 );
	
		
		arffile.putHeader();
		
		/*cada instancia esta compuesto por varios tamanhos, 
		 * solo se utilizan los tamanhos para la reduccion por pca  */
		
		for(int od = 0; od < distriSizeOrient.length ; od++ ){
			//String val = od + "" ;
			String val =  "" + distriSizeOrient[ od ][ 0 ];		
			for (int s = 1 ;  s < listSizes.length  ; s++ ){
				val = val  +"," + distriSizeOrient[ od ][ s ] ; 			
			}
			
			//val = val +"," +od  ;
			//val = val +",? "  ;
			
			arffile.insertDataRow( val  );	
		}
		
		arffile.closeFile();  
		
		
		/* Analisis de PCA */
		double [] finalAttributes = null ;
		
		try {
			
			finalAttributes = RunPCAUnsupervisedFilter.reduce( filename );
			//finalAttributes = OtherReductionMethods.varianMedianVector(distriSizeOrient);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//if ( macroOptions  == null ){

		//prepara una cadena legible con los parametros 
		String paramdesc = ParamMng.getstrHybridCov(CANT_DIST, STEP_DISTANCE,
				CANT_SIZES, STEP_SIZES );

		DatabaseMng dbmg = new DatabaseMng();
		dbmg.typedata= TYPEDATA.TRAIN;
		
		dbmg.inserTextureProp(imp.getTitle() ,  finalAttributes,macroOptions,-1, paramdesc);
		
		//borrar arrff archivo generado anteriormente 
		try{
			File delFile  = new File(filename);
			//if( delFile.exists() ) delFile.delete();
		}
		catch(Exception e ){
			System.out.println("error al borrar el archivo");
		}

		
		//}
		
	}
	

	
}
