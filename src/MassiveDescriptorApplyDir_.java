import java.io.File;
import java.util.Arrays;

import clasiffier.DatabaseMng;
import clasiffier.DatabaseMng.TYPEDATA;
import descriptor.Descriptor;
import descriptor.Util;
import ij.*;  
import ij.plugin.PlugIn;
import ij.gui.GenericDialog;

/**
 * Genera registros de un nuevo experimento utilizando las series 
 * morfologicas generadas en un directorio de entrada.
 * Asumiendo que se utilizo un esquema de nombres que permite ordenar los archivos
 * por su nombre con longitud fija. Por ejemplo 000002_ANT_0002.bmp..000002_ANT_0004.bmp  
 * */
public class MassiveDescriptorApplyDir_ implements PlugIn {
	
	String experimentId ;
	String dirPath ;
	String descriptorName ;
	int serieSize ; 
	String database;

	/** tipo de descriptor para cada elemento de la serie */
	Descriptor descriptorInstance = null ; 
	

	
	/**
	 * 
	 * */
	public void run(String  args ){
		
		//leer los parametros desde el form
		guiReadParams();
		
		long tiempoInicio = System.currentTimeMillis();
		
		//instanciar el descriptor 
		descriptorInstance = Util.getDescriptorInstance( descriptorName ) ;
		descriptorInstance.setDim(serieSize);
		
		//iterar la lista de archivos 
		String [] dirs  = listFilesinDir(this.dirPath);
		
		String currentFilename = dirs[0].split( "_" )[0];
		String filename = "";
		String extension =  ".bmp";
		
		//base de datos 
		DatabaseMng dbmg = new DatabaseMng();
		dbmg.typedata= TYPEDATA.TRAIN;

		
		for(int f = 0 ; f < dirs.length ; f++  ){
			
			String[] partes =  dirs[f].split( "_" );
					
			filename =  partes[0];
			
			
			System.out.println("filename =" + filename );
			
					
			ImagePlus imp = IJ.openImage(dirPath + dirs[f] );  
			
			//corte de control e  insertar el registro
			if (! currentFilename.equals(filename) ) {
				
				descriptorInstance.postProcess();
				
				/*--- almacenar vector de caracteristicas */
				
				/*Insercion */
				dbmg.inserTextureProp( currentFilename + extension, descriptorInstance.getVectorDescriptor() , experimentId , -1, dirPath + filename  );	
				/*-- fin almacenar */
				
				//reinicio de descriptor y nueva imagen 
				descriptorInstance = Util.getDescriptorInstance( descriptorName ) ;
				descriptorInstance.setDim(this.serieSize);
				currentFilename = filename ;
				
			} 
			
			System.out.println(dirPath + filename);
			
			descriptorInstance.calculateAndAdd( imp.getProcessor() );
        	IJ.run(imp,"Close","");		
		}
		
		//evitar ultimo elemento sin procesar 
		descriptorInstance.postProcess();
		/*Insercion */
		dbmg.inserTextureProp( filename + extension, descriptorInstance.getVectorDescriptor() , experimentId , -1, dirPath + filename  );	
		/*-- fin almacenar */
		
		/*generacion del archivo ARF */
		RunDBtoARF_ runARFfilegen = new RunDBtoARF_();
		runARFfilegen.experimentId = this.experimentId ; 
		runARFfilegen.database = this.database ; 
		
		runARFfilegen.run(null);
		
		long tiempoDelta  = ( System.currentTimeMillis()  - tiempoInicio) / (1000 * 60) ;
		String msg = "pasaron :" + tiempoDelta;
		showDialog( msg) ;

	}
	

	/**
	 * Lista de archivos ordenados por nombre
	 * */
	static String []  listFilesinDir(String path ){
		
		File folder = new File( path );
		File[] listOfFiles = folder.listFiles();
		
		String lsfilesNames []= new String[listOfFiles.length ]; 

		    for (int i = 0; i < listOfFiles.length; i++) {
		      if (listOfFiles[i].isFile()) {
		        
		    	  lsfilesNames[i]= listOfFiles[i].getName();
		    	  
		      } else if (listOfFiles[i].isDirectory()) {
		        //System.out.println("Directory " + listOfFiles[i].getName());
		      }
		    }
		    
		    //IMPORTANTE : asegurar el ordenamiento 
		    Arrays.sort(lsfilesNames);
		    
		return lsfilesNames  ; 
	}
	
	
	/**
	 * lectura de parametros desde la gui 
	 * */
	public void guiReadParams(){
		
		
		GenericDialog gd = new  GenericDialog("Aplicacion de medida de evaluacion");
		
		gd.addStringField("Id del experimento:", "", 50);
		gd.addStringField("Directorio de las imagenes a procesar:", "", 70);
		gd.addStringField("Nombre del descriptor:", "descriptor.", 30);
		gd.addNumericField("Tamaño de la serie (si es Antigranul * 2 ):", 3, 0);
		gd.addStringField("Base de datos(outex13, outex14_hor.. ):", "", 30);
		
		gd.showDialog();
	   
		if (gd.wasCanceled()) return ;	    
	    
		this.experimentId = gd.getNextString(); 
	    this.dirPath = gd.getNextString(); 
	    this.descriptorName = gd.getNextString();
	    this.serieSize = ( int )gd.getNextNumber();
	    this.database = gd.getNextString();
	    
	    return ;
	}
	
	
	public static void showDialog(String msg) {
        GenericDialog gd = new GenericDialog("Mensaje");
        gd.addMessage( msg );
        gd.showDialog();
        if (gd.wasCanceled()) {
            
        }
        
        return ;
    }
	
}
