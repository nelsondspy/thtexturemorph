
import order.Bitmixing;
import order.Lexicographical;
import order.Marginal;
import order.NormalEuclid;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.util.Various;
import ij.IJ;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

/**
 * Probar las operaciones de dilatacion , erosion, apertura y cierre con varios metodos de ordenacion 
 * directorio de salida en test color 
 * */
public class Ordering_Misc_Test implements PlugInFilter  {
	ImagePlus imp ;
	
	public int setup(String arg, ImagePlus imp) {
		this.imp = imp ;
		return DOES_ALL ;	
	}
	
	public void run(ImageProcessor orig) {
		Bitmixing bm= new Bitmixing();
		Marginal mar= new Marginal();
		NormalEuclid  euclidnorm = new NormalEuclid ();
		Lexicographical lexic = new Lexicographical();
		
		
		//Pixel [] se = StructElementListPixels.getSESquare(0 , 10, 135);
		Pixel[] se = Various.getShiftArray( 5 );
		
		//
		String PATH_OUT = "testcolor/";
		
		//bitmixing 
		bm.erode( orig, se, PATH_OUT , "bm_erode" );
		ImagePlus impBMeroded = IJ.openImage( PATH_OUT + "bm_erode.bmp" );
		bm.dilate( orig, se, PATH_OUT , "bm_dilate" );
		ImagePlus impBMdilated = IJ.openImage( PATH_OUT + "bm_dilate.bmp" );
		bm.dilate( impBMeroded.getProcessor() , se, PATH_OUT , "bm_opening" );
		bm.erode( impBMdilated.getProcessor() , se,  PATH_OUT , "bm_closing" );
	
		//prueba de operaciones con procesamiento marginal 
		mar.erode( orig, se, PATH_OUT , "mar_erode" );
		ImagePlus impMAReroded = IJ.openImage( PATH_OUT + "mar_erode.bmp" );
		mar.dilate( orig, se, PATH_OUT , "mar_dilate" );
		ImagePlus impMARdilated = IJ.openImage( PATH_OUT + "mar_dilate.bmp" );
		mar.dilate( impMAReroded .getProcessor() , se, PATH_OUT , "mar_opening" );
		mar.erode( impMARdilated.getProcessor() , se,  PATH_OUT , "mar_closing" );
		
		
		//prueba de operaciones con ordenamiento por distancia euclidiana
		euclidnorm.erode( orig, se, PATH_OUT , "eucl_erode" );
		ImagePlus impECLUDeroded = IJ.openImage( PATH_OUT + "eucl_erode.bmp" );
		euclidnorm.dilate( orig, se, PATH_OUT , "eucl_dilate" );
		ImagePlus impECLUDdilated = IJ.openImage( PATH_OUT + "eucl_dilate.bmp" );
		euclidnorm.dilate( impECLUDeroded .getProcessor() , se, PATH_OUT , "eucl_opening" );
		euclidnorm.erode( impECLUDdilated.getProcessor() , se,  PATH_OUT , "eucl_closing" );
		
	    //
		//prueba de operaciones con ordenamiento por distancia euclidiana
		lexic.erode( orig, se, PATH_OUT , "lex_erode" );
		ImagePlus impLEXeroded = IJ.openImage( PATH_OUT + "lex_erode.bmp" );
		
		lexic.dilate( orig, se, PATH_OUT , "lex_dilate" );
		ImagePlus impLEXdilated = IJ.openImage( PATH_OUT + "lex_dilate.bmp" );
		
		lexic.dilate( impLEXeroded.getProcessor() , se, PATH_OUT , "lex_opening" );
		lexic.erode( impLEXdilated.getProcessor() , se,  PATH_OUT , "lex_closing" );
		
		
		
	}

}
