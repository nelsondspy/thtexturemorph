import order.NormalEuclid;
import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;

/**
 * 
 * Covarianza morgologica usando pesos ENTROPIA CON VENTANAS 
 * 
 * */
public class CovarianceColorEuclidDist_ extends CovarianceBasic {
	
	/**
	 * covarianza M. usando distancias euclidea 
	 * */
	public ImageProcessor applyMorphoOperation(int distance, int orientation , Pixel [] se  ){

		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 

		//String imgErod = imp.getShortTitle()+ "_M=covarEucl" + "_dist="+distance  + "_angl="+ orientation+ "_ROI=" + PRM.ROISIZE ;
		
		//nombre de archivo de longitud fija tal que al ordenar queda ordenada por orientacion y distancia
		String imgErod = imp.getShortTitle() + "_" + String.format("%04d",orientation) + "_"+ String.format("%04d",distance)   ;
		
		NormalEuclid normDist = new NormalEuclid();
		normDist.erode(this.imp.getProcessor(),se, dirOutput , imgErod );
		
		//
		ImagePlus impFinal = IJ.openImage( dirOutput + imgErod + ".bmp" ); 
		return impFinal.getProcessor();
		
	}	

	
	
}
