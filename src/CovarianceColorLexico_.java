import order.Lexicographical;
import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;

/**
 * Covarianza morgologica usando pesos ENTROPIA CON VENTANAS 
 * */
public class CovarianceColorLexico_ extends CovarianceBasic {
	
	
	/**
	 * covarianza M. usando distancias euclidea 
	 * */
	public ImageProcessor applyMorphoOperation(int distance, int orientation , Pixel [] se  ){

		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 

		String imgErod = imp.getShortTitle()+ "_M=covarLex" + "_dist="+distance  + "_angl="+ orientation+ "_ROI=" + PRM.ROISIZE ;

		Lexicographical lex = new Lexicographical();
		lex.erode(this.imp.getProcessor(),se, dirOutput , imgErod );
		
		//
		ImagePlus impFinal = IJ.openImage( dirOutput + imgErod + ".bmp" ); 
		return impFinal.getProcessor();
		
	}	

	
	
}
