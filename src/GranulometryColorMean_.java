import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.profe.plugins.TesisRGBPromedioMaximoPlugin;
import morpho.color.jvazqnog.profe.plugins.TesisRGBPromedioMinimoPlugin;


/**
 * Granulometria color, procesamiento vectorial fijando pesos con promedio CON VENTANAS
 * 
 * */
public class GranulometryColorMean_ extends GranulometryBasic {
	
	
	/**
	 * Aplica las operaciones morfologicas 
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		
		String imgcurrent = imp.getShortTitle() + "_gran" + "_s" + sizeSE  ;
		
		
		//erosion  (minimo)  , genera una nueva imagen , no modifica la entrada
		TesisRGBPromedioMinimoPlugin  promMin  = new TesisRGBPromedioMinimoPlugin();
		
		//PARAMETROS 
		promMin.setup("",this.imp );
		promMin.setOutput( imgcurrent , dirOutput);
		promMin.setseEight( sizeSE );
		promMin.setRois(PRM.ROISIZE ,  PRM.ROISIZE );
		
		//EJECUCION
		promMin.run( this.imp.getProcessor() );
			 
		/* dilatacion ( maximo ) */
		//abre la imagen recien erosionada 
		ImagePlus impDil = IJ.openImage( dirOutput + imgcurrent + ".bmp" ); 
		
		TesisRGBPromedioMaximoPlugin promMax = new TesisRGBPromedioMaximoPlugin ();
		//PARAMETROS 
		promMax.setup("", impDil );
		promMax.setOutput( imgcurrent, dirOutput);
		promMax.setseEight( sizeSE );
		promMax.setRois(PRM.ROISIZE ,  PRM.ROISIZE );
		//EJECUCION 
		promMax.run(impDil.getProcessor());
				
		//imagen final , necesita ser cargada en memoria 
		ImagePlus impFinal = IJ.openImage( dirOutput + imgcurrent + ".bmp" );
		
		return impFinal.getProcessor().convertToShortProcessor();		

	}

}



