import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.profe.plugins.TesisRGBPromedioSinVentanasMaximoPlugin;
import morpho.color.jvazqnog.profe.plugins.TesisRGBPromedioSinVentanasMinimoPlugin;

/**
 * Granulometria color, procesamiento vectorial fijando pesos con promedio SIN VENTANAS
 * 
 * */

public class GranulometryColorMeanWW_ extends GranulometryBasic {
	
	
	
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		
		String imgcurrent = "WW_"+ imp.getShortTitle() + "_gran" + "_s" + sizeSE  ;
		
		
		//erosion  (minimo)  , genera una nueva imagen , no modifica la entrada
		TesisRGBPromedioSinVentanasMinimoPlugin  promMin  = new TesisRGBPromedioSinVentanasMinimoPlugin();
		promMin.setup("",this.imp );
		promMin.setOutput( imgcurrent , dirOutput);
		promMin.setseEight( sizeSE );
		promMin.run( this.imp.getProcessor() );
			 
		/* dilatacion ( maximo ) */
		//abre la imagen recien erosionada 
		ImagePlus impDil = IJ.openImage( dirOutput + imgcurrent + ".bmp" ); 
		
		TesisRGBPromedioSinVentanasMaximoPlugin promMax = new TesisRGBPromedioSinVentanasMaximoPlugin ();
		
		promMax.setup("", impDil );
		promMax.setOutput( imgcurrent, dirOutput);
		promMax.setseEight( sizeSE );
		promMax.run(impDil.getProcessor());
				
		//imagen final 
		ImagePlus impFinal = IJ.openImage( dirOutput + imgcurrent + ".bmp" );
		
		return impFinal.getProcessor().convertToShortProcessor();		

	}

}



