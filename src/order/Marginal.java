package order;

import ij.ImagePlus;
import ij.io.FileSaver;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import ij.plugin.ChannelSplitter ;
import ij.plugin.RGBStackMerge ; 

 
/** Procesamiento marginal de una imagen
 * Procesa cada canal por separado como si fuera escala de grises 
 * */

public class Marginal {
	
public void  erode(ImageProcessor imgpr , Pixel [] se , String outputdir , String namefile  ){

		//copia para no modificar la imagen de textura original 
		ImageProcessor imgproc = imgpr.duplicate();

		//separara cada canal
		ImagePlus implus = new ImagePlus(  "", imgproc );
		ImagePlus[] implusRGB =  ChannelSplitter.split( implus) ;
		
		//se aplica la erosion en cada canal con el mismo elemento estructurante 
		OrderUtils.MinimumSingleChanel( implusRGB[0].getProcessor(), se );
		OrderUtils.MinimumSingleChanel( implusRGB[1].getProcessor(), se );
		OrderUtils.MinimumSingleChanel( implusRGB[2].getProcessor(), se );
		
		//se vuelven a unir los canales , en una representacion plana
		ImagePlus implusFinal = RGBStackMerge.mergeChannels( implusRGB , false ).flatten();
		
		FileSaver fs = new FileSaver( implusFinal );
		fs.saveAsBmp( outputdir + namefile + ".bmp"  );

		
	}//fin metodo 

/**
 * Dilatacion 
 * 
 * */
public void  dilate(ImageProcessor imgpr , Pixel [] se , String outputdir , String namefile  ){

	//copia para no modificar la imagen de textura original 
	ImageProcessor imgproc = imgpr.duplicate();

	//separara cada canal
	ImagePlus implus = new ImagePlus(  "", imgproc );
	ImagePlus[] implusRGB =  ChannelSplitter.split( implus) ;
	
	//se aplica la erosion en cada canal con el mismo elemento estructurante 
	OrderUtils.MaximumSingleChanel( implusRGB[0].getProcessor(), se );
	OrderUtils.MaximumSingleChanel( implusRGB[1].getProcessor(), se );
	OrderUtils.MaximumSingleChanel( implusRGB[2].getProcessor(), se );
	
	//se vuelven a unir los canales , en una representacion plana
	ImagePlus implusFinal = RGBStackMerge.mergeChannels( implusRGB , false ).flatten();
	
	FileSaver fs = new FileSaver( implusFinal );
	fs.saveAsBmp( outputdir + namefile + ".bmp"  );

	
}//fin metodo 
	

}
