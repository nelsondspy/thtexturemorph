package order;
import morpho.color.jvazqnog.models.Pixel;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import ij.io.FileSaver ; 

/**
 * Orden alpha-lexicografico 
 * erosion y dilatacion 
 * 
 * */
public class AlphaModulusLexicograph {
	
	/** por defecto es 10  */
	public int alpha = 10 ;
	
	
	public void setAlpha( int alpha ){
		this.alpha = alpha;
	}
	
	public void  erode(ImageProcessor imgpr , Pixel [] se , String outputdir , String namefile  ){
		
		//copia para no modificar la imagen de textura original 
		ImageProcessor imgproc = imgpr.duplicate();
		ImageProcessor imgproc_back = imgpr.duplicate();

		final int WIDTH = imgproc.getWidth();
		final int HEIGHT = imgproc.getHeight();
	
		
		//itererar HEIGHT X WIDTH ( fila x colummna , x=u y=v )
		for(int v = 0 ; v < HEIGHT; v++  ){
			
			for(int u = 0 ; u < WIDTH  ; u++  ){
				
				int pvalMin = 0 ; 	
				int []RGB_MIN = {Integer.MAX_VALUE , Integer.MAX_VALUE , Integer.MAX_VALUE };
				
				
				for(Pixel t : se ){
					
					int px = t.getX() + u  ;
					int py = t.getY() + v ;					
					
					if ( px >= WIDTH || py >= HEIGHT   ||  px < 0 || py < 0 ) continue ;
					
					int pval = imgproc_back.get(px ,py );
					
					int R = (pval & 0xff0000 ) >> 16 ;
					int G = (pval & 0x00ff00 ) >> 8 ;
					int B = (pval & 0x0000ff )  ;
					
					int []RGB_CURRENT = { R, G , B };
					
					//en este caso como es el minimo ,solo si el minimo es mayor al actual 
					//significa que ya no es el minimo
					if (isMajor(RGB_MIN, RGB_CURRENT, this.alpha )){
						
						
						
						//valores de cada canal 
						RGB_MIN[0] = RGB_CURRENT[0] ;
						RGB_MIN[1] = RGB_CURRENT[1] ;
						RGB_MIN[2] = RGB_CURRENT[2] ;
						
						//valor concreto de pixel 
						pvalMin = pval ;   		
					}
					
					
				}  
				
				imgproc.set( u , v , pvalMin );
				
			}
		}
		
		ImagePlus imp = new ImagePlus();
		imp.setProcessor(imgproc);
		FileSaver fs = new FileSaver(imp);
		
		fs.saveAsBmp(outputdir + namefile + ".bmp"   );

		
	}//fin metodo 

	
public void  dilate(ImageProcessor imgpr , Pixel [] se , String outputdir , String namefile  ){
		
		//copia para no modificar la imagen de textura original 
		ImageProcessor imgproc = imgpr.duplicate();
		
		ImageProcessor imgproc_back = imgpr.duplicate();
		
		final int WIDTH = imgproc.getWidth();
		final int HEIGHT = imgproc.getHeight();
	
		
		
		//itererar HEIGHT X WIDTH ( fila x colummna , x=u y=v )
		for(int v = 0 ; v < HEIGHT; v++  ){
			
			for(int u = 0 ; u < WIDTH  ; u++  ){
				
				int pvalMin = 0 ; 	
				
				int []RGB_MAX = {0, 0 , 0};
				
				for(Pixel t : se ){
					
					int px = t.getX() + u  ;
					int py = t.getY() + v ;					
					
					if ( px >= WIDTH || py >= HEIGHT   ||  px < 0 || py < 0 ) continue ;
					
					int pval = imgproc_back.get(px ,py );
					
					int R = (pval & 0xff0000 ) >> 16 ;
					int G = (pval & 0x00ff00 ) >> 8 ;
					int B = (pval & 0x0000ff )  ;
					
					int []RGB_CURRENT = {R, G , B};
					
					//en este caso como es el minimo ,solo si el minimo es mayor al actual 
					//significa que ya no es el minimo
					if (isMajor(RGB_CURRENT, RGB_MAX, this.alpha )){
						
						//valores de cada canal 
						RGB_MAX[0] = RGB_CURRENT[0] ;
						RGB_MAX[1] = RGB_CURRENT[1] ;
						RGB_MAX[2] = RGB_CURRENT[2] ;
						
						//valor real de pixel 
						pvalMin = pval ;   		
					}
					
				}  
				
				imgproc.set( u , v , pvalMin );
				
			}
		}
		
		ImagePlus imp = new ImagePlus();
		imp.setProcessor(imgproc);
		FileSaver fs = new FileSaver(imp);
		
		fs.saveAsBmp(outputdir + namefile + ".bmp"   );

		
	}//fin metodo 


	/**
	 * retorna verdadero si RGB1 es mayor a RGB2 segun el algoritmo del modulo
	 * lexicografico
	 * */
	private boolean isMajor(int[] RGB1,  int[] RGB2, int alpha ){
		boolean ismajor = false;  
		
		//si es mayor directamente true
		if( Math.ceil(RGB1[ 0 ] / alpha) >  Math.ceil(RGB2 [ 0 ] / alpha) ) {
			ismajor = true ;
		}  //si es menor directamente false
		else if(Math.ceil( RGB1[ 0 ]/ alpha)  <  Math.ceil(RGB2 [ 0 ] /  alpha) ){
			ismajor = false ;
		}//si es igual 
		else if(Math.ceil(RGB1[ 0 ] / alpha)  ==  Math.ceil(RGB2 [ 0 ] / alpha) ){
			if (RGB1[ 1 ]  >  RGB2 [ 1 ]){
				ismajor = true ;
			}
			else if(RGB1[ 1 ]  <  RGB2 [ 1 ]){
				ismajor = false ;
			}
			else if(RGB1[ 1 ]  ==  RGB2 [ 1 ]){
				if( RGB1[ 2 ]  >  RGB2 [ 2 ] ){
					ismajor = true ; 
				}
				else if( RGB1[ 2 ]  <  RGB2 [ 2 ]){
					ismajor = false;
				}
			}
		}
		
		return ismajor; 	
	} 
}
