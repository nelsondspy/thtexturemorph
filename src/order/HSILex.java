package order;

import ij.ImagePlus;
import ij.ImageStack;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import utils.Color_Transformer;
import ij.io.FileSaver ; 

public class HSILex {
	
	ImageStack stack ; 
	ImageProcessor cI ;
	ImageProcessor cH ;
	ImageProcessor cS ;

	int WIDTH ;
	int HEIGHT ;
	
	
	public enum filterType {
	    MAX,
	    MIN
	}
	
	
	/**
	 * no altera el parametro de entrada , es decir el ImageProcessor 
	 * */
	public void setImageProcessor(ImageProcessor imgpr ){
        Color_Transformer colorTransformer = new Color_Transformer();
		
		ImagePlus imp = new ImagePlus();
		imp.setProcessor(imgpr);
		
		colorTransformer.setup("", imp);
		colorTransformer.setColorSpace("HSI");
		colorTransformer.run(imgpr);
		stack = colorTransformer.getStack();
		

		//H S I 
		cH= stack.getProcessor(1);
		cS= stack.getProcessor(2);
		cI= stack.getProcessor(3);
		
		WIDTH = imgpr.getWidth();
		HEIGHT = imgpr.getHeight();
		
	}

  /**
   * 
   * */
  public void setImagePlus(ImagePlus imp ){
	  stack = imp.getStack();
		

		//H S I 
		cH= stack.getProcessor(1);
		cS= stack.getProcessor(2);
		cI= stack.getProcessor(3);
		
		WIDTH = imp.getWidth();
		HEIGHT = imp.getHeight();

  }	
	
  /**
   * 
   * */
	public ImagePlus filter( Pixel [] se , String outputdir , String namefile, filterType ft  ){
		
		ImageProcessor copyI= cI.duplicate();
		ImageProcessor copyH= cH.duplicate();
		ImageProcessor copyS= cS.duplicate();
		 
		
		//inicializacion de valores que permite primera entrada, segun el tipo de filtro 
		float [] PIXEL_INIT = new float[3]; 
		if (ft.equals(filterType.MIN))
			PIXEL_INIT[0] = PIXEL_INIT[1] = PIXEL_INIT[2] = Float.MAX_VALUE ;
		
		if (ft.equals(filterType.MAX))
			PIXEL_INIT[0] = PIXEL_INIT[1] = PIXEL_INIT[2] = Float.MIN_VALUE ;
		
		
		//itererar HEIGHT X WIDTH ( fila x colummna , x=u y=v )
		for(int v = 0 ; v < HEIGHT; v++  ){
			
			for(int u = 0 ; u < WIDTH  ; u++  ){
				
				
				float []PIXEL_SEL =  new float[3]; 
				PIXEL_SEL[0] = PIXEL_INIT[0] ;
				PIXEL_SEL[1] = PIXEL_INIT[1] ;
				PIXEL_SEL[2] = PIXEL_INIT[2] ;

				for(Pixel t : se ){
					
					int px = t.getX() + u  ;
					int py = t.getY() + v ;					
					 
					if ( px >= WIDTH || py >= HEIGHT   ||  px < 0 || py < 0 ) continue ;
					
					//int pval = imgproc_back.get(px ,py );
					
					float C1 = copyH.getf(px,py);
					float C2 = copyS.getf(px,py) ;
					float C3 = copyI.getf(px,py) ;
					
					//CASCADA I..S..H
					float []PIXEL_CURRENT = { C3, C2 , C1 };
					
					if (ft.equals(filterType.MIN)) {
						//en este caso como es el minimo ,solo si el minimo es mayor al actual 
						//significa que ya no es el minimo
						if (isMajor(PIXEL_SEL, PIXEL_CURRENT )){
							//valores de cada canal 
							PIXEL_SEL[0] = PIXEL_CURRENT[0] ;
							PIXEL_SEL[1] = PIXEL_CURRENT[1] ;
							PIXEL_SEL[2] = PIXEL_CURRENT[2] ;							
						}
					}
					
					if (ft.equals(filterType.MAX)) {
						if ( isMajor(PIXEL_CURRENT, PIXEL_SEL  )){
							//valores de cada canal 
							PIXEL_SEL[0] = PIXEL_CURRENT[0] ;
							PIXEL_SEL[1] = PIXEL_CURRENT[1] ;
							PIXEL_SEL[2] = PIXEL_CURRENT[2] ;							
						}
					}
					
				}
				
				cI.setf( u , v , PIXEL_SEL[0] );
				cH.setf( u , v , PIXEL_SEL[2] );
				cS.setf( u , v , PIXEL_SEL[1]) ;
				
			}
		}
		
		
		ImagePlus impSav = new ImagePlus();
		
		impSav.setProcessor(cI);
		(new FileSaver(impSav)).saveAsBmp(outputdir + namefile + "_I.bmp"   );
		
		impSav.setProcessor(cS);
		(new FileSaver(impSav)).saveAsBmp(outputdir + namefile + "_S.bmp"   );
		
		//impSav.setProcessor(cH);
		//(new FileSaver(impSav)).saveAsBmp(outputdir + namefile + "_H.bmp"   );
	    
		ImagePlus ret = new ImagePlus(outputdir + namefile ,stack);
		
		return ret ;
		
		
	}
	
	 
	/**
	 * 
	 * */
	/**
	 * retorna verdadero si RGB1 es mayor a RGB2
	 * */
	private boolean isMajor(float[] RGB1,  float[] RGB2 ){
		boolean ismajor = false;  
		
		if(RGB1[ 0 ]  >  RGB2 [ 0 ]) {
			ismajor = true ;
		}
		else if( RGB1[ 0 ]  <  RGB2 [ 0 ] ){
			ismajor = false ;
		}
		else if(RGB1[ 0 ]  ==  RGB2 [ 0 ]){
			if (RGB1[ 1 ]  >  RGB2 [ 1 ]){
				ismajor = true ;
			}
			else if(RGB1[ 1 ]  <  RGB2 [ 1 ]){
				ismajor = false ;
			}
			else if(RGB1[ 1 ]  ==  RGB2 [ 1 ]){
				if( RGB1[ 2 ]  >  RGB2 [ 2 ] ){
					ismajor = true ; 
				}
				else if( RGB1[ 2 ]  <  RGB2 [ 2 ]){
					ismajor = false;
				}
			}
		}
		
		return ismajor; 	
	} 
	
	
}
