package order;
import morpho.color.jvazqnog.models.Pixel;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import ij.io.FileSaver ; 



 
public class Bitmixing {
	
	public void  erode(ImageProcessor imgpr , Pixel [] se , String outputdir , String namefile  ){
		
		//copia para no modificar la imagen de textura original 
		ImageProcessor imgproc = imgpr.duplicate();
		ImageProcessor imgproc_back = imgpr.duplicate();
		
		final int WIDTH = imgproc.getWidth();
		final int HEIGHT = imgproc.getHeight();
		for(Pixel t : se ){
			
			System.out.println("t.getX():" + t.getX() + ",t.getY():" + t.getY());
		}
		
		//itererar HEIGHT X WIDTH ( fila x colummna , x=u y=v )
		for(int v = 0 ; v < HEIGHT; v++  ){
			
			for(int u = 0 ; u < WIDTH  ; u++  ){
				
				//inicializa con el mayor valor posible 
				int bitmMIN  = ( 0xffffff);
				int concreteMIN = 0 ;
				
				for(Pixel t : se ){
					
					int px = t.getX() + u  ;
					int py = t.getY() + v ;	
						
					if ( px >= WIDTH || py >= HEIGHT   ||  px < 0 || py < 0 ) continue ;
					
					final int pval = imgproc_back.get(px ,py );

					
					final int R = (pval & 0xff0000 ) >> 16 ;
					final int G = (pval & 0x00ff00 ) >> 8 ;
					final int B = (pval & 0x0000ff )  ;				
					
					//valor de pixel sin el canal alpha que vale -1
					int bitmVal = ( R & 0xff << 16  ) | ( G & 0xff << 8  ) | ( B & 0xff )  ;

					
					//busqueda del maximo 
					if (bitmMIN > bitmVal ){ 
						bitmMIN = bitmVal;
						concreteMIN = pval;	
					}
					
				}  
				
				imgproc.set( u , v , concreteMIN  );
				
			}
		}
		
		ImagePlus imp = new ImagePlus();
		imp.setProcessor(imgproc);
		FileSaver fs = new FileSaver(imp);
		
		fs.saveAsBmp(outputdir + namefile + ".bmp"   );

		
	}//fin metodo 
	
	
	public void  dilate (ImageProcessor imgpr , Pixel [] se , String outputdir , String namefile  ){

		//copia para no modificar la imagen de textura original 
		ImageProcessor imgproc = imgpr.duplicate();
		ImageProcessor imgproc_back = imgpr.duplicate();
		
		final int WIDTH = imgproc.getWidth();
		final int HEIGHT = imgproc.getHeight();
		for(Pixel t : se ){
			
			System.out.println("t.getX():" + t.getX() + ",t.getY():" + t.getY());
		}
		
		//itererar HEIGHT X WIDTH ( fila x colummna , x=u y=v )
		for(int v = 0 ; v < HEIGHT; v++  ){
			
			for(int u = 0 ; u < WIDTH  ; u++  ){
				
				//inicializa con el mayor valor posible 
				int bitmMAX  = 0 ;
				int concreteMAX = 0 ;
				
				for(Pixel t : se ){
					
					int px = t.getX() + u  ;
					int py = t.getY() + v ;	
						
					if ( px >= WIDTH || py >= HEIGHT   ||  px < 0 || py < 0 ) continue ;
					
					final int pval = imgproc_back.get(px ,py );

					
					final int R = (pval & 0xff0000 ) >> 16 ;
					final int G = (pval & 0x00ff00 ) >> 8 ;
					final int B = (pval & 0x0000ff )  ;				
					
					//valor de pixel sin el canal alpha que vale -1
					int bitmVal = ( R & 0xff << 16  ) | ( G & 0xff << 8  ) | ( B & 0xff )  ;

					
					//busqueda del maximo 
					if (bitmMAX <= bitmVal ){ 
						bitmMAX = bitmVal;
						concreteMAX = pval;	
					}
					
				}  
				
				imgproc.set( u , v , concreteMAX  );
				
			}
		}
		
		ImagePlus imp = new ImagePlus();
		imp.setProcessor(imgproc);
		FileSaver fs = new FileSaver(imp);
		
		fs.saveAsBmp(outputdir + namefile + ".bmp"   );

	}	
	
	
}
