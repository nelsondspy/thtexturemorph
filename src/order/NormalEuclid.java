package order;
import morpho.color.jvazqnog.models.Pixel;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import ij.io.FileSaver ; 



 
public class NormalEuclid {
	
	public void  erode(ImageProcessor imgpr , Pixel [] se , String outputdir , String namefile  ){
		
		//copia para no modificar la imagen de textura original 
		ImageProcessor imgproc = imgpr.duplicate();
		ImageProcessor imgproc_back = imgpr.duplicate();
		
		final int WIDTH = imgproc.getWidth();
		final int HEIGHT = imgproc.getHeight();
		

		
		//itererar HEIGHT X WIDTH ( fila x colummna , x=u y=v )
		for(int v = 0 ; v < HEIGHT; v++  ){
			
			for(int u = 0 ; u < WIDTH  ; u++  ){
				
				//inicializa con el mayor valor posible 
				double distanceMIN  = Double.MAX_VALUE;
				
				//valor concreto del pixel minimo 
				int concreteMIN = 0 ;
				
				for(Pixel t : se ){
					
					int px = t.getX() + u  ;
					int py = t.getY() + v ;	
						
					if ( px >= WIDTH || py >= HEIGHT   ||  px < 0 || py < 0 ) continue ;
					
					//valor del pixel
					final int pval = imgproc_back.get(px ,py );

					//separar los componentes 
					final int R = (pval & 0xff0000 ) >> 16 ;
					final int G = (pval & 0x00ff00 ) >> 8 ;
					final int B = (pval & 0x0000ff )  ;				
					
					//valor de la distancia con respecto a (0,0,0)
					double distanceVal = Math.pow(R,2) + Math.pow(G,2) + Math.pow(B,2) ;
					
					//busqueda del maximo 
					if (distanceMIN > distanceVal ){ 
						distanceMIN = distanceVal  ;
						concreteMIN = pval;	
					}
					
				}  
				
				imgproc.set( u , v , concreteMIN  );
				
			}
		}
		
		ImagePlus imp = new ImagePlus();
		imp.setProcessor(imgproc);
		FileSaver fs = new FileSaver(imp);
		
		fs.saveAsBmp(outputdir + namefile + ".bmp"   );

		
	}//fin metodo 
	
	/**
	 * 
	 * */
	public void  dilate (ImageProcessor imgpr , Pixel [] se , String outputdir , String namefile  ){

		//copia para no modificar la imagen de textura original 
		ImageProcessor imgproc = imgpr.duplicate();
		ImageProcessor imgproc_back = imgpr.duplicate();
		
		final int WIDTH = imgproc.getWidth();
		final int HEIGHT = imgproc.getHeight();
	
		
		//itererar HEIGHT X WIDTH ( fila x colummna , x=u y=v )
		for(int v = 0 ; v < HEIGHT; v++  ){
			
			for(int u = 0 ; u < WIDTH  ; u++  ){
				//inicializa con el mayor valor posible 
				double distanceMAX  = 0 ;
			
				//valor concreto del pixel minimo 
				int concreteMAX = 0 ;
				
				for(Pixel t : se ){
					
					int px = t.getX() + u  ;
					int py = t.getY() + v ;	
						
					if ( px >= WIDTH || py >= HEIGHT   ||  px < 0 || py < 0 ) continue ;
					
					//valor del pixel
					final int pval = imgproc_back.get(px ,py );

					//separar los componentes 
					final int R = (pval & 0xff0000 ) >> 16 ;
					final int G = (pval & 0x00ff00 ) >> 8 ;
					final int B = (pval & 0x0000ff )  ;				
					
					//valor de la distancia con respecto a (0,0,0)
					double distanceVal = Math.pow(R,2) + Math.pow(G,2) + Math.pow(B,2) ;
					
					//busqueda del maximo 
					if (distanceMAX < distanceVal ){ 
						distanceMAX = distanceVal  ;
						concreteMAX = pval;	
					}
					
				}  
				
				imgproc.set( u , v , concreteMAX  );
				
			}
		}
		
		ImagePlus imp = new ImagePlus();
		imp.setProcessor(imgproc);
		FileSaver fs = new FileSaver(imp);
		
		fs.saveAsBmp(outputdir + namefile + ".bmp"   );

	
	}	
	
	
}
