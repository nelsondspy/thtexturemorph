package order;

import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;

public class OrderUtils {
	
	/**
	 * Calcula el minimo de un Processor en cuyos valores son enteros usando un elemento estructurante
	 * Modifica la imagen de entrada 
	 * @param imagen cuyos valores estan encapsulados en enteros simples 
	 * @param elemento estructurante en forma de array de desplazamientos 
	 * */
	public static void MinimumSingleChanel(ImageProcessor imgproc, Pixel [] se){
		
		//copia para no modificar la imagen de textura original 
						
		final int WIDTH = imgproc.getWidth();
		final int HEIGHT = imgproc.getHeight();
			
		final ImageProcessor imgproc_back = imgproc.duplicate();

		//itererar HEIGHT X WIDTH ( fila x colummna , x=u y=v )
		for(int v = 0 ; v < HEIGHT; v++  ){
					
			for(int u = 0 ; u < WIDTH  ; u++  ){
						
				int pvalMin = ( 0xffffff);
						
				for(Pixel t : se ){	
						int px = t.getX() + u  ;
						int py = t.getY() + v ;					
							
						if ( px >= WIDTH || py >= HEIGHT   ||  px < 0 || py < 0 ) continue ;
							
						int pval = imgproc_back.get(px ,py );
						
						//busqueda del minimo  
						if (pvalMin > pval ) pvalMin = pval;	
						
					}  
						
					imgproc.set( u , v , pvalMin );	
					}
		}

	}
	
	
	/**
	 * Calcula el maximo de un Processor en cuyos valores son enteros usando un elemento estructurante
	 * Modifica la imagen de entrada 
	 * @param imagen cuyos valores estan encapsulados en enteros simples 
	 * @param elemento estructurante en forma de array de desplazamientos 
	 * */
	public static void MaximumSingleChanel(ImageProcessor imgproc , Pixel [] se){
		
				
		final int WIDTH = imgproc.getWidth();
		final int HEIGHT = imgproc.getHeight();
		
		//copia para no modificar la imagen de textura original 		
		final ImageProcessor imgproc_back = imgproc.duplicate();

		//itererar HEIGHT X WIDTH ( fila x colummna , x=u y=v )
		for(int v = 0 ; v < HEIGHT; v++  ){
					
			for(int u = 0 ; u < WIDTH  ; u++  ){
						
				int pvalMax = ( 0x000000) ;
		
				for(Pixel t : se ){	
					
						int px = t.getX() + u  ;
						int py = t.getY() + v ;					
							
						if ( px >= WIDTH || py >= HEIGHT   ||  px < 0 || py < 0 ) continue ;
						
						///valor del pixel 
						int pval = imgproc_back.get(px ,py );
		
						//busqueda del maximo 
						if ( pvalMax < pval ) pvalMax = pval ;
						
				}  	
				
				imgproc.set( u , v , pvalMax );	
					
			}
		}
	}
	
}
