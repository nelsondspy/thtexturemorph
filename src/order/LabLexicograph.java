package order;

import ij.ImagePlus;
import ij.ImageStack;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import utils.Color_Transformer;
import ij.io.FileSaver ; 

public class LabLexicograph {
	
	ImageStack stack ; 
	ImageProcessor cL ;
	ImageProcessor ca ;
	ImageProcessor cb ;

	int WIDTH ;
	int HEIGHT ;
	
	
	public enum filterType {
	    MAX,
	    MIN
	}
	
	
	/**
	 * no altera el parametro de entrada , es decir el ImageProcessor 
	 * */
	public void setImageProcessor(ImageProcessor imgpr ){
        Color_Transformer colorTransformer = new Color_Transformer();
		
		ImagePlus imp = new ImagePlus();
		imp.setProcessor(imgpr);
		
		colorTransformer.setup("", imp);
		colorTransformer.setColorSpace("Lab");
		colorTransformer.run(imgpr);
		stack = colorTransformer.getStack();
		

		//L A B 
		cL= stack.getProcessor(1);
		ca= stack.getProcessor(2);
		cb= stack.getProcessor(3);
		
		WIDTH = imgpr.getWidth();
		HEIGHT = imgpr.getHeight();
		
	}
  /**
   * 
   * */
	public ImagePlus filter( Pixel [] se , String outputdir , String namefile, filterType ft  ){
		
		//se asegura que no sea alterado el imageprocesor original 
		
		ImageProcessor copyL= cL.duplicate();
		ImageProcessor copya= ca.duplicate();
		ImageProcessor copyb= cb.duplicate();
		 
		
		//inicializacion de valores que permite primera entrada, segun el tipo de filtro 
		float [] PIXEL_INIT = new float[3]; 
		if (ft.equals(filterType.MIN))
			PIXEL_INIT[0] = PIXEL_INIT[1] = PIXEL_INIT[2] = Float.MAX_VALUE ;
		
		if (ft.equals(filterType.MAX))
			PIXEL_INIT[0] = PIXEL_INIT[1] = PIXEL_INIT[2] = Float.MIN_VALUE ;
		
		
		//itererar HEIGHT X WIDTH ( fila x colummna , x=u y=v )
		for(int v = 0 ; v < HEIGHT; v++  ){
			
			for(int u = 0 ; u < WIDTH  ; u++  ){
				
				
				float []PIXEL_SEL =  new float[3]; 
				PIXEL_SEL[0] = PIXEL_INIT[0] ;
				PIXEL_SEL[1] = PIXEL_INIT[1] ;
				PIXEL_SEL[2] = PIXEL_INIT[2] ;

				for(Pixel t : se ){
					
					int px = t.getX() + u  ;
					int py = t.getY() + v ;					
					 
					if ( px >= WIDTH || py >= HEIGHT   ||  px < 0 || py < 0 ) continue ;
					
					//int pval = imgproc_back.get(px ,py );
					
					float C1 = copyL.getf(px,py);
					float C2 = copya.getf(px,py) ;
					float C3 = copyb.getf(px,py) ;
					
					//CASCADA I..S..H
					float []PIXEL_CURRENT = { C1, C2 , C3 };
					
					if (ft.equals(filterType.MIN)) {
						//en este caso como es el minimo ,solo si el minimo es mayor al actual 
						//significa que ya no es el minimo
						if (isMajor(PIXEL_SEL, PIXEL_CURRENT )){
							//valores de cada canal 
							PIXEL_SEL[0] = PIXEL_CURRENT[0] ;
							PIXEL_SEL[1] = PIXEL_CURRENT[1] ;
							PIXEL_SEL[2] = PIXEL_CURRENT[2] ;							
						}
					}
					
					if (ft.equals(filterType.MAX)) {
						if ( isMajor(PIXEL_CURRENT, PIXEL_SEL  )){
							//valores de cada canal 
							PIXEL_SEL[0] = PIXEL_CURRENT[0] ;
							PIXEL_SEL[1] = PIXEL_CURRENT[1] ;
							PIXEL_SEL[2] = PIXEL_CURRENT[2] ;							
						}
					}
					
				}
				
				cL.setf( u , v , PIXEL_SEL[0] );
				ca.setf( u , v , PIXEL_SEL[1]) ;
				cb.setf( u , v , PIXEL_SEL[2] );
			}
		}
		
		
		ImagePlus impSav = new ImagePlus();
		
		impSav.setProcessor(cL);
		(new FileSaver(impSav)).saveAsBmp(outputdir + namefile + "_L.bmp"   );
		
		impSav.setProcessor(ca);
		(new FileSaver(impSav)).saveAsBmp(outputdir + namefile + "_a.bmp"   );
		
		//impSav.setProcessor(cH);
		//(new FileSaver(impSav)).saveAsBmp(outputdir + namefile + "_H.bmp"   );
	    
		ImagePlus ret = new ImagePlus(outputdir + namefile ,stack);
		
		return ret ;
		
		
	}
	
	 
	/**
	 * 
	 * */
	/**
	 * retorna verdadero si RGB1 es mayor a RGB2
	 * */
	private boolean isMajor(float[] RGB1,  float[] RGB2 ){
		boolean ismajor = false;  
		
		if(RGB1[ 0 ]  >  RGB2 [ 0 ]) {
			ismajor = true ;
		}
		else if( RGB1[ 0 ]  <  RGB2 [ 0 ] ){
			ismajor = false ;
		}
		else if(RGB1[ 0 ]  ==  RGB2 [ 0 ]){
			if (RGB1[ 1 ]  >  RGB2 [ 1 ]){
				ismajor = true ;
			}
			else if(RGB1[ 1 ]  <  RGB2 [ 1 ]){
				ismajor = false ;
			}
			else if(RGB1[ 1 ]  ==  RGB2 [ 1 ]){
				if( RGB1[ 2 ]  >  RGB2 [ 2 ] ){
					ismajor = true ; 
				}
				else if( RGB1[ 2 ]  <  RGB2 [ 2 ]){
					ismajor = false;
				}
			}
		}
		
		return ismajor; 	
	} 
	
	
}
