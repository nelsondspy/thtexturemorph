import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.profe.plugins.RGBDeviationWWMin;

/**
 * Covarianza morgologica usando pesos POR DESVIACION CON VENTANAS 
 * */
public class CovarianceColorDeviationWW_ extends CovarianceBasic {
	
	/**
	* Internamente el metodo debe retornar el ImageProcessor que sea compatible 
	* con el metodo que calcula el descriptor 
	* 
	* */
	public ImageProcessor applyMorphoOperation(int distance, int orientation , Pixel [] se  ){
	
		String imgErod = "WW_" + imp.getShortTitle()+ "_covarDESV" + "_dist"+distance  + "_angle"+ orientation ;
		
		RGBDeviationWWMin  desvMin  = new RGBDeviationWWMin(imgErod , dirOutput,
				"bmp", (ColorProcessor)  this.imp.getProcessor(), 
	             se );
		
		desvMin.run();
		
		//abrir la imagen recien erosionada 
		ImagePlus impFinal = IJ.openImage( dirOutput + imgErod + ".bmp" ); 
		return impFinal.getProcessor();

	}	

	
	
}
