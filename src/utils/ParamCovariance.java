
package utils;

import ij.gui.GenericDialog;

public class ParamCovariance {

	/** numero de distancias a ser evaluadas */
    public int NUM_DIST = 10 ;
    
	public int getNUM_DIST() {
		return NUM_DIST;
	}

	public void setNUM_DIST(int nUM_DIST) {
		NUM_DIST = nUM_DIST;
	}

	public int getINCREMENT_SIZE() {
		return INCREMENT_SIZE;
	}

	public void setINCREMENT_SIZE(int iNCREMENT_SIZE) {
		INCREMENT_SIZE = iNCREMENT_SIZE;
	}

	public int getROISIZE() {
		return ROISIZE;
	}

	public void setROISIZE(int rOISIZE) {
		ROISIZE = rOISIZE;
	}

	public String getExperimentId() {
		return experimentId;
	}

	public void setExperimentId(String experimentId) {
		this.experimentId = experimentId;
	}

	public String getDESCRIPTOR_NAME() {
		return DESCRIPTOR_NAME;
	}

	public void setDESCRIPTOR_NAME(String dESCRIPTOR_NAME) {
		DESCRIPTOR_NAME = dESCRIPTOR_NAME;
	}


	/** paso entre las distancias si es para 2: 0, 2, 4, 6.. ; para 1: 0,1,2,3..    */
    public int INCREMENT_SIZE = 1 ;
    
   /** tamanhos  de rois para las ventanas   */
    public int ROISIZE = 0 ;
    
    
    /** id del experimento  */
	public String experimentId  = null ;
	
	
	/** nombre del descriptor  */
	public String DESCRIPTOR_NAME= null;

	/** parametros del descriptor  */
	public String DESCRIPTOR_PRM = null;
	
	public String getDESCRIPTOR_PRM() {
		return DESCRIPTOR_PRM;
	}

	public void setDESCRIPTOR_PRM(String dESCRIPTOR_PRM) {
		DESCRIPTOR_PRM = dESCRIPTOR_PRM;
	}

	/**
     * Parametros de granulometria desde un String  
     * @author nelsond
     * 
     * */
    public void setParamFromString(String param ) {
		
		String[] listTokens = param.split( ParamMng.PARAM_STRING_SEPARATOR ); 
		
		for (String token : listTokens) {
			
			//cada token tiene la forma "NOMBREPARAMETRO=VALOR" EJ. "MAXSIZE_SE=10"
			//entonces se vuelve a separar
			
			String [] par = token.split( "=" );
			
			String paramName = par[ 0 ];
			
			
			
			if ( paramName.equals( "NUM_DIST" ) ) NUM_DIST = Integer.parseInt(  par[ 1 ]  )   ;  
			if ( paramName.equals( "INCREMENT_SIZE" ) )INCREMENT_SIZE = Integer.parseInt(  par[ 1 ]  )  ;
			if ( paramName.equals( "ROISIZE" ) ) ROISIZE =  Integer.parseInt(  par[ 1 ]  )  ;
			if ( paramName.equals( "ID" ) ) experimentId =  par[ 1 ]  ;
			if ( paramName.equals( "DESCRIPTOR_NAME" ) ) DESCRIPTOR_NAME =  par[ 1 ]  ;
			if ( paramName.equals( "DESCRIPTOR_PRM" ) ) DESCRIPTOR_PRM =  par[ 1 ]  ;
			
		}
	}
    
    /**
     * */
	public void setParamFromGUI(){
	
		GenericDialog gd = new  GenericDialog("Parametros de covarianza morfologica ");
		
		gd.addNumericField("Numero de distancias", 10, 2);
		gd.addNumericField("Salto entre distancias ", 1, 2);
		gd.addStringField("clase Descriptor", "",40);
		gd.addNumericField("ROI size", 2, 2);
		gd.addStringField("experimentid", "test", 45);
		gd.addStringField("DESCRIPTOR_PRM", "", 30);
		
		 
		gd.showDialog();
	    if (gd.wasCanceled()) return ;

	    NUM_DIST = (int)gd.getNextNumber();  ;
		
	    INCREMENT_SIZE = ( int )gd.getNextNumber() ;
		
		DESCRIPTOR_NAME= gd.getNextString() ;
		
		ROISIZE = ( int )gd.getNextNumber()  ;

		experimentId  = gd.getNextString() ;
		
		DESCRIPTOR_PRM = gd.getNextString() ;

	}
	
	
	/**
	 * 
	 * */
	public String toString(){
		
		String r = "NUM_DIST="  + NUM_DIST + ParamMng.PARAM_STRING_SEPARATOR + 
				"INCREMENT_SIZE=_"  + INCREMENT_SIZE	+ ParamMng.PARAM_STRING_SEPARATOR + 	
				"DESCRIPTOR_NAME=_" + DESCRIPTOR_NAME + ParamMng.PARAM_STRING_SEPARATOR  +  
				"ROISIZE="  + ROISIZE + ParamMng.PARAM_STRING_SEPARATOR  +
				"experimentId="  + experimentId + ParamMng.PARAM_STRING_SEPARATOR  +
				"DESCRIPTOR_PRM=" + DESCRIPTOR_PRM ;
		
		return r ;		

	}
}
