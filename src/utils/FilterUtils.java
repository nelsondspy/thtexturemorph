package utils;


import ij.process.ImageProcessor;


public class FilterUtils  {
	
	
	/**
	 * @param L: filter width / 2 
	 * @param K: filter heght / 2
	 * 
	*/
	public static void borderExtended(ImageProcessor orig, ImageProcessor copy, int L , int K ){
		
		int M = orig.getWidth();
		int N = orig.getHeight();
		
		int Mcopy = copy.getWidth();
		int Ncopy = copy.getHeight();
		
		//completar superiormente
		int[]lastwidth = new int [ Mcopy ]; 
		int[]lastheight = new int [ Ncopy ]; 
		
		//SUPERIOR VALUES 
		for(int u =0 ; u< Mcopy ; u++){
			int p;
			if (u < M) p = orig.get(u,0);
			else p = lastwidth[M-1];
			lastwidth[u] = p ;	
		}
		
		//SUPERIOR ASIG 
		for(int v =0 ; v< L ; v++){
			for(int u =0 ; u< Mcopy ; u++){
				copy.set(u,v, lastwidth[u] );
			}		
		}
	
		//INFERIOR VALUES 
		for(int u =0 ; u< M ; u++){
			int p;
			if (u < M) p = orig.get(u,N-1);
			else p = lastwidth[M-1];
			lastwidth[u] = p ;	
		}
			
		//INFERIOR ASIG 
		for(int v =N+L; v< Ncopy ; v++){
			for(int u =0 ; u< Mcopy ; u++){
				copy.set(u,v, lastwidth[u]);
			}		
		}	
		
		//LEFT VALUES 
		for(int v = 0; v < Ncopy ; v++ ) {
			int p;
			if (v < N) p = orig.get(0,v);
			else p = lastheight[N-1];
			lastheight[v] = p ; 	
		}
		
		//LEFT ASIG lastwidth[u]00
		for(int v =0; v< Ncopy ; v++){
			for(int u =0 ; u< K ; u++){
				copy.set(u,v, lastheight[v] );
			}		
		}	
		
		//RIGHT VALUES 
		for(int v = 0; v < Ncopy ; v++ ) {
			int p;
			
			//if (v < N) p = orig.get(N-1,v);
			if (v < N) p = orig.get(M-1,v);
			else p = lastheight[N-1];
			lastheight[v] = p ; 	
		}
		
		//RIGHT ASIG lastwidth[u]00
		for(int v =0; v< Ncopy ; v++){
			for(int u =M+K ; u< Mcopy ; u++){
				copy.set(u,v, lastheight[v]);
			}		
		}

	}
	
	
	
	/**
	 * 
	 * 
	 * */
	public static float[] makeGausKernel1d(double sigma){
		int center = (int) (5.0 * sigma);
		float[] kernel = new float[(2*center)+ 1 ];
		double sigma2 = sigma * sigma ;
		for(int i = 0 ; i< kernel.length;  i++){
			double r = center - i;
			kernel[ i ]= (float ) Math.exp(-0.5*( r * r ) / sigma2 );
		}
		return kernel ;
		
	}

}
