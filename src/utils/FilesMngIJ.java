package utils;

import java.io.File;

import ij.ImagePlus;
import ij.io.FileSaver;
import ij.process.ImageProcessor;

/**
 * clase que agrupa funciones varias que permiten almacenar archivos de salida, carpetas etc 
 * 
 * */

public class FilesMngIJ {
	
	
	/**
	 *  Guarda en formato bmp en la carpeta/nombrearchivo.bmp 
	 *  
	 *  @param imgplus 
	 *  @param path con caracter de separacion 
	 *  @param nombre del archivo sin extension 
	 * */
	public static void saveBMP( ImageProcessor imageproc, String path, String filename  ){
		
		//crea la carpeta si no existe 
		File theDir = new File( path );
		if (!theDir.exists()) {
			try{
		        theDir.mkdir();
		    } 
		    catch(SecurityException e){
		        System.out.println("directorio no creado " + e.getMessage());
		    }   
		}
		
		ImagePlus imp = new ImagePlus("",  imageproc); 
		new FileSaver(imp).saveAsBmp(path + filename + "." +  "bmp");
	}
	
	
	/**
	 * Crea un directorio si no existe
	 * 
	 * */
	public static void createDirIfNoExist(String directoryName){
		File theDir = new File( directoryName );

		// if the directory does not exist, create it
		if (!theDir.exists()) {
		    boolean result = false;

		    try{
		        theDir.mkdir();
		        result = true;
		    } 
		    catch(SecurityException se){
		        //handle it
		    }        
		    if(result) {    
		        System.out.println("DIR created");  
		    }
		}
	}

}
