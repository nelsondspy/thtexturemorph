package utils;


import java.util.ArrayList;
import morpho.StructElement;


public class MiscelaneousArray {
	
	
	/**
	 * Completa un array de enteros con una serie dada por el inicio y  el salto 
	 * @autor nelsond
	 * @param array de enteros 
	 * @param valor del primer elemento 
	 * @param incremento entre cada elemento 
	 * */
	public static void setSerialArray(int[] arr ,int init, int increment){
		arr[0] = init;
		int auxd = init ;
		for(int s = 0  ; s < arr.length ; s++ ){
			arr[s]= auxd;
			auxd = auxd + increment;
			
		}
	}
	
	/**
	 * Genera elementos estructurantes con la informacion de tamanho 
	 * 
	 * @author nelsond
	 * @param array de tamanhos de los elementos estructurantes a ser generados
	 * @param arraylist de elementos estructurantes generados 
	 * */
	public static void setArraySESizes(final int [] listSizes, ArrayList<int [][]>arraSEsize  ){
		for(int s = 0 ; s < listSizes.length; s++  ){
			int[][] se = StructElement.SESquare( listSizes[s] );
			arraSEsize.add(se );
		}
	}
}
