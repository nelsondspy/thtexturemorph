package utils;

import ij.gui.GenericDialog;

/**
 * Clase que agrupa funciones que gestionan parametros de entrada 
 * y salida de los diferentes algoritmos y programas. 
 * */
public class ParamMng {
	
	final static public String  PARAM_STRING_SEPARATOR  = "&"; 

	/**
	 * Cuadro de dialogo que lee parametros para la covarianza morfologica
	 * @return array de dos enteros {0= cantidad_de_tamanhos, 1= salto_entre_tamanhos }.  
	 * 
	 * */
	public static int[] dialogGranulometry(){
		
		int [] params = {0,0};
		
		GenericDialog gd = new  GenericDialog("Parametros de covarianza morfologica ");
		
		gd.addNumericField("Cantidad de tamanhos", 10, 2);
		gd.addNumericField("Salto entre tamanhos ", 1, 2);
		
		 
		gd.showDialog();
	    if (gd.wasCanceled()) return params;
	    
	    params[0] = (int)gd.getNextNumber(); 
	    params[1]=  (int )gd.getNextNumber(); 
	    
	    return params;
	}
	
	/**
	 * Cuadro de dialogo que lee parametros para la covarianza morfologica
	 * @return array de dos enteros {0= cantidad_de_dist, 1= salto_entre_dist }.  
	 * 
	 * */
	public static int[] dialogCovariance(){
		
		int [] params = {0,0};
		
		GenericDialog gd = new  GenericDialog("Parametros de covarianza morfologica ");
		
		gd.addNumericField("Cantidad de distancias", 10, 2);
		gd.addNumericField("Salto entre distancias ", 1, 2);
		
		 
		gd.showDialog();
	    if (gd.wasCanceled()) return params;
	    
	    params[0] = (int)gd.getNextNumber(); 
	    params[1]=  (int )gd.getNextNumber(); 
	    
	    return params;
	}
	
	
	/**
	 * Cuadro de dialogo  que lee el id del experimento experimentId
	 * */
	public static String [] dialogExperimentId(){
		String [] p = {"",""};
		
		GenericDialog gd = new  GenericDialog("Generar archivos arff del experimentId)");
		
		gd.addMessage("Generar archivos arff de entrenamiento y test del experimentId \n "
				+ "El experimentId: Es el codigo del conjunto de datos generados y almacenados en la base de datos ");
		
		gd.addStringField("Experimentid","",40);
		gd.addStringField("Database","",40);
		
		gd.showDialog();
		
		if (gd.wasCanceled()) return p;
	    
	    p[ 0 ] = gd.getNextString(); 
	    p[ 1 ] = gd.getNextString(); 
	    
	    
		return p ;

	}
	
	
	/**
	 * Cuadro de dialogo que lee parametros de HibridCovarianceGran 
	 * @return array de tres enteros 
	 *  {0= cantidad_de_distancias,
	 *   1= salto_entre_distancias,
	 *   2 = cantidad de tamanhos ,
	 *   3 = saltos entre los tamanhos }.  
	 * 
	 * */
	public static int[] dialogHybridCovarGran(){
		
		int [] params = {0,0,0,0};
		
		GenericDialog gd = new  GenericDialog("Parametros de covarianza morfologica ");
		
		gd.addNumericField("Cantidad de distancias", 10, 2);
		gd.addNumericField("Salto entre distancias ", 1, 2);
		gd.addNumericField("Cantidad de tamaños ", 10, 2);
		gd.addNumericField("Salto entre tamaños ", 1, 2);
		
		 
		gd.showDialog();
	    if (gd.wasCanceled()) return params;
	    
	    params[0] = (int)gd.getNextNumber(); 
	    params[1]=  (int )gd.getNextNumber();
	    params[2] = (int)gd.getNextNumber(); 
	    params[3]=  (int )gd.getNextNumber(); 

	    
	    return params;
	}

	
	/**
	 * Retorna una cadena legible que contiene los parametros 
	 * @param cantida de distancias 
	 * @param  salto entre distancias 
	 * @param cantida de tamanhos
	 * @param santo entre tamanhos 
	 * @return cadena con los parametros concatenados 
	 * 
	 * */
	public static String getstrHybridCov(int cantdist, int saltdist, int cantsiz, int salsiz){
		String r = "cantdist=" + cantdist + 
				  ",saltodist= "  + saltdist +
				  ",canttam= "+  cantsiz  + 
				  ",saltotam= "+  salsiz  ;
		return r;
	}
	
	
	/**
	 * La ejecucion de algoritmo requiere parametros generales que no tienen nada que ver 
	 * con el algoritmo en si mismo, mas bien informacion general de salida o entrada  
	 * @author nelsond
	 * @return array de strings con los datos recolectados en el dialogo
	 * <p>
	 * [0] : string de conexion con la base de datos <br>
	 * [1] : id del experimento   <br>
	 * [2] : directorio con las imagenes a procesar 
	 * </p> 
	 * */
	
	public static String [] dialogGralMassiveRun(){
		String [] params= { "", "" ,  "", "" };
		
		GenericDialog gd = new  GenericDialog("Parametros generales ");
		
		gd.addStringField("Cadena de conexion con la bdd ", "");
		gd.addStringField("Id del experimento o  parametros ", "", 50);
		gd.addStringField("Directorio de las imagenes a procesar:", "", 50);
		gd.addStringField("Nombre del plugin (con espacios) :", "", 25);

		gd.showDialog();
	   
		if (gd.wasCanceled()) return params;	    
	    
		params[0] = gd.getNextString(); 
	    params[1] = gd.getNextString(); 
	    params[2] = gd.getNextString(); 
	    params[3] = gd.getNextString(); 
	    
	    return params;
	    
	}
	
	
	/**
	 * */
	public static String [] splitParamString(String sparam  ){
		
		String[] parts = sparam.split( ParamMng.PARAM_STRING_SEPARATOR ); 
		return parts;
	}
	
	
	
}
