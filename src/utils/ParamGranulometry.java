package utils;

import ij.gui.GenericDialog;

public class ParamGranulometry {

	public int getMAXSIZE_SE() {
		return MAXSIZE_SE;
	}

	public void setMAXSIZE_SE(int mAXSIZE_SE) {
		MAXSIZE_SE = mAXSIZE_SE;
	}

	public int getSTEPSIZE_SE() {
		return STEPSIZE_SE;
	}

	public void setSTEPSIZE_SE(int sTEPSIZE_SE) {
		STEPSIZE_SE = sTEPSIZE_SE;
	}

	public int getROISIZE() {
		return ROISIZE;
	}

	public void setROISIZE(int rOISIZE) {
		ROISIZE = rOISIZE;
	}
	
	
    public String getExperimentId() {
		return experimentId;
	}

	public void setExperimentId(String experimentId) {
		this.experimentId = experimentId;
	}

	public String getDESCRIPTOR_NAME() {
		return DESCRIPTOR_NAME;
	}

	public void setDESCRIPTOR_NAME(String descriptorName) {
		this.DESCRIPTOR_NAME = descriptorName;
	}
	
	public String getDESCRIPTOR_PRM() {
		return DESCRIPTOR_PRM;
	}

	public void setDESCRIPTOR_PRM(String dESCRIPTOR_PRM) {
		DESCRIPTOR_PRM = dESCRIPTOR_PRM;
	}
	
	//parametros del algoritmo 
	public int MAXSIZE_SE = 15 ;
	
	public int STEPSIZE_SE = 2 ;
	
	/* tamanhos  de rois para las ventanas   */
	public int ROISIZE = 2 ;

	public String experimentId  = null ;
	
	public String DESCRIPTOR_NAME= null;
	
	/** parametros del descriptor  */
	public String DESCRIPTOR_PRM = null;
	
	

	
	public ParamGranulometry(){
		
	}
	
	/**
     * Parametros de granulometria desde un String  
     * @author nelsond
     * 
     * */
    public void setParamFromString(String param ) {
		
		String[] listTokens = param.split( ParamMng.PARAM_STRING_SEPARATOR ); 
		
		for (String token : listTokens) {
			
			//cada token tiene la forma "NOMBREPARAMETRO=VALOR" EJ. "MAXSIZE_SE=10"
			//entonces se vuelve a separar
			
			String [] par = token.split( "=" );
			
			String paramName = par[ 0 ];
			
			
			
			if ( paramName.equals( "MAXSIZE_SE" ) ) MAXSIZE_SE = Integer.parseInt(  par[ 1 ]  )   ;  
			if ( paramName.equals( "STEPSIZE_SE" ) )STEPSIZE_SE = Integer.parseInt(  par[ 1 ]  )  ;
			if ( paramName.equals( "ROISIZE" ) ) ROISIZE =  Integer.parseInt(  par[ 1 ]  )  ;
			if ( paramName.equals( "ID" ) ) experimentId =  par[ 1 ]  ;
			if ( paramName.equals( "DESCRIPTOR_NAME" ) ) DESCRIPTOR_NAME =  par[ 1 ]  ;
			if ( paramName.equals( "DESCRIPTOR_PRM" ) ) DESCRIPTOR_PRM =  par[ 1 ]  ;
			
		}
	}
    
    /**
     * */
	public void setParamFromGUI(){
	
		GenericDialog gd = new  GenericDialog("Parametros de covarianza morfologica ");
		
		gd.addNumericField("Cantidad de tamanhos", 10, 2);
		gd.addNumericField("Salto entre tamanhos ", 1, 2);
		gd.addStringField("clase Descriptor", "", 40);
		gd.addNumericField("ROI size", 2, 2);
		gd.addStringField("experimentid", "test", 30);
		gd.addStringField("DESCRIPTOR_PRM", "", 40);
		 
		gd.showDialog();
	    if (gd.wasCanceled()) return ;

		MAXSIZE_SE = (int)gd.getNextNumber();  ;
		
		STEPSIZE_SE = ( int )gd.getNextNumber() ;
		
		DESCRIPTOR_NAME= gd.getNextString() ;
		
		ROISIZE = ( int )gd.getNextNumber()  ;

		experimentId  = gd.getNextString() ;

		DESCRIPTOR_PRM = gd.getNextString() ; 

	}
	
	
	/**
	 * 
	 * */
	public String toString(){
		
		String r = "MAXSIZE_SE="  + MAXSIZE_SE + ParamMng.PARAM_STRING_SEPARATOR + 
				"STEPSIZE_SE=_"  + STEPSIZE_SE	+ ParamMng.PARAM_STRING_SEPARATOR + 	
				"DESCRIPTOR_NAME=_" + DESCRIPTOR_NAME + ParamMng.PARAM_STRING_SEPARATOR  +  
				"ROISIZE="  + ROISIZE + ParamMng.PARAM_STRING_SEPARATOR  +
				"experimentId="  + experimentId  + ParamMng.PARAM_STRING_SEPARATOR + 
				"DESCRIPTOR_PRM" + DESCRIPTOR_PRM   ; 
		
		return r ;		

	}
}
