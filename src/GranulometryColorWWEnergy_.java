
import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.implespesoventana.TesisRGBEnergyWW ;
import morpho.color.jvazqnog.models.Pixel;


/**
 * Granulometria color, procesamiento vectorial fijando pesos con ENERGIA SIN VENTANAS 
 * 
 * */
public class GranulometryColorWWEnergy_ extends GranulometryBasic {
	
	
	/**
	 * Aplica las operaciones morfologicas 
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		
			//----------------------------------
				
			String pathwork= "granulcolor_mw/images/";
				
			String nameImageinput =  pathwork + imp.getTitle() ;
			String nameImageOuput =  "_M=granWWEnergy" +  "_s" + sizeSE  ;
				
			//erosion  (minimo)  
			TesisRGBEnergyWW energyMin = new TesisRGBEnergyWW("Min", nameImageinput , 
		               "bmp", (ColorProcessor)  this.imp.getProcessor(), se );
				
			energyMin.setFilterName(nameImageOuput);
			energyMin.run();
				
			//dilatacion 
			String iminput2 = pathwork + imp.getTitle() + nameImageOuput   +"Min" +  ".bmp" ;
			//abrir la imagen erosionada 
			ImagePlus impcurrent = IJ.openImage( iminput2 );
			
			TesisRGBEnergyWW energyMax = new  TesisRGBEnergyWW("Max", iminput2 , 
		                "bmp", (ColorProcessor)  impcurrent.getProcessor(), se );
				
			energyMax.setFilterName( "" );
				
			energyMax.run();
				
			ImagePlus impFinal = IJ.openImage( iminput2  +   "Max.bmp"  ); 
			return impFinal.getProcessor();
				
	}

}
