import java.util.Arrays;
import java.util.List;

import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.implespesoventana.TesisRGBMin;
import morpho.color.jvazqnog.models.Pixel;


/**
 * Granulometria color, procesamiento vectorial fijando pesos con ENTROPIA CON VENTANAS
 * 
 * */
public class GranulometryColorMultiWMin_ extends GranulometryBasic {
	
	
	/**
	 * Aplica las operaciones morfologicas 
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		
		   List<Integer> windowList = Arrays.asList( 7    );
		//  List<Integer> windowList = Arrays.asList( 2 );

			//----------------------------------
				
			String pathwork= "granulcolor_mw/images/";
				
			String nameImageinput =  pathwork + imp.getTitle() ;
			String nameImageOuput =  "_M=granMultWinMin" +  "_s" + sizeSE  ;
				
			//erosion  (minimo)  
			TesisRGBMin minMin = new TesisRGBMin("Min", nameImageinput , 
		               "bmp", (ColorProcessor)  this.imp.getProcessor(), se, windowList );
				
			minMin.setFilterName(nameImageOuput);
			minMin.run();
				
			//dilatacion 
			String iminput2 = pathwork + imp.getTitle() + nameImageOuput   +"Min" +  ".bmp" ;
			//abrir la imagen erosionada 
			ImagePlus impcurrent = IJ.openImage( iminput2 );
			
			TesisRGBMin minMax = new  TesisRGBMin("Max", iminput2 , 
		                "bmp", (ColorProcessor)  impcurrent.getProcessor(), se, windowList  );
				
			minMax.setFilterName( "" );
				
			minMax.run();
				
			ImagePlus impFinal = IJ.openImage( iminput2  +   "Max.bmp"  ); 
			return impFinal.getProcessor();
				
	}

}
