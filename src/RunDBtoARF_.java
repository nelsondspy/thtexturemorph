import ij.plugin.PlugIn;
import clasiffier.MainGenARFF;
import utils.ParamMng ;


public class RunDBtoARF_ implements PlugIn {
	   String experimentId =  null ;
	   String database = null;

	
	  /**
		* 
		* */
		public void run( String arg ) {
			
			//usar gui para tomar valores 
			if (this.database == null ){
				String [] p = ParamMng.dialogExperimentId() ;
				
				experimentId =  p[ 0 ] ;
				database = p[ 1 ];

			}
			
			
			//generacion del archivo con todas las instancias ocultando con "?" clases de las instancias de test 
			String [] args = {experimentId , null  , "0", "68" , database };
			MainGenARFF.main(args);
			
			//generacion del archivo con solo las instancias de test , mostrando el id de la clase a la que pertenece
			String [] args2 = {experimentId , "TEST"  , "1", "68" , database };
			MainGenARFF.main(args2);
			
		}
		
	}

