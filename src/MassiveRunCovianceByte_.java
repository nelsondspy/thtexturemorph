import ij.*;  
import ij.plugin.PlugIn;  
import ij.process.*;  
import ij.io.Opener; 

import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;  
import utils.ParamMng ;

/**
 * Ejecucion masiva multhilo , paralelizacion por particion de datos
 * -Cada hilo recibe la misma cantidad de imagenes a procesar. <br>
 * -Las Tareas de tamanho homogeneo <br>
 * 
 * */
public class MassiveRunCovianceByte_ implements PlugIn {
	
	/**
	 * @param Ruta completa con separador final "/path/"
	 *
	 * */
	public void run(String  args ){
		//String PATH = "/home/nelsond/Documentos/inginf/tesis/Outex_TC_00013/images/";
		
		String [] p =  ParamMng.dialogGralMassiveRun();
		final String PATHDIR = p[ 2 ];
		final String experimentId = p[ 1 ];
		
		
		/*un hilo por procesador disponible */
		int numProcessors  = Runtime.getRuntime().availableProcessors();
		final Thread[] threads = new Thread[ numProcessors ];
		
        final AtomicInteger ai = new AtomicInteger(0);
        
        final String [] lisfiles = listFilesinDir(PATHDIR) ; 
        
        //imagenes por procesador 
        final int nunImgforProc = lisfiles.length /  numProcessors;
        
        for (int ithread = 0; ithread < threads.length; ithread++) {
        	
        	final int indexIni =  ithread   * nunImgforProc;
        	
        	final int indexEnd  =  (ithread  * nunImgforProc) + nunImgforProc;
        	final int thethread =  ithread ; 
        	
        	threads[ithread] = new Thread() {
        		int idthread = thethread;	
        		
        		//constructor 
                { 
                	setPriority(Thread.NORM_PRIORITY); 
                }  
  
                public void run() {  
                	
                    for (int i = indexIni ; i < indexEnd ; i++){
                    	
                    	ImagePlus imp = IJ.openImage(PATHDIR + lisfiles[ i ]);  
                    	 
                    	IJ.run(imp,"8-bit","");
                    	IJ.run(imp, "CovarianceByte ", experimentId );  
                    	IJ.run(imp,"Close","");
                    	
                    	System.out.println( idthread +":"+ lisfiles[ i ] );
                    	
                    	//ai.getAndIncrement();
                    }  
                }
            };
  
        }
        
        startAndJoin(threads);
		//ImagePlus imp = IJ.openImage(PATH);  
		//imp.show();  
		//System.out.println("ai.getAndIncrement():" + ai.getAndIncrement());
	}
	
	public static void startAndJoin(Thread[] threads)  
    {  
        for (int ithread = 0; ithread < threads.length; ++ithread)  
        {  
            threads[ithread].setPriority(Thread.NORM_PRIORITY);  
            threads[ithread].start();  
        }  
  
        try  
        {     
            for (int ithread = 0; ithread < threads.length; ++ithread)  
                threads[ithread].join();  
        } catch (InterruptedException ie)  
        {  
            throw new RuntimeException(ie);  
        }  
    }
	
	/**
	 * 
	 * 
	 * */
	static String []  listFilesinDir(String path ){
		
		File folder = new File( path );
		File[] listOfFiles = folder.listFiles();
		
		String lsfilesNames []= new String[listOfFiles.length]; 

		    for (int i = 0; i < listOfFiles.length; i++) {
		      if (listOfFiles[i].isFile()) {
		        
		    	  lsfilesNames[i]= listOfFiles[i].getName();
		    	  
		      } else if (listOfFiles[i].isDirectory()) {
		        //System.out.println("Directory " + listOfFiles[i].getName());
		      }
		    }
		    return lsfilesNames  ; 
	}
	
	  

}
