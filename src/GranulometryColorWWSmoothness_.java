
import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.implespesoventana.TesisRGBSmoothnessWW;
import morpho.color.jvazqnog.models.Pixel;


/**
 * Granulometria color, procesamiento vectorial fijando pesos con ENTROPIA CON VENTANAS
 * 
 * */
public class GranulometryColorWWSmoothness_ extends GranulometryBasic {
	
	
	/**
	 * Aplica las operaciones morfologicas 
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		
			//----------------------------------
				
			String pathwork= "granulcolor_mw/images/";
				
			String nameImageinput =  pathwork + imp.getTitle() ;
			String nameImageOuput =  "_M=granWWSmooth" +  "_s" + sizeSE  ;
				
			//erosion  (minimo)  
			TesisRGBSmoothnessWW smooMin = new TesisRGBSmoothnessWW("Min", nameImageinput , 
		               "bmp", (ColorProcessor)  this.imp.getProcessor(), se );
				
			smooMin.setFilterName(nameImageOuput);
			smooMin.run();
				
			//dilatacion 
			String iminput2 = pathwork + imp.getTitle() + nameImageOuput   +"Min" +  ".bmp" ;
			//abrir la imagen erosionada 
			ImagePlus impcurrent = IJ.openImage( iminput2 );
			
			TesisRGBSmoothnessWW smooMax = new  TesisRGBSmoothnessWW("Max", iminput2 , 
		                "bmp", (ColorProcessor)  impcurrent.getProcessor(), se );
				
			smooMax.setFilterName( "" );
				
			smooMax.run();
				
			ImagePlus impFinal = IJ.openImage( iminput2  +   "Max.bmp"  ); 
			return impFinal.getProcessor();
				
	}

}
