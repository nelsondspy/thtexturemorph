
import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.implespesoventana.TesisRGBAsimetricWW ;
/**
 * Covarianza morgologica usando pesos ASIMETRIA SIN VENTANAS
 * */
public class CovarianceColorWWAsimetric_ extends CovarianceBasic {
	
	
	/**
	 * */
	public ImageProcessor applyMorphoOperation(int distance, int orientation , Pixel [] se  ){
	
		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 
		String pathwork= "covarcolor_promv/images/";
		
		String nameImageinput =  pathwork + imp.getTitle() ;
		String nameImageOuput =  "_M=covarWWAsim" + "_dist=" + distance  + "_angl="+ orientation+ "_ROI=" + PRM.ROISIZE;
		
		//erosion  (minimo)  
		TesisRGBAsimetricWW asimWW = new TesisRGBAsimetricWW("Min", nameImageinput , 
                "bmp", (ColorProcessor)  this.imp.getProcessor(), se );
		
		asimWW.setFilterName(nameImageOuput);
		
		asimWW.run();
		 
		//
		ImagePlus impFinal = IJ.openImage( pathwork + imp.getTitle() + nameImageOuput   +"Min" +  ".bmp" ); 
		return impFinal.getProcessor();
		
	}	

	
	
}
