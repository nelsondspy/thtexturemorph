import order.Bitmixing;
import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;


/**
 * Granulometria color, procesamiento vectorial fijando pesos con ENTROPIA CON VENTANAS
 * 
 * */
public class GranulometryColorBitmix_ extends GranulometryBasic {
	
	
	/**
	 * Aplica las operaciones morfologicas 
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 
				String currentcolorimg =  imp.getShortTitle() + "_granBitmix" + "_s" + sizeSE  ;
					
				Bitmixing bitmix = new Bitmixing();
				
				//erosion  (minimo) 
				bitmix.erode(this.imp.getProcessor(),se, dirOutput , currentcolorimg  );
				
				//dilatacion maximo, 
				ImagePlus impEro = IJ.openImage( dirOutput + currentcolorimg + ".bmp" );
				bitmix.dilate(impEro.getProcessor(),se, dirOutput , currentcolorimg  );
				
		
				//cargar en memoria la imagen sobreescrita 
				ImagePlus impFinal = IJ.openImage( dirOutput + currentcolorimg + ".bmp" ); 
				return impFinal.getProcessor();
	
	}

}



