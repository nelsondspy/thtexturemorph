import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import ij.IJ;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;


public class ExperimentRunner {
	
	public static void main(String []args) {
		String dirImages = null;
		String pluginName = null;
		String experimentId = null;
		
		Options options = new Options(); 
		
		options.addRequiredOption("n", "name", true, "plugin class name ");
		options.addRequiredOption("e", "experiment", true, "all experiment params");
		options.addRequiredOption("p", "path", true, "images path");
		CommandLineParser parser = new DefaultParser();
		try {
			CommandLine cmd = parser.parse( options, args);
			pluginName = cmd.getOptionValue("n");
			experimentId = cmd.getOptionValue("e");
			dirImages = cmd.getOptionValue("p");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		 
		List<File> listFiles = getFilelist(dirImages);
		
		for (File file : listFiles) {
			PlugInFilter plugin = getPluginInstance(pluginName);
			ImagePlus implus = IJ.openImage(file.getAbsolutePath()); 
			plugin.setup(experimentId, implus);
			plugin.run(implus.getChannelProcessor());
			implus.close();
		}
	}
	
	public static List<File> getFilelist(String path) {
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();
		return Arrays.asList(listOfFiles);
		
	}
	
	public static PlugInFilter getPluginInstance(String className) {
		PlugInFilter loadedDescriptor = null;
		try {
			Class c = Class.forName(className);
			loadedDescriptor = (PlugInFilter) c.newInstance();
		} catch (ClassNotFoundException e) {
			System.err.println("error con ClassNotFoundException : " + e.getMessage());
		} catch (Exception e) {
			System.err.println("error con reflect : " + e.getMessage());
		}
		return loadedDescriptor;
	}
}
