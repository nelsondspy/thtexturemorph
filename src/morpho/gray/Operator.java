package morpho.gray;

import utils.FilterUtils;
import ij.process.ByteBlitter;
import ij.process.ByteProcessor;
import ij.process.ImageProcessor;
import ij.ImagePlus;

public class Operator {
	
public static void erode(ImageProcessor orig, int[][]H){
		
		int M = orig.getWidth();
		int N = orig.getHeight();

		int K= H[0].length / 2; // filter width 
		int L = H.length / 2;   //filter height
		
		
		//ImageProcessor copy = orig.duplicate();
		int copyM =  M + (2 * K);
		int copyN = N + (L * 2 );
		
		ImageProcessor copy = new ByteProcessor(copyM, copyN);
		
		copy.copyBits(orig, K, L, ByteBlitter.COPY );
		
		//con fines de depuracion 
		/*
		 * con fines de depuracion 
		 * 
		String title = "im_copy ";
		ImagePlus imgBorder = new ImagePlus(title, copy);
		imgBorder.show();
		*/
		
		FilterUtils.borderExtended(orig, copy,  L ,  K );

		

 
		for(int v = L; v <= copyN-L-1 ; v++ ){
			for(int u = K; u <= copyM-K-1 ; u++ ){
				int minp=255 ;
				int minvalexpErode = 255;
				
				//aplicacion del filtro
				for(int j=-L ; j<=L ; j++){
					for(int i = -K ; i <= K ; i++){
						
						//valor del elemento en el elemento estructurante
						int Hval = H[j+L][i+K];
						
						//ignora los valores 0 del elemento estructurante para que 
						//no afecten con el calculo
						if (Hval == 0) continue ;  

						//valor del pixel en el elemento estructurante
						int p = copy.getPixel(u+i, v+j);
						
						
						if (minvalexpErode  > (p - Hval) ){
							minvalexpErode = p - Hval;
							minp = p;
						} 
						
					}
				}
				
				orig.putPixel(u-K , v -L , minp );
			}
		}

		
	}


	public static void dilate(ImageProcessor orig, int[][]H){
		
		int M = orig.getWidth();
		int N = orig.getHeight();
	
		int K= H[0].length / 2; // filter width /2
		int L = H.length / 2;   //filter height
		
		
		//ImageProcessor copy = orig.duplicate();
		int copyM =  M + (2 * K);
		int copyN = N + (L * 2 );
		
		ImageProcessor copy = new ByteProcessor(copyM, copyN);
		
		copy.copyBits(orig, K, L, ByteBlitter.COPY );
				
		FilterUtils.borderExtended(orig, copy,  L ,  K );
	
		
		//String title = "im_copy ";
		//ImagePlus imgBorder = new ImagePlus(title, copy);
		//imgBorder.show();
		
	
		for(int v = L; v <= copyN-L-1 ; v++ ){
			for(int u = K; u <= copyM-K-1 ; u++ ){
				
				int maxp =0;
				int maxvalexpErode = 0;
				
				//aplicacion del filtro
				for(int j=-L ; j<=L ; j++){
					
					for(int i = -K ; i <= K ; i++){
						//valor del elemento en el elemento estructurante
						int Hval = H[j+L][i+K];
						
						//ignora los valores 0 del elemento estructurante para que 
						//no afecten con el calculo
						if (Hval == 0) continue ;  
						
						//valor del pixel en el elemento estructurante
						int p = copy.getPixel(u+i, v+j);
						
						
						if (maxvalexpErode  < (p + Hval) ){
							maxvalexpErode = p + Hval;
							maxp = p;
						} 
						
					}
				}
				
				orig.putPixel(u-K , v-L , maxp );
			}
		}

	
}



}
