package morpho.gray;

import utils.FilterUtils;
import ij.process.ByteBlitter;
import ij.process.ByteProcessor;
import ij.process.ImageProcessor;
import ij.ImagePlus;

public class OperatorPairSE {

	
/**
 * Aplica el filtro morfologico de erosion a una de la imagen 
 * usando un elemento estructurante plano de dimensiones impares.
 * Se espera que el elemento estructurante sea una matriz de valores enteros (0,1) , 
 * donde los valores 0 son ignorados. 
 *
 * @author nelsond 
 * @param orig imagen original 
 * @param H elemento estructuante plano de dimensiones impares.
 * @return orig modificada  
 * */	
public static void erode(ImageProcessor orig, int[][]H, int distance, int angle ){
	
	
		int M = orig.getWidth();
		int N = orig.getHeight();

		int K= H[0].length / 2; // filter width 
		int L = H.length / 2;   //filter height

		/* calculo del incremento debido al otro SE */
		int dezY ;
		if(angle == 90 ){
			dezY = distance + L ;
		}else{
			dezY =  (int)(Math.round(Math.tan(Math.toRadians(angle))) * (distance + K) ) ;	
		}
	
		//altos y anchos requeridos por 
		int widthSE2req = distance  + ( 2 * K ) + 1   ;
		int heightSE2req = Math.abs( dezY ) + ( 2 * L ) + 1   ;
		/* fin calculo del incremento debido al otro SE */
		
		
		int copyM =  M + ( 2 * widthSE2req ) ;  // izquierdo = K  y derecho = K
		int copyN = N + ( 2 * heightSE2req )  ;  // inferior = L  y superior = L 
		
		ImageProcessor copy = new ByteProcessor(copyM, copyN);
		
		copy.copyBits(orig, widthSE2req , heightSE2req, ByteBlitter.COPY );
		
		
		//con fines de depuracion  
		
		/*
		String title = "im_copy , angle: " + angle ;
		ImagePlus imgBorder = new ImagePlus(title, copy);
		imgBorder.show();
		*/
		
		FilterUtils.borderExtended(orig, copy,   heightSE2req, widthSE2req );
		
		/* define los desplazamientos  punto al final del vector separador 
		 * se tratan los casos especiales para 0,90 grados 
		 * */
		int dU =  (angle > 90? -1 : angle==90? 0 : 1  ) * (distance + ( K * (angle!=0 ? 1 : 2) + 1 ) ) ;
		int dV =  Math.abs( dezY ) + ( L * (angle==0 ? 0 : 1)  + 1) ;
		/*--*/

		for(int v = heightSE2req ; v <= copyN - heightSE2req - 1 ; v++ ){
			for(int u =  widthSE2req ; u <= copyM - widthSE2req - 1 ; u++ ){
				
			//con fines de depuracion 	
			/*
			if ( ! (u==50 && v ==50 ) ) {
					continue;
			}	
			*/
				
			//se inicializa con 255 para que siempre pueda tomar valores menores
				int minp=255 ;
				int minvalexpErode = 255;
				
				/*SE separado por el vector  */
				int u2 = u + dU  ;
				int v2 = v - dV	;	
				
				/*Aplicacion del filtro del primer SE */
				for(int j=-L ; j<=L ; j++){
					for(int i = -K ; i <= K ; i++){
						//valor del elemento en el elemento estructurante
						int Hval = H[j+L][i+K];
						//ignora los valores 0 del elem estruc para evitar el calc de esas posiciones
						if (Hval == 0) continue ;  
						
						/* en cada posicion de ambos elementos estructurantes 
						 * se calcula el minimo */
						int p1 = copy.get(u + i, v + j) ;
						int p2 = copy.get(u2 + i, v2 + j ) ;
						int p = p1 < p2 ? p1 : p2 ; 
						
						//valor minimo general 
						if (minvalexpErode  > (p - Hval) ){
							minvalexpErode = p - Hval;
							minp = p;
						}
						
						
						/* con fines de prueba 
						 * 
						copy.set(u+i, v+j,255);			
						//punto separado por un vector
						copy.set(u2+i, v2+j,255);
						*/
						
						
					}	
				}
				
				/* Fin del procesamiento del pixel */		
				orig.set(u - widthSE2req, v - heightSE2req  , minp );
				
			}
		}
		
		
		/*
		String title = "im_copy: " ;
		ImagePlus imgse = new ImagePlus(title, copy);
		imgse.show();
		*/
		
	}


/**
 * Aplica el filtro morfologico de dilatacion a una de la imagen 
 * usando un elemento estructurante plano de dimensiones impares.
 * Se espera que el elemento estructurante sea una matriz de valores enteros (0,1) , 
 * donde los valores 0 son ignorados. 
 *
 * @author nelsond 
 * @param orig imagen original 
 * @param H elemento estructuante plano de dimensiones impares.
 * @return orig modificada  
 * */	
public static void dilate(ImageProcessor orig, int[][]H, int distance, int angle ){
	
	
		int M = orig.getWidth();
		int N = orig.getHeight();

		int K= H[0].length / 2; // filter width 
		int L = H.length / 2;   //filter height

		/* calculo del incremento debido al otro SE */
		int dezY ;
		if(angle == 90 ){
			dezY = distance + L ;
		}else{
			dezY =  (int)(Math.round(Math.tan(Math.toRadians(angle))) * (distance + K) ) ;	
		}
	
		//altos y anchos requeridos por 
		int widthSE2req = distance  + ( 2 * K ) + 1   ;
		int heightSE2req = Math.abs( dezY ) + ( 2 * L ) + 1   ;
		/* fin calculo del incremento debido al otro SE */
		
		
		int copyM =  M + ( 2 * widthSE2req ) ;  // izquierdo = K  y derecho = K
		int copyN = N + ( 2 * heightSE2req )  ;  // inferior = L  y superior = L 
		
		ImageProcessor copy = new ByteProcessor(copyM, copyN);
		
		copy.copyBits(orig, widthSE2req , heightSE2req, ByteBlitter.COPY );
		
		
		//con fines de depuracion  
		/*
		String title = "im_copy , angle: " + angle ;
		ImagePlus imgBorder = new ImagePlus(title, copy);
		imgBorder.show();
		*/
		
		FilterUtils.borderExtended(orig, copy,   heightSE2req, widthSE2req );
		
		/* define los desplazamientos  punto al final del vector separador 
		 * se tratan los casos especiales para 0,90 grados 
		 * */
		int dU =  (angle > 90? -1 : angle==90? 0 : 1  ) * (distance + ( K * (angle!=0 ? 1 : 2) + 1 ) ) ;
		int dV =  Math.abs( dezY ) + ( L * (angle==0 ? 0 : 1)  + 1) ;
		/*--*/

		for(int v = heightSE2req ; v <= copyN - heightSE2req - 1 ; v++ ){
			for(int u =  widthSE2req ; u <= copyM - widthSE2req - 1 ; u++ ){
				
			//con fines de depuracion 	
			/*
			if ( ! (u==widthSE2req  && v ==heightSE2req ) ) {
					continue;
			}	
			*/
			//se inicializa con 255 para que siempre pueda tomar valores menores
				int maxp = 0 ;
				int maxvalexpErode = 0;
				
				/*SE separado por el vector  */
				int u2 = u + dU  ;
				int v2 = v - dV	;	
				
				/*Aplicacion del filtro del primer SE */
				for(int j=-L ; j<=L ; j++){
					for(int i = -K ; i <= K ; i++){
						//valor del elemento en el elemento estructurante
						int Hval = H[j+L][i+K];
						//ignora los valores 0 del elem estruc para evitar el calc de esas posiciones
						if (Hval == 0) continue ;  
						
						/* en cada posicion de ambos elementos estructurantes 
						 * se calcula el maximo */
						int p1 = copy.get(u + i, v + j) ;
						int p2 = copy.get(u2 + i, v2 + j ) ;
						int p = p1 > p2 ? p1 : p2 ; 
						
						//valor minimo general 
						if (maxvalexpErode  < (p + Hval) ){
							maxvalexpErode = p + Hval;
							maxp = p;
						}
						/* con fines de prueba 
						copy.set(u+i, v+j,255);			
						//punto separado por un vector
						copy.set(u2+i, v2+j,255);
						*/
					}	
				}
				/* Fin del procesamiento del pixel */		
				orig.set(u - widthSE2req, v - heightSE2req  , maxp );
				
			}
		}
		
	}
	

}
