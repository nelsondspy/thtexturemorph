package morpho;
import morpho.color.jvazqnog.models.Pixel ;
import morpho.color.jvazqnog.util.Various ;


/**
 * Elemento estructurante implementado como lista  objetos Pixel 
 * */
public class StructElementListPixels {
	
	
	/**
	 * @param size : tamanho del elemento estructurante 1x1 , 3x3
	 * 
	 * */
	public static Pixel [] getSESquare( int size, int distance, int angle ){
		
		
		// si size = 1 entonces la matriz sera de 2*1 + 1 = 3 x 3 = 9 elementos 
		int totalsize =  ( 2  * size ) + 1 ;
		
		/*- -*/
		int K= totalsize / 2; // filter width 
		
		int L = totalsize / 2;   //filter height
	
		/* calculo del incremento debido al otro SE */
		int dezY ;
		if(angle == 90 ){
			dezY = distance + L ;
		}else{
			dezY =  (int)(Math.round(Math.tan(Math.toRadians(angle))) * (distance + K) ) ;	
		}
		
	
		/* define los desplazamientos  punto al final del vector separador 
		 * se tratan los casos especiales para 0,90 grados 
		 * */
		int dU =  (angle > 90? -1 : angle==90? 0 : 1  ) * (distance + ( K * (angle!=0 ? 1 : 2) + 1 ) ) ;
		int dV =  Math.abs( dezY ) + ( L * (angle==0 ? 0 : 1)  + 1) ;
		/*--*/

		
		Pixel [] setpix1  =  Various.getShiftArray( totalsize );
		Pixel [] listPixelSE  =  new Pixel[ 2 * setpix1.length];
		
		
        for (int i = 0; i < setpix1.length ;   i++) {
        	listPixelSE[i] = setpix1[ i ];
        	
        	//
        	// listPixelSE[ i + setpix1.length ] = new Pixel( setpix1[ i ].getY() + dV , setpix1[ i ].getX() - dU ) ;
        	listPixelSE[ i + setpix1.length ] = new Pixel( setpix1[ i ].getX() - dU , setpix1[ i ].getY() + dV  ) ;
        }
        
        for (int i = 0; i < listPixelSE.length ;   i++) {	
        	System.out.println("x:" + listPixelSE[i ].getX( ) + 
        			", y: " + listPixelSE[i ].getY()  );
        
        }
        
      return listPixelSE ; 
		
	}
}
