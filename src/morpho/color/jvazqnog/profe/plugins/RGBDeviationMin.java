/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.profe.plugins;

import java.util.List;

import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.models.PixelWeight;
import morpho.color.jvazqnog.util.TesisRGBSdeviationAbstract;
import ij.process.ColorProcessor;



/**
 *
 * @author Derlis Argüello
 * @Nomenclatura: [Metodo][Orden][Filtro] 
 * @Ejemplo: [TesisRGB][Mean][Median]
 */
public class RGBDeviationMin extends TesisRGBSdeviationAbstract{
	
	
    
    public RGBDeviationMin(String methodName, String path, String extension, ColorProcessor colProcessor, 
            Pixel[] se, int xRoi, int yRoi) {
        super(methodName, path, extension, colProcessor, se, xRoi, yRoi);
    }
    
    @Override
    public void show() {
        //print();
        save();
    }

    @Override
    public int[] getFilter(List<PixelWeight> orderPixelWeight) {
        return min(orderPixelWeight);
    }

}