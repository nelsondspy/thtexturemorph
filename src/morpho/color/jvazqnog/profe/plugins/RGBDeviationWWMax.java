/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.profe.plugins;

import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.models.PixelWeight;
import morpho.color.jvazqnog.util.TesisRGBSDeviationWWAbstract;

import ij.process.ColorProcessor;

import java.util.List;


/**
 *
 * @author Derlis Argüello
 * @Nomenclatura: [Metodo][Orden][Filtro] 
 * @Ejemplo: [TesisRGB][Mean][Median]
 */
public class RGBDeviationWWMax extends TesisRGBSDeviationWWAbstract{
	
	
    
    public RGBDeviationWWMax(String methodName, String path, String extension, ColorProcessor colProcessor, 
            Pixel[] se) {
        super(methodName, path, extension, colProcessor, se);
    }
    
    @Override
    public void show() {
        //print();
        save();
    }

    @Override
    public int[] getFilter(List<PixelWeight> orderPixelWeight) {
        return max(orderPixelWeight);
    }

}