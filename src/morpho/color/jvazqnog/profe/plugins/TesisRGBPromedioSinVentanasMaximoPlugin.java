/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.profe.plugins;

import morpho.color.jvazqnog.impl.profe.TesisRGBMeanWWMax;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.util.Various;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;

/**
 *
 * @author Derlis Argüello
 */
public class TesisRGBPromedioSinVentanasMaximoPlugin implements PlugInFilter{
    
	//agregados por nelsond
	//nombre del archivo de salida, agregado por nelsond
	String outputFilename = "TesisRGBPromedioSinVentanasMaximo";
	
	Pixel[] seEight ;
	String pathDir ;
	
		
    @Override
    public int setup(String arg, ImagePlus imp) {
        if (imp == null) {
            return DONE;
        }
        return DOES_RGB + DOES_STACKS + NO_CHANGES + NO_UNDO;

    }

    @Override
    public void run(ImageProcessor ip) {
        
        
    	//String extension = "jpg";
    	
    	String extension = "bmp";
    	
        //imagen original
        ImagePlus imgOriginal = new ImagePlus();
        imgOriginal.setProcessor(ip);
        ColorProcessor colProcessor = (ColorProcessor) imgOriginal.getProcessor();
        
        // Create 8N Structuring Element.
        //int[] rEight = {-1, -1, -1,  0, 0, 0,  1, 1, 1};
       // int[] cEight = {-1,  0,  1, -1, 0, 1, -1, 0, 1};
      //  Pixel[] seEight = new Pixel[rEight.length];

        /*
         
        for (int i = 0; i < rEight.length; i++) {
            seEight[i] = new Pixel(rEight[i], cEight[i]);
        }
        
        */
        
        TesisRGBMeanWWMax tesisRGBMeanWWMax = new TesisRGBMeanWWMax(this.outputFilename , this.pathDir, 
                extension, colProcessor, seEight);
        
        tesisRGBMeanWWMax.run();
        
    }
    
    
    public boolean showDialog() {
        GenericDialog gd = new GenericDialog("TesisRGBPromedioSinVentanasMaximo");
        gd.addNumericField("XRoi:", 0, 0);
        gd.addNumericField("YRoi:", 0, 0);
        gd.showDialog();
        if (gd.wasCanceled()) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Agregado por nelsond 
     * 
     * */
    public void setOutput(String filaname, String pathDir    ){
    	this.outputFilename = filaname  ;
    	this.pathDir = pathDir;
    }
    
    public void setseEight(int size ){
    	seEight = Various.getShiftArray(size);
    }
    
    
    /**
     * 
     * Establecer el elemento estructurante 
     * 
     * */
    public void setSE(Pixel[] seEight ){
    	this.seEight = seEight ;
    }


}