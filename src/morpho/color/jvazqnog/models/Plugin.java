/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.models;

import static morpho.color.jvazqnog.util.BasicTestAbstract.logger;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

/**
 *
 * @author Derlis Argüello
 */
public abstract class Plugin {
    public static Logger logger;
    String pluginName;
    
    public Plugin(String pluginName) {
        this.pluginName = pluginName;
        configurarLog();
    }

    private void configurarLog() {
        
        //Crear logger 
        String loggerName = pluginName;
        logger = Logger.getLogger(loggerName);
        String PATTERN = "%m%n";
        //creando el appender para el arhivo log
        DailyRollingFileAppender fileApp = new DailyRollingFileAppender();
        
        fileApp.setFile("logs/"+ loggerName+".csv");
        fileApp.setImmediateFlush(true);
        fileApp.setDatePattern("'.'yyyy-MM-dd'.log'");
        fileApp.setLayout(new PatternLayout(PATTERN));
        fileApp.activateOptions();
        
        logger.addAppender(fileApp);

        //creando el appender para el log de la consola
        ConsoleAppender consoleApp = new ConsoleAppender();
        consoleApp.setImmediateFlush(true);
        consoleApp.setLayout(new PatternLayout(PATTERN));
        consoleApp.activateOptions();
        
        logger.addAppender(consoleApp);
 
    }
}
