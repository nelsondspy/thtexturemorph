/*
 * Tesis Arguello Balbuena
 * Derechos Reservados 2015 - 2016
 */
package morpho.color.jvazqnog.models;

import java.util.Comparator;

/**
 *
 * @author daasalbion
 */
public class BitMixingComparator implements Comparator<Integer>{
    //por defecto es el primer componente el mayor
    Double[] arr2;
    public int [] order = {0, 1, 2};
    //cantidad de veces que se opta por canal
    public int [] componentChoiceCounter;
    RGB[] coloresVentana;
    int contadorComparaciones = 0;
    
    public BitMixingComparator() {
    }
    
    public BitMixingComparator(Double[] array, RGB[] coloresVentana, int [] order) {
        this.arr2 = array;
        this.coloresVentana = coloresVentana;
        componentChoiceCounter = new int[4];
        this.order = order;
    }
    
    public int compare(Integer i1, Integer i2) {
        int result = arr2[i1].compareTo(arr2[i2]);
        if( result == 0){
            int[] color1 = coloresVentana[i1].getRGB();
            int[] color2 = coloresVentana[i2].getRGB();
            for (int i = 0; i < order.length; i++) {
                if ( color1[order[i]] < color2[order[i]] ) {
                    componentChoiceCounter[i+1]++;
                    contadorComparaciones++;
                    return -1;
                }
                if ( color1[order[i]] > color2[order[i]] ) {
                    componentChoiceCounter[i+1]++;
                    contadorComparaciones++;
                    return 1;
                }
            }
        } else{
            componentChoiceCounter[0]++;
            contadorComparaciones++;
            return result;
        }
        return 0;
    }
    
}