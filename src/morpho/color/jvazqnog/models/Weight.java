/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.models;

import ij.gui.Roi;

/**
 *
 * @author daasalbion 
 */
public class Weight {
    
    private Roi roi;
    private double[] sum;
    private int totalPixels;
    private int processed;
    private int[][] channelHistogram;

    public Weight(Roi roi, double[] sum, int totalPixels) {
        this.roi = roi;
        this.sum = sum;
        this.totalPixels = totalPixels;
        this.processed = 0;
    }

    public Roi getRoi() {
        return roi;
    }

    public void setRoi(Roi roi) {
        this.roi = roi;
    }

    public double[] getSum() {
        return sum;
    }

    public void setSum(double[] sum) {
        this.sum = sum;
    }

    public int getTotalPixels() {
        return totalPixels;
    }

    public void setTotalPixels(int totalPixels) {
        this.totalPixels = totalPixels;
    }

    public int getProcessed() {
        return processed;
    }

    public void setProcessed(int processed) {
        this.processed = processed;
    }

    @Override
    public String toString() {
        return "Weight{" + "roi=" + roi + ", sum=" + sum.toString() + ", totalPixels=" + totalPixels + ", processed=" + processed + '}';
    }
    
}
