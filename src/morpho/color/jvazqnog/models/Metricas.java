/*
 * Tesis Arguello Balbuena
 * Derechos Reservados 2015 - 2016
 */
package morpho.color.jvazqnog.models;

import ij.process.ByteProcessor;
import ij.process.ColorProcessor;

/**
 *
 * @author daasalbion
 */
public class Metricas {
    
    private ColorProcessor original, result;
    private int height, width, cLength;
    private ByteProcessor [] channelsOriginal, channelsResult;
    
    public Metricas ( ColorProcessor original, ColorProcessor result ) {
        
        this.original = original;
        this.result = result;
        this.width = original.getWidth();
        this.height = original.getHeight();
        
        cLength = original.getNChannels();
        channelsOriginal = new ByteProcessor[cLength];
        channelsResult = new ByteProcessor[cLength];
        
        for (int i = 0; i < cLength; i++) {
            //cargas los canales de la imagen por separado en channel
            channelsOriginal[i] = original.getChannel(i + 1, channelsOriginal[i]);
            channelsResult[i] = result.getChannel(i + 1, channelsResult[i]);
            
        }
        
    }
    
    public ColorProcessor getOriginal() {
        return original;
    }

    public void setOriginal(ColorProcessor original) {
        this.original = original;
    }

    public ColorProcessor getResult() {
        return result;
    }

    public void setResult(ColorProcessor result) {
        this.result = result;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
    
    /*
        Calculo de Mean Absolute Error
    */
    public double mae() {
        
        double mae = 0;
        int absDifference, originalPixel, resultPixel;

        for (int m = 0; m < height; m++) {
            for (int n = 0; n < width; n++) {
                //para cada espacio de color
                for (int i = 0; i < cLength; i++) {
                    originalPixel = channelsOriginal[i].get(n, m);
                    resultPixel = channelsResult[i].get(n, m);
                    absDifference = Math.abs( originalPixel - resultPixel );
                    mae +=  absDifference;
                }
            }
        }
        
        mae = mae/(double)(height*width*cLength);
        
        return mae;
    }
    
    /*
     *  Calculo de Mean Square Error
    */
    public double mse() {
        
        double mse = 0;
        int originalPixel, resultPixel;
        double difference;

        for (int m = 0; m < height; m++) {
            for (int n = 0; n < width; n++) {
                //para cada espacio de color
                for (int i = 0; i < cLength; i++) {
                    originalPixel = channelsOriginal[i].get(n, m);
                    resultPixel = channelsResult[i].get(n, m);
                    difference = Math.pow( ( originalPixel - resultPixel ), 2 );
                    mse +=  difference;
                }
            }
        }
        
        mse = mse/(double)(height*width*cLength);
        
        
        return mse;
    }
    
    /*
     *  Calculo de Normalize Mean Square Error
    */
    public double nmse() {
        
        double nmse = 0;
        int originalPixel, resultPixel;
        double numeratorSum = 0;
        double denominatorSum = 0;

        for (int m = 0; m < height; m++) {
            for (int n = 0; n < width; n++) {
                //para cada espacio de color
                for (int i = 0; i < cLength; i++) {
                    originalPixel = channelsOriginal[i].get(n, m);
                    resultPixel = channelsResult[i].get(n, m);
                    numeratorSum += Math.pow( ( originalPixel - resultPixel ), 2 );
                    denominatorSum += Math.pow( ( originalPixel ), 2 );
                }
            }
        }
        
        nmse = (double)numeratorSum/(double)denominatorSum;
        
        return nmse;
    }
    
}