/*
 * Tesis Arguello Balbuena
 * Derechos Reservados 2015
 */
package morpho.color.jvazqnog.models;

/**
 *
 * @author daasalbion
 */
public class PixelWeight {
    private int[] pixel;
    private float weight;

    public PixelWeight(int[] pixel, float weight) {
        this.pixel = pixel;
        this.weight = weight;
    }

    public int[] getPixel() {
        return pixel;
    }

    public void setPixel(int[] pixel) {
        this.pixel = pixel;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "PixelWeight{" + "pixel=" + pixel + ", weight=" + weight + '}';
    }

}