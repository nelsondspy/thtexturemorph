/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.models;
import java.util.Arrays;

/**
 *
 * @author Thelma
 */
public class BitMixing_ {
        
    public static final int[] NEGRO_REF = {0, 0, 0};
    
    public static int[] decisionCanalCounter = new int[4];
    public static int contadorComparaciones;
    public static int [] order = {2,0,1};
    
    public static int[] erosionar(int[] imagen, int FILAS, int COLUMNAS, int[][] se) throws Exception{
        int MSE = se.length;
        int NSE = se[0].length;
        int F_CENTRAL_SE = (int) Math.floor((MSE + 1)/2) - 1; //fila del elemento central del SE
        int C_CENTRAL_SE = (int) Math.floor((NSE + 1)/2) - 1; //columna del elemento central del SE
        int FILA_SE_FROM = -F_CENTRAL_SE;
        int FILA_SE_TO = MSE - F_CENTRAL_SE - 1;
        int COLUMNA_SE_FROM = -C_CENTRAL_SE;
        int COLUMNA_SE_TO = NSE - C_CENTRAL_SE - 1;
        
        int[] imagenRetorno = new int[FILAS*COLUMNAS];
        double[] pesosNodos = new double[MSE*NSE];
        
        //mantenemos otro array con los pesos de los nodos
        int[] metricas = calcularMetricaPixeles(imagen);
        RGB[] coloresVentana;
                
        //Aqui haremos todo el trabajo, trasladando el elemento estructurante y 
        //haciendo los calculos correspondientes
        for (int i = 0; i < FILAS; i++) {
            for (int j = 0; j < COLUMNAS; j++) {
                coloresVentana = new RGB[pesosNodos.length];
                for (int k = FILA_SE_FROM; k <= FILA_SE_TO; k++) {
                    for (int l = COLUMNA_SE_FROM; l <= COLUMNA_SE_TO; l++){
                        //controlamos si las coordenadas estan fuera de rango en la imagen
                        int indicePN = sub2ind(NSE, k + F_CENTRAL_SE, l + C_CENTRAL_SE);
                        if(i + k < 0 || i+k >= FILAS || j + l < 0 || j + l >= COLUMNAS){
                          //estan fuera de rango, se elimina este vertice del grafo grafoActual
                          pesosNodos[indicePN] = -1;
                          coloresVentana[indicePN] = new RGB(0, 0, 0);
                          continue;  
                        }
                        int indiceM = sub2ind(COLUMNAS, i + k, j + l);
                        pesosNodos[indicePN] = metricas[indiceM];
                        int R = (imagen[indiceM] &0xff0000)>>16;
                        int G = (imagen[indiceM] &0x00ff00)>>8;
                        int B = (imagen[indiceM] &0x0000ff);
                        coloresVentana[indicePN] = new RGB(R, G, B);
                    }
                }
                //aqui llegamos una vez que identificamos a los valores de la ventana actual
                //ubicamos en la posicion (i,j) el menor valor
                int indice = hallarIndexMenor(pesosNodos, coloresVentana);
                int[] coordenadasSe = ind2sub(NSE, indice);
                //hallamos las coordenadas considerando al origen del SE en (0, 0)
                coordenadasSe[0] = coordenadasSe[0] + FILA_SE_FROM;
                coordenadasSe[1] = coordenadasSe[1] + COLUMNA_SE_FROM;
                //Finalmente, el infimo en esta ventana sera el situado en (i, j) - (coordenadaSe[0], coordenadaSe[1])
                imagenRetorno[sub2ind(COLUMNAS, i, j)] = imagen[sub2ind(COLUMNAS, i + coordenadasSe[0], j + coordenadasSe
[1])];
            }
        }
        return imagenRetorno;
    }

    public static int[] dilatar(int[] imagen, int FILAS, int COLUMNAS, int[][] se) throws Exception{
        int MSE = se.length;
        int NSE = se[0].length;
        int F_CENTRAL_SE = (int) Math.floor((MSE + 1)/2) - 1; //fila del elemento central del SE
        int C_CENTRAL_SE = (int) Math.floor((NSE + 1)/2) - 1; //columna del elemento central del SE
        int FILA_SE_FROM = -F_CENTRAL_SE;
        int FILA_SE_TO = MSE - F_CENTRAL_SE - 1;
        int COLUMNA_SE_FROM = -C_CENTRAL_SE;
        int COLUMNA_SE_TO = NSE - C_CENTRAL_SE - 1;
        
        int[] imagenRetorno = new int[FILAS*COLUMNAS];
        double[] pesosNodos = new double[MSE*NSE];
        
        //mantenemos otro array con los pesos de los nodos
        int[] metricas = calcularMetricaPixeles(imagen);
        RGB[] coloresVentana;
                
        //Aqui haremos todo el trabajo, trasladando el elemento estructurante y 
        //haciendo los calculos correspondientes
        for (int i = 0; i < FILAS; i++) {
            for (int j = 0; j < COLUMNAS; j++) {
                coloresVentana = new RGB[pesosNodos.length];
                for (int k = FILA_SE_FROM; k <= FILA_SE_TO; k++) {
                    for (int l = COLUMNA_SE_FROM; l <= COLUMNA_SE_TO; l++){
                        //controlamos si las coordenadas estan fuera de rango en la imagen
                        int indicePN = sub2ind(NSE, k + F_CENTRAL_SE, l + C_CENTRAL_SE);
                        if(i + k < 0 || i+k >= FILAS || j + l < 0 || j + l >= COLUMNAS){
                          //estan fuera de rango, se elimina este vertice del grafo grafoActual
                          pesosNodos[indicePN] = -1;
                          coloresVentana[indicePN] = new RGB(0, 0, 0);
                          continue;  
                        }
                        int indiceM = sub2ind(COLUMNAS, i + k, j + l);
                        pesosNodos[indicePN] = metricas[indiceM];
                        int R = (imagen[indiceM] &0xff0000)>>16;
                        int G = (imagen[indiceM] &0x00ff00)>>8;
                        int B = (imagen[indiceM] &0x0000ff);
                        coloresVentana[indicePN] = new RGB(R, G, B);
                    }
                }
                //aqui llegamos una vez que identificamos a los valores de la ventana actual
                //ubicamos en la posicion (i,j) el menor valor
                int indice = hallarIndexMayor(pesosNodos, coloresVentana);
                int[] coordenadasSe = ind2sub(NSE, indice);
                //hallamos las coordenadas considerando al origen del SE en (0, 0)
                coordenadasSe[0] = coordenadasSe[0] + FILA_SE_FROM;
                coordenadasSe[1] = coordenadasSe[1] + COLUMNA_SE_FROM;
                //Finalmente, el infimo en esta ventana sera el situado en (i, j) - (coordenadaSe[0], coordenadaSe[1])
                imagenRetorno[sub2ind(COLUMNAS, i, j)] = imagen[sub2ind(COLUMNAS, i + coordenadasSe[0], j + coordenadasSe
[1])];
            }
        }
        return imagenRetorno;
    }

    public static int[] filtroMediana(int[] imagen, int FILAS, int COLUMNAS, int[][] se) throws Exception{
        int MSE = se.length;
        int NSE = se[0].length;
        int F_CENTRAL_SE = (int) Math.floor((MSE + 1)/2) - 1; //fila del elemento central del SE
        int C_CENTRAL_SE = (int) Math.floor((NSE + 1)/2) - 1; //columna del elemento central del SE
        int FILA_SE_FROM = -F_CENTRAL_SE;
        int FILA_SE_TO = MSE - F_CENTRAL_SE - 1;
        int COLUMNA_SE_FROM = -C_CENTRAL_SE;
        int COLUMNA_SE_TO = NSE - C_CENTRAL_SE - 1;
        
        int[] imagenRetorno = new int[FILAS*COLUMNAS];
        double[] pesosNodos = new double[MSE*NSE];
        
        //mantenemos otro array con los pesos de los nodos
        int[] metricas = calcularMetricaPixeles(imagen);
        RGB[] coloresVentana = null;
                        
        //Aqui haremos todo el trabajo, trasladando el elemento estructurante y 
        //haciendo los calculos correspondientes
        for (int i = 0; i < FILAS; i++) {
            for (int j = 0; j < COLUMNAS; j++) {
                coloresVentana = new RGB[pesosNodos.length];
                for (int k = FILA_SE_FROM; k <= FILA_SE_TO; k++) {
                    for (int l = COLUMNA_SE_FROM; l <= COLUMNA_SE_TO; l++){
                        //controlamos si las coordenadas estan fuera de rango en la imagen
                        int indicePN = sub2ind(NSE, k + F_CENTRAL_SE, l + C_CENTRAL_SE);
                        if(i + k < 0 || i+k >= FILAS || j + l < 0 || j + l >= COLUMNAS){
                          //estan fuera de rango, se elimina este vertice del grafo grafoActual
                          pesosNodos[indicePN] = -1;
                          coloresVentana[indicePN] = new RGB(0, 0, 0);
                          continue;  
                        }
                        int indiceM = sub2ind(COLUMNAS, i + k, j + l);
                        pesosNodos[indicePN] = metricas[indiceM];
                        int R = (imagen[indiceM] &0xff0000)>>16;
                        int G = (imagen[indiceM] &0x00ff00)>>8;
                        int B = (imagen[indiceM] &0x0000ff);
                        coloresVentana[indicePN] = new RGB(R, G, B);
                    }
                }
                //aqui llegamos una vez que identificamos a los valores de la ventana actual
                //ubicamos en la posicion (i,j) el menor valor
                int indice = hallarIndexMediana(pesosNodos, coloresVentana);
                int[] coordenadasSe = ind2sub(NSE, indice);
                //hallamos las coordenadas considerando al origen del SE en (0, 0)
                coordenadasSe[0] = coordenadasSe[0] + FILA_SE_FROM;
                coordenadasSe[1] = coordenadasSe[1] + COLUMNA_SE_FROM;
                //Finalmente, el infimo en esta ventana sera el situado en (i, j) - (coordenadaSe[0], coordenadaSe[1])
                imagenRetorno[sub2ind(COLUMNAS, i, j)] = imagen[sub2ind(COLUMNAS, i + coordenadasSe[0], j + coordenadasSe
[1])];
            }
        }
        return imagenRetorno;
    }
   
    private static int hallarIndexMediana(double[] vectorReferencia, RGB[] colores) throws Exception{
        Integer[] indices = ordenar(vectorReferencia, colores);
        int inicio = 0;
        int fin = indices.length - 1;
        int i = 0;
        for (Integer indice : indices) {
            if(vectorReferencia[indice] != -1){
                inicio = i;
                break;
            }
            i++;
        }
        try{
            return indices[inicio + (fin - inicio)/2];
        } catch (Exception e){
            System.out.println("debug me");
        }
        return 0;
    }
    
    private static int[] calcularMetricaPixeles(int[] pixeles){
        int[] metricas = new int[pixeles.length];
        for (int i = 0; i < pixeles.length; i++) {
            int valor = (int) pixeles[i];
            int r = (valor &0xff0000)>>16;
            int g = (valor &0x00ff00)>>8;
            int b = valor &0x0000ff;
            metricas[i] = calcularMetrica(r, g, b);
        }
        return metricas;
    }
    
    public static Integer calcularMetrica(int r, int g, int b){
        String indexStr = "";
        //entrelazamos los bits que resultaron en X
        for (int i = 8 - 1; i >= 0; i--) {
            indexStr = indexStr + String.valueOf(r >>i & 1);
            indexStr = indexStr + String.valueOf(g >>i & 1);
            indexStr = indexStr + String.valueOf(b >>i & 1);
        }
        return Integer.parseInt(indexStr, 2);
    }
    
    /**
     * Recibe la cantidad de columnas que tiene una matriz, y las coordenadas y
     * retorna el indice correspondiente de 1 a N
     * @param columnas
     * @param subFila
     * @param subColumna
     * @return 
     */
    private static int sub2ind(int columnas, int subFila, int subColumna){
        return subFila * columnas + subColumna;
    }
    
        /**
     * Retorna las coordenadas de un elemento dentro de una matriz dada, dadas
     * las dimensiones de la matriz y el indice unidimensional del elemento.
     * @param columnas la cantidad de columnas de la matriz
     * @param index indice unidimensional
     * @return 
     */
    private static int[] ind2sub(int columnas, int index){
        int[] coordenadas = new int[2];
        coordenadas[0] = index / columnas;
        coordenadas[1] = index % columnas;
        return coordenadas;
    }
    
    private static int hallarIndexMenor(double[] vectorReferencia,
            RGB[] colores) throws Exception{
        
        Integer[] indices = ordenar(vectorReferencia, colores);
        for (Integer indice : indices) {
            if(vectorReferencia[indice] != -1){
                return indice;
            }
        }
        return indices.length-1;
    }
    
    private static int hallarIndexMayor(double[] vectorReferencia,
            RGB[] colores) throws Exception{
        
        Integer[] indices = ordenar(vectorReferencia, colores);
        return indices[indices.length - 1]; 
    }
    
    public static Integer[] ordenar(double[] imagen, RGB[] colores) throws Exception{
        Double[] imagenDouble =  new Double[imagen.length];
        //Mantendremos un array que nos indique el orden en el que deben aparecer los pixeles
        //por eso solo tendra indices
        Integer[] indices = new Integer[imagen.length];
        for (int i = 0; i < imagen.length; i++) {
            indices[i] = i;
            imagenDouble[i] = imagen[i];
        }

        //ordenamos los indices de acuerdo a las metricas
        BitMixingComparator comparador =
                new BitMixingComparator(imagenDouble, colores, order);
        
        Arrays.sort(indices, comparador);
        for (int i = 0; i < comparador.componentChoiceCounter.length; i++) {
            decisionCanalCounter[i] += comparador.componentChoiceCounter[i];
        }
        contadorComparaciones += comparador.contadorComparaciones;
        return indices;
    }
}
