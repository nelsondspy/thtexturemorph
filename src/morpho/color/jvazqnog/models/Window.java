/*
 * Tesis Arguello Balbuena
 * Derechos Reservados 2015 - 2016
 */
package morpho.color.jvazqnog.models ;

import ij.gui.Roi;
import ij.process.ByteProcessor;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 *
 * @author Derlis Argüello
 * @reference http://stats.stackexchange.com/questions/99777/calculating-the-variance-of-the-histogram-of-a-grayscale-image
 */
public class Window {
    private final Roi roi;
    private double[] xFxSum;
    private final int totalPixels;
    private Boolean processed = false;
    private int[][] channelHistogram;
    private final ByteProcessor[] channels;

    public Window(Roi roi, ByteProcessor[] channels, int totalPixels) {
        this.roi = roi;
        this.channels = channels;
        this.totalPixels = totalPixels;
        this.xFxSum = new double[channels.length];
    }

    public void setChannelHistogram() {
        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        channelHistogram = new int[cSize][hSize];

        for (int channel = 0; channel < cSize; channel++) {
            channels[channel].setRoi(roi);
            channelHistogram[channel] = channels[channel].getHistogram();
            
            for (int intensity = 0; intensity < hSize; intensity++) {
                //se calcula la sumatoria de intensidad por valor de histograma de esa intensidad
                xFxSum[channel] += (intensity) * channelHistogram[channel][intensity];
            }
        }
    }
    
    public void setChannelHistogramCMY() {
        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        channelHistogram = new int[cSize][hSize];

        for (int channel = 0; channel < cSize; channel++) {
            channels[channel].setRoi(roi);
            channelHistogram[channel] = channels[channel].getHistogram();
            
            for (int intensity = 0; intensity < hSize; intensity++) {
                //se calcula la sumatoria de intensidad por valor de histograma de esa intensidad
                xFxSum[channel] += (255 - intensity) * channelHistogram[channel][intensity];
            }
        }
    }
    
    public void setChannelHistogramCMYK() {
        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        Hashtable<Double, Integer> k_cont = new Hashtable<>();
        Hashtable<Double, Integer> c_cont = new Hashtable<>();
        Hashtable<Double, Integer> m_cont = new Hashtable<>();
        Hashtable<Double, Integer> y_cont = new Hashtable<>();

        int[][] channelHistogram = new int[3][hSize];

        for (int i = 0; i < cSize - 1; i++) {
            channels[i].setRoi(roi);
            channelHistogram[i] = channels[i].getHistogram();

        }
       
        double c, m, y1 = 0;
        for (int i = 0; i < channels[0].getPixelCount(); i++){
            c = 1 - (double)channels[0].get(i)/255.0d;
            m = 1 - (double)channels[1].get(i)/255.d;
            y1 = 1 - (double)(channels[2].get(i))/255.d;
            double k = Math.min(c, Math.min(m, y1));

            if (k >= 1) {

                if (!c_cont.containsKey(0d)) {
                    c_cont.put(0d, 1);
                } else {
                    int frecuencia = c_cont.get(0d) + 1;
                    c_cont.put(0d, frecuencia);
                }
                if (!m_cont.containsKey(0)) {
                    m_cont.put(0d, 1);
                } else {
                    int frecuencia = m_cont.get(0) + 1;
                    m_cont.put(0d, frecuencia);
                }
                if (!y_cont.containsKey(0)) {
                    y_cont.put(0d, 1);

                } else {
                    int frecuencia = y_cont.get(0) + 1;
                    y_cont.put(0d, frecuencia);
                }

            } else {
                double s = 1 - k;
                double c1 = (c - k)/s;
                double m1 = (m - k)/s;
                double y2 = (y1 - k)/s;


                if (!c_cont.containsKey(c1)) {
                    c_cont.put(c1, 1);

                } else {
                    int frecuencia = c_cont.get(c1) + 1;
                    c_cont.put(c1, frecuencia);

                }
                if (!m_cont.containsKey(m1)) {
                    m_cont.put(m1, 1);

                } else {
                    int frecuencia = m_cont.get(m1) + 1;
                    m_cont.put(m1, frecuencia);

                }
                if (!y_cont.containsKey(y2)) {
                    y_cont.put(y2, 1);

                } else {
                    int frecuencia = y_cont.get(y2) + 1;
                    y_cont.put(y2, frecuencia);

                }


            }
            if (!k_cont.containsKey(k)) {
                k_cont.put(k, 1);

            } else {
                int frecuencia = k_cont.get(k) + 1;
                k_cont.put(k, frecuencia);

            }
        }

        Enumeration c2 = c_cont.keys();
        double clave;
        int valor;
        while (c2.hasMoreElements()) {
            clave = (Double) c2.nextElement();
            valor = c_cont.get(clave);
            xFxSum[0] += clave*valor;
        }
        Enumeration m2 = m_cont.keys();

        while (m2.hasMoreElements()) {
            clave = (Double) m2.nextElement();
            valor = m_cont.get(clave);
            xFxSum[1] += clave * valor;
        }

        Enumeration y2 = y_cont.keys();

        while (y2.hasMoreElements()) {
            clave = (Double) y2.nextElement();
            valor = y_cont.get(clave);
            xFxSum[2] += clave * valor;
        }

        Enumeration k2 = k_cont.keys();

        while (k2.hasMoreElements()) {
            clave = (Double) k2.nextElement();
            valor = k_cont.get(clave);
            xFxSum[3] = clave * valor;
        }
    }
    
    public void getMode() {
        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        channelHistogram = new int[cSize][hSize];

        for (int channel = 0; channel < cSize; channel++) {
            channels[channel].setRoi(roi);
            channelHistogram[channel] = channels[channel].getHistogram();
            int[] maxValues = new int[cSize];
            
            for (int intensity = 0; intensity < hSize; intensity++) {
                //se calcula la sumatoria de intensidad por valor de histograma de esa intensidad
            	//if(channelHistogram[channel][intensity] > maxValues[channel]){
            	if(channelHistogram[channel][intensity] < maxValues[channel]){
                    xFxSum[channel] = intensity;
                    maxValues[channel] = channelHistogram[channel][intensity];
                }
            }
        }
    }
    
    public void getModeCMY() {
        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        channelHistogram = new int[cSize][hSize];

        for (int channel = 0; channel < cSize; channel++) {
            channels[channel].setRoi(roi);
            channelHistogram[channel] = channels[channel].getHistogram();
            int[] maxValues = new int[cSize];
            
            for (int intensity = 0; intensity < hSize; intensity++) {
                //se calcula la sumatoria de intensidad por valor de histograma de esa intensidad
                if(channelHistogram[channel][intensity] > maxValues[channel]){
                    xFxSum[channel] = 255 - intensity;
                    maxValues[channel] = channelHistogram[channel][intensity];
                }
            }
        }
    }
    
    public void getMax() {
        int cSize = channels.length;
        int n = channels[0].getPixelCount();
        for (int i = 0; i < cSize; i++) {
            for (int j = 0; j < n; j++) {
                int intensityValue = channels[i].get(j);
                if(intensityValue > xFxSum[i]){
                    xFxSum[i] = intensityValue;
                }
            }
        }
    }
    
    public void getMaxCMY() {
        int cSize = channels.length;
        int n = channels[0].getPixelCount();
        for (int i = 0; i < cSize; i++) {
            for (int j = 0; j < n; j++) {
                int intensityValue = channels[i].get(j);
                if(intensityValue > xFxSum[i]){
                    xFxSum[i] = 255 - intensityValue;
                }
            }
        }
    }
    
    public void getMin() {
        int cSize = channels.length;
        int n = channels[0].getPixelCount();
        for (int i = 0; i < cSize; i++) {
            xFxSum[i] = Integer.MAX_VALUE;
            for (int j = 0; j < n; j++) {
                int intensityValue = channels[i].get(j);
                if(intensityValue < xFxSum[i]){
                    xFxSum[i] = intensityValue;
                }
            }
        }
    }
    
    public void getMinCMY() {
        int cSize = channels.length;
        int n = channels[0].getPixelCount();
        for (int i = 0; i < cSize; i++) {
            xFxSum[i] = Integer.MAX_VALUE;
            for (int j = 0; j < n; j++) {
                int intensityValue = channels[i].get(j);
                if(intensityValue < xFxSum[i]){
                    xFxSum[i] = 255 - intensityValue;
                }
            }
        }
    }
    
    public void getRGBSumEntropyR() {
        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        channelHistogram = new int[cSize][hSize];
        int k;

        for (int i = 0; i < cSize; i++) {
            channels[i].setRoi(roi);
            channelHistogram[i] = channels[i].getHistogram();
        }

        for (int i = 0; i < hSize; i++) {
            for (k = 0; k < cSize; k++) {
                //se calcula el numerador de (6)
                if(channelHistogram[k][i] != 0){
                    xFxSum[k] = xFxSum[k] - (channelHistogram[k][i] * (Math.log(channelHistogram[k][i])/Math.log(2)));
                }
            }
        }
        
        for (k = 0; k < channelHistogram.length; k++) {
            xFxSum[k] = (double)1/xFxSum[k];
        }
    }
    
    /*
     * 
    public void getRGBSumEntropy() {
        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        channelHistogram = new int[cSize][hSize];
        int k;

        for (int i = 0; i < cSize; i++) {
            channels[i].setRoi(roi);
            channelHistogram[i] = channels[i].getHistogram();
        }

        for (int i = 0; i < hSize; i++) {
            for (k = 0; k < cSize; k++) {
                //se calcula el numerador de (6)
                double p = (double)channelHistogram[k][i]/(double)(totalPixels);
                if(channelHistogram[k][i] != 0){
                    xFxSum[k] = xFxSum[k] - (p * (Math.log(p)/Math.log(2)));
                }
            }
        }
    }
    
    */
    
    public void getRGBSumEntropy() {
        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        channelHistogram = new int[cSize][hSize];
        int k;

        for (int i = 0; i < cSize; i++) {
            channels[i].setRoi(roi);
            channelHistogram[i] = channels[i].getHistogram();
        }

        for (int i = 0; i < hSize; i++) {
            for (k = 0; k < cSize; k++) {
                //se calcula el numerador de (6)
                if(channelHistogram[k][i] != 0){
                    xFxSum[k] = xFxSum[k] - (channelHistogram[k][i] * (Math.log(channelHistogram[k][i])/Math.log(2)));
                }
            }
        }
    }
    
    public void getRGBSumEnergy() {
        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        channelHistogram = new int[cSize][hSize];
        int k;

        for (int i = 0; i < cSize; i++) {
            channels[i].setRoi(roi);
            channelHistogram[i] = channels[i].getHistogram();
        }

        for (int i = 0; i < hSize; i++) {
            for (k = 0; k < cSize; k++) {
                //se calcula el numerador de (6)
                double p = (double)channelHistogram[k][i]/(double)(totalPixels);
                xFxSum[k] += Math.pow(p, 2);
            }
        }
    }
    
    public void getRGBSumUniformity() {
        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        channelHistogram = new int[cSize][hSize];
        int k;

        for (int i = 0; i < cSize; i++) {
            channels[i].setRoi(roi);
            channelHistogram[i] = channels[i].getHistogram();
        }

        for (int i = 0; i < hSize; i++) {
            for (k = 0; k < cSize; k++) {
                //se calcula el numerador de (6)
                xFxSum[k] = (int) Math.pow(channelHistogram[k][i], 2);
            }
        }
    }

    public Roi getRoi() {
        return roi;
    }

    public double[] getxFxSum() {
        return xFxSum;
    }

    public int getTotalPixels() {
        return totalPixels;
    }

    public Boolean getProcessed() {
        return processed;
    }

    public int[][] getChannelHistogram() {
        return channelHistogram;
    }

    public ByteProcessor[] getChannels() {
        return channels;
    }

    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }
        
}