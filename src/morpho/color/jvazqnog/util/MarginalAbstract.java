/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.util;


import ij.process.ByteProcessor;
import ij.process.ColorProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.models.RGB;
import morpho.color.jvazqnog.models.Weight;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Derlis Argüello
 */
public abstract class MarginalAbstract extends BasicAbstract{
    
    ArrayList<Weight> weightList = new ArrayList<>(); // Array of weight
    public String [] component = {"R", "G", "B"};
    public long [] contadorDecisionComp = {0, 0, 0};
    public double[] decisionComp = {0, 0, 0};
    public ByteProcessor[] channels = new ByteProcessor[component.length]; // RGB channels

    public MarginalAbstract(String methodName, String path, String extension, 
            ColorProcessor colProcessor, Pixel[] se) {
        super(methodName, path, extension, colProcessor, se);
        
        for (int i = 0; i < channels.length; i++) {
            //cargas los canales de la imagen por separado en channels
            this.channels[i] = colProcessor.getChannel(i + 1, channels[i]);
        }
    }
    
    public int[] order(Pixel[] SE, Pixel p) {

        int x, y;
        int cLength = channels.length;
        int[] pixel;
        int[] filterP;
        int[] sum = {0, 0, 0};
        List<RGB> orderPixelWeight = new ArrayList<>();

        for (int i = 0; i < SE.length; i++) {
            x = p.getX() + SE[i].getX();
            y = p.getY() + SE[i].getY();
            //verificamos si esta en la ventana del elemento estructurante
            if (x > -1 && x < width && y > -1 && y < height) {
                pixel = new int[cLength];
                for (int k = 0; k < cLength; k++) {
                    pixel[k] = channels[k].get(x, y);
                }
                RGB rgb = new RGB(pixel);
                orderPixelWeight.add(rgb);
            }
        }
        
        filterP = getFilter(orderPixelWeight);

        return sum;
        
    }
    //implementaciones de los filtros
    public int[] min(List<RGB> orderPixelWeight) {
        int element = 0;
        return orderPixelWeight.get(element).getRGB();
    }
    
    public int[] max(List<RGB> orderPixelWeight) {
        int element = orderPixelWeight.size() - 1;
        return orderPixelWeight.get(element).getRGB();
    }
    
    public int[] median(List<RGB> orderPixelWeight) {
        
        List<Integer> orderPixelsRedChannel = new ArrayList<>();
        List<Integer> orderPixelsGreenChannel = new ArrayList<>();
        List<Integer> orderPixelsBlueChannel = new ArrayList<>();
        int [] filterP = {0, 0, 0};
        
        for (RGB pixelWeight : orderPixelWeight) {
            orderPixelsRedChannel.add(pixelWeight.getR());
            orderPixelsGreenChannel.add(pixelWeight.getG());
            orderPixelsBlueChannel.add(pixelWeight.getB());
        }
        
        Collections.sort(orderPixelsRedChannel);
        Collections.sort(orderPixelsGreenChannel);
        Collections.sort(orderPixelsBlueChannel);
        
        //obtenemos el elemento central
        int element = (int) Math.ceil(orderPixelWeight.size() / 2);
        filterP[0] = orderPixelsRedChannel.get(element);
        filterP[1] = orderPixelsGreenChannel.get(element);
        filterP[2] = orderPixelsBlueChannel.get(element);
        
        return filterP;
    }
    
    public int[] mean(List<RGB> orderPixelWeight) {
        ///obtenemos el promedio por canal (MARGINAL)
        int []sum = {0,0,0};
        
        for (RGB pixelWeight : orderPixelWeight) {
            for (int c = 0; c < channels.length; c++) {
                sum[c] += pixelWeight.getRGB()[c];
            }
        }
        
        for (int i = 0; i < channels.length; i++ ) {
            sum[i] = (int)(sum[i]/orderPixelWeight.size());
        }
        
        return sum;
    }
    
    public abstract int[] getFilter(List<RGB> orderPixelWeight);
    
    @Override
    public ColorProcessor run() {

        // Create 8N Structuring Element.
        int[] rEight = {-1, -1, -1, 0, 0, 0, 1, 1, 1};
        int[] cEight = {-1, 0, 1, -1, 0, 1, -1, 0, 1};
        Pixel[] seEight = new Pixel[rEight.length];

        for (int i = 0; i < rEight.length; i++) {
            seEight[i] = new Pixel(rEight[i], cEight[i]);
        }

        int[] elementP;

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                elementP = order(seEight, new Pixel(x, y));
                restoredColProcessor.putPixel(x, y, elementP);
            }
        }
        
        show();

        return restoredColProcessor;
    }
    
}
