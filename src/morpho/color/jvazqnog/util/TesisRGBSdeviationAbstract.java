/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.util;

import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.models.Window;
import ij.process.ColorProcessor;

/**
 *
 * @author Derlis Argüello
 */
public abstract class TesisRGBSdeviationAbstract extends TesisRGBMeanAbstract{
    
    public TesisRGBSdeviationAbstract(String methodName, String path, String extension, 
            ColorProcessor colProcessor, Pixel[] se, int xRoi, int yRoi) {
        super(methodName, path, extension, colProcessor, se, xRoi, yRoi);
    }

    @Override
    public float[] getRealWeight(Pixel p) {
        
        float[] mean = super.getRealWeight(p);
        // VARIABLES
        int numPixels = 0;
        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        float[] sDeviation = {0, 0, 0};
            
        for (Window window : windowsList) {
            if ( window.getProcessed() ) {
                numPixels += window.getTotalPixels();
                for (int channel = 0; channel < cSize; channel++) {
                    for (int intensity = 0; intensity < hSize; intensity++) {
                        sDeviation[channel] += Math.pow( ( (intensity - mean[channel]) ), 2)
                                *(window.getChannelHistogram()[channel][intensity]);
                    }
                }
            }
        }

        for (int channel = 0; channel < cSize; channel++) {
            sDeviation[channel] =  (float)(sDeviation[channel]/numPixels);
        }
        
        return sDeviation;
    } 
}