/*
 * Tesis Arguello Balbuena
 * Derechos Reservados 2015 - 2016
 */
package morpho.color.jvazqnog.util;

import java.util.Comparator;

import morpho.color.jvazqnog.models.RGB;

/**
 *
 * @author daasalbion
 */
public  class AlphaLexicographicalComparator implements Comparator<RGB>{
    //por defecto es el primer componente el mayor
    public int [] order = {0, 1, 2};
    //cantidad de veces que se opta por canal
    public long [] choseChannel = {0, 0, 0};
    public int alpha = 10;

    public AlphaLexicographicalComparator() {
    }
    
    public AlphaLexicographicalComparator(int [] order, int alpha) {
        this.order = order;
        this.alpha = alpha;
    }
    
    @Override
    public int compare(RGB o1, RGB o2) {
        //obtenemos los pixeles
        int [] color1 = o1.getRGB();
        int [] color2 = o2.getRGB();
        
        if ( ( color1[order[0]] ) + alpha  < ( color2[order[0]] ) ) {
            choseChannel[order[0]]++;
            return -1;
        } else {
            for (int i = 1; i < order.length; i++) {
                if ( color1[order[i]] < color2[order[i]] ) {
                    choseChannel[i]++;
                    return -1;
                }
                if ( color1[order[i]] > color2[order[i]] ) {
                    choseChannel[i]++;
                    return 1;
                }
            }
        }

        return 0;
        
    }
    
}