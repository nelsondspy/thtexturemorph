/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.util;


import ij.gui.Roi;
import ij.process.ColorProcessor;
import morpho.color.jvazqnog.models.Pixel;

import java.util.Iterator;

/**
 *
 * @author Derlis Argüello
 */
public abstract class TesisRGBSmoothnessWWAbstract extends TesisRGBBasicAbstractWithoutWindows{

    public TesisRGBSmoothnessWWAbstract(String methodName, String path, String extension, ColorProcessor colProcessor, Pixel[] se) {
        super(methodName, path, extension, colProcessor, se);
    }

    @Override
    public float[] getRGBSum(Roi roi) {

        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        float[] sum = new float[cSize];
        int[][] channelHistogram = new int[cSize][hSize];

        for (int i = 0; i < cSize; i++) {
            channels[i].setRoi(roi);
            channelHistogram[i] = channels[i].getHistogram();
        }

        for (int i = 0; i < hSize; i++) {
            for (int k = 0; k < cSize; k++) {
                //se calcula el numerador de (6)
                sum[k] = (sum[k] + (i) * channelHistogram[k][i]);
            }
        }

        return sum;
    }
    
    @Override
    public float[] getRealWeight(Pixel p, Pixel[] se) {
        Roi roi;
        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        int[][] channelHistogram = new int[cSize][hSize];
        float[] mean = new float[cSize];
        float[] sDeviation = {0,0,0};
        float[] smoothness = {0,0,0};

        // SET ALL SUMS TO 0
        for (int i = 0; i < channels.length; i++) {
            mean[i] = 0;
        }

        // ITERATE STRUCTURE ELEMENT
        roi = new Roi(p.getX() - 1, p.getY() - 1, 3, 3);
        mean = getRGBSum(roi);
        for (int i = 0; i < channels.length; i++) {
            mean[i] = mean[i] / (float)9;
            channels[i].setRoi(roi);
            channelHistogram[i] = channels[i].getHistogram();
        }
        
        for (int intensity = 0; intensity < hSize; intensity++) {
            for (int channel = 0; channel < cSize; channel++) {
                sDeviation[channel] += Math.pow( ( (intensity - mean[channel]) ), 2)
                        *(channelHistogram[channel][intensity]);
            }
        }
        
        for (int channel = 0; channel < cSize; channel++) {
            sDeviation[channel] =  sDeviation[channel]/(float)9;
            smoothness[channel] = 1 - 1/(1 + sDeviation[channel]);
        }

        return smoothness;
    } // end of getRegions();
    
}