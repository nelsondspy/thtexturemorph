/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.util;

import morpho.color.jvazqnog.models.Pixel;
import ij.ImagePlus;
import ij.io.FileSaver;
import ij.process.ColorProcessor;

/**
 *
 * @author Derlis Argüello
 */
public abstract class BasicAbstract {
    
    public String methodName;
    String path;
    String extension;
    ColorProcessor colProcessor;
    ColorProcessor restoredColProcessor;
    int width;
    int height;
    Pixel[] se;

    public BasicAbstract(String methodName, String path, String extension, ColorProcessor colProcessor, Pixel[] se) {
        this.methodName = methodName;
        this.path = path;
        this.extension = extension;
        this.colProcessor = colProcessor;
        this.height = colProcessor.getHeight();
        this.width = colProcessor.getWidth();
        this.restoredColProcessor = new ColorProcessor(width, height);
        this.se = se;
    }
    
    public void print(){
        ImagePlus imgPlus = new ImagePlus(methodName, restoredColProcessor);
        imgPlus.show();
    }
    
    public void save(){
        ImagePlus imgPlus = new ImagePlus(methodName, restoredColProcessor);
        if (extension.equalsIgnoreCase("png")) {
            new FileSaver(imgPlus).saveAsPng(path + methodName + "." + extension);
        }else if (extension.equalsIgnoreCase("jpg")){
            new FileSaver(imgPlus).saveAsJpeg(path + methodName + "." +  extension);
        }
        else if (extension.equalsIgnoreCase("bmp")){
            new FileSaver(imgPlus).saveAsBmp(path + methodName + "." +  extension);
        }
    }
    //abstracts methods
    public abstract ColorProcessor run();
    public abstract void show();
}
