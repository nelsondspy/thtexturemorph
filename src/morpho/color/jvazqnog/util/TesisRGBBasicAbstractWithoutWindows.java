/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.util;

import morpho.color.jvazqnog.models.Metricas;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.models.PixelWeight;
import morpho.color.jvazqnog.models.Weight;
import ij.gui.Roi;
import ij.process.ByteProcessor;
import ij.process.ColorProcessor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Derlis Argüello
 * @Nomenclatura: [Metodo][Orden][Filtro] 
 * @Ejemplo: [TesisRGB][Mean][Median]
 */
public abstract class TesisRGBBasicAbstractWithoutWindows extends BasicAbstract{
    
    public final ArrayList<Weight> weightList = new ArrayList<>(); // Array of weight
    public long [] contadorDecisionComp = {0, 0, 0};
    public long contadorDecisionValorReducido;
    public double[] decisionComp = {0, 0, 0};
    public double decisionValorReducido = 0;
    public String [] component = {"R", "G", "B"};
    public ByteProcessor[] channels = new ByteProcessor[component.length]; // RGB channels

    public TesisRGBBasicAbstractWithoutWindows(String methodName, String path, String extension, ColorProcessor colProcessor, Pixel[] se) {
        super(methodName, path, extension, colProcessor, se);
        
        for (int i = 0; i < channels.length; i++) {
            //cargas los canales de la imagen por separado en channel
            this.channels[i] = colProcessor.getChannel(i + 1, channels[i]);
        }
    }

    public abstract float[] getRGBSum(Roi roi);

    public float[] getRealWeight(Pixel p, Pixel[] se) {
        Roi roi;
        float[] sum = new float[channels.length];

        // SET ALL SUMS TO 0
        for (int i = 0; i < channels.length; i++) {
            sum[i] = 0;
        }

        // ITERATE STRUCTURE ELEMENT
        roi = new Roi(p.getX() - 1, p.getY() - 1, 3, 3);
        sum = getRGBSum(roi);

        return sum;
    } // end of getRegions();
   
    public int[] order(float[] weight, Pixel[] SE, Pixel p) {
        int cLength = channels.length;
        int x, y;
        float t = 0.0f;
        int[] pixel;
        List<PixelWeight> orderPixelWeight = new ArrayList<>();
        PixelWeight pixelWeight;
        int[] filterP;

        for (int i = 0; i < SE.length; i++) {
            x = p.getX() + SE[i].getX();
            y = p.getY() + SE[i].getY();
            //verificamos si esta en la ventana del elemento estructurante
            if (x > -1 && x < width && y > -1 && y < height) {
                pixel = new int[cLength];
                // Se carga el pixel y su valor T(p).
                //es decir se obtiene (7)
                for (int k = 0; k < cLength; k++) {
                    //se obtiene I_k
                    pixel[k] = channels[k].get(x, y);
                    t = t + weight[k] * pixel[k];
                }

                pixelWeight = new PixelWeight(pixel, t);
                orderPixelWeight.add(pixelWeight);
                t = 0;
            }
        }
        
        TesisRGBComparator comparator = new TesisRGBComparator();
        //ordenamos por peso
        Collections.sort(orderPixelWeight, comparator);
        
        for (int i = 0; i < cLength; i++) {
            contadorDecisionComp[i] += comparator.chooseChannel[i];
        }
        //los valores reducidos
        contadorDecisionValorReducido += comparator.valorReducido;
        //obtenemos el filtro
        filterP = getFilter(orderPixelWeight);

        return filterP;
    }
    //implementaciones de los filtros
    public int[] min(List<PixelWeight> orderPixelWeight) {
        int element = 0;
        return orderPixelWeight.get(element).getPixel();
    }
    
    public int[] max(List<PixelWeight> orderPixelWeight) {
        int element = orderPixelWeight.size() -  1;
        return orderPixelWeight.get(element).getPixel();
    }
    
    public int[] median(List<PixelWeight> orderPixelWeight) {
        int element = (int) Math.ceil(orderPixelWeight.size() / 2);
        return orderPixelWeight.get(element).getPixel();
    }
    //solicitar tipo de filtro
    public abstract int[] getFilter(List<PixelWeight> orderPixelWeight);
    
    @Override
    public ColorProcessor run() {
        
        // Create 8N Structuring Element.
        //int[] rEight = {-1, -1, -1, 0, 0, 0, 1, 1, 1};
        //int[] cEight = {-1, 0, 1, -1, 0, 1, -1, 0, 1};
        
        //Pixel[] seEight = new Pixel[rEight.length];
    	
    	Pixel[] seEight = this.se ;
    	
        //for (int i = 0; i < rEight.length; i++) {
        //    seEight[i] = new Pixel(rEight[i], cEight[i]);
        //}

        //setWeightList();

        int[] elementP;
        float[] realWeight;

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                realWeight = getRealWeight(new Pixel(x, y), seEight);
                elementP = order(realWeight, seEight, new Pixel(x, y));
                restoredColProcessor.putPixel(x, y, elementP);
            }
        }
        
        show();
        
        long totalDecisiones = contadorDecisionComp[0] + contadorDecisionComp[1] + contadorDecisionComp[2] + contadorDecisionValorReducido;
        //System.out.printf("\n");

        for (int i = 0; i < channels.length; i++) {
            decisionComp[i] = (double)contadorDecisionComp[i]/(double)totalDecisiones;
            //System.out.printf("Orden (%s) Porc. de decisión canal (%d) : (%f)\n", component[i], i, decisionComp[i]);
        }
        
        decisionValorReducido = (double)contadorDecisionValorReducido/(double)totalDecisiones;
        //System.out.printf("Orden Porc. de decisión valor reducido: (%f)\n", decisionValorReducido);
        
        return restoredColProcessor; 
    }
    
    public void log(int indice, String noiseName, Logger logger, Metricas metricas){
        logger.info(indice + ", "  + noiseName + ", " + methodName + ", " + metricas.mae() + ", " + metricas.mse() + ", " + metricas.nmse() + ", " + decisionValorReducido + ", " + decisionComp[0] + ", " + decisionComp[1] + ", " + decisionComp[2]);
    }
}