/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.util;


import ij.gui.Roi;
import ij.process.ColorProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.models.Window;

/**
 *
 * @author Derlis Argüello
 */
public abstract class TesisRGBTmomentAbstract extends TesisRGBMeanAbstract{
    
    public TesisRGBTmomentAbstract(String methodName, String path, String extension, 
            ColorProcessor colProcessor, Pixel[] se, int xRoi, int yRoi) {
        super(methodName, path, extension, colProcessor, se, xRoi, yRoi);
    }
    
    @Override
    public float[] getRealWeight(Pixel p) {

        // VARIABLES
        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        float[] tMoment = {0, 0, 0};
        
        float[] mean = super.getRealWeight(p);

        for (Window window : windowsList) {
            if ( window.getProcessed() ) {
                for (int channel = 0; channel < cSize; channel++) {
                    for (int intensity = 0; intensity < hSize; intensity++) {
                        tMoment[channel] += Math.pow( ( (intensity - mean[channel]) ), 3)
                                *(window.getChannelHistogram()[channel][intensity]);
                    }
                }
            }
        }
        
        return tMoment;
    }
   
}