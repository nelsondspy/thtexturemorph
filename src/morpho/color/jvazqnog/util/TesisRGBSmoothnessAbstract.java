
package morpho.color.jvazqnog.util;

import ij.process.ColorProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.models.Window;

/**
 *
 * @author Derlis Argüello
 */
public abstract class TesisRGBSmoothnessAbstract extends TesisRGBSdeviationAbstract{
    
    public TesisRGBSmoothnessAbstract(String methodName, String path, String extension, 
            ColorProcessor colProcessor, Pixel[] se, int xRoi, int yRoi) {
        super(methodName, path, extension, colProcessor, se, xRoi, yRoi);
    }
    
    @Override
    public float[] getRealWeight(Pixel p) {

        // VARIABLES
        int numPixels = 0;
        int cSize = channels.length;
        float[] smoothness = {0, 0, 0};
        
        float[] sDeviation = super.getRealWeight(p);
            
        for (Window window : windowsList) {
            //si todavia no fue procesado, se hace
            if (!window.getProcessed()) {
                numPixels += window.getTotalPixels();
            } 
        }

        for (int channel = 0; channel < cSize; channel++) {
            sDeviation[channel] =  sDeviation[channel]/numPixels;
            smoothness[channel] = 1 - 1/(1 + sDeviation[channel]);
        }
        
        return smoothness;
    } 
   
}