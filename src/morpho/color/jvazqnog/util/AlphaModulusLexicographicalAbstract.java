/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.util;

import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.models.RGB;
import morpho.color.jvazqnog.models.Weight;

import ij.process.ByteProcessor;
import ij.process.ColorProcessor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Derlis Argüello
 */
public abstract class AlphaModulusLexicographicalAbstract extends BasicAbstract{
    
    ArrayList<Weight> weightList = new ArrayList<>(); // Array of weight
    private int alpha = 10;
    public int [] order = {2,0,1};
    public String [] component = {"R", "G", "B"};
    public long [] contadorDecisionComp = {0, 0, 0};
    public double[] decisionComp = {0, 0, 0};
    public ByteProcessor[] channels = new ByteProcessor[component.length]; // RGB channels

    public AlphaModulusLexicographicalAbstract(String methodName, String path, String extension, 
            ColorProcessor colProcessor, int[] order, int alpha, Pixel[] se) {
        super(methodName, path, extension, colProcessor, se);
        this.order = order;
        this.alpha = alpha;
        
        for (int i = 0; i < channels.length; i++) {
            //cargas los canales de la imagen por separado en channels
            this.channels[i] = colProcessor.getChannel(i + 1, channels[i]);
        }
    }
    
    public int[] order(Pixel[] SE, Pixel p) {

        int x, y;
        int cLength = channels.length;
        int[] pixel;
        int[] filterP;
        List<RGB> orderPixelWeight = new ArrayList<>();

        for (int i = 0; i < SE.length; i++) {
            x = p.getX() + SE[i].getX();
            y = p.getY() + SE[i].getY();
            //verificamos si esta en la ventana del elemento estructurante
            if (x > -1 && x < width && y > -1 && y < height) {
                pixel = new int[cLength];
                for (int k = 0; k < cLength; k++) {
                    pixel[k] = channels[k].get(x, y);
                }
                RGB rgb = new RGB(pixel);
                orderPixelWeight.add(rgb);
            }
        }
        
        //ordenamos alfalexicograficamente
        AlphaModulusLexicographicalComparator comparador = new AlphaModulusLexicographicalComparator(order, alpha);
        Collections.sort(orderPixelWeight, comparador);
        
        for (int i = 0; i < cLength; i++) {
            contadorDecisionComp[i] += comparador.choseChannel[i];
        }

        //obtenemos el filtro
        filterP = getFilter(orderPixelWeight);

        return filterP;
        
    }
    //implementaciones de los filtros
    public int[] min(List<RGB> orderPixelWeight) {
        int element = 0;
        return orderPixelWeight.get(element).getRGB();
    }
    
    public int[] max(List<RGB> orderPixelWeight) {
        int element = orderPixelWeight.size() - 1;
        return orderPixelWeight.get(element).getRGB();
    }
    
    public int[] median(List<RGB> orderPixelWeight) {
        int element = (int) Math.ceil(orderPixelWeight.size() / 2);
        return orderPixelWeight.get(element).getRGB();
    }
    
    public abstract int[] getFilter(List<RGB> orderPixelWeight);
    
    @Override
    public ColorProcessor run() {

        // Create 8N Structuring Element.
        int[] rEight = {-1, -1, -1, 0, 0, 0, 1, 1, 1};
        int[] cEight = {-1, 0, 1, -1, 0, 1, -1, 0, 1};
        Pixel[] seEight = new Pixel[rEight.length];

        for (int i = 0; i < rEight.length; i++) {
            seEight[i] = new Pixel(rEight[i], cEight[i]);
        }

        int[] elementP;

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                elementP = order(seEight, new Pixel(x, y));
                restoredColProcessor.putPixel(x, y, elementP);
            }
        }
        
        show();

        String combination = "";
        long totalDecisiones = contadorDecisionComp[0] + contadorDecisionComp[1] + contadorDecisionComp[2];
        //System.out.printf("\n");
        
        for (int i = 0; i < channels.length; i++) {
            decisionComp[i] = (double)contadorDecisionComp[i]/(double)totalDecisiones;
            combination += component[order[i]];
            //System.out.printf("Orden (%s) Porc. de decisiÃ³n canal (%d) : (%f)\n", component[order[i]], i, decisionComp[i]);
        }
        
        return restoredColProcessor;
    }
    
}
