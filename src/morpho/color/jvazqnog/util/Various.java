package morpho.color.jvazqnog.util;

import morpho.color.jvazqnog.models.Pixel;

public class Various {
	
	
	/**
	 * Que es una función que usamos para que reciba la dimensión de un SE cuadrado y retorne 
	 * su vector de desplazamientos. Por ejemplo recibe 4 y retorna el vector de desplazamientos 
	 * para un elemento estructurante 4x4.
	 * Porque cada objeto Pixel es en realidad un par de coordenadas fila columna. 
	 * */
	public static Pixel[] getShiftArray(int dimensionSE){
	        
	        int centro = dimensionSE/2;
	        int indexFrom = centro * (-1);
	        int indexTo = dimensionSE - (centro + 1);
	        int cantidadDesplazamientos = (int)(Math.pow(dimensionSE, 2));
	        Pixel[] retorno = new Pixel[cantidadDesplazamientos];
	        int counter = 0;
	        for (int i = indexFrom; i <= indexTo; i++) {
	            for (int j = indexFrom; j <= indexTo; j++) {
	                retorno[counter] = new Pixel(i, j);
	                counter++;
	            }
	        }
	        
	        return retorno;
	 }
	
	
	
}
