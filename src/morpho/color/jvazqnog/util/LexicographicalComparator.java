/*
 * Tesis Arguello Balbuena
 * Derechos Reservados 2015 - 2016
 */
package morpho.color.jvazqnog.util;

import java.util.Comparator;

import morpho.color.jvazqnog.models.RGB;

/**
 *
 * @author daasalbion
 */
public class LexicographicalComparator implements Comparator<RGB>{
    //por defecto es el primer componente el mayor
    public int [] order = {0, 1, 2};
    //cantidad de veces que se opta por canal
    public long [] choseChannel = {0, 0, 0};

    public LexicographicalComparator() {
    }
    
    public LexicographicalComparator(int [] order) {
        this.order = order;
    }
    
    @Override
    public int compare(RGB o1, RGB o2) {
        //obtenemos los pixeles
        int [] color1 = o1.getRGB();
        int [] color2 = o2.getRGB();
        
        for (int i = 0; i < order.length; i++) {
            if ( color1[order[i]] < color2[order[i]] ) {
                choseChannel[i]++;
                return -1;
            }
            if ( color1[order[i]] > color2[order[i]] ) {
                choseChannel[i]++;
                return 1;
            }
        }

        return 0;
        
    }
    
}