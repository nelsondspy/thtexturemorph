/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.util;

import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.models.Window;
import ij.process.ColorProcessor;

/**
 *
 * @author Derlis Argüello
 */
public abstract class TesisRGBEntropyAbstract extends TesisRGBBasicAbstract{
    
    public TesisRGBEntropyAbstract(String methodName, String path, String extension, 
            ColorProcessor colProcessor, Pixel[] se, int xRoi, int yRoi) {
        super(methodName, path, extension, colProcessor, se, xRoi, yRoi);
    }
    
    @Override
    public float[] getRealWeight(Pixel p) {

        // VARIABLES
        Pixel pixel = new Pixel(0, 0);
        int cSize = channels.length;
        int numPixels = 0;
        float[] sum = {0, 0, 0};

        // ITERATE STRUCTURE ELEMENT
        for (Pixel se1 : se) {
            pixel.setX(p.getX() + se1.getX());
            pixel.setY(p.getY() + se1.getY());
            for (Window window : windowsList) {
                if (window.getRoi().contains(pixel.getX(), pixel.getY())) {
                    //si todavia no fue procesado, se hace
                    if (!window.getProcessed()) {
                        numPixels += window.getTotalPixels();
                        for (int j = 0; j < cSize; j++) {
                            sum[j] -= window.getxFxSum()[j];
                        }
                        window.setProcessed(true);
                    } 
                }
            }
        }

        // DIVIDE SUM BY NUMPIXELS
        //se obtiene la definicion completa de (6)
        for (int i = 0; i < channels.length; i++) {
            sum[i] /= numPixels;
        }
        return sum;
    } // end of getRegions();
    
    @Override
    public void setXfxSum(Window window){
        window.getRGBSumEntropy();
    }
   
}