/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.util;

import ij.process.ColorProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.models.Window;

/**
 *
 * @author Derlis Argüello
 */
public abstract class TesisRGBUniformityAbstract extends TesisRGBMeanAbstract{
    
    public TesisRGBUniformityAbstract(String methodName, String path, String extension, 
            ColorProcessor colProcessor, Pixel[] se, int xRoi, int yRoi) {
        super(methodName, path, extension, colProcessor, se, xRoi, yRoi);
    }
    
    @Override
    public void setXfxSum(Window window){
        window.getRGBSumUniformity();
    }
   
}