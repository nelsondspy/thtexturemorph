/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.util;

import morpho.color.jvazqnog.models.Metricas;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.models.PixelWeight;
import morpho.color.jvazqnog.models.Window;
import ij.gui.Roi;
import ij.process.ByteProcessor;
import ij.process.ColorProcessor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Derlis Argüello
 * @Nomenclatura: [Metodo][Orden][Filtro] 
 * @Ejemplo: [TesisRGB][Mean][Median]
 */
public abstract class TesisRGBBasicAbstract extends BasicAbstract{
    
    public List<Window> windowsList = new ArrayList<>();
    public long [] contadorDecisionComp = {0, 0, 0};
    public long contadorDecisionValorReducido;
    public double[] decisionComp = {0, 0, 0};
    public double decisionValorReducido = 0;
    public String [] component = {"R", "G", "B"};
    public ByteProcessor[] channels = new ByteProcessor[component.length]; // RGB channels
    public int xRoi = 2; // Roi coordinate x
    public int yRoi = 2; // Roi coordinate y

    public TesisRGBBasicAbstract(String methodName, String path, String extension, ColorProcessor colProcessor, 
            Pixel[] se, int xRoi, int yRoi) {
        super(methodName, path, extension, colProcessor, se);
        this.xRoi = xRoi;
        this.yRoi = yRoi;
        
        for (int i = 0; i < channels.length; i++) {
            //cargas los canales de la imagen por separado en channel
            this.channels[i] = colProcessor.getChannel(i + 1, channels[i]);
        }
    }
    
    // Se divide en ventanas la imagen
    public void setWeightList() {
        int roiWidth = (int)Math.ceil((double)width / (double)xRoi);
        int roiHeight = (int)Math.ceil((double)height / (double)yRoi);
        //variables
        Roi roi;
        Window window;
        int totalPixels;
        int roiWidthAux;
        int roiHeightAux;
        int xStart = 0, yStart = 0;
        // ITERATE THROUGH THE IMAGINARY MATRIX OF ROI'S
        for (int yr = 0; yr < yRoi; yr++) { // Y
            roiWidthAux = roiWidth;
            roiHeightAux = roiHeight;
            for (int xr = 0; xr < xRoi; xr++) { // X
                if(xStart + roiWidth >= width){
                    roiWidthAux = width - xStart - 1;
                }
                if(yStart + roiHeight >= height){
                    roiHeightAux = height - yStart - 1;
                }
                
                roi = new Roi(xStart, yStart, roiWidthAux, roiHeightAux);
                //System.out.println("[" + roi.toString() + "]");
                totalPixels = roiWidthAux * roiHeightAux;
                //aca tengo que setear como se van a crear los xFxSum
                window = new Window(roi, channels, totalPixels);
                setXfxSum(window);
                windowsList.add(window);
                xStart += roiWidthAux; // SHIFT STARTING X
            }
            xStart = 0;
            yStart += roiHeightAux; // SHIFT STARTING Y
        }
    }

    public abstract float[] getRealWeight(Pixel p);
    public abstract void setXfxSum(Window window);
   
    public int[] order(float[] weight, Pixel p) {
        int cLength = channels.length;
        int x, y;
        float t = 0.0f;
        int[] pixel;
        List<PixelWeight> orderPixelWeight = new ArrayList<>();
        PixelWeight pixelWeight;
        int[] filterP;

        for (Pixel SE1 : se) {
            x = p.getX() + SE1.getX();
            y = p.getY() + SE1.getY();
            //verificamos si esta en la ventana del elemento estructurante
            if (x > -1 && x < width && y > -1 && y < height) {
                pixel = new int[cLength];
                // Se carga el pixel y su valor T(p).
                //es decir se obtiene (7)
                for (int k = 0; k < cLength; k++) {
                    //se obtiene I_k
                    pixel[k] = channels[k].get(x, y);
                    t = t + weight[k] * pixel[k];
                }

                pixelWeight = new PixelWeight(pixel, t);
                orderPixelWeight.add(pixelWeight);
                t = 0;
            }
        }
        
        TesisRGBComparator comparator = new TesisRGBComparator();
        //ordenamos por peso
        Collections.sort(orderPixelWeight, comparator);
        
        for (int i = 0; i < cLength; i++) {
            contadorDecisionComp[i] += comparator.chooseChannel[i];
        }
        //los valores reducidos
        contadorDecisionValorReducido += comparator.valorReducido;
        //obtenemos el filtro
        filterP = getFilter(orderPixelWeight);

        return filterP;
    }
    //implementaciones de los filtros
    public int[] min(List<PixelWeight> orderPixelWeight) {
        int element = 0;
        return orderPixelWeight.get(element).getPixel();
    }
    
    public int[] max(List<PixelWeight> orderPixelWeight) {
        int element = orderPixelWeight.size() -  1;
        return orderPixelWeight.get(element).getPixel();
    }
    
    public int[] median(List<PixelWeight> orderPixelWeight) {
        int element = (int) Math.ceil(orderPixelWeight.size() / 2);
        return orderPixelWeight.get(element).getPixel();
    }
    //solicitar tipo de filtro
    public abstract int[] getFilter(List<PixelWeight> orderPixelWeight);
    
    @Override
    public ColorProcessor run() {
        int[] elementP;
        float[] realWeight;
        Pixel pixel;
        
        setWeightList();

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                pixel = new Pixel(x, y);
                realWeight = getRealWeight(pixel);
                elementP = order(realWeight, pixel);
                restoredColProcessor.putPixel(x, y, elementP);
                //reseteamos windowsList
                resetProcessed();
            }
        }
        
        show();
        
        long totalDecisiones = contadorDecisionComp[0] + contadorDecisionComp[1] + contadorDecisionComp[2] + contadorDecisionValorReducido;
        //System.out.printf("\n");

        for (int i = 0; i < channels.length; i++) {
            decisionComp[i] = (double)contadorDecisionComp[i]/(double)totalDecisiones;
            //System.out.printf("Orden (%s) Porc. de decisión canal (%d) : (%f)\n", component[i], i, decisionComp[i]);
        }
        
        decisionValorReducido = (double)contadorDecisionValorReducido/(double)totalDecisiones;
        //System.out.printf("Orden Porc. de decisión valor reducido: (%f)\n", decisionValorReducido);
        
        //long stopTime = System.currentTimeMillis();
        //long elapsedTime = stopTime - startTime;
        //System.out.println("ElapsedTime: " + elapsedTime);
        
        return restoredColProcessor; 
    }
    
    public void log(int indice, String noiseName, Logger logger, Metricas metricas){
        logger.info(indice + ", "  + xRoi + "x" + yRoi + ", " + noiseName + ", " + methodName + ", " + metricas.mae() + ", " + metricas.mse() + ", " + metricas.nmse() + ", " + decisionValorReducido + ", " + decisionComp[0] + ", " + decisionComp[1] + ", " + decisionComp[2]);
    }

    private void resetProcessed() {
        //se setean a no procesados por defecto
        for (Window window : windowsList) {
            window.setProcessed(false);
        }
    }
}