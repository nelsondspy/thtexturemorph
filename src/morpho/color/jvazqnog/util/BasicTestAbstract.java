/*
 * Tesis Arguello Balbuena
 * Derechos Reservados 2015 - 2016
 */
package morpho.color.jvazqnog.util;
import morpho.color.jvazqnog.models.Metricas;
import ij.plugin.PlugIn;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;


/**
 *
 * @author Derlis Argüello
 */
public abstract class BasicTestAbstract implements PlugIn{
    public static Logger logger;
    public int cantidadPruebas;
    public String testName;
    public String noiseName;
    //imgs path
    public String pathOriginalImg;
    public String basePathNoisyImg;
    public String pathRestoredImg;
    public String imgName;
    public Metricas metricas;

    public BasicTestAbstract(int cantidadPruebas, String testName, String noiseName, String pathOriginalImg, 
            String basePathNoisyImg, String pathRestoredImg, String imgName) {
        this.cantidadPruebas = cantidadPruebas;
        this.testName = testName;
        this.noiseName = noiseName;
        this.pathOriginalImg = pathOriginalImg;
        this.pathRestoredImg = pathRestoredImg;
        this.basePathNoisyImg = basePathNoisyImg;
        this.imgName = imgName;
        
        configurarLog();
    }

    private void configurarLog() {
        
        //Crear logger 
        String loggerName = testName;
        logger = Logger.getLogger(loggerName);
        String PATTERN = "%m%n";
        //creando el appender para el arhivo log
        DailyRollingFileAppender fileApp = new DailyRollingFileAppender();
        
        fileApp.setFile("logs/"+ loggerName+".csv");
        fileApp.setImmediateFlush(true);
        fileApp.setDatePattern("'.'yyyy-MM-dd'.log'");
        fileApp.setLayout(new PatternLayout(PATTERN));
        fileApp.activateOptions();
        
        logger.addAppender(fileApp);

        //creando el appender para el log de la consola
        ConsoleAppender consoleApp = new ConsoleAppender();
        consoleApp.setImmediateFlush(true);
        consoleApp.setLayout(new PatternLayout(PATTERN));
        consoleApp.activateOptions();
        
        logger.addAppender(consoleApp);
 
    }
    
    //public abstract void formatOut(Metricas metricas, T methodObject);
}
