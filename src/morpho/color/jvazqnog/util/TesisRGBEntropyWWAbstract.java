/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.util;

import morpho.color.jvazqnog.models.Pixel;
import ij.gui.Roi;
import ij.process.ColorProcessor;

/**
 *
 * @author Derlis Argüello
 */
public abstract class TesisRGBEntropyWWAbstract extends TesisRGBBasicAbstractWithoutWindows{

    public TesisRGBEntropyWWAbstract(String methodName, String path, String extension, ColorProcessor colProcessor, Pixel[] se) {
        super(methodName, path, extension, colProcessor, se);
    }

    @Override
    public float[] getRGBSum(Roi roi) {
        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        float[] sum = new float[cSize];
        int[][] channelHistogram = new int[cSize][hSize];
        int k;

        for (int i = 0; i < cSize; i++) {
            channels[i].setRoi(roi);
            channelHistogram[i] = channels[i].getHistogram();
        }

        for (int i = 0; i < hSize; i++) {
            for (k = 0; k < cSize; k++) {
                //se calcula el numerador de (6)
                if(channelHistogram[k][i] != 0){
                    sum[k] = (float) (sum[k] - (channelHistogram[k][i] * (Math.log(channelHistogram[k][i])/Math.log(2))));
                }
            }
        }

        return sum;
    }
 
}