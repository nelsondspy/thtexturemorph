/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.impl;

import morpho.color.jvazqnog.models.PixelWeight;
import ij.process.ColorProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.util.TesisRGBEntropyRAbstract;

import java.util.List;


/**
 *
 * @author Derlis Argüello
 * @Nomenclatura: [Metodo][Orden][Filtro] 
 * @Ejemplo: [TesisRGB][Sdeviation][Median]
 */
public class TesisRGBEntropyRMedian extends TesisRGBEntropyRAbstract{
    
    public TesisRGBEntropyRMedian(String methodName, String path, String extension, ColorProcessor colProcessor, 
            Pixel[] se, int xRoi, int yRoi) {
        super(methodName, path, extension, colProcessor, se, xRoi, yRoi);
    }
    
    @Override
    public void show() {
        //print();
        save();
    }

    @Override
    public int[] getFilter(List<PixelWeight> orderPixelWeight) {
        return median(orderPixelWeight);
    }

}