/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.impl.profe;

import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.models.PixelWeight;
import morpho.color.jvazqnog.util.TesisRGBMeanWWAbstract;
import ij.process.ColorProcessor;
import java.util.List;


/**
 *
 * @author Derlis Argüello
 * @Nomenclatura: [Metodo][Orden][Filtro] 
 * @Ejemplo: [TesisRGB][Mean][Median]
 */
public class TesisRGBMeanWWMax extends TesisRGBMeanWWAbstract{

    public TesisRGBMeanWWMax(String methodName, String path, String extension, ColorProcessor colProcessor, Pixel[] se) {
        super(methodName, path, extension, colProcessor, se);
    }
    
    @Override
    public void show() {
        save();
    }

    @Override
    public int[] getFilter(List<PixelWeight> orderPixelWeight) {
        return max(orderPixelWeight);
    }

}