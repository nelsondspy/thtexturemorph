/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.implespesoventana ;

import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.implespesoventana.PixelWeight;
import morpho.color.jvazqnog.implespesoventana.TesisComparator;
import ij.gui.Roi;
import ij.process.ColorProcessor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Derlis Argüello
 */
public abstract class TesisRGBBasicWWAbstract extends BasicFilterAbstract{

    public TesisRGBBasicWWAbstract(String filter, String noisyImgPath, String imgExtension, 
            ColorProcessor noisyColProcessor, Pixel[] se) {
        super(filter, noisyImgPath, imgExtension, noisyColProcessor, se);
    }
   
    public double[] order(float[] weight, Pixel p) {
        int cLength = channels.length;
        int x, y;
        double t = 0.0f;
        double[] pixel;
        List<PixelWeight> orderPixelWeight = new ArrayList<>();
        PixelWeight pixelWeight;
        double[] filterP;

        for (Pixel SE1 : se) {
            x = p.getX() + SE1.getX();
            y = p.getY() + SE1.getY();
            //verificamos si esta en la ventana del elemento estructurante
            if (x > -1 && x < width && y > -1 && y < height) {
                pixel = new double[cLength];
                // Se carga el pixel y su valor T(p).
                //es decir se obtiene (7)
                for (int k = 0; k < cLength; k++) {
                    //se obtiene I_k
                    pixel[k] = channels[k].get(x, y);
                    t = t + weight[k] * pixel[k];
                }

                pixelWeight = new PixelWeight(pixel, t);
                orderPixelWeight.add(pixelWeight);
                t = 0;
            }
        }
        
        TesisComparator comparator = new TesisComparator(3);
        //ordenamos por peso
        Collections.sort(orderPixelWeight, comparator);
        
        for (int i = 0; i < cLength; i++) {
            decisionByCompCounter[i] += comparator.chooseChannel[i];
        }
        //los valores reducidos
        reducedValueCounter += comparator.valorReducido;
        //obtenemos el filtro
        filterP = getFilter(orderPixelWeight);

        return filterP;
    }
    //implementaciones de los filtros
    public double[] min(List<PixelWeight> orderPixelWeight) {
        int element = 0;
        return orderPixelWeight.get(element).getPixel();
    }
    
    public double[] max(List<PixelWeight> orderPixelWeight) {
        int element = orderPixelWeight.size() -  1;
        return orderPixelWeight.get(element).getPixel();
    }
    
    public double[] median(List<PixelWeight> orderPixelWeight) {
        int element = (int) Math.ceil(orderPixelWeight.size() / 2);
        return orderPixelWeight.get(element).getPixel();
    }
    
    @Override
    public ColorProcessor run() {
        
        double[] elementP;
        float[] realWeight;
        int[] elementPInt;
        Pixel pixel;

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                pixel = new Pixel(x, y);
                realWeight = getRealWeight(pixel);
                elementP = order(realWeight, pixel);
                elementPInt = new int[elementP.length];
                for (int i = 0; i < elementP.length; i++) {
                    elementPInt[i] = (int)elementP[i];
                }
                restoredColProcessor.putPixel(x, y, elementPInt);
            }
        }
        
        show();
        
        long totalDecisiones = decisionByCompCounter[0] + decisionByCompCounter[1] + decisionByCompCounter[2] + reducedValueCounter;
        //System.out.printf("\n");

        for (int i = 0; i < channels.length; i++) {
            decisionByComp[i] = (double)decisionByCompCounter[i]/(double)totalDecisiones;
            //System.out.printf("Orden (%s) Porc. de decisión canal (%d) : (%f)\n", component[i], i, decisionComp[i]);
        }
        
        reducedValue = (double)reducedValueCounter/(double)totalDecisiones;
        //System.out.printf("Orden Porc. de decisión valor reducido: (%f)\n", decisionValorReducido);
        
        return restoredColProcessor; 
    }
    
    //solicitar tipo de filtro
    public double[] getFilter(List<PixelWeight> orderPixelWeight){
        switch(filter){
            case "Min":
                return min(orderPixelWeight);
            case "Max":
                return max(orderPixelWeight);
            case "Median":
                return median(orderPixelWeight);
            default:
                return null;
        }
    }
    
    public float[] getRealWeight(Pixel p) {
        Roi roi;
        float[] sum = new float[channels.length];

        // SET ALL SUMS TO 0
        for (int i = 0; i < channels.length; i++) {
            sum[i] = 0;
        }

        // ITERATE STRUCTURE ELEMENT
        roi = new Roi(p.getX() - 1, p.getY() - 1, se.length, se.length);
        sum = setXfxSum(roi);

        return sum;
    } // end of getRegions();
    
    //abstract methods, depends of the order
    public abstract float[] setXfxSum(Roi roi);
    
}