/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.implespesoventana;


import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.models.Window;
import ij.process.ColorProcessor;
import java.util.List;

/**
 *
 * @author Derlis Argüello
 * @Nomenclatura: [Metodo][Orden] 
 * @Ejemplo: [TesisRGB][Mean]
 */
public abstract class TesisRGBModeAbstract extends TesisRGBBasicAbstract{

    public TesisRGBModeAbstract(String filter, String noisyImgPath, String imgExtension, ColorProcessor noisyColProcessor, Pixel[] se, List<Integer> windowsRoiList) {
        super(filter, noisyImgPath, imgExtension, noisyColProcessor, se, windowsRoiList);
    }

    @Override
    public float[] getRealWeight(Pixel p) {

        // VARIABLES
        Pixel pixel = new Pixel(0, 0);
        int numPixels = 0;
        int cSize = channels.length;
        float[] mode = {0, 0, 0};
            
        // ITERATE STRUCTURE ELEMENT
        for (Pixel se1 : se) {
            pixel.setX(p.getX() + se1.getX());
            pixel.setY(p.getY() + se1.getY());
            for (Window window : windowsList) {
                if (window.getRoi().contains(pixel.getX(), pixel.getY())) {
                    //si todavia no fue procesado, se hace
                    if (!window.getProcessed()) {
                        numPixels += window.getTotalPixels();
                        for (int j = 0; j < cSize; j++) {
                            mode[j] += window.getxFxSum()[j];
                        }
                        window.setProcessed(true);
                    } 
                }
            }
        }
        
        resetProcessed();
        
        for (int channel = 0; channel < cSize; channel++) {
            //se obtiene el mode 
            mode[channel] /= numPixels;
        }

        return mode;
    } 
    
    @Override
    public void setXfxSum(Window window){
        window.getMode();
    }
    
}