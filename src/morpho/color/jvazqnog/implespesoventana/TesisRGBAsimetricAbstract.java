/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.implespesoventana ;

import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.models.Window;
import ij.process.ColorProcessor;
import java.util.List;

/**
 *
 * @author Derlis Argüello
 */
public abstract class TesisRGBAsimetricAbstract extends TesisRGBMeanAbstract{

    public TesisRGBAsimetricAbstract(String filter, String noisyImgPath, String imgExtension, 
            ColorProcessor noisyColProcessor, Pixel[] se, List<Integer> windowsRoiList) {
        super(filter, noisyImgPath, imgExtension, noisyColProcessor, se, windowsRoiList);
    }
    
    @Override
    public float[] getRealWeight(Pixel p) {
        
        float[] mean = super.getRealWeight(p);
        int numPixels = 0;
        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        float[] asimetric = {0, 0, 0};
            
        for (Window window : windowsList) {
            if (window.getRoi().contains(p.getX(), p.getY())) {
                numPixels += window.getTotalPixels();
                for (int channel = 0; channel < cSize; channel++) {
                    for (int intensity = 0; intensity < hSize; intensity++) {
                        asimetric[channel] += Math.abs( Math.pow( ( (intensity - mean[channel]) ), 3) )
                                *(window.getChannelHistogram()[channel][intensity]);
                    }
                }
                break;
            }
        }
       
        return asimetric;
    } 
   
}