/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.implespesoventana ; 

import morpho.color.jvazqnog.models.Pixel;
import ij.gui.Roi;
import ij.process.ColorProcessor;

/**
 *
 * @author Derlis Argüello
 */
public abstract class TesisRGBSdeviationWWAbstract extends TesisRGBMeanWWAbstract{

    public TesisRGBSdeviationWWAbstract(String filter, String noisyImgPath, String imgExtension, 
            ColorProcessor noisyColProcessor, Pixel[] se) {
        super(filter, noisyImgPath, imgExtension, noisyColProcessor, se);
        
    }

    @Override
    public float[] setXfxSum(Roi roi) {
        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        float[] sum = new float[cSize];
        int[][] channelHistogram = new int[cSize][hSize];

        for (int i = 0; i < cSize; i++) {
            channels[i].setRoi(roi);
            channelHistogram[i] = channels[i].getHistogram();
        }

        for (int i = 0; i < hSize; i++) {
            for (int k = 0; k < cSize; k++) {
                //se calcula el numerador de (6)
                sum[k] = (sum[k] + (i) * channelHistogram[k][i]);
            }
        }

        return sum;
    }
    
    @Override
    public float[] getRealWeight(Pixel p) {
        Roi roi;
        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        int[][] channelHistogram = new int[cSize][hSize];
        float[] mean = {0,0,0};
        float[] sDeviation = {0,0,0};
        int seLength = se.length;
        int n = seLength*seLength;

        // ITERATE STRUCTURE ELEMENT
        roi = new Roi(p.getX() - 1, p.getY() - 1, seLength, seLength);
        mean = setXfxSum(roi);
        for (int i = 0; i < channels.length; i++) {
            mean[i] = mean[i] / (float)n;
            channels[i].setRoi(roi);
            channelHistogram[i] = channels[i].getHistogram();
        }
        
        for (int intensity = 0; intensity < hSize; intensity++) {
            for (int channel = 0; channel < cSize; channel++) {
                sDeviation[channel] += Math.pow( ( (intensity - mean[channel]) ), 2)
                        *(channelHistogram[channel][intensity]);
            }
        }
        
        for (int channel = 0; channel < cSize; channel++) {
            sDeviation[channel] =  (float)Math.sqrt(sDeviation[channel]/(float)9);
        }

        return sDeviation;
    } // end of getRegions();
    
}