/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.implespesoventana;

import morpho.color.jvazqnog.models.Pixel;
import ij.ImagePlus;
import ij.io.FileSaver;
import ij.process.ByteProcessor;
import ij.process.ColorProcessor;

/**
 *
 * @author Derlis Argüello
 */
public abstract class BasicFilterAbstract {
    
    public String filterName;
    public String filter;
    public String noisyImgPath;
    public String imgExtension;
    public ColorProcessor noisyColProcessor;
    public ColorProcessor restoredColProcessor;
    public int width;
    public int height;
    public Pixel[] se;
    
    public String [] components = {"R", "G", "B"};
    //defaultOrder
    public int[] componentsOrder = {0, 1, 2};
    public ByteProcessor[] channels = new ByteProcessor[3];

    public double[] decisionByComp = {0, 0, 0};
    public long [] decisionByCompCounter = {0, 0, 0};
    
    public double reducedValue = 0;
    public long reducedValueCounter = 0;

    public BasicFilterAbstract(String filter, String noisyImgPath, String imgExtension, 
            ColorProcessor noisyColProcessor, Pixel[] se) {
        this.filter = filter;
        this.noisyImgPath = noisyImgPath;
        this.imgExtension = imgExtension;
        this.noisyColProcessor = noisyColProcessor;
        this.se = se;
        
        //set default values
        this.height = noisyColProcessor.getHeight();
        this.width = noisyColProcessor.getWidth();
        this.restoredColProcessor = new ColorProcessor(width, height);
        
        for (int i = 0; i < channels.length; i++) {
            //load channel for each component 
            this.channels[i] = noisyColProcessor.getChannel(i + 1, channels[i]);
        }
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName + filter;
    }
    
    /**
     * debug
     */
    public void print(){
        ImagePlus imgPlus = new ImagePlus(filterName, restoredColProcessor);
        imgPlus.show();
    }
    
    /**
     * for save restored image in disk
     */
    public void save(){
        ImagePlus imgPlus = new ImagePlus(filterName, restoredColProcessor);
        if (imgExtension.equalsIgnoreCase("png")) {
            new FileSaver(imgPlus).saveAsPng(noisyImgPath + filterName + "." + imgExtension);
        }else if (imgExtension.equalsIgnoreCase("jpg")){
            new FileSaver(imgPlus).saveAsJpeg(noisyImgPath + filterName + "." +  imgExtension);
        }
        else if (imgExtension.equalsIgnoreCase("bmp")){
            new FileSaver(imgPlus).saveAsBmp( noisyImgPath + filterName + "." +  imgExtension );
        }
    }
    //abstracts methods
    public abstract ColorProcessor run();
    public void show(){
        save();
    };
    
}