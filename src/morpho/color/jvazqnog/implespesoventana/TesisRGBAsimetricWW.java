/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.implespesoventana ;

import morpho.color.jvazqnog.models.Pixel;
import static morpho.color.jvazqnog.implespesoventana.TestConstants.Filters.TesisRGB.SinVentanas.TESIS_RGB_ASIMETRIC_WW;
import morpho.color.jvazqnog.implespesoventana.TesisRGBAsimetricWWAbstract;
import ij.process.ColorProcessor;


/**
 *
 * @author Derlis Argüello
 * @Nomenclatura: [Metodo][Orden]
 * @Ejemplo: [TesisRGB][Mean]
 */
public class TesisRGBAsimetricWW extends TesisRGBAsimetricWWAbstract{

    public TesisRGBAsimetricWW(String filter, String noisyImgPath, String imgExtension, 
            ColorProcessor noisyColProcessor, Pixel[] se) {
        super(filter, noisyImgPath, imgExtension, noisyColProcessor, se);
        setFilterName(TESIS_RGB_ASIMETRIC_WW);
    }

    @Override
    public void show() {
        //print();
        //save();
        super.show();
    }

}