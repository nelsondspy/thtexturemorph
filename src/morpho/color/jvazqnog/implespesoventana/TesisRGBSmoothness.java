/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.implespesoventana ;

import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.implespesoventana.TesisRGBSmoothnessAbstract;
import ij.process.ColorProcessor;
import java.util.List;


/**
 *
 * @author Derlis Argüello
 * @Nomenclatura: [Metodo][Orden]
 * @Ejemplo: [TesisRGB][Mean]
 */
public class TesisRGBSmoothness extends TesisRGBSmoothnessAbstract{

    public TesisRGBSmoothness(String filter, String noisyImgPath, String imgExtension, 
            ColorProcessor noisyColProcessor, Pixel[] se, List<Integer> windowsRoiList) {
        super(filter, noisyImgPath, imgExtension, noisyColProcessor, se, windowsRoiList);
        setFilterName("TesisRGBSmoothness");
    }
    
    @Override
    public void show() {
        //print();
        //save();
        super.show();
    }

}