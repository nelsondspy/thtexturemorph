/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.implespesoventana ;

import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.models.Window;
import ij.process.ColorProcessor;
import java.util.List;

/**
 *
 * @author Derlis Argüello
 * @Nomenclatura: [Metodo][Orden] 
 * @Ejemplo: [TesisRGB][Mean]
 */
public abstract class TesisRGBEntropyAbstract extends TesisRGBBasicAbstract{

    public TesisRGBEntropyAbstract(String filter, String noisyImgPath, 
            String imgExtension, ColorProcessor noisyColProcessor, Pixel[] se, List<Integer> windowsRoiList) {
        super(filter, noisyImgPath, imgExtension, noisyColProcessor, se, windowsRoiList);
    }

    @Override
    public float[] getRealWeight(Pixel p) {

        int cSize = channels.length;
        int numPixels = 0;
        float[] sum = {0, 0, 0};

        for (Window window : windowsList) {
            if (window.getRoi().contains(p.getX(), p.getY())) {
                numPixels += window.getTotalPixels();
                for (int j = 0; j < cSize; j++) {
                    sum[j] -= window.getxFxSum()[j];
                }
                break;
            }
        }

        // DIVIDE SUM BY NUMPIXELS
        //se obtiene la definicion completa de (6)
        for (int i = 0; i < channels.length; i++) {
            sum[i] /= numPixels;
        }
        return sum;
    } // end of getRegions();
    
    @Override
    public void setXfxSum(Window window){
        window.getRGBSumEntropy();
    }
    
}