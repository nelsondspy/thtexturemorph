/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.implespesoventana ;

import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.models.Window;
import ij.process.ColorProcessor;
import java.util.List;

/**
 *
 * @author Derlis Argüello
 * @Nomenclatura: [Metodo][Orden] 
 * @Ejemplo: [TesisRGB][Mean]
 */
public abstract class TesisRGBMeanAbstract extends TesisRGBBasicAbstract{

    public TesisRGBMeanAbstract(String filter, String noisyImgPath, 
            String imgExtension, ColorProcessor noisyColProcessor, Pixel[] se, List<Integer> windowsRoiList) {
        super(filter, noisyImgPath, imgExtension, noisyColProcessor, se, windowsRoiList);
    }

    @Override
    public float[] getRealWeight(Pixel pixel) {

        int numPixels = 0;
        int cSize = channels.length;
        float[] mean = {0, 0, 0};
            
        for (Window window : windowsList) {
            if (window.getRoi().contains(pixel.getX(), pixel.getY())) {
                numPixels += window.getTotalPixels();
                for (int channel = 0; channel < cSize; channel++) {
                    mean[channel] += window.getxFxSum()[channel];
                }
                break;
            } 
        }
        
        for (int channel = 0; channel < cSize; channel++) {
            mean[channel] /= numPixels;
        }
        
        return mean;
    } 
    
    @Override
    public void setXfxSum(Window window){
        window.setChannelHistogram();
    }
}