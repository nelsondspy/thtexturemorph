/*
 * Tesis Arguello Balbuena
 * Derechos Reservados 2015
 */
package morpho.color.jvazqnog.implespesoventana ;

/**
 *
 * @author daasalbion
 */
public class PixelWeight {
    private double[] pixel;
    private double weight;

    public PixelWeight(double[] pixel, double weight) {
        this.pixel = pixel;
        this.weight = weight;
    }

    public double[] getPixel() {
        return pixel;
    }

    public void setPixel(double[] pixel) {
        this.pixel = pixel;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "PixelWeight{" + "pixel=" + pixel + ", weight=" + weight + '}';
    }

}