/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.implespesoventana;

import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.models.PixelWeight;
import morpho.color.jvazqnog.implespesoventana.RoiLambda;
import morpho.color.jvazqnog.models.Window;
import morpho.color.jvazqnog.implespesoventana.TesisRGBComparator;
import ij.gui.Roi;
import ij.process.ColorProcessor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Derlis Argüello
 */
public abstract class TesisRGBBasicAbstract extends BasicFilterAbstract{
    
    public List<Window> windowsList = new ArrayList<>();
    public List<RoiLambda> roiLambdaList = new ArrayList<>();
    public List<Integer> windowsRoiList;
    public float[] lambdaFinal = {0, 0, 0};
    
    public TesisRGBBasicAbstract(String filter, String noisyImgPath, 
            String imgExtension, ColorProcessor noisyColProcessor, Pixel[] se, List<Integer> windowsRoiList) {
        super(filter, noisyImgPath, imgExtension, noisyColProcessor, se);
        this.windowsRoiList = windowsRoiList;
    }
    
    @Override
    public ColorProcessor run() {
        int[] elementP;
        Pixel pixel;
        
        setWeightList();

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                pixel = new Pixel(x, y);
                elementP = order(pixel);
                restoredColProcessor.putPixel(x, y, elementP);
            }
        }
        
        show();
        
        long totalDecisiones = decisionByCompCounter[0] + decisionByCompCounter[1] + decisionByCompCounter[2] + reducedValueCounter;
        //System.out.printf("\n");

        for (int i = 0; i < channels.length; i++) {
            decisionByComp[i] = (double)decisionByCompCounter[i]/(double)totalDecisiones;
            //System.out.printf("Orden (%s) Porc. de decisión canal (%d) : (%f)\n", component[i], i, decisionByComp[i]);
        }
        
        reducedValue = (double)reducedValueCounter/(double)totalDecisiones;
        //System.out.printf("Orden Porc. de decisión valor reducido: (%f)\n", reducedValue);
        
        return restoredColProcessor; 
    }
    
    // Se divide en ventanas la imagen
    public void setWeightList() {
        //variables
        Roi roi;
        Window window;
        int totalPixels;
        int roiWidthAux;
        int roiHeightAux;
        int xStart = 0, yStart = 0;
        int roiWidth;
        int roiHeight;
        
        for (Integer wRoi: windowsRoiList){
            roiWidth = (int)Math.ceil((double)width / (double)wRoi);
            roiHeight = (int)Math.ceil((double)height / (double)wRoi);
            for (int yr = 0; yr < wRoi; yr++) { // Y
                roiWidthAux = roiWidth;
                roiHeightAux = roiHeight;
                for (int xr = 0; xr < wRoi; xr++) { // X
                    if(xStart + roiWidth > width){
                        roiWidthAux = width - xStart - 1;
                    }
                    if(yStart + roiHeight > height){
                        roiHeightAux = height - yStart - 1;
                    }
                    roi = new Roi(xStart, yStart, roiWidthAux, roiHeightAux);
                    //System.out.println("[" + roi.toString() + "]");
                    totalPixels = roiWidthAux * roiHeightAux;
                    //aca tengo que setear como se van a crear los xFxSum
                    window = new Window(roi, channels, totalPixels);
                    setXfxSum(window);
                    //System.out.printf("Rois: {%s}\n", window.getRoi().toString());
                    windowsList.add(window);
                    xStart += roiWidthAux; // SHIFT STARTING X
                }
                xStart = 0;
                yStart += roiHeightAux; // SHIFT STARTING Y
            }
            
            setLambdaSum(wRoi);
            windowsList.removeAll(windowsList);
            xStart = 0;
            yStart = 0;
        }
        
        for ( RoiLambda roiLambda : roiLambdaList ) {
            for (int i = 0; i < roiLambda.getLambda().length; i++){
                lambdaFinal[i] += roiLambda.getLambda()[i];
                lambdaFinal[i] /= windowsRoiList.size();
            }  
        }
    }
    
    public int[] order(Pixel p) {
        int cLength = channels.length;
        int x, y;
        float t = 0.0f;
        int[] pixel;
        List<PixelWeight> orderPixelWeight = new ArrayList<>();
        PixelWeight pixelWeight;
        int[] filterP;

        for (Pixel SE : se) {
            x = p.getX() + SE.getX();
            y = p.getY() + SE.getY();
            //verificamos si esta en la ventana del elemento estructurante
            if (x > -1 && x < width && y > -1 && y < height) {
                pixel = new int[cLength];
                // Se carga el pixel y su valor T(p).
                //es decir se obtiene (7)
                for (int k = 0; k < cLength; k++) {
                    //se obtiene I_k
                    pixel[k] = channels[k].get(x, y);
                    t = t + lambdaFinal[k] * pixel[k];
                }

                pixelWeight = new PixelWeight(pixel, t);
                orderPixelWeight.add(pixelWeight);
                t = 0;
            }
        }
        
        TesisRGBComparator comparator = new TesisRGBComparator();
        //ordenamos por peso
        Collections.sort(orderPixelWeight, comparator);
        
        for (int i = 0; i < cLength; i++) {
            decisionByCompCounter[i] += comparator.chooseChannel[i];
        }
        //los valores reducidos
        reducedValueCounter += comparator.valorReducido;
        //obtenemos el filtro
        filterP = getFilter(orderPixelWeight);

        return filterP;
    }
    
    public void resetProcessed() {
        //se setean a no procesados por defecto
        for (Window window : windowsList) {
            window.setProcessed(false);
        }
    }
    
    //implementaciones de los filtros
    public int[] min(List<PixelWeight> orderPixelWeight) {
        int element = 0;
        return orderPixelWeight.get(element).getPixel();
    }
    
    public int[] max(List<PixelWeight> orderPixelWeight) {
        int element = orderPixelWeight.size() -  1;
        return orderPixelWeight.get(element).getPixel();
    }
    
    public int[] median(List<PixelWeight> orderPixelWeight) {
        int element = (int) Math.ceil(orderPixelWeight.size() / 2);
        return orderPixelWeight.get(element).getPixel();
    }
    
    //solicitar tipo de filtro
    public int[] getFilter(List<PixelWeight> orderPixelWeight){
        switch(filter){
            case "Min":
                return min(orderPixelWeight);
            case "Max":
                return max(orderPixelWeight);
            case "Median":
                return median(orderPixelWeight);
            default:
                return null;
        }
    }
    
    //abstract methods
    public abstract float[] getRealWeight(Pixel p);
    public abstract void setXfxSum(Window window);

    private void setLambdaSum(int wRoi) {
        Pixel p;
        int cLength = channels.length;
        float [] realWeight;
        double [] lambda = {0, 0, 0};
        RoiLambda roiLambda;
        
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                p = new Pixel(x, y);
                realWeight = getRealWeight(p);
                for (int k = 0; k < cLength; k++) {
                    lambda[k] = lambda[k] + realWeight[k];
                }
            }
        }
               
        //promediamos lambda
        for (int j = 0; j < lambda.length; j++) {
            lambda[j] /= Math.pow(2, wRoi);
        }
        
        roiLambda = new RoiLambda(wRoi, lambda);
        roiLambdaList.add(roiLambda);
    }
}