/*
 * Tesis Arguello Balbuena
 * Derechos Reservados 2015 - 2016
 */
package morpho.color.jvazqnog.implespesoventana ;

//import morpho.color.jvazqnog.models.PixelWeight;
import morpho.color.jvazqnog.implespesoventana.PixelWeight ; 
import java.util.Comparator;

/**
 *
 * @author daasalbion
 */
public class TesisComparator implements Comparator<PixelWeight>{
    
    //cantidad de veces que se opta por canal
    public long [] chooseChannel;
    public long valorReducido = 0;
    
    public TesisComparator() {
    }
    
    public TesisComparator(int channelsSize){
        chooseChannel = new long[channelsSize];
    }

    @Override
    public int compare(PixelWeight o1, PixelWeight o2) {
        if (o1.getWeight() == o2.getWeight()) {
            //pesos iguales, se va a decidir en el lexicografico
            double [] color1 = (double[])o1.getPixel();
            double [] color2 = (double[])o2.getPixel();

            for (int i = 0; i < color2.length; i++) {
                if ( color1[i] < color2[i] ) {
                    chooseChannel[i]++;
                    return -1;
                } else if ( color1[i] > color2[i] ) {
                    chooseChannel[i]++;
                    return 1;
                }
            }
            return 0;
        }else if ( o1.getWeight() < o2.getWeight() ) {
            valorReducido++;
            return -1;
        } else {
            valorReducido++;
            return  1;
        }
    }
    
}
