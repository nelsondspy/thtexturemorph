/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.implespesoventana;


import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.implespesoventana.TesisRGBMode;
import morpho.color.jvazqnog.implespesoventana.BasicFilterAbstract;
import ij.ImagePlus;
import ij.plugin.PlugIn;
import ij.process.ColorProcessor;
import java.util.Arrays;

/**
 *
 * @author Derlis Argüello
 */
public class TesisRGBModePlugin implements PlugIn{
    
    @Override
    public void run(String string) {
        
        String basePath = "C:\\images\\lena\\errorImage.png";
        int sLength = basePath.length();
        String extension = basePath.substring(sLength-3, sLength);
        //imagen original
        ImagePlus imgOriginal = new ImagePlus( basePath );
        ColorProcessor colProcessor = (ColorProcessor) imgOriginal.getProcessor();
        int xRoi = 10;
        int yRoi = 10;
        
        // Create 8N Structuring Element.
        int[] rEight = {-1, -1, -1, 0, 0, 0, 1, 1, 1};
        int[] cEight = {-1, 0, 1, -1, 0, 1, -1, 0, 1};
        Pixel[] seEight = new Pixel[rEight.length];

        for (int i = 0; i < rEight.length; i++) {
            seEight[i] = new Pixel(rEight[i], cEight[i]);
        }
        
        BasicFilterAbstract tesisRGB;
        
        
        tesisRGB = new TesisRGBMode("Median", basePath, 
                extension, colProcessor, seEight,  Arrays.asList(2, 4, 8));
        
        tesisRGB.run();
        
        tesisRGB = new TesisRGBMode("Max", basePath, 
                extension, colProcessor, seEight,  Arrays.asList(2, 4, 8));
        
        tesisRGB.run();
        
        tesisRGB = new TesisRGBMode("Min", basePath, 
                extension, colProcessor, seEight,  Arrays.asList(2, 4, 8));
        
        tesisRGB.run();
    }
}
