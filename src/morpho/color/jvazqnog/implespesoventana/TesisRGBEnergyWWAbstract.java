
package morpho.color.jvazqnog.implespesoventana; 


import morpho.color.jvazqnog.models.Pixel;
import ij.gui.Roi;
import ij.process.ColorProcessor;

/**
 *
 * @author Derlis Argüello
 */
public abstract class TesisRGBEnergyWWAbstract extends TesisRGBBasicWWAbstract{

    public TesisRGBEnergyWWAbstract(String filter, String noisyImgPath, String imgExtension, 
            ColorProcessor noisyColProcessor, Pixel[] se) {
        super(filter, noisyImgPath, imgExtension, noisyColProcessor, se);
        
    }

    @Override
    public float[] setXfxSum(Roi roi) {
        
        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        int[][] channelHistogram = new int[cSize][hSize];
        int n = se.length*se.length;
        float [] sum = {0, 0, 0};

        for (int i = 0; i < cSize; i++) {
            channels[i].setRoi(roi);
            channelHistogram[i] = channels[i].getHistogram();
        }

        for (int i = 0; i < hSize; i++) {
            for (int k = 0; k < cSize; k++) {
                //se calcula el numerador de (6)
                double p = ((double)channelHistogram[k][i])/(double)n;
                sum[k] += Math.pow(p, 2);
            }
        }
        
        return sum;
    }
    
}