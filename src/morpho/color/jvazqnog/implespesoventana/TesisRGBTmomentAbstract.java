/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.implespesoventana ;

import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.models.Window;
import ij.process.ColorProcessor;
import java.util.List;

/**
 *
 * @author Derlis Argüello
 * @Nomenclatura: [Metodo][Orden] 
 * @Ejemplo: [TesisRGB][Mean]
 */
public abstract class TesisRGBTmomentAbstract extends TesisRGBMeanAbstract{

    public TesisRGBTmomentAbstract(String filter, String noisyImgPath, 
            String imgExtension, ColorProcessor noisyColProcessor, Pixel[] se, List<Integer> windowsRoiList) {
        super(filter, noisyImgPath, imgExtension, noisyColProcessor, se, windowsRoiList);
    }

    @Override
    public float[] getRealWeight(Pixel p) {

        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        float[] tMoment = {0, 0, 0};
        float[] mean = super.getRealWeight(p);

        for (Window window : windowsList) {
            if (window.getRoi().contains(p.getX(), p.getY())) {
                for (int channel = 0; channel < cSize; channel++) {
                    for (int intensity = 0; intensity < hSize; intensity++) {
                        tMoment[channel] += Math.pow( ( (intensity - mean[channel]) ), 3)
                                *(window.getChannelHistogram()[channel][intensity]);
                    }
                }
                break;
            }
        }
        
        return tMoment;
    }
    
}