/*
 * Tesis Arguello Balbuena
 * Derechos Reservados 2015 - 2016
 */
package morpho.color.jvazqnog.implespesoventana ;

import morpho.color.jvazqnog.models.Pixel;
import static morpho.color.jvazqnog.implespesoventana.TestConstants.Filters.TesisRGB.ConVentanas.TESIS_RGB_ENERGY;
import morpho.color.jvazqnog.implespesoventana.TesisRGBEnergyAbstract;
import ij.process.ColorProcessor;
import java.util.List;

/**
 *
 * @author Derlis Argüello
 */
public class TesisRGBEnergy extends TesisRGBEnergyAbstract{
    
    public TesisRGBEnergy(String filter, String noisyImgPath, String imgExtension, 
            ColorProcessor noisyColProcessor, Pixel[] se, List<Integer> windowsRoiList) {
        super(filter, noisyImgPath, imgExtension, noisyColProcessor, se, windowsRoiList);
        setFilterName(TESIS_RGB_ENERGY);
    }
    
    @Override
    public void show() {
        //print();
        //save();
        super.show();
    }
}
