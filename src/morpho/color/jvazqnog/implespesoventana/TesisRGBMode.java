/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.implespesoventana;


import morpho.color.jvazqnog.models.Pixel;
import static morpho.color.jvazqnog.implespesoventana.TestConstants.Filters.TesisRGB.ConVentanas.TESIS_RGB_MODE;
import morpho.color.jvazqnog.implespesoventana.TesisRGBModeAbstract;
import ij.process.ColorProcessor;
import java.util.List;


/**
 *
 * @author Derlis Argüello
 * @Nomenclatura: [Metodo][Orden]
 * @Ejemplo: [TesisRGB][Mean]
 */
public class TesisRGBMode extends TesisRGBModeAbstract{

    public TesisRGBMode(String filter, String noisyImgPath, String imgExtension, ColorProcessor noisyColProcessor, Pixel[] se, List<Integer> windowsRoiList) {
        super(filter, noisyImgPath, imgExtension, noisyColProcessor, se, windowsRoiList);
        setFilterName(TESIS_RGB_MODE);
    }
    
    @Override
    public void show() {
        //print();
        //save();
        super.show();
    }

}