/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.implespesoventana ;


import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.implespesoventana.TesisRGBSmoothnessWWAbstract;
import ij.process.ColorProcessor;


/**
 *
 * @author Derlis Argüello
 * @Nomenclatura: [Metodo][Orden]
 * @Ejemplo: [TesisRGB][Mean]
 */
public class TesisRGBSmoothnessWW extends TesisRGBSmoothnessWWAbstract{

    public TesisRGBSmoothnessWW(String filter, String noisyImgPath, String imgExtension, 
            ColorProcessor noisyColProcessor, Pixel[] se) {
        super(filter, noisyImgPath, imgExtension, noisyColProcessor, se);
        setFilterName("TesisRGBSmoothnessWW");
    }

    @Override
    public void show() {
        //print();
        //save();
        super.show();
    }

}