/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.implespesoventana ;

import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.models.Window;
import ij.process.ColorProcessor;
import java.util.List;

/**
 *
 * @author Derlis Argüello
 */
public abstract class TesisRGBSmoothnessAbstract extends TesisRGBSdeviationAbstract{

    public TesisRGBSmoothnessAbstract(String filter, String noisyImgPath, String imgExtension, 
            ColorProcessor noisyColProcessor, Pixel[] se, List<Integer> windowsRoiList) {
        super(filter, noisyImgPath, imgExtension, noisyColProcessor, se, windowsRoiList);
    }
    
    @Override
    public float[] getRealWeight(Pixel p) {

        // VARIABLES
        int numPixels = 0;
        int cSize = channels.length;
        float[] smoothness = {0, 0, 0};
        
        float[] sDeviation = super.getRealWeight(p);
            
        for (Window window : windowsList) {
            if (window.getRoi().contains(p.getX(), p.getY())) {
                numPixels += window.getTotalPixels();
                break;
            }
        }

        for (int channel = 0; channel < cSize; channel++) {
            sDeviation[channel] =  sDeviation[channel]/numPixels;
            smoothness[channel] = 1 - 1/(1 + sDeviation[channel]);
        }
        
        return smoothness;
    } 
}
