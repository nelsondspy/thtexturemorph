/*
 * Tesis Arguello Balbuena
 * Derechos Reservados 2015 - 2016
 */
package morpho.color.jvazqnog.implespesoventana;

/**
 *
 * @author Derlis Argüello
 */
public class RoiLambda {
    private int wRoi;
    private double[] lambda;

    public RoiLambda(int wRoi, double[] lambda) {
        this.wRoi = wRoi;
        this.lambda = lambda;
    }

    public int getwRoi() {
        return wRoi;
    }

    public void setwRoi(int wRoi) {
        this.wRoi = wRoi;
    }

    public double[] getLambda() {
        return lambda;
    }

    public void setLambda(double[] lambda) {
        this.lambda = lambda;
    }
    
}