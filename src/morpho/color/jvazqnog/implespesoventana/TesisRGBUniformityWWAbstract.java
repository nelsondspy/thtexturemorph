/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.implespesoventana ;

import morpho.color.jvazqnog.models.Pixel;
import ij.gui.Roi;
import ij.process.ColorProcessor;

/**
 *
 * @author Derlis Argüello
 */
public abstract class TesisRGBUniformityWWAbstract extends TesisRGBMeanWWAbstract{

    public TesisRGBUniformityWWAbstract(String filter, String noisyImgPath, String imgExtension, 
            ColorProcessor noisyColProcessor, Pixel[] se) {
        super(filter, noisyImgPath, imgExtension, noisyColProcessor, se);
        
    }

    @Override
    public float[] setXfxSum(Roi roi) {
        int cSize = channels.length;
        int hSize = channels[0].getHistogramSize();
        float[] sum = new float[cSize];
        int[][] channelHistogram = new int[cSize][hSize];
        int k;

        for (int i = 0; i < cSize; i++) {
            channels[i].setRoi(roi);
            channelHistogram[i] = channels[i].getHistogram();
        }

        for (int i = 0; i < hSize; i++) {
            for (k = 0; k < cSize; k++) {
                //se calcula el numerador de (6)
                sum[k] = (float) Math.pow(channelHistogram[k][i], 2);
            }
        }

        return sum;
    }
    
}