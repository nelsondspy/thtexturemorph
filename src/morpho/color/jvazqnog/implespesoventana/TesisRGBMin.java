/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package morpho.color.jvazqnog.implespesoventana;

import morpho.color.jvazqnog.models.Pixel;
import static morpho.color.jvazqnog.implespesoventana.TestConstants.Filters.TesisRGB.ConVentanas.TESIS_RGB_MIN;
import morpho.color.jvazqnog.implespesoventana.TesisRGBMinAbstract;
import ij.process.ColorProcessor;
import java.util.List;


/**
 *
 * @author Derlis Argüello
 * @Nomenclatura: [Metodo][Orden]
 * @Ejemplo: [TesisRGB][Mean]
 */
public class TesisRGBMin extends TesisRGBMinAbstract{

    public TesisRGBMin(String filter, String noisyImgPath, String imgExtension, ColorProcessor noisyColProcessor, Pixel[] se, List<Integer> windowsRoiList) {
        super(filter, noisyImgPath, imgExtension, noisyColProcessor, se, windowsRoiList);
        setFilterName(TESIS_RGB_MIN);
    }
    
    @Override
    public void show() {
        //print();
        //save();
        super.show();
    }

}