package morpho;

public class StrucElementSizeDist {
	
	public static int [][]SESquare(int distance , int angle, int size){
		//
		System.out.println("distance:"  + distance );
		System.out.println("angle:"  + angle );
		System.out.println("size:"  + size);
		
		int dezY = 0 ;
		int unitSEWidth= 1 + (2 * size);
		int totalWidth = 0 ;   
		int totalHeight = 0 ;
	
		int p1X = 0 ;
		int p2X = 0  ;
		
		int p1Y = 0 ;
		int p2Y = 0  ;
		
		if(angle == 90 ){
			dezY = distance + size ;
			totalWidth = ( Math.abs(dezY) * 2 ) + ( size * 2 ) + 1;  
			totalHeight = distance  + (2 * unitSEWidth ) ;
		}
		else{
			dezY =  (int)(Math.round(Math.tan(Math.toRadians(angle))) * (distance + size) ) ;	
			totalWidth = distance  + (2 * unitSEWidth ) ;  
			totalHeight = ( Math.abs(dezY) * 2 ) + ( size * 2 ) + 1;
		}
		
		
		int [][] se;
		se = new int[totalHeight][totalWidth];
		
		int centerX = totalWidth / 2;
		int centerY  = totalHeight  / 2;
		
		if(angle != 90 ){
			//tratar los casos de tamanho par o impar
			int offsetX1 = -1 ;
			int offsetX2 = 0 ;
			if (totalWidth % 2 != 0 ){
				offsetX1= -1;
				offsetX2 = 1 ;
			}
			
			p1X = centerX - (distance/2)  - size  + offsetX1  ;
			p2X = centerX + (distance/2)  + size  + offsetX2  ;
			
			p1Y = centerY + dezY ;
			p2Y = centerY - dezY  ;

		}
		else{
			System.out.println("Es noventa!");
			
			int offsetX1 = -1 ;
			int offsetX2 = 0 ;
			if (totalHeight % 2 != 0 ){
				offsetX1= -1;
				offsetX2 = 1 ;
			}
			p1X = centerY ;
			p2X = centerY  ;
			
			p1Y = centerX - (distance/2)  - size  + offsetX1   ;
			p2Y = centerX + (distance/2)  + size  + offsetX2 ;
		}
		
		
		//
		se[p1Y ][p1X ] = 1;
		se[p2Y ][p2X ] = 1;
		
		
		//teniendo el par de centros de cada elemento estructurante, se completa el cuadrado
		
		for(int f = (p1Y - size ) ; f<= (p1Y + size  ); f++){
			for(int c = p1X - size ; c <= (p1X + size); c++){
				se[f][c] = 1 ;
			}
		}
		for(int f = (p2Y - size ) ; f<= (p2Y + size  ); f++){
			for(int c = p2X - size ; c <= (p2X + size); c++){
				se[f][c] = 1 ;
			}
		}	
      
		
		
		
		for(int i = 0 ; i< se.length; i++){
			for(int j = 0 ; j < se[0].length; j++){
				System.out.print(  se[i][j] + ", " ) ;
			}
			System.out.println("") ;	
		}	
		
		//generar un par de puntos distantes 
	     
	     
		//encontrar las coordenadas de los puntos.
		//crear una nueva matriz con un credimiendo dx dy 
		//reubicar los centros de esos puntos usando los crecimientosd dx, dy 
		//utilizar como elemento central 
		
		
		
		return se ;
	}
	
	//

}
