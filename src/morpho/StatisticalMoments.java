package morpho;

import ij.process.ImageProcessor;

/**
 * Finalidad de la clase es agrupar las funciones de reduccion de imagenes con   
 * estadisticas a escalares
 *  
 * */
public class StatisticalMoments {
	
	
	/**
	 * Volumen estandar: sumatoria de todos valores de los pixeles 
	 * @author nelsond
	 * @param image procesor en escala de grises
	 * @return sumatoria de los valores de intensidad de los pixeles
	 * */
	public static double  calcVolumen(ImageProcessor ip){
		int h = ip.getHeight();
		int w = ip.getWidth();
		double sum = 0 ;
		for(int v = 0 ; v< h ; v++){
			for(int u = 0 ; u< w ; u++){
				sum = sum + ( double )( ip.get(u,v) );
			}	
		}
		return sum;
	}
}
