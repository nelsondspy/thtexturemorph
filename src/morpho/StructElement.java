package morpho;

public class StructElement {
	
	public int [][] invert(int [][] H){
		
		int K= H[0].length / 2; // filter width /2
		int L = H.length / 2;   //filter height

		int [][]R = new int[H.length][H[0].length];
		
		return R;
		
	}
	
	
	/**
	 * 
	 * */
	public static int [][] SEDisk(int radius ){
		int M =  (radius * 2)  + 1;
		int radius2 = radius*radius;
		//centro del elemento estructurante
		int K= radius  ; // filter width 
		int L = radius ;   //filter height

		
		
		int [][]SE = new int[M][M];
		SE[radius][radius] = 1;
		
		for(int i = 0 ; i< SE.length; i++){
			for(int j = 0 ; j< SE[0].length; j++){
				
				SE[i][j] = 0;
				
				if (Math.pow(i-L,2) + Math.pow( j-K,2) <= radius2   ){
					SE[i][j] = 1;
				} 
			}
			
		}
		
		/*
		for(int i = 0 ; i< SE.length; i++){
			
			for(int j = 0 ; j < SE[0].length; j++){
				System.out.print(  SE[i][j] + ", " ) ;
			}
			System.out.println("") ;
			
		}
		*/
		
		return SE ;
		
	}
	
	
	/**
	 * Retorna un elemento estructurante cuadrado con tamanho  (2*L)+1, 
	 * @author nelsond
	 * @param size es el tamanho de lado, en cuadrado tendra un lado total de (2*size) + 1
	 * @return SE elemento estructurante
	 * */
	public static int [][] SESquare(int size ){
		int M =  (size * 2)  + 1;
		int [][]SE = new int[M][M];
		for(int f = 0 ;f < M ; f++ ){
			for(int c = 0 ;c < M ; c++ ){
				SE[f][c] = 1 ;
			}
			
		}
		
		return SE;
		
	}

	
	
}
