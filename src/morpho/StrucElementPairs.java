package morpho;

public class StrucElementPairs {
	public static int [][]SEPoint(int distance , int angle ){
		int totalsizex =   (distance *2) + 1;
		int totalsizey = 1 ;   
		if (angle > 0) 
			totalsizey = totalsizex;

		
		//centro del elemento estructurante 
		int px = totalsizex / 2;
		int py = totalsizey / 2;
		
		//punto a la izquierda 
		int p1x ;
		int p1y ;
		
		//punto a la derecha
		int p2x ;
		int p2y ;
		
		int [][] se;
		se = new int[totalsizey][totalsizex];
		
		if ( angle != 0 &&  angle != 90 ){
			
			int factordez = (int)(Math.round(Math.tan(Math.toRadians(angle))) * distance) ;
			
			System.out.println("Math.tan(Math.toRadians(angle)):" + Math.tan(Math.toRadians(angle)));
			
			System.out.println("angle: " + angle);
			System.out.println("factordez: " + factordez  );
				
			p1x = px - distance;
			p1y = py + factordez;
	
			p2x = px + distance;
			p2y = py - factordez;
			
		}
		else if(angle == 90 ){

			p1x = px ;
			p1y = py - distance;
	
			p2x = px ;
			p2y = py + distance;
		}
		else{
			
			p1x = px - distance;
			p1y = 0;
	
			p2x = px + distance;
			p2y = 0;
			
		}
		
		
		se[p1y][p1x] = 1;
		se[p2y][p2x] = 1;
		
		
		for(int i = 0 ; i< se.length; i++){
			for(int j = 0 ; j < se[0].length; j++){
				System.out.print(  se[i][j] + ", " ) ;
			}
			System.out.println("") ;	
		}	
		
		
		return se ;
	}
	
	
	

}
