
import utils.Color_Transformer;
import utils.FilesMngIJ;
import utils.MiscelaneousArray;
import utils.ParamGranulometry;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.util.Various;
import ij.gui.Plot;
import ij.Macro;

import java.io.File;

import clasiffier.DatabaseMng;
import clasiffier.DatabaseMng.TYPEDATA;
import descriptor.DescriptorStackAbstract;
import descriptor.Util;

/** 
 * Implementa la granulometria de forma general al trabajar con un stack 
 * ideal para otros espacios de color que  NO sean RGB , es decir L a*b* y HSI y otr
 * */

public abstract class GranulometryBasicStack  implements PlugInFilter {
	
	/** parametros de la granulometria */
	ParamGranulometry PRM ; 
    
	/** otros parametros */
	ImagePlus  imp  = null;
	
	/** opciones recibidas desde algun macro o ejecucion masiva */
	String macroOptions = null;
	
	/** path de salida debe incluir barra separadora al final */
	String dirOutput = "granulcolor/";
	
	/** tipo de descriptor para cada elemento de la serie */
	DescriptorStackAbstract DESCRIPTOR = null ; 
	
	
	String colorSpace =null;
	
	
	/**
	 * Metodo de PlugInFilter 
	 * */
	public int setup(String arg, ImagePlus imp) {
		this.imp = imp ;			
		
		macroOptions = Macro.getOptions();
		
		PRM = new ParamGranulometry();
		
		
		if (macroOptions == null ){
			PRM.setParamFromGUI();
		} 
		else{
			/*lectura de parametros */
			PRM.setParamFromString( macroOptions );
		}
		
		//crea una carpeta con el mismo nombre del id del experimento 
		dirOutput = PRM.experimentId.trim() + File.separator ;
		
		FilesMngIJ.createDirIfNoExist(PRM.experimentId.trim());

		DESCRIPTOR = Util.getDescriptorStackInstance( PRM.getDESCRIPTOR_NAME() ) ;
		
		//considerando que el volumen primer elemento la imagen sin alterar 
		DESCRIPTOR.setDim( PRM.MAXSIZE_SE  );
		
		return DOES_ALL ;	
	}
	
	
	public void run(ImageProcessor orig) {
		
		int [] listSizes = new int[PRM.MAXSIZE_SE] ;
		MiscelaneousArray.setSerialArray(listSizes, 1 , PRM.STEPSIZE_SE ) ;

		//grafico para granulometria 
		double[] granPlotx = new double[ PRM.MAXSIZE_SE ] ;
		double[] granPloty = new double[ PRM.MAXSIZE_SE ] ;
		
		
		int aux=0 ; 
		
		//imagen anterior en la serie diferencial 
		ImagePlus copyback = new ImagePlus(imp.getTitle(), orig.duplicate());
		
		
		/*primer elemento de la serie morfologica */
		/*  agregar al vector del descriptor */
		Color_Transformer colorTransformer = new Color_Transformer();
		colorTransformer.setup("", copyback);
		colorTransformer.setColorSpace(this.getColorspace());
		colorTransformer.run(copyback.getProcessor());
		
		
		DESCRIPTOR.calculateAndAdd( colorTransformer.getImpResult() ) ;
		
		for(int size = 1; size < listSizes.length ; size++ ){
			
			int sizeSE = listSizes[size];
			
			Pixel[] se = Various.getShiftArray( sizeSE );
			
			ImagePlus resultProcElement= applyMorphoOperation(sizeSE, se);
			
			//-----------------
			DESCRIPTOR.calculateAndAdd( resultProcElement ) ;	
			
			//todos los ejes x correponden al tamanho del ES 
			
			granPlotx[aux] = size;
						
			aux++;	
					
		}
		
		/*es necesario llamar el metodo para normaliza o 
		 * realizar alguna operacion especifica descriptor  */
		DESCRIPTOR.postProcess();
		
		
		if ( macroOptions == null ) {
			//grafica de granulometria 
			Plot granPlot ;
			granPlot = new Plot("Granulometria","size SE","vol",granPlotx, granPloty); 
			granPlot.show() ;		
		}
			
		
	/*--- almacenar vector de caracteristicas */
		
	/*Insercion */
	DatabaseMng dbmg = new DatabaseMng();
	dbmg.typedata= TYPEDATA.TRAIN;
	dbmg.inserTextureProp( imp.getTitle(), DESCRIPTOR.getVectorDescriptor() , PRM.getExperimentId() , -1, macroOptions );	
			
	/*-- fin almacenar */
		
		
		
	}
	
	
	/**
	 * Internamente el metodo debe retornar un ImagePlus que contiene una stack 
	 *  de c-canales 
	 * 
	 * */
	public ImagePlus applyMorphoOperation(int sizeSE , Pixel [] se ){
		ImagePlus salida = null ; 
		
		return salida;

	}
	
	
	public abstract String getColorspace();
	
	
	
}
