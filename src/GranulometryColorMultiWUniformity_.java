import java.util.Arrays;
import java.util.List;

import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.implespesoventana.TesisRGBUniformity;
import morpho.color.jvazqnog.models.Pixel;


/**
 * Granulometria color, procesamiento vectorial fijando pesos 
 * con el momento estadistico : UNIFORMIDAD 
 * 
 * */
public class GranulometryColorMultiWUniformity_ extends GranulometryBasic {
	
	
	/**
	 * Aplica las operaciones morfologicas 
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		
	     List<Integer> windowList = Arrays.asList(2, 4, 8);
		 //List<Integer> windowList = Arrays.asList( 2 );

			//----------------------------------
				
			String pathwork= "granulcolor_mw/images/";
				
			String nameImageinput =  pathwork + imp.getTitle() ;
			String nameImageOuput =  "_M=granMultWinUniform" +  "_s" + sizeSE  ;
				
			//erosion  (minimo)  
			TesisRGBUniformity  uniformMin = new TesisRGBUniformity("Min", nameImageinput , 
		               "bmp", (ColorProcessor)  this.imp.getProcessor(), se, windowList );
				
			uniformMin.setFilterName(nameImageOuput);
			uniformMin.run();
				
			//dilatacion 
			String iminput2 = pathwork + imp.getTitle() + nameImageOuput   +"Min" +  ".bmp" ;
			//abrir la imagen erosionada 
			ImagePlus impcurrent = IJ.openImage( iminput2 );
			
			TesisRGBUniformity uniformMax = new  TesisRGBUniformity("Max", iminput2 , 
		                "bmp", (ColorProcessor)  impcurrent.getProcessor(), se, windowList );
				
			uniformMax.setFilterName( "" );
				
			uniformMax.run();
				
			ImagePlus impFinal = IJ.openImage( iminput2  +   "Max.bmp"  ); 
			return impFinal.getProcessor();
				
	}

}
