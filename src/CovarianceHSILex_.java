import order.HSILex;
import order.Lexicographical;
import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;

/**
 * Covarianza morgologica usando pesos ENTROPIA CON VENTANAS 
 * */
public class CovarianceHSILex_ extends CovarianceBasicStack {
	
	
	/**
	 * covarianza M. usando distancias euclidea 
	 * */
	public ImagePlus applyMorphoOperation(int distance, int orientation , Pixel [] se  ){

		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 

		String imgErod = imp.getShortTitle()+ "_M=covarLex" + "_dist="+distance  + "_angl="+ orientation+ "_ROI=" + PRM.ROISIZE ;

		HSILex lexHSI = new HSILex();
		lexHSI.setImageProcessor(this.imp.getProcessor());

		//erosion  (minimo) 
		ImagePlus eroded = lexHSI.filter( se, dirOutput , imgErod , HSILex.filterType.MIN );
		
		return eroded ;
		
	}	

	public String getColorspace() {
		// TODO Auto-generated method stub
		return "HSI";
	}
	
}
