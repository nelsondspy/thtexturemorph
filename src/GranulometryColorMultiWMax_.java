import java.util.Arrays;
import java.util.List;

import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.implespesoventana.TesisRGBMax;
import morpho.color.jvazqnog.models.Pixel;


/**
 * Granulometria color, procesamiento vectorial fijando pesos con MAXIMO CON VENTANAS 
 * 
 * */
public class GranulometryColorMultiWMax_ extends GranulometryBasic {
	
	
	/**
	 * Aplica las operaciones morfologicas 
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		
		   List<Integer> windowList = Arrays.asList( 1  );
			//List<Integer> windowList = Arrays.asList( 2 );

			//----------------------------------
				
			String pathwork= "granulcolor_mw/images/";
				
			String nameImageinput =  pathwork + imp.getTitle() ;
			String nameImageOuput =  "_M=granMultWinMax" +  "_s" + sizeSE  ;
				
			//erosion  (minimo)  
			TesisRGBMax maxMin = new TesisRGBMax("Min", nameImageinput , 
		               "bmp", (ColorProcessor)  this.imp.getProcessor(), se, windowList );
				
			maxMin.setFilterName(nameImageOuput);
			maxMin.run();
				
			//dilatacion 
			String iminput2 = pathwork + imp.getTitle() + nameImageOuput   +"Min" +  ".bmp" ;
			//abrir la imagen erosionada 
			ImagePlus impcurrent = IJ.openImage( iminput2 );
			
			TesisRGBMax maxMax = new  TesisRGBMax("Max", iminput2 , 
		                "bmp", (ColorProcessor)  impcurrent.getProcessor(), se, windowList  );
				
			maxMax.setFilterName( "" );
				
			maxMax.run();
				
			ImagePlus impFinal = IJ.openImage( iminput2  +   "Max.bmp"  ); 
			return impFinal.getProcessor();
				
	}

}
