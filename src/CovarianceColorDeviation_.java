import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.profe.plugins.RGBDeviationMin;

/**
 * Covarianza morgologica usando pesos POR DESVIACION CON VENTANAS 
 * */
public class CovarianceColorDeviation_ extends CovarianceBasic {
	
	/**
	* Internamente el metodo debe retornar el ImageProcessor que sea compatible 
	* con el metodo que calcula el descriptor 
	* 
	* */
	public ImageProcessor applyMorphoOperation(int distance, int orientation , Pixel [] se  ){
	
		//erosion  (minimo)  
		String imgErod = imp.getShortTitle()+ "_covar" + "_dist"+distance  + "_angle"+ orientation+ "_ROI" + PRM.ROISIZE ;
		RGBDeviationMin  desvMin  = new RGBDeviationMin(imgErod , dirOutput,
				"bmp", (ColorProcessor)  this.imp.getProcessor(), 
	             se, PRM.ROISIZE , PRM.ROISIZE );
		
		desvMin.run();
		
		//abrir la imagen recien erosionada 
		ImagePlus impFinal = IJ.openImage( dirOutput + imgErod + ".bmp" ); 
		
		return impFinal.getProcessor();
		
	}	

	
	
}
