import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.profe.plugins.TesisRGBPromedioMinimoPlugin;

/**
 * Covarianza morgologica usando pesos POR PROMEDIO CON VENTANAS 
 * */
public class CovarianceColorMean_ extends CovarianceBasic {
	
	/**
	* Internamente el metodo debe retornar el ImageProcessor que sea compatible 
	* con el metodo que calcula el descriptor 
	* 
	* */
	public ImageProcessor applyMorphoOperation(int distance, int orientation , Pixel [] se  ){
	
		//erosion  (minimo)  
		String imgErod = imp.getShortTitle()+ "_M=covarMean" + "_dist="+distance  + "_angl="+ orientation+ "_ROI=" + PRM.ROISIZE ;
		
		TesisRGBPromedioMinimoPlugin   promMin  = new TesisRGBPromedioMinimoPlugin ();
		promMin.setup("",this.imp );
		promMin.setRois( PRM.ROISIZE ,  PRM.ROISIZE );
		promMin.setOutput( imgErod , dirOutput);
		promMin.setSE( se );
		promMin.run( this.imp.getProcessor() );	
				
		
		//abrir la imagen recien erosionada 
		ImagePlus impFinal = IJ.openImage( dirOutput + imgErod + ".bmp" ); 
		
		return impFinal.getProcessor();
	}	

	
	
}
