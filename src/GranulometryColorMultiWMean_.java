import java.util.Arrays;
import java.util.List;

import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.implespesoventana.TesisRGBMean;
import morpho.color.jvazqnog.models.Pixel;


/**
 * Granulometria color, procesamiento vectorial fijando pesos con ENTROPIA CON VENTANAS
 * 
 * */
public class GranulometryColorMultiWMean_ extends GranulometryBasic {
	
	
	/**
	 * Aplica las operaciones morfologicas 
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		
			//----------------------------------
		
			List<Integer> windowList = Arrays.asList(  1  );
				
			String pathwork= "granulcolor_mw/images/";
				
			String nameImageinput =  pathwork + imp.getTitle() ;
			String nameImageOuput =  "_M=granMultWinMean" +  "_s" + sizeSE  ;
				
			//erosion  (minimo)  
			TesisRGBMean meanwMin = new TesisRGBMean("Min", nameImageinput , 
		               "bmp", (ColorProcessor)  this.imp.getProcessor(), se, windowList);
				
			meanwMin.setFilterName(nameImageOuput);
				meanwMin.run();
				
			//dilatacion 
			String iminput2 = pathwork + imp.getTitle() + nameImageOuput   +"Min" +  ".bmp" ;
			//abrir la imagen erosionada 
			ImagePlus impcurrent = IJ.openImage( iminput2 );
			
			TesisRGBMean meanwMax = new  TesisRGBMean("Max", iminput2 , 
		                "bmp", (ColorProcessor)  impcurrent.getProcessor(), se, windowList);
				
				meanwMax.setFilterName( "" );
				
				meanwMax.run();
				
				ImagePlus impFinal = IJ.openImage( iminput2  +   "Max.bmp"  ); 
				return impFinal.getProcessor();
				
				
	
	}

}



