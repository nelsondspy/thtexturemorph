import java.util.Arrays;
import java.util.List;

import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.implespesoventana.TesisRGBAsimetric ;
/**
 * Covarianza morgologica usando pesos ENTROPIA CON VENTANAS 
 * */
public class CovarianceColorMultiWAsimetric_ extends CovarianceBasic {
	
	
	/**
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int distance, int orientation , Pixel [] se  ){
	
		//List<Integer> windowList = Arrays.asList(2,4,8 );
	    List<Integer> windowList = Arrays.asList( 2  );
		
		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 
		//String imgErod = imp.getShortTitle()+ "_M=covarMeanWindow" + "_dist="+distance  + "_angl="+ orientation+ "_ROI=" + PRM.ROISIZE ;
		String pathwork= "covarcolor_promv/images/";
		
		String nameImageinput =  pathwork + imp.getTitle() ;
		String nameImageOuput =  "_M=covarMultiWAsimetric" + "_dist=" + distance  + "_angl="+ orientation+ "_ROI=" + PRM.ROISIZE;
		
		//erosion  (minimo)  
		TesisRGBAsimetric  asimetricMW = new TesisRGBAsimetric("Min", nameImageinput , 
                "bmp", (ColorProcessor)  this.imp.getProcessor(), se, windowList );
		
		asimetricMW.setFilterName(nameImageOuput);
		
		asimetricMW.run();
		 
		//
		ImagePlus impFinal = IJ.openImage( pathwork + imp.getTitle() + nameImageOuput   +"Min" +  ".bmp" ); 
		return impFinal.getProcessor();
		
	}	

	
	
}
