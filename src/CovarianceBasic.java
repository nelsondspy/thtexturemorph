import java.io.File;

import clasiffier.DatabaseMng;
import clasiffier.DatabaseMng.TYPEDATA;
import descriptor.Descriptor;
import descriptor.Util;
import ij.ImagePlus;
import ij.Macro;
import ij.io.FileSaver;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import morpho.StructElementListPixels;
import morpho.color.jvazqnog.models.Pixel;
import utils.ParamCovariance;
import utils.FilesMngIJ;
import utils.MiscelaneousArray ;

/**
 * <p>Implementacion basica de covarianza morfologica .  
 * retorna una curva de distribución de orientacion,distancia y volumen .
 * Utiliza las orientaciones (grandos sexagesimales ):  0 , 45 , 90 , 135 
 *  </p>
 * @author nelsond
 * @param  NUM_DIST es el numero o cantidad  de distancias 
 * @param  INCREMENT_SIZE  tamanho del paso entre cada distancia    
 * @return retorna una curva de distribucion de orientacion.distancia mapeado a tamanho
 *  
 * */


public class CovarianceBasic  implements PlugInFilter {
	String macroOptions = null ;
	
	ImagePlus  imp  = null;
	
	/**parametros de la covarianza */
	ParamCovariance PRM ;
	
  /** directorio de salida */
   String dirOutput = "covarcolor/" ;
   
	/** tipo de descriptor para cada elemento de la serie */
	Descriptor DESCRIPTOR = null ; 
	
	
	
    
	/**cantidad de tamanhos de angulos, solo los significativos */
	private int CANT_ANGLES = 4; 
			
    
	public int setup(String arg, ImagePlus imp) {
		this.imp = imp ;			
		
		macroOptions = Macro.getOptions();
		
		PRM = new ParamCovariance();
		
		//si no reciben parametros entonces mostrar gui 
		if (macroOptions == null && arg == null) {
			PRM.setParamFromGUI();
		} else {
			/* lectura de parametros */
			if (macroOptions != null)
				PRM.setParamFromString(macroOptions);
			if (arg != null) {
				PRM.setParamFromString(arg);
				macroOptions = arg;
			}
		}
		
		//crea una carpeta con el mismo nombre del id del experimento 
		dirOutput = PRM.experimentId.trim() + File.separator;
		
		FilesMngIJ.createDirIfNoExist(PRM.experimentId.trim());

		
		DESCRIPTOR = Util.getDescriptorInstance( PRM.getDESCRIPTOR_NAME() ) ;
		
		DESCRIPTOR.setDESCRIPTOR_PRM(PRM.DESCRIPTOR_PRM);   
		DESCRIPTOR.setLocalParameters();
		
		//se agrega + 1 porque el volumen original debe ser incluido
		//DESCRIPTOR.setDim( (PRM.NUM_DIST + 1) * CANT_ANGLES );
		DESCRIPTOR.setDim( (PRM.NUM_DIST  * CANT_ANGLES)  + 1 )  ;
		
		return DOES_ALL;
	
	}
	
	public void run(ImageProcessor orig) {	
	
		//lista de distancias 
		int [] listDistances = new int[PRM.NUM_DIST];
		MiscelaneousArray.setSerialArray(listDistances, 1 , PRM.INCREMENT_SIZE);
		
		//lista de angulos 
		int [] angles = {0, 45, 90, 135};
			
		/*primer elemento de la serie morfologica */
		/*  agregar al vector del descriptor */
		ImageProcessor copyback = orig.duplicate(); 
		DESCRIPTOR.calculateAndAdd(  copyback ) ;
		
		//guarda la imagen original en el directorio de salida porque tambien forma
		//parte de la serie
		FileSaver fs = new FileSaver(imp);
		fs.saveAsBmp(dirOutput + imp.getShortTitle()+"_"+String.format("%04d", 0) +".bmp" );
	
		
		//angulos X distancias 
		for(int a = 0; a< angles.length; a++){
			int angle = angles[ a ] ;
		
		for(int s  = 0; s < PRM.NUM_DIST ; s++ ){
			
			int distance = listDistances[s];
			
			//elemento estructurante de forma cuadrada par una par (distancias, direccion)
			Pixel [] se = StructElementListPixels.getSESquare( 0 , distance, angle);
		
			//aplica la operacion morfologica 
			ImageProcessor resultProcElement= applyMorphoOperation( distance, angle,  se );
			
			//agrega a la lista de descriptores
			DESCRIPTOR.calculateAndAdd( resultProcElement ) ;	
			
			}
		
		}
		
		/*es necesario llamar el metodo para normaliza o 
		 * realizar alguna operacion especifica descriptor  */
		DESCRIPTOR.postProcess();
				
		/*Generacion de archivos auxiliares de salida para el clasificador */
		DatabaseMng dbmg = new DatabaseMng();
		dbmg.typedata= TYPEDATA.TRAIN;
		dbmg.inserTextureProp(imp.getTitle() ,  DESCRIPTOR.getVectorDescriptor()  ,
				PRM.getExperimentId() , -1, PRM.toString());	
		
	}
		
	/**
	* Internamente el metodo debe retornar el ImageProcessor que sea compatible 
	* con el metodo que calcula el descriptor 
	* 
	* */
	public ImageProcessor applyMorphoOperation(int distance, int orientation , Pixel [] se ){
		
		ImageProcessor salida = null ; 
		
		return salida;
			
	}	
	
}
	
	

	





