import order.NormalEuclid;
import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;


/**
 * Granulometria color, procesamiento vectorial fijando pesos con ENTROPIA CON VENTANAS
 * 
 * */
public class AntiGranulometryColorNormalEuclid_ extends GranulometryBasicFull {
	
	
	/**
	 * Cierre
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 
				String currentcolorimg =  imp.getShortTitle() + "_CIERRgranEuclid" + "_" + String.format("%04d", sizeSE)  ;
				
	
				NormalEuclid normeuclid = new NormalEuclid();
				
				//dilatacion  
				normeuclid.dilate(this.imp.getProcessor(),se, dirOutput , currentcolorimg  );
				
				//erosion, 
				ImagePlus impEro = IJ.openImage( dirOutput + currentcolorimg + ".bmp" );
				normeuclid.erode(impEro.getProcessor(),se, dirOutput , currentcolorimg  );
				
				//cargar en memoria la imagen sobreescrita 
				ImagePlus impFinal = IJ.openImage( dirOutput + currentcolorimg + ".bmp" ); 
				return impFinal.getProcessor();
	
	}
	
	/**
	 * 
	 * Apertura
	 * */
	public ImageProcessor applyMorphoOperation2(int sizeSE , Pixel [] se ){
		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 
				String currentcolorimg =  imp.getShortTitle() + "_APERgranEuclid" + "_" + String.format("%04d", sizeSE)  ;
					
				NormalEuclid normeuclid = new NormalEuclid();
				
				//erosion, 
				
				normeuclid.erode(this.imp.getProcessor(),se, dirOutput , currentcolorimg  );
				
				//dilatacion  
				ImagePlus impEro = IJ.openImage( dirOutput + currentcolorimg + ".bmp" );
				normeuclid.dilate(impEro.getProcessor(),se, dirOutput , currentcolorimg  );
				
				//cargar en memoria la imagen sobreescrita 
				ImagePlus impFinal = IJ.openImage( dirOutput + currentcolorimg + ".bmp" ); 
				return impFinal.getProcessor();
	
	}

}



