
import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.implespesoventana.TesisRGBAsimetricWW;
import morpho.color.jvazqnog.models.Pixel;


/**
 * Granulometria color, procesamiento vectorial fijando pesos con ENTROPIA CON VENTANAS
 * 
 * */
public class GranulometryColorWWAsimetric_ extends GranulometryBasic {
	
	
	/**
	 * Aplica las operaciones morfologicas 
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		
			//----------------------------------
				
			String pathwork= "granulcolor_mw/images/";
				
			String nameImageinput =  pathwork + imp.getTitle() ;
			String nameImageOuput =  "_M=granWWAsim" +  "_s" + sizeSE  ;
				
			//erosion  (minimo)  
			TesisRGBAsimetricWW asimMin = new TesisRGBAsimetricWW("Min", nameImageinput , 
		               "bmp", (ColorProcessor)  this.imp.getProcessor(), se );
				
			asimMin.setFilterName(nameImageOuput);
			asimMin.run();
				
			//dilatacion 
			String iminput2 = pathwork + imp.getTitle() + nameImageOuput   +"Min" +  ".bmp" ;
			//abrir la imagen erosionada 
			ImagePlus impcurrent = IJ.openImage( iminput2 );
			
			TesisRGBAsimetricWW asimMax = new  TesisRGBAsimetricWW("Max", iminput2 , 
		                "bmp", (ColorProcessor)  impcurrent.getProcessor(), se );
				
			asimMax.setFilterName( "" );
				
			asimMax.run();
				
			ImagePlus impFinal = IJ.openImage( iminput2  +   "Max.bmp"  ); 
			return impFinal.getProcessor();
				
	}

}
