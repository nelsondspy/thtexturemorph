import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.profe.plugins.TesisRGBPromedioSinVentanasMinimoPlugin;

/**
 * Covarianza morgologica usando pesos POR PROMEDIO SIN VENTANAS 
 * */
public class CovarianceColorMeanWW_ extends CovarianceBasic {
	
	/**
	* Internamente el metodo debe retornar el ImageProcessor que sea compatible 
	* con el metodo que calcula el descriptor 
	* 
	* */
	public ImageProcessor applyMorphoOperation(int distance, int orientation , Pixel [] se  ){
	
		//erosion  (minimo)  
		String imgErod = imp.getShortTitle()+ "_M=covarMEANWW" + "_dist="+distance  + "_angl="+ orientation ;
		
		TesisRGBPromedioSinVentanasMinimoPlugin   promMin  = new TesisRGBPromedioSinVentanasMinimoPlugin ();
		promMin.setup("",this.imp );
		promMin.setOutput( imgErod , dirOutput);
		promMin.setSE( se );
		promMin.run( this.imp.getProcessor() );	
				
		
		//abrir la imagen recien erosionada 
		ImagePlus impFinal = IJ.openImage( dirOutput + imgErod + ".bmp" ); 
		
		return impFinal.getProcessor();
	}	

	
	
}
