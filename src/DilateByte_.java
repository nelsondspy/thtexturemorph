import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import morpho.StructElement;
import morpho.gray.*;



public class DilateByte_ implements PlugInFilter {
	public int setup(String arg, ImagePlus imp) {
		return DOES_8G; 
		//return DOES_ALL;
	}

		
	/*
	public void dilate(ImageProcessor orig, int[][]H){
		orig.invert();
		//erode(orig, ImageProcessor.reflect(H));
		orig.invert();
	}
	*/
	
	
	
	public void run(ImageProcessor orig) {
		
		int[][] filter2 =  StructElement.SEDisk(1);
		
		Operator.dilate(orig, filter2);	

	}
	
	
}
