import order.LabNormaEuclid;
import ij.ImagePlus;
import morpho.color.jvazqnog.models.Pixel;

/**
 * Covarianza morgologica Lab lexicografico 
 * */
public class CovarianceLabNorm_ extends CovarianceBasicStack {
	
	
	/**
	 * covarianza M. usando distancias euclidea 
	 * */
	public ImagePlus applyMorphoOperation(int distance, int orientation , Pixel [] se  ){

		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 

		String imgErod = imp.getShortTitle()+ "_M=covarLex" + "_dist="+distance  + "_angl="+ orientation+ "_ROI=" + PRM.ROISIZE ;

		LabNormaEuclid normLab = new LabNormaEuclid();
		normLab.setImageProcessor(this.imp.getProcessor());

		//erosion  (minimo) 
		ImagePlus eroded = normLab.filter( se, dirOutput , imgErod , LabNormaEuclid.filterType.MIN );
		return eroded ;
		
	}	

	@Override
	public String getColorspace() {
		return "Lab";
		
	}	
	
}
