import java.util.Arrays;

import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.implespesoventana.TesisRGBSdeviation ;
/**
 * Covarianza morgologica usando pesos ENTROPIA CON VENTANAS 
 * */
public class CovarianceColorMultiWDeviation_ extends CovarianceBasic {
	
	
	/**
	 * */
	public ImageProcessor applyMorphoOperation(int distance, int orientation , Pixel [] se  ){
	
		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 
		//String imgErod = imp.getShortTitle()+ "_M=covarMeanWindow" + "_dist="+distance  + "_angl="+ orientation+ "_ROI=" + PRM.ROISIZE ;
		String pathwork= "covarcolor_promv/images/";
		
		String nameImageinput =  pathwork + imp.getTitle() ;
		String nameImageOuput =  "_M=covarMultiWDevia" + "_dist=" + distance  + "_angl="+ orientation+ "_ROI=" + PRM.ROISIZE;
		
		//erosion  (minimo)  
		TesisRGBSdeviation entropyMW = new TesisRGBSdeviation("Min", nameImageinput , 
                "bmp", (ColorProcessor)  this.imp.getProcessor(), se, Arrays.asList( 1 ));
		
		entropyMW.setFilterName(nameImageOuput);
		
		entropyMW.run();
		 
		//
		ImagePlus impFinal = IJ.openImage( pathwork + imp.getTitle() + nameImageOuput   +"Min" +  ".bmp" ); 
		return impFinal.getProcessor();
		
	}	

	
	
}
