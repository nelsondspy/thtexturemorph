import order.AlphaLexicograph;
import order.Lexicographical;
import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;

/**
 * Covarianza Morfologica usando alpha-lexicografico RGB
 * */
public class CovarianceColorAlphaLex_ extends CovarianceBasic {
	
	static int ALPHA = 10 ;
	
	/**
	 * 
	 * covarianza M. usando alpha-lexicografico RGB
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int distance, int orientation , Pixel [] se  ){

		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 

		String imgErod = imp.getShortTitle()+ "_M=covarLex" + "_dist="+distance  + "_angl="+ orientation+ "_ROI=" + PRM.ROISIZE ;

		AlphaLexicograph lex = new AlphaLexicograph();
		//
		lex.setAlpha(  ALPHA  );
		
		lex.erode(this.imp.getProcessor(),se, dirOutput , imgErod );
		
		//
		ImagePlus impFinal = IJ.openImage( dirOutput + imgErod + ".bmp" ); 
		return impFinal.getProcessor();
		
	}	

	
	
}
