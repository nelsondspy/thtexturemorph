import java.util.Arrays;

import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.implespesoventana.TesisRGBSdeviation ;
import morpho.color.jvazqnog.models.Pixel;


/**
 * Granulometria color, procesamiento vectorial fijando pesos con ENTROPIA CON VENTANAS
 * 
 * */
public class GranulometryColorMultiWDeviation_ extends GranulometryBasic {
	
	
	/**
	 * Aplica las operaciones morfologicas 
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		
			//----------------------------------
				
			String pathwork= "granulcolor_mw/images/";
				
			String nameImageinput =  pathwork + imp.getTitle() ;
			String nameImageOuput =  "_M=granMultWinDeviat" +  "_s" + sizeSE  ;
				
			//erosion  (minimo)  
			TesisRGBSdeviation devMin = new TesisRGBSdeviation("Min", nameImageinput , 
		               "bmp", (ColorProcessor)  this.imp.getProcessor(), se, Arrays.asList( 1 ));
				
			devMin.setFilterName(nameImageOuput);
			devMin.run();
				
			//dilatacion 
			String iminput2 = pathwork + imp.getTitle() + nameImageOuput   +"Min" +  ".bmp" ;
			//abrir la imagen erosionada 
			ImagePlus impcurrent = IJ.openImage( iminput2 );
			
			TesisRGBSdeviation devMax = new  TesisRGBSdeviation("Max", iminput2 , 
		                "bmp", (ColorProcessor)  impcurrent.getProcessor(), se, Arrays.asList(  1  ));
				
			devMax.setFilterName( "" );
				
			devMax.run();
				
			ImagePlus impFinal = IJ.openImage( iminput2  +   "Max.bmp"  ); 
			return impFinal.getProcessor();
				
	}

}
