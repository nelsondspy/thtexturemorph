import java.util.Arrays;

import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.implespesoventana.TesisRGBEntropy;
import morpho.color.jvazqnog.models.Pixel;


/**
 * Granulometria color, procesamiento vectorial fijando pesos con ENTROPIA CON VENTANAS
 * 
 * */
public class GranulometryColorMultiWEntropy_ extends GranulometryBasic {
	
	
	/**
	 * Aplica las operaciones morfologicas 
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		
			//----------------------------------
				
			String pathwork= "granulcolor_mw/images/";
				
			String nameImageinput =  pathwork + imp.getTitle() ;
			String nameImageOuput =  "_M=granMultWinEntropy" +  "_s" + sizeSE  ;
				
			//erosion  (minimo)  
			TesisRGBEntropy entropyMin = new TesisRGBEntropy("Min", nameImageinput , 
		               "bmp", (ColorProcessor)  this.imp.getProcessor(), se, Arrays.asList(2, 4, 8));
				
			entropyMin.setFilterName(nameImageOuput);
			entropyMin.run();
				
			//dilatacion 
			String iminput2 = pathwork + imp.getTitle() + nameImageOuput   +"Min" +  ".bmp" ;
			//abrir la imagen erosionada 
			ImagePlus impcurrent = IJ.openImage( iminput2 );
			
			TesisRGBEntropy entropyMax = new  TesisRGBEntropy("Max", iminput2 , 
		                "bmp", (ColorProcessor)  impcurrent.getProcessor(), se, Arrays.asList(2, 4, 8));
				
			entropyMax.setFilterName( "" );
				
			entropyMax.run();
				
			ImagePlus impFinal = IJ.openImage( iminput2  +   "Max.bmp"  ); 
			return impFinal.getProcessor();
				
	}

}
