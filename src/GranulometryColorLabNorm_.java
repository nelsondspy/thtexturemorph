import order.LabNormaEuclid;
import ij.ImagePlus;
import morpho.color.jvazqnog.models.Pixel;


/**
 * Granulometria color, procesamiento vectorial fijando pesos con ENTROPIA CON VENTANAS
 * 
 * */
public class GranulometryColorLabNorm_ extends GranulometryBasicStack {
	
	String colorSpace =  "Lab";
	
	/**
	 * Aplica las operaciones morfologicas 
	 * 
	 * */
	public ImagePlus applyMorphoOperation(int sizeSE , Pixel [] se ){
		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 
				String currentcolorimg =  imp.getShortTitle() + "_granLexic" + "_s" + sizeSE  ;
				
				//erosion  (minimo)
				LabNormaEuclid labeuclid1 = new LabNormaEuclid();
				labeuclid1.setImageProcessor(this.imp.getProcessor());
				ImagePlus eroded = labeuclid1.filter( se, dirOutput , currentcolorimg , LabNormaEuclid.filterType.MIN );
				
				//dilatacion maximo, 
				LabNormaEuclid labEuclid2 = new LabNormaEuclid();
				labEuclid2.setImagePlus(eroded);
				ImagePlus dilated = labEuclid2.filter( se, dirOutput , currentcolorimg , LabNormaEuclid.filterType.MAX );
			
				return dilated ;
	
	}

	@Override
	public String getColorspace() {
		return "Lab";
		
	}

}



