import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.profe.plugins.RGBEntropyMax;
import morpho.color.jvazqnog.profe.plugins.RGBEntropyMin;


/**
 * Granulometria color, procesamiento vectorial fijando pesos con ENTROPIA CON VENTANAS
 * 
 * */
public class AntiGranulometryColorEntropy_ extends GranulometryBasicFull {
	
	
	/**
	 * Aplica las operaciones morfologicas 
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 
				String currentcolorimg =  imp.getShortTitle() + "_AperGranENTRO" + "_" + String.format("%04d",sizeSE)  ;
					
				//erosion  (minimo)  
				RGBEntropyMin  entropyMin  = new RGBEntropyMin(currentcolorimg , dirOutput,
						"bmp", (ColorProcessor)  this.imp.getProcessor(), se, PRM.ROISIZE, PRM.ROISIZE);
				entropyMin.run();
			
				//dilatacion maximo,  
				ImagePlus impEro = IJ.openImage( dirOutput + currentcolorimg + ".bmp" );
				//se reemplaza la imagen a color actual erosionada con su dilatacion 
				RGBEntropyMax  entropyMax  = new RGBEntropyMax( currentcolorimg, dirOutput,
						"bmp", (ColorProcessor)  impEro.getProcessor(), se ,PRM.ROISIZE, PRM.ROISIZE);
				entropyMax.run();
				
				//cargar en memoria la imagen sobreescrita 
				ImagePlus impFinal = IJ.openImage( dirOutput + currentcolorimg + ".bmp" ); 
				return impFinal.getProcessor();
	
	}
	
	public ImageProcessor applyMorphoOperation2(int sizeSE , Pixel [] se ){
		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 
				String currentcolorimg =  imp.getShortTitle() + "_CierrGranENTRO" + "_" + String.format("%04d",sizeSE) ;
					
				//erosion  (minimo)  
				RGBEntropyMax  entropyMax  = new RGBEntropyMax(currentcolorimg , dirOutput,
						"bmp", (ColorProcessor)  this.imp.getProcessor(), se, PRM.ROISIZE, PRM.ROISIZE);
				entropyMax.run();
			
				//dilatacion maximo,  
				ImagePlus impEro = IJ.openImage( dirOutput + currentcolorimg + ".bmp" );
				//se reemplaza la imagen a color actual erosionada con su dilatacion 
				RGBEntropyMin  entropyMin  = new RGBEntropyMin( currentcolorimg, dirOutput,
						"bmp", (ColorProcessor)  impEro.getProcessor(), se ,PRM.ROISIZE, PRM.ROISIZE);
				entropyMin.run();
				
				//cargar en memoria la imagen sobreescrita 
				ImagePlus impFinal = IJ.openImage( dirOutput + currentcolorimg + ".bmp" ); 
				return impFinal.getProcessor();
	
	}

}



