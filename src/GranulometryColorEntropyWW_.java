import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.profe.plugins.RGBEntropyWWMax;
import morpho.color.jvazqnog.profe.plugins.RGBEntropyWWMin;


/**
 * Granulometria color, procesamiento vectorial fijando pesos con ENTROPIA SIN VENTANAS
 * 
 * */
public class GranulometryColorEntropyWW_ extends GranulometryBasic {
	
	
	/**
	 * Aplica las operaciones morfologicas 
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 
				String currentcolorimg =  imp.getShortTitle() + "_granENTRO" + "_s" + sizeSE  ;
					
				//erosion  (minimo)  
				RGBEntropyWWMin  entropyMin  = new RGBEntropyWWMin(currentcolorimg , dirOutput,
						"bmp", (ColorProcessor)  this.imp.getProcessor(), se );
				entropyMin.run();
			
				//dilatacion maximo,  
				ImagePlus impEro = IJ.openImage( dirOutput + currentcolorimg + ".bmp" );
				//se reemplaza la imagen a color actual erosionada con su dilatacion 
				RGBEntropyWWMax  entropyMax  = new RGBEntropyWWMax( currentcolorimg, dirOutput,
						"bmp", (ColorProcessor)  impEro.getProcessor(), se );
				entropyMax.run();
				
				//cargar en memoria la imagen sobreescrita 
				ImagePlus impFinal = IJ.openImage( dirOutput + currentcolorimg + ".bmp" ); 
				return impFinal.getProcessor();
	
	}

}



