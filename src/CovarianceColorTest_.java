import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import morpho.StructElementListPixels;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.profe.plugins.TesisRGBPromedioSinVentanasMinimoPlugin;


public class CovarianceColorTest_ implements PlugInFilter {
	ImagePlus imp ;
	
	int SE_SIZE = 0 ;
	int ANGLE = 0;
	int DISTANCE = 0 ;
	 
	public int setup(String arg, ImagePlus imp) {
		this.imp = imp ;
		
		return DOES_ALL;		
	}

	
	public void run(ImageProcessor orig) {
		
		guiParams() ;
		Pixel [] se = StructElementListPixels.getSESquare(SE_SIZE , DISTANCE, ANGLE);
		
		//erosion  (minimo)  
		String imgErod = imp.getShortTitle()+ "_covar" ;
		String dirOutput = "covarcolor/" ;
		TesisRGBPromedioSinVentanasMinimoPlugin  promMin  = new TesisRGBPromedioSinVentanasMinimoPlugin();
		promMin.setup("",this.imp );
		promMin.setOutput( imgErod , dirOutput);
		promMin.setSE( se );
		promMin.run(orig);	
	}
	
	public void test(ImageProcessor orig ){
		Pixel [] se = StructElementListPixels.getSESquare(2 , 1, 135);
		
		int u , v ;
		u=50; v=50;

		int WIDTH = orig.getWidth();
		int HEIGHT = orig.getHeight();

		
		// 
		for(Pixel t : se ){
			
			int px = t.getX() + u  ;
			
			int py = t.getY() + v ;
			
			if ( px < WIDTH && py < HEIGHT   && px > 0 && py > 0 )
				orig.set( u + t.getX(), v + t.getY() , 255 );
			
			
		}  
		
		return ;

	}
	
	public void guiParams(){
		GenericDialog gd = new  GenericDialog("Parametros de covarianza morfologica ");
		
		gd.addNumericField("SE_SIZE", SE_SIZE , 2);
		gd.addNumericField("ANGLE ", ANGLE , 2);
		gd.addNumericField("DISTANCE", DISTANCE , 2);
		 
		gd.showDialog();
	    if (gd.wasCanceled()) 
	    
	    SE_SIZE  = (int)gd.getNextNumber(); 
	    ANGLE  = (int)gd.getNextNumber(); 
	    DISTANCE = (int)gd.getNextNumber(); 
	 
	}


}



