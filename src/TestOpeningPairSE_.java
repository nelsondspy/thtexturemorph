import morpho.StructElement;
import morpho.gray.OperatorPairSE;
import ij.ImagePlus;
import ij.Macro;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;


public class TestOpeningPairSE_ implements PlugInFilter  {
	int sizeSE;
	int distance;
	int angle ;
	
	public int setup(String arg, ImagePlus imp) {
		dialog();
		
		return DOES_8G ;	
	}
	
	public void run(ImageProcessor orig) {
		
		int[][] filter2 =  StructElement.SEDisk( sizeSE ) ;
		
		OperatorPairSE.erode( orig , filter2 , distance , angle );
		
		ImageProcessor copy1 = orig.duplicate();
		ImagePlus imgse1 = new ImagePlus("im_copy1: " + distance + ",angle :" + angle , copy1 );
		imgse1.show();
		
	
		
		OperatorPairSE.dilate( orig , filter2 , distance , angle  );

		
		
	}
	
	private void dialog(){
		GenericDialog gd = new  GenericDialog("Parametros de covarianza morfologica ");
		
		gd.addNumericField("sizeSE", 0, 2);
		gd.addNumericField("distance", 1, 2);
		gd.addNumericField("angle ", 1, 2);
		
		 
		gd.showDialog();
	    if (gd.wasCanceled()) return ;
	    
	    sizeSE = (int)gd.getNextNumber();
		distance = (int )gd.getNextNumber();
		angle = (int )gd.getNextNumber();
		

	}
}
