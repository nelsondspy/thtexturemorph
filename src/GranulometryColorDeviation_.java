import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.profe.plugins.RGBDeviationMax;
import morpho.color.jvazqnog.profe.plugins.RGBDeviationMin;

/**
 * Granulometria color, procesamiento vectorial fijando pesos con DESVIACION CON VENTANAS
 * 
 * */

public class GranulometryColorDeviation_ extends GranulometryBasic {
	
	
	
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		
		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 
		String currentcolorimg =  imp.getShortTitle() + "_granDESV" + "_s" + sizeSE  ;
			
		//erosion  (minimo)  
		RGBDeviationMin  desvMin  = new RGBDeviationMin(currentcolorimg , dirOutput,
				"bmp", (ColorProcessor)  this.imp.getProcessor(), se, PRM.ROISIZE, PRM.ROISIZE);
		desvMin.run();
	
		//dilatacion maximo,  
		ImagePlus impEro = IJ.openImage( dirOutput + currentcolorimg + ".bmp" );
		//se reemplaza la imagen a color actual erosionada con su dilatacion 
		RGBDeviationMax  desvMax  = new RGBDeviationMax( currentcolorimg, dirOutput,
				"bmp", (ColorProcessor)  impEro.getProcessor(), se ,PRM.ROISIZE, PRM.ROISIZE);
		desvMax.run();
		
		//cargar en memoria la imagen sobreescrita 
		ImagePlus impFinal = IJ.openImage( dirOutput + currentcolorimg + ".bmp" ); 
		return impFinal.getProcessor();
		
	
	}

}



