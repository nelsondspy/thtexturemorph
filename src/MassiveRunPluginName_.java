import ij.*;  
import ij.plugin.PlugIn;  
import ij.gui.GenericDialog;


import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;  

import utils.ParamMng ;

/**
 * Ejecucion masiva multhilo , paralelizacion por particion de datos
 * -Cada hilo recibe la misma cantidad de imagenes a procesar. <br>
 * -Las Tareas de tamanho homogeneo <br>
 * 
 * */
public class MassiveRunPluginName_ implements PlugIn {
	
	/**
	 * @param Ruta completa con separador final "/path/"
	 *
	 * */
	public void run(String  args ){
		
		//para los tiempos de inicio y fin 
		long tiempoInicio = System.currentTimeMillis();
		long tiempoFin = 0;
		
		String [] p =  ParamMng.dialogGralMassiveRun();
		final String PATHDIR = p[ 2 ];
		final String experimentId = p[ 1 ];
		
		//nombre del plugin tal cual debe ser llamado  
		final String pluginName = p[ 3 ];
		
		
		/*un hilo por procesador disponible */
		int numProcessors  = Runtime.getRuntime().availableProcessors();
		final Thread[] threads = new Thread[ numProcessors ];
		
        final AtomicInteger ai = new AtomicInteger(0);
        
        final String [] lisfiles = listFilesinDir(PATHDIR) ; 
        
        //imagenes por procesador 
        final int nunImgforProc = lisfiles.length /  numProcessors;
        final int remanente = lisfiles.length % numProcessors ;
        
        
        	
        	
        for (int ithread = 0; ithread < threads.length; ithread++) {
        	
        	final int indexIni =  ithread   * nunImgforProc;
        	
        	int TempindexEnd  =  (ithread  * nunImgforProc) + nunImgforProc;
        	
        	final int thethread =  ithread ; 
        	
        	//si es el hilo con id final, asignarle los datos sobrantes 
        	if ( ithread == threads.length-1 ){
        		TempindexEnd  =  TempindexEnd + remanente ;
        	}
        	
        	final int indexEnd = TempindexEnd;
        	
        	threads[ithread] = new Thread() {
        		int idthread = thethread;	
        		
        		//constructor 
                { 
                	setPriority(Thread.NORM_PRIORITY); 
                }  
  
                public void run() {  
                	
                    for (int i = indexIni ; i < indexEnd ; i++){
                    	
                    	ImagePlus imp = IJ.openImage(PATHDIR + lisfiles[ i ]);  
                    	 
                    	IJ.run(imp, pluginName , experimentId );  
                    	IJ.run(imp,"Close","");
                    	
                    	System.out.println( idthread +":"+ lisfiles[ i ] );
                    	
                    	//ai.getAndIncrement();
                    }  
                }
            };
  
        }
        
        startAndJoin(threads);

    	 
		
        //tiempo de finalizacion
		long tiempoDelta  = ( System.currentTimeMillis()  - tiempoInicio) / (1000 * 60) ;
	
		String msg = "finalizado en minutos:" + tiempoDelta ;
        showDialog( msg );
        
        
        
	}
	
	public static void startAndJoin(Thread[] threads)  
    {  
        for (int ithread = 0; ithread < threads.length; ++ithread)  
        {  
            threads[ithread].setPriority(Thread.NORM_PRIORITY);  
            threads[ithread].start();  
        }  
  
        try  
        {     
            for (int ithread = 0; ithread < threads.length; ++ithread)  
                threads[ithread].join();  
        } catch (InterruptedException ie)  
        {  
            throw new RuntimeException(ie);  
        }  
    }
	
	/**
	 * 
	 * 
	 * */
	static String []  listFilesinDir(String path ){
		
		File folder = new File( path );
		File[] listOfFiles = folder.listFiles();
		
		String lsfilesNames []= new String[listOfFiles.length]; 

		    for (int i = 0; i < listOfFiles.length; i++) {
		      if (listOfFiles[i].isFile()) {
		        
		    	  lsfilesNames[i]= listOfFiles[i].getName();
		    	  
		      } else if (listOfFiles[i].isDirectory()) {
		        //System.out.println("Directory " + listOfFiles[i].getName());
		      }
		    }
		    return lsfilesNames  ; 
	}
	
	
	public void showDialog(String msg) {
        GenericDialog gd = new GenericDialog("Mensaje");
        gd.addMessage( msg );
        gd.showDialog();
        if (gd.wasCanceled()) {
            
        }
        
        return ;
    }

}
