
import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.implespesoventana.TesisRGBTmomentWW ;
import morpho.color.jvazqnog.models.Pixel;


/**
 * Granulometria color, procesamiento vectorial fijando pesos con ENTROPIA CON VENTANAS
 * 
 * */
public class GranulometryColorWWMoment_ extends GranulometryBasic {
	
	
	/**
	 * Aplica las operaciones morfologicas 
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		
			//----------------------------------
				
			String pathwork= "granulcolor_mw/images/";
				
			String nameImageinput =  pathwork + imp.getTitle() ;
			String nameImageOuput =  "_M=granWWMoment" +  "_s" + sizeSE  ;
				
			//erosion  (minimo)  
			TesisRGBTmomentWW momentMin = new TesisRGBTmomentWW("Min", nameImageinput , 
		               "bmp", (ColorProcessor)  this.imp.getProcessor(), se );
				
			momentMin.setFilterName(nameImageOuput);
			momentMin.run();
				
			//dilatacion 
			String iminput2 = pathwork + imp.getTitle() + nameImageOuput   +"Min" +  ".bmp" ;
			//abrir la imagen erosionada 
			ImagePlus impcurrent = IJ.openImage( iminput2 );
			
			TesisRGBTmomentWW momentMax = new  TesisRGBTmomentWW("Max", iminput2 , 
		                "bmp", (ColorProcessor)  impcurrent.getProcessor(), se );
				
			momentMax.setFilterName( "" );
				
			momentMax.run();
				
			ImagePlus impFinal = IJ.openImage( iminput2  +   "Max.bmp"  ); 
			return impFinal.getProcessor();
				
	}

}
