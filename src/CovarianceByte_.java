import clasiffier.DatabaseMng;
import clasiffier.DatabaseMng.TYPEDATA;
import ij.ImagePlus;
import ij.Macro;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import ij.gui.GenericDialog ;
import ij.gui.Plot ;
import morpho.StructElement;
import morpho.gray.* ;
import utils.ParamMng ;
import utils.MiscelaneousArray ;


/**
 * <p>Implementacion de covarianza morfologica
 * retorna una curva de distribución de orientacion,distancia y volumen .
 * Utiliza las orientaciones (grandos sexagesimales ):  0 , 45 , 90 , 135 
 *  </p>
 * @author nelsond
 * @param  CANT_DIST cantidad de distancias 
 * @param  INCREMENT_SIZE  tamanho del paso entre cada distancia    
 * @return retorna una curva de distribucion de orientacion.distancia mapeado a tamanho
 *  
 * */
public class CovarianceByte_ implements PlugInFilter {
	
	String macroOptions = null ;
	
	ImagePlus  imp  = null;
	
	/*parametros principales del algoritmo */
	
	/* cantidad de distancias a ser evaluadas */
    private int CANT_DIST = 10 ;
    
	/* paso entre las distancias si es para 2: 0, 2, 4, 6.. ; para 1: 0,1,2,3..    */
    private int INCREMENT_SIZE = 1;

	
	public int setup(String arg, ImagePlus imp) {
		this.imp = imp ;			
		
		
		macroOptions = Macro.getOptions();
		
		//si no reciben parametros entonces mostrar dialogbox
		if (macroOptions == null) {
			int [] p =  ParamMng.dialogCovariance();
			  this.CANT_DIST = p[0] ; 
			  this.INCREMENT_SIZE =  p[1] ; 
		}
		
		return DOES_8G; 
	
	}
	
	public void run(ImageProcessor orig) {	
	
		
		//cantidad de tamanhos de elementos estructurantes  
		int CANT_ANGLES = 4; 
		
		//grafico
		double[] plotx = new double[ CANT_DIST * CANT_ANGLES ];
		double[] ploty = new double[ CANT_DIST * CANT_ANGLES ];
				
		//lista de distancias 
		int [] listSizes = new int[CANT_DIST ];
		MiscelaneousArray.setSerialArray(listSizes, INCREMENT_SIZE, INCREMENT_SIZE);
		
		//lista de angulos 
		int [] angles = {0, 45, 90, 135};
		
		//volumen  de imagen de entrada
		double volOrig = getSumPixels( orig, 1);
		
		System.out.println("volOrig : " + volOrig );
		
		int auxi = 0 ; 
		
		//elemento estructurante de forma cuadrada 
		int[][] se = StructElement.SESquare ( 5 );
		
		//angulos X distancias 
		for(int a = 0; a< angles.length; a++){
			int angle = angles[ a ] ;
		
		for(int s  = 0; s < CANT_DIST ; s++ ){
			int distance = listSizes[s];
			
			ImageProcessor copy = orig.duplicate();
						
				/*
				int[][] se =  StrucElementPairs.SEPoint( distance ,angle );
				//solo erosion 
				Operator.erode(copy, se);		
				*/
				
			
				OperatorPairSE.erode(copy, se , distance , angle );
				
				
				double volumen = getSumPixels( copy , 1);
				
				System.out.println(distance + "\t" + volumen ) ;

				//plotx[auxi] = distance ;	
				plotx[auxi] = auxi ;	
				ploty[auxi] = volumen / volOrig ;

				auxi++;	 
			}
		
		}
		
		/*Generacion de archivos auxiliares de salida para el clasificador */
		//if (  macroOptions != null ){
		
			DatabaseMng dbmg = new DatabaseMng();
			dbmg.typedata= TYPEDATA.TRAIN;
			
			dbmg.inserTextureProp(imp.getTitle() ,  ploty ,macroOptions,-1,"");	
			 
			 
			 
		//}
		
		if ( macroOptions == null ) {	
			Plot covarPlot ;
			covarPlot = new Plot("","distancia","volumen",plotx, ploty); 
			covarPlot.show() ;
		}
		
		
	}
	
	
	/**
	 * 
	 * */
	public double  getSumPixels(ImageProcessor ip, double factornorm){
		int h = ip.getHeight();
		int w = ip.getWidth();
		double sum = 0 ;
		for(int v = 0 ; v< h ; v++){
			for(int u = 0 ; u< w ; u++){
				sum = sum + ( double )( ip.get(u,v) / factornorm );
			}	
		}
		return sum;
	}	

	
	
	
	
	
	
	
}
