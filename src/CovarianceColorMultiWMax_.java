import java.util.Arrays;
import java.util.List;

import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.implespesoventana.TesisRGBMax ;
/**
 * Covarianza morgologica usando pesos ENTROPIA CON VENTANAS 
 * */
public class CovarianceColorMultiWMax_ extends CovarianceBasic {
	
	
	/**
	 * */
	public ImageProcessor applyMorphoOperation(int distance, int orientation , Pixel [] se  ){
	
		List<Integer> windowList = Arrays.asList( 1 );
		//List<Integer> windowList = Arrays.asList( 2 );

		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 
		//String imgErod = imp.getShortTitle()+ "_M=covarMeanWindow" + "_dist="+distance  + "_angl="+ orientation+ "_ROI=" + PRM.ROISIZE ;
		String pathwork= "covarcolor_promv/images/";
		
		String nameImageinput =  pathwork + imp.getTitle() ;
		String nameImageOuput =  "_M=covarMultiWMin" + "_dist=" + distance  + "_angl="+ orientation+ "_ROI=" + PRM.ROISIZE;
		
		//erosion  (minimo)  
		TesisRGBMax maxMW = new TesisRGBMax("Min", nameImageinput , 
                "bmp", (ColorProcessor)  this.imp.getProcessor(), se, windowList );
		
		maxMW.setFilterName(nameImageOuput);
		
		maxMW.run();
		 
		//
		ImagePlus impFinal = IJ.openImage( pathwork + imp.getTitle() + nameImageOuput   +"Min" +  ".bmp" ); 
		return impFinal.getProcessor();
		
	}	

	
	
}
