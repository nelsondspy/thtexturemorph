import order.AlphaModulusLexicograph;
import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;


/**
 * Granulometria RGB, usando alpha-modulus-lexicografico 
 * 
 * */
public class GranulometryColorAlphaModuLex_ extends GranulometryBasic {
	
	public static int ALPHA = 10 ;
	
	/**
	 * Aplica las operaciones morfologicas 
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 
				String currentcolorimg =  imp.getShortTitle() + "_granLexic" + "_s" + sizeSE  ;
					
				AlphaModulusLexicograph lex = new AlphaModulusLexicograph();
				//
				lex.setAlpha(  ALPHA  );
				
				//erosion  (minimo) 
				lex.erode(this.imp.getProcessor(),se, dirOutput , currentcolorimg  );
				
				//dilatacion maximo, 
				ImagePlus impEro = IJ.openImage( dirOutput + currentcolorimg + ".bmp" );
				lex.dilate(impEro.getProcessor(),se, dirOutput , currentcolorimg  );
				
			
				
				//cargar en memoria la imagen sobreescrita 
				ImagePlus impFinal = IJ.openImage( dirOutput + currentcolorimg + ".bmp" ); 
				return impFinal.getProcessor();
	
	}

}



