import java.io.File;

import ij.ImagePlus;
import ij.io.FileSaver;
import ij.plugin.ChannelSplitter;
import ij.plugin.RGBStackMerge;
import ij.plugin.filter.PlugInFilter;
import order.OrderUtils;
import ij.io.FileSaver;
import ij.plugin.ChannelSplitter ;
import ij.plugin.ContrastEnhancer ;




import ij.ImagePlus;
import ij.Macro;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

public class EcualizacionMarginal_ implements PlugInFilter {
	ImagePlus  imp  = null;
	
	String outputdir = null ;
	
	public int setup(String arg, ImagePlus imp) {
		this.imp = imp ;
		
		outputdir = Macro.getOptions();
		if (outputdir == null ){
			outputdir = "equaliz/" ;
		}

		return DOES_ALL;

	}
	public void run(ImageProcessor imgproc) {	
	ImagePlus implus = new ImagePlus(  "", imgproc );
	ImagePlus[] implusRGB =  ChannelSplitter.split( implus) ;
	
	ContrastEnhancer eq = new ContrastEnhancer();

	eq.equalize(implusRGB[0].getProcessor());
	eq.equalize(implusRGB[1].getProcessor());
	eq.equalize(implusRGB[2].getProcessor());
	 
	
	//se vuelven a unir los canales , en una representacion plana
	ImagePlus implusFinal = RGBStackMerge.mergeChannels( implusRGB , false ).flatten();
	//implusFinal.show();
	
	File theDir = new File(outputdir);
	if (!theDir.exists()) {
	    try{ theDir.mkdir(); } 
	    catch(SecurityException se){
	    	
	    }        
	}
	
	FileSaver fs = new FileSaver( implusFinal );
	fs.saveAsBmp( outputdir + this.imp.getShortTitle() + ".bmp"  );
	
	}

}
