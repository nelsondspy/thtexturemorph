package clasiffier.pca;

public class OtherReductionMethods {
	
	
	
	/**
	 * Calcular un vector de caracteristicas donde para cada lista de tamanhos( columnas  ) se generan 
	 * dos descriptores, la desviacion estandar y el promedio  .  
	 * 
	 * @author nelsond
	 * @param matriz distribucion [ filas = cantidad de distancia-orientaciones] , 
	 * [ columnas =cant de tamanhos de elementos estructurantes] 
	 * @return vector de caracteristicas 
	 * 
	 * */
	public static double [] varianMedianVector(double [][] dist ){
		int numSizes = dist[0].length;
		int numDistanceOrient = dist.length;
		double [] finalCaracteristVector = new double[numDistanceOrient * 2 ];
		
		int auxIndex = 0  ;
		
		for( int d = 0 ; d< numDistanceOrient ; d++){
			
			double variance = 0;
			double mediana =  dist[d][(int ) numSizes/2 ];
			double avrg = 0 ;
			
			for( int s = 0 ; s< numSizes ; s++){
				avrg = dist[d][s] ;
			}
			
			avrg = avrg / numSizes;
			
			for( int s = 0 ; s< numSizes ; s++){
				variance = variance + Math.pow(dist[d][s] - avrg ,2) ;
			}
			
			variance = variance / (numSizes-1 );
			
			finalCaracteristVector[auxIndex] = variance ;
			auxIndex++;
			
			finalCaracteristVector[auxIndex] = mediana ;
			//finalCaracteristVector[auxIndex] = avrg  ( clasificacion inferior con respecto a la mediana ); 
			auxIndex++;
			
		}
		
		return finalCaracteristVector;
	}
	
	
	
	/**
	 * 
	 * 
	 * */
	public void entropia(){
		
	}
	
	
}
