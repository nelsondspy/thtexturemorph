package clasiffier.pca;


import weka.core.Instance ;
import weka.core.Instances ;
import weka.core.converters.ConverterUtils.DataSource ;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.PrincipalComponents ;
import weka.attributeSelection.AttributeSelection ;



/**
 * <p> Clase que lleva a cabo la reduccion de la matriz generado por el operador hibrido 
 * M * N a  M * 2  , donde M es la cantidad de distancias * orientacion y 
 * N es la cantidad de tamanhos que finalmente queda reducida a 2 
 * utilizando weka como libreria de soporte
 * </p>
 * @author nelsond
 * 
 * */
public class RunPCAUnsupervisedFilter {

  /**
   * Ejecuta la reduccion de dimensiones con PCA.
   * 
   * @param args args[0] archivo a ser procesado  
   * @throws Exception	
   */
    
  public static double [] reduce(String filedata ) throws Exception
  {
	  
	  final int MAX_ATTRIB = 2;

	  double [] planeArray = null ; 
		 
	// load data
	  Instances data = DataSource.read(filedata);
	  
	  PrincipalComponents pca = new PrincipalComponents();
	 
      pca.setInputFormat( data );
      pca.setVarianceCovered(0.95);
      //pca.setCenterData(true);
      pca.setMaximumAttributes( MAX_ATTRIB );
     
      Instances reduced = Filter.useFilter(data, pca);

      int reducedNumatt = reduced.instance(0).numAttributes() ;
      int numInstances = reduced.numInstances();
      planeArray = new double[ MAX_ATTRIB  * numInstances ];
      System.out.println( "reduced.instance(0).numAttributes():" + reducedNumatt );
      
      int aux = 0 ;
      for (int i = 0; i < numInstances ; i++) {
    	  	  
    	  Instance insI = reduced.instance(i) ;
    	  
    	  planeArray[ aux  ]= insI.value( 0 );
    	  aux ++;
    	  if (reducedNumatt == MAX_ATTRIB) {
    		  planeArray[ aux  ]= insI.value( 1 );
    	  }
    	  
    	  
    	  aux ++;
    	  
    	  //System.out.println( insI.value(0) + ","  + insI.value(1) );
    	  
      }
       /* 
        ArffSaver saver = new ArffSaver();
	    saver.setInstances(newData);
	    saver.setFile(new File("/home/nvdia/Ranked_pca.arff"));
	    saver.setDestination(new File("/home/nvdia/Ranked_pca.arff")); 
	    saver.writeBatch();
	    */
     return planeArray; 
  }
 
  
}
