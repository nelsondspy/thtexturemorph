package clasiffier;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


/**
 * Clase que parsea los archivos de texto de la base de datos de textura outex TC_0013
 * 
 * */

public class TextDataLoader {

	//datos de tipos de clases existentes 
	String [] classesNames;
	int [] classesIndexes ;
	byte [] classesMark  ;
	
	//datos del conjunto de entrenamiento o conjunto de pruebas 
	String [] texturesNames;
	int [] textureClassId ;
	
	/**
	 * @param  clasesfile 
	 * Formato de registro :  "nombreclase[espacio]indiceclase[espacio]marcador "
	 */
	public  void clasesLoader(String clasesfile ){
		BufferedReader br = null;
		
		try {
			String sCurrentLine;
			br = new BufferedReader(new FileReader( clasesfile ));
				
			//lee la primera linea que tiene la cantidad de registros 
			sCurrentLine = br.readLine();
			
			int totalclass = Integer.parseInt(sCurrentLine);
			
			classesNames = new String[totalclass]  ;
			classesIndexes = new int[totalclass]  ;
			classesMark = new byte [totalclass] ;
			
			int current = 0; 
					
			while ( (sCurrentLine = br.readLine()) != null ) {
				
				//mapea los campos separados con espacio 
				String[] tokens = sCurrentLine.split(" ");
				classesNames[ current ] = tokens[  0 ]; 
				classesIndexes[ current ] =  Integer.parseInt(tokens[  1 ]); 					
				classesMark[ current ] = Byte.parseByte(tokens[  2 ]);
				
				current++ ;
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	/**
	 * @author nelsond
	 * @param test or train file 
	 * Formato del registro : nombrearchivotexuta[espacio]clase 
	 * */
	public  void testOrTrainLoader(String testfile ){
		BufferedReader br = null;
		
		try {
			String sCurrentLine;
			br = new BufferedReader(new FileReader( testfile ));	
			//lee la primera linea que tiene la cantidad de registros 
			sCurrentLine = br.readLine();
			
			int totalTest = Integer.parseInt(sCurrentLine);
			
			texturesNames = new String[totalTest]  ;
			textureClassId = new int[totalTest]  ;
			
			int current = 0 ;
			
			System.out.println("archivo de : " +  totalTest + " tests");
			
			while ( (sCurrentLine = br.readLine()) != null ) {

				//parte importante que mapea los campos 
				String[] tokens = sCurrentLine.split(" ");
				texturesNames[ current ] = tokens[  0 ];   ;
				textureClassId[ current ] =  Integer.parseInt(tokens[  1 ]); 
				
				current ++ ;
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

}
