package clasiffier;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 * Clase que se encarga exportar al formato "arff" soportado por weka-3-6-13
 * */
public class ExportToARFF {
	
	
	/*input  */
	String relationName = "" ;  
	
	String generalAttribName = "" ;
	
	double[] listIndexattrib =null ;
	
	/*cantidad de atributos */
	int qAttributes = 0 ;  
	
	/*cantidad de clases */
	int qClasses  = 0 ; 
	
	String output ;
	
	/* propiedades   */
	
	
	private BufferedWriter writer = null;
	
	private File arffFile;
	private boolean fileExist = false;
	
	
	public ExportToARFF(String relationName, String generalAttribName, 
			int qAttributes  , String outputFile , int qClasses ){
		
		this.relationName = relationName ;
		this.generalAttribName = generalAttribName ;
		this.qAttributes  = qAttributes  ;
		this.output = outputFile;
		this.qClasses = qClasses ;
		
		createOrOpenFile();
		
	}
	
	/**
	 * Establece la cabecera: comentarios y los atributos 
	 * 
	 * */
	public void putHeader(){
		
		//if ( fileExist ) return ;
		
		writeFile( "% comentario \n@relation " + relationName+ "\n" ); 
		
		//lista de atributos que pueden ser tamanho/distancia-orientacion/
		writeFile( createStrAttrib( generalAttribName, qAttributes , qClasses)	); 
		
		writeFile("@data\n");
		
	}
	
	
	private String createStrAttrib(String name, int quantAttributes , int classCount){
		
		String attributes="" ;
		
		for(int i=0; i< quantAttributes ; i++  ){
			
			//@attribute nombre_1 numeric
			String atribute = "@attribute "+ name + "_" + i    + " numeric \n"  ;
			attributes = attributes + atribute ;
		}
		
		if (classCount > 0 ) {
			//ultimo atributo class a determinar por convencion 
			String classline = " {";
			for( int c = 0 ; c < classCount-1 ; c++ ){
				classline = classline + c +",";
			}			
			classline = classline  +  (classCount-1)  +"}";
			
			attributes = attributes +  "@attribute class " + classline  + " \n" ;		
		} 
		
		return attributes ;
		
	}
	

	
	public void insertDataRow(double [] listValuesIns ){
		
		String data =  String.valueOf(listValuesIns[0])  ;
		for(int i = 1 ; i< listValuesIns.length ; i++ ){
			data = data +","+ String.valueOf(listValuesIns[i])   ;
		}
		
		//
		writeFile(data  + "\n");
	}
	
	
	/**
	 *  
	 * */
	public void insertDataRow(double [] listValuesIns, double [] listValuesIns2 ){
		
		String data =  String.valueOf(listValuesIns[0])  ;
		for(int i = 1 ; i< listValuesIns.length ; i++ ){
			data = data +","+ String.valueOf(listValuesIns[i])   ;
		}
		
		if( listValuesIns2 != null ){
		
			data = data + "," +  String.valueOf(listValuesIns2[0])  ;
			
			for(int i = 1 ; i< listValuesIns2.length ; i++ ){
				data = data +","+ String.valueOf(listValuesIns2[i])   ;
			}	
		
		}
		//
		writeFile(data  + "\n");
	}

	
	public void insertDataRow(String row ){
		
		writeFile(row   + "\n");
	}

	
	/**
	 * 
	 * 
	 * */
	private void createOrOpenFile(){
		try {
		  arffFile = new File(output);
		  
		  fileExist = arffFile.exists();
		  
		  if( fileExist ) arffFile.delete();

		 arffFile.createNewFile();
		  
		  writer = new BufferedWriter(new FileWriter(arffFile));
		  
		  
	      
	    } catch (Exception e) {
	          e.printStackTrace();
	    } 
	}

	/**
	 * 
	 * */
	private void writeFile( String expr ){
		try {
				writer.write( expr );
		    } catch (Exception e) {
		          e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * */
	public void closeFile(){
		 try {
             // Close the writer regardless of what happens...
             writer.close();
         } catch (Exception e) {
        	 
         }
	}

}
