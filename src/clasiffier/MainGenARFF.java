package clasiffier;
import clasiffier.DatabaseMng;
import clasiffier.DatabaseMng.TYPEDATA;
import clasiffier.ExportToARFF;
import clasiffier.Texture_prop;

import java.util.List;


public class MainGenARFF {
	
	/**
	 * args[0] = experimentid convertido a numerico , identificador del experimento 
	 * args[1] = experimentid , tipo de conjunto de registros convertido a TYPEDATA o null
	 * args[2] = showid {0,1}, se convierte a booleano. y determina si mostrara o 
	 *      no el classid aun si el tipo es TEST 
	 * args[3] = cantidad de clases existentes  
	 * args[4] = base de datos de la textura
	 * 
	 * */
 	public static void main(String[] args ){
		
		
		TYPEDATA typeData ; // {TEST ,TRAIN} 
		String experimentId = null;
		typeData = null ;
		boolean showIdclass = false;
		int classesCount = 0 ;
		String database = null;
		//
		
		List<Texture_prop> listTextureprop;	
		
		/* carga  de parametros */
		experimentId  = args[0] ;
		
		//recibir un valor null indica que se utilizara todo el conjunto 
		if( args[1]  != null){
			typeData = TYPEDATA.TRAIN;
			if (! args[1].equals( TYPEDATA.TRAIN.toString()) ) {
				 typeData = TYPEDATA.TEST ;
			} 
		}  
		
		if( args[2]  == "1" ) showIdclass =  true ;
		
		classesCount = Integer.parseInt(args[ 3 ]); ;
		database = args[ 4 ];
		
		/*fin de lectura de parametros */
		
		DatabaseMng dmng = new DatabaseMng() ;
		dmng.setConnection() ;
		
		/*actualiza los tipos de imagenes , si son de entrenamiento o de test*/
		dmng.updateAllTypeIdclass(experimentId, database);
		
		/*recupera la lista de propiedades calculadas para una sesion de pruebas */
		listTextureprop = dmng.getResultTextProp( experimentId , typeData );
		int arrayLength = 0 ;
	
		//registro inicial con la cantidad de atributos 
		arrayLength = listTextureprop.get(0).array_length;		 
		
		ExportToARFF arffile = new ExportToARFF("dist_distanorient", "distanorient", 
				arrayLength, 
				experimentId + "_" + (typeData==null? "FULL" : typeData.toString()) +  ".arff" , 
				classesCount );
		
		arffile.putHeader();

		for (Texture_prop temp : listTextureprop) {
			if (typeData == null ){
				
				System.out.println("temp.testortrain :" + temp.testortrain);
				
				if ( showIdclass ){
					temp.array_properties = temp.array_properties  + temp.classid;
				}
				else if  (temp.testortrain.equals (TYPEDATA.TEST.toString())  ){
					temp.array_properties = temp.array_properties  +  "?" ;
				}
				else{
					temp.array_properties = temp.array_properties  + temp.classid;
				}
				
			}
			else if (typeData == TYPEDATA.TRAIN ){
				temp.array_properties = temp.array_properties  + temp.classid;
		    }
		    else{
		    	if ( showIdclass ){
					temp.array_properties = temp.array_properties  + temp.classid;
				}else{
					temp.array_properties = temp.array_properties  +  "?" ;
				}
		        
		    }  
			
			/*insercion de la fila en el archivo arff*/
			arffile.insertDataRow( "%"+ temp.name + ":\n" + temp.array_properties );
		
		}
		
		
		arffile.closeFile();  
		
	}
}
