package clasiffier;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class DatabaseMng {
	
	
	public enum TYPEDATA   {TRAIN, TEST};
	public TYPEDATA typedata = null ;
			
	Connection connection = null ;
	
	  public DatabaseMng(){
		  
		  
	  }
	
	  public void  setConnection (){
		 try {
			 
			//Class.forName("org.sqlite.JDBC");
			Class.forName("org.postgresql.Driver");
			
			
			//connection = DriverManager.getConnection( DATABASE_PATH );
			
			connection = DriverManager.getConnection(
					   "jdbc:postgresql://localhost:5432/texturec","postgres", "postgres");
			
			connection.setAutoCommit(true); 
		 
		 } 
	    catch ( Exception e ) {
	    	System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	    	System.exit(0);
	      }
	    
	    System.out.println("Opened database successfully");
	    
	  }
	  
	  
	  /**
	   * Agrega un registro en la tabla de propiedades calculadas (texture_prop ) 
	   * con algun metodo 
	   * 
	   * 
	   * */
	  public  void inserTextureProp(String texturename , double[] listIndexattrib ,
		String experimentid ,int classid, String params  ){ 
		  
		  setConnection ();
		  String attributes  = "";
		  
		  for(int i=0; i< listIndexattrib.length ; i++  ){  
				attributes = attributes +  listIndexattrib[ i ]  + ",";
		  }
		  try {
			 
			  String sql = "INSERT INTO texture_prop  (name, array_properties, classid, "
			  		+ " experimentid, testortrain , array_length , params ) " +
                   "VALUES (?, ? ,? , ? , ? , ?, ?);"; 
			  
			  PreparedStatement stmt = connection.prepareStatement( sql );
			  stmt.setString( 1, texturename);
			  stmt.setString( 2, attributes);
			  stmt.setInt( 3, classid);
			  stmt.setString( 4,experimentid );
			  stmt.setString( 5, typedata.toString() );
			  stmt.setInt( 6, listIndexattrib.length );
			  stmt.setString( 7, params );
			  
			  stmt.executeUpdate();  
			  stmt.close();
			  
			  connection.close();

		  }
		  catch(Exception e ){
			  System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		  }
	  }
	  
	 /**
	  * Actualiza los registros de entrenamiento de la tabla texture_prop con su identificador
	  * de clase correspondiente 
	  * 
	  * @author nelsond
	  * @param experimentid
	  * @param database 
	  * 
	  * */	  
	  public void updateAllTypeIdclass(String experimentid, String database ){
		  // la parte mbore de la cuestión es que.. el objetivo de la fuga("ir a jugar jueguito") era mucho menos espectacular que la fuga misma ..  
		  setConnection ();
		  
		  String sqltestortrain = "SELECT s.type FROM texture_sets AS s WHERE s.name = texture_prop.name AND s.database ='" + database + "' " ;
		  String sqlclassid =  "SELECT s.classid FROM texture_sets AS s WHERE s.name = texture_prop.name AND s.database ='" + database + "' ";
		  
		  String sql = 	"UPDATE texture_prop SET testortrain= (" +  sqltestortrain + ") ," +  
				 " classid  =  ( " + sqlclassid + ") WHERE TRIM(experimentid) = '" + experimentid + "' ;" ;
		  
		  try {
			  Statement stmt = connection.createStatement();
		      stmt.executeUpdate( sql );
		      stmt.close();
		      
		      connection.close();
		      
		  }
		  catch ( Exception e ) {
		    	System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		    	System.exit(0);
		  }
 
	  }
	  
	  /**
	   * Retorna la lista de texturas con sus propiedades segun un experimentid
	   *
	   * */
	  public List<Texture_prop> getResultTextProp(String experimentId, TYPEDATA typedata ) {
		  ResultSet rs = null ;
		  Texture_prop textp = null ;
		  List<Texture_prop > lisprop = new ArrayList<Texture_prop >();
		  String sql = "SELECT * FROM texture_prop"
		      		+ " WHERE trim(experimentid)='" + experimentId + "'";
		  
		  if (typedata != null){
			  sql = sql + " AND trim(testortrain) ='" + typedata.toString() + "';" ;
		  } 
		  else{
			  sql = sql + " ;";
		  }
		  
		  System.out.println("sql ="  + sql );
		  
		  try {
		  
		  setConnection ();	  
		  
		  Statement stmt = connection.createStatement();
		  stmt = connection.createStatement();
	      rs = stmt.executeQuery( sql );
	      
	      while ( rs.next() ) {
	    	  textp = new Texture_prop (); 
	    	  textp.classid = rs.getInt("classid");
	    	  textp.array_properties = rs.getString("array_properties");
	    	  textp.array_length = rs.getInt("array_length");
	    	  textp.testortrain = rs.getString("testortrain");
	    	  textp.name = rs.getString("name");
	    	  
	    	  lisprop.add(textp);
	      }
	      
	      stmt.close();
	      rs.close();
	      
	      connection.close();  
	      
		  }
		  catch ( Exception e ) {
		    	System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		    	System.exit(0);
		  }
		  return lisprop; 
	
	  }
	  
	  /**
	   * Crear base de datos en directorio actual de trabajo si no existe
	   * 
	   * */
	  public void createDB(){
		  
	  }
	  
	  /**
	   * consulta un solo registro de textura por su experimentid y su nombre de textura 
	   * */
	  public Texture_prop getOneTextProp(String experimentId,   String texturename) {
		  ResultSet rs = null ;
		  Texture_prop textp = null ;
		  String sql = "SELECT * FROM texture_prop"
		      		+ " WHERE trim(experimentid)='" + experimentId + "' AND " + 
		  				" name = '" + texturename +  "'  ; ";
		
		  try {
		  
		  setConnection ();	  
		  
		  Statement stmt = connection.createStatement();
		  stmt = connection.createStatement();
	      rs = stmt.executeQuery( sql );
	      
	      while ( rs.next() ) {
	    	  textp = new Texture_prop (); 
	    	  textp.classid = rs.getInt("classid");
	    	  textp.array_properties = rs.getString("array_properties");
	    	  textp.array_length = rs.getInt("array_length");
	    	  textp.testortrain = rs.getString("testortrain");
	    	  textp.name = rs.getString("name");
	    	  
	    	
	      }
	      
	      stmt.close();
	      rs.close();
	      
	      connection.close();  
	      
		  }
		  catch ( Exception e ) {
		    	System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		    	System.exit(0);
		  }
		  return textp; 
	
	  }

}
