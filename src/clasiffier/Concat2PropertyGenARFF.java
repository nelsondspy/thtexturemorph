package clasiffier;
import clasiffier.DatabaseMng;
import clasiffier.DatabaseMng.TYPEDATA;
import clasiffier.ExportToARFF;
import clasiffier.Texture_prop;

import java.util.List;


/**
 * Concatena dos arrays de caracteristicas 
 * 
 * */
public class Concat2PropertyGenARFF {
	
	/**
	 * args[0] = experimentid  identificador del experimento 
	 * args[1] =  typeData , tipo de conjunto de registros convertido a TYPEDATA o null, 
	 *           OSEA TEST O ENTRENAMIENTO
	 * args[2] = showid {0,1}, se convierte a booleano. y determina si mostrara o 
	 *      no el classid aun si el tipo es TEST 
	 * args[3] = cantidad de clases existentes  
	 * args[4] = experimentid  identificador del SEGUNDO experimento a concatenar
	 * 
	 * */
 	public static void main(String[] args ){
		
		
		TYPEDATA typeData ; // {TEST ,TRAIN} 
		typeData = null ;
		
		boolean showIdclass = false;
		
		int classesCount = 0 ;
		
		String experimentId = null;
		List<Texture_prop> listTextureprop;	
		
		/* carga  de parametros */
		if( args[0]  != null )  experimentId  = args[0] ;
		
	
		if( args[1]  != null){
			typeData = TYPEDATA.TRAIN;
			if (! args[1].equals( TYPEDATA.TRAIN.toString()) ) {
				 typeData = TYPEDATA.TEST ;
			} 
		}  
		
		if( args[2]  == "1" ) showIdclass =  true ;
		
		classesCount = Integer.parseInt(args[ 3 ]); 
		
		//segundo experimentid 
		String experimentIdTwo = args[ 4 ];
		
		/*fin de lectura de parametros */
		
		DatabaseMng dmng = new DatabaseMng() ;
		dmng.setConnection() ;
		
		/*actualiza los tipos de imagenes , si son de entrenamiento o de test*/
		
		//dmng.updateAllTypeIdclass( experimentid, String database);
		
		/*recupera la lista de propiedades calculadas para una sesion de pruebas */
		listTextureprop = dmng.getResultTextProp( experimentId , typeData );
		int arrayLength = 0 ;
	
		//registro inicial con la cantidad de atributos
		
		System.out.println("listTextureprop.get(0).name:" + listTextureprop.get(0).name);
		int arrayLengtExp2 = (dmng.getOneTextProp(experimentIdTwo,
				listTextureprop.get(0).name)).array_length;
		
		
		//se suman ambas cantidades de atributos 
		arrayLength = listTextureprop.get(0).array_length + arrayLengtExp2 ;	
		
		
		
		ExportToARFF arffile = new ExportToARFF("dist_distanorient", "distanorient", 
				arrayLength, 
				experimentId + "+" + experimentIdTwo + "_" + (typeData==null? "FULL" : typeData.toString()) +  ".arff" , 
				classesCount );
		
		arffile.putHeader();

		for (Texture_prop temp : listTextureprop) {
			//por cada textura se busca la misma textura 
			Texture_prop tempTwo = dmng.getOneTextProp(experimentIdTwo, temp.name);
			
			if (tempTwo== null ){
				System.out.println("tempTwo es nulo para: " + temp.name );
				break;
			}
			
			if (typeData == null ){
				
				System.out.println("temp.testortrain :" + temp.testortrain);
				
				if ( showIdclass ){
					temp.array_properties = temp.array_properties  + tempTwo.array_properties  + temp.classid;
				}
				else if  (temp.testortrain.equals (TYPEDATA.TEST.toString())  ){
					temp.array_properties = temp.array_properties  + tempTwo.array_properties +  "?" ;
				}
				else{
					temp.array_properties = temp.array_properties  + tempTwo.array_properties + temp.classid;
				}
				
			}
			else if (typeData == TYPEDATA.TRAIN ){
				temp.array_properties = temp.array_properties   + tempTwo.array_properties  + temp.classid;
		    }
		    else{
		    	if ( showIdclass ){
					temp.array_properties = temp.array_properties  + tempTwo.array_properties  +  temp.classid;
				}else{
					temp.array_properties = temp.array_properties + tempTwo.array_properties  +  "?" ;
				}
		        
		    }  
			
			/*insercion de la fila en el archivo arff*/
			arffile.insertDataRow( "%"+ temp.name + ":\n" + temp.array_properties );
		
		}
		
		
		arffile.closeFile();  
		
	}
}
