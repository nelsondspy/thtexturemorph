import order.HSILex;
import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.profe.plugins.RGBEntropyMax;
import morpho.color.jvazqnog.profe.plugins.RGBEntropyMin;


/**
 * Granulometria color, procesamiento vectorial fijando pesos con ENTROPIA CON VENTANAS
 * 
 * */
public class GranulometryColorLexicHSI_ extends GranulometryBasicStack {
	
	String colorSpace =  "HSI";

	
	/**
	 * Aplica las operaciones morfologicas 
	 * 
	 * */
	public ImagePlus applyMorphoOperation(int sizeSE , Pixel [] se ){
		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 
				String currentcolorimg =  imp.getShortTitle() + "_granLexic" + "_s" + sizeSE  ;
				
				//erosion  (minimo) 
				HSILex lexHSI1 = new HSILex();
				lexHSI1.setImageProcessor(this.imp.getProcessor());
				ImagePlus eroded = lexHSI1.filter( se, dirOutput , currentcolorimg , HSILex.filterType.MIN );
				
				//dilatacion (maximo) 
				HSILex lexHSI2 = new HSILex();
				lexHSI2.setImagePlus(eroded); //cuidado que ahora es un stack en un imageplus 
				ImagePlus dilated =lexHSI2.filter( se, dirOutput , currentcolorimg , HSILex.filterType.MAX );
			
				return dilated;
	
	}


	@Override
	public String getColorspace() {
		// TODO Auto-generated method stub
		return "HSI";
	}

}



