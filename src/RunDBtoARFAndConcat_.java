import ij.Macro;
import ij.gui.GenericDialog;
import clasiffier.Concat2PropertyGenARFF;
import ij.plugin.PlugIn;

/**
 * Concatena dos experimentos , utilizando el id de cada experimento.<br>
 * Para cada instancia de clase el vector de caracteristicas está dado por :
 * I = [ EXP1 , EXP2] , 
 * donde EXP1: vector de caracteristicas de la instancia del experimento 1  <br>, 
 *       EXP2 :vector de caracteristicas de la instancia del experimento 1
 *       
 * 
 * */
public class RunDBtoARFAndConcat_ implements PlugIn {
		
		String experimento1 = null ;
		String experimento2 = null ;		
		
		String [] p ;
		
		public void run( String arg) {
			if (Macro.getOptions() != null ){
				
				p = Macro.getOptions().split(" ");
			}
			else{
				p = dialogExperimentIds() ;
			}
				
			String experimentId =  p[ 0 ].trim() ;
			String experimentIdTwo =  p[ 1 ].trim() ;
			
			
			String [] args = {experimentId , null  , "0", "68", experimentIdTwo};
			Concat2PropertyGenARFF.main(args);
			
			String [] args2 = {experimentId , "TEST"  , "1", "68", experimentIdTwo};
			Concat2PropertyGenARFF.main(args2);
			
		}
		
		
		
		public static String [] dialogExperimentIds(){
				String [] p = {"",""};
				
				GenericDialog gd = new  GenericDialog("Concatenar arrays de propiedades y generar archivos arff del experimentId)");
				
				gd.addMessage("Generar archivos arff de entrenamiento y test del experimentId \n "
						+ "El experimentId: Es el codigo del conjunto de datos generados y almacenados en la base de datos ");
				
				gd.addStringField("Experimentid","",30);
				gd.addStringField("Experimentid 2 ","",30);
				
				gd.showDialog();
				
				if (gd.wasCanceled()) return p;
			    
			    p[ 0 ] = gd.getNextString(); 
			    
			    p[ 1 ] = gd.getNextString(); 
				return p ;

		}
		
		
	}


