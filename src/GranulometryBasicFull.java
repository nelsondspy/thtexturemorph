

import utils.FilesMngIJ;
import utils.MiscelaneousArray;
import utils.ParamGranulometry;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.util.Various;
import ij.gui.Plot;
import ij.io.FileSaver;
import ij.Macro;

import java.io.File;

import clasiffier.DatabaseMng;
import clasiffier.DatabaseMng.TYPEDATA;
import descriptor.Descriptor;
import descriptor.Util;

/**
 * A Diferencia de GranulometryBasic , esta clase si contempla la aplicacion
 * de dos filtros morfológicos , tanto para la apertura y el cierre permitiendo 
 * dar soporte a la Antigranulometria
 * */
public class GranulometryBasicFull  implements PlugInFilter {
	
	/** parametros de la granulometria */
	ParamGranulometry PRM ; 
    
	/** otros parametros */
	ImagePlus  imp  = null;
	
	/** opciones recibidas desde algun macro o ejecucion masiva */
	String macroOptions = null;
	
	/** path de salida debe incluir barra separadora al final */
	String dirOutput = "granulcolor/";
	
	/** tipo de descriptor para cada elemento de la serie */
	Descriptor DESCRIPTOR = null ; 
	
	
	
	/**
	 * Metodo de PlugInFilter 
	 * */
	public int setup(String arg, ImagePlus imp) {
		this.imp = imp ;			
		
		macroOptions = Macro.getOptions();
		
		PRM = new ParamGranulometry();
		
		
		if (macroOptions == null ){
			PRM.setParamFromGUI();
		} 
		else{
			/*lectura de parametros */
			PRM.setParamFromString( macroOptions );			
			
		}
		
		//crea una carpeta con el mismo nombre del id del experimento 
		dirOutput = PRM.experimentId.trim() + File.separator;
		
		System.out.println("dirOutput : " + dirOutput);
		
		FilesMngIJ.createDirIfNoExist(PRM.experimentId.trim());

		//instancia el descriptor 
		DESCRIPTOR = Util.getDescriptorInstance( PRM.getDESCRIPTOR_NAME() ) ;
		
		//considerando que el volumen primer elemento la imagen sin alterar 
		DESCRIPTOR.setDim( 2 * PRM.MAXSIZE_SE  );
		
		return DOES_ALL ;	
	}
	
	
	public void run(ImageProcessor orig) {
		
		int [] listSizes = new int[PRM.MAXSIZE_SE] ;
		MiscelaneousArray.setSerialArray(listSizes, 1 , PRM.STEPSIZE_SE ) ;

		//grafico para granulometria 
		double[] granPlotx = new double[ PRM.MAXSIZE_SE ] ;
		double[] granPloty = new double[ PRM.MAXSIZE_SE ] ;
		
		
		int aux=0 ; 
		
		//imagen anterior en la serie diferencial 
		ImageProcessor copyback = orig.duplicate(); 
		
		/*primer elemento de la serie morfologica */
		/*  agregar al vector del descriptor */
		DESCRIPTOR.calculateAndAdd( copyback ) ;
		
		//guarda la imagen original en el directorio de salida porque tambien forma
		//parte de la serie
		FileSaver fs = new FileSaver(imp);
		fs.saveAsBmp(dirOutput + imp.getShortTitle()+"_"+String.format("%04d", 0) +".bmp"  );
		
		for(int size = 1; size < listSizes.length ; size++ ){
			
			int sizeSE = listSizes[size];
			
			Pixel[] se = Various.getShiftArray( sizeSE );
			
			ImageProcessor resultProcElement1 = applyMorphoOperation(sizeSE, se);
			
			ImageProcessor resultProcElement2 = applyMorphoOperation2(sizeSE, se);
			//-----------------
			DESCRIPTOR.calculateAndAdd( resultProcElement1 ) ;	
			
			DESCRIPTOR.calculateAndAdd( resultProcElement2 ) ;
			//todos los ejes x correponden al tamanho del ES 
			
			granPlotx[aux] = size;
						
			aux++;	
					
		}
		
		/*es necesario llamar el metodo para normaliza o 
		 * realizar alguna operacion especifica descriptor  */
		DESCRIPTOR.postProcess();
		
		
		if ( macroOptions == null ) {
			//grafica de granulometria 
			Plot granPlot ;
			granPlot = new Plot("Granulometria","size SE","vol",granPlotx, granPloty); 
			granPlot.show() ;		
		}
			
		
	/*--- almacenar vector de caracteristicas */
		
	/*Insercion */
	DatabaseMng dbmg = new DatabaseMng();
	dbmg.typedata= TYPEDATA.TRAIN;
	dbmg.inserTextureProp( imp.getTitle(), DESCRIPTOR.getVectorDescriptor() , PRM.getExperimentId() , -1, macroOptions );	
			
	/*-- fin almacenar */
		
		
		
	}
	
	
	/**
	 * Internamente el metodo debe retornar el ImageProcessor que sea compatible 
	 * con el metodo que calcula el descriptor 
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		ImageProcessor salida = null ; 
		
		return salida;

	}
	
	/**
	 * Internamente el metodo debe retornar el ImageProcessor que sea compatible 
	 * con el metodo que calcula el descriptor 
	 * 
	 * */
	public ImageProcessor applyMorphoOperation2(int sizeSE , Pixel [] se ){
		ImageProcessor salida = null ; 
		
		return salida;

	}
	
	
	
	
	
}
