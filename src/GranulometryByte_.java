import utils.MiscelaneousArray;
import utils.ParamMng;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import ij.process.Blitter;
import morpho.StatisticalMoments;
import morpho.StructElement;
import morpho.gray.*;
import ij.gui.Plot;
import ij.Macro;
import clasiffier.DatabaseMng;
import clasiffier.DatabaseMng.TYPEDATA;

import utils.FilesMngIJ;


/**
 * Granulometria en escala de grises 
 * con elemento estructurante con forma de disco<br>
 * -genera el grafico del patron del especto <br>
 * -genera el grafico de la granulometria normalizada <br>
 * -genera el grafico de la funcion de distribucion acumulativa <br>
 * */

public class GranulometryByte_ implements PlugInFilter {
	
	//parametros del algoritmo 
	int MAXSIZE_SE = 25 ;
	int STEPSIZE_SE = 1 ;
	
	//otros parametros 
	ImagePlus  imp  = null;
	String macroOptions = null;
	
	
	
	public int setup(String arg, ImagePlus imp) {
		this.imp = imp ;			
		
		macroOptions = Macro.getOptions();
		
		if (macroOptions == null ){
			int [] p = ParamMng.dialogGranulometry();
			MAXSIZE_SE = p[0] ;
			STEPSIZE_SE = p[1] ;
		} 
		
		return DOES_8G ;	
	}
	
	
	
	public void run(ImageProcessor orig) {
		
		int [] listSizes = new int[MAXSIZE_SE];
		MiscelaneousArray.setSerialArray(listSizes, STEPSIZE_SE , STEPSIZE_SE );

		//volumen de la imagen original 
		double volumeOrig = StatisticalMoments.calcVolumen( orig );
		
		//grafico para la funcion acumulativa de probabilidad  
		double[] fprobPlotx = new double[ MAXSIZE_SE ];
		double[] fprobPloty = new double[ MAXSIZE_SE ];
		
		//grafico para granulometria 
		double[] granPlotx = new double[ MAXSIZE_SE ];
		double[] granPloty = new double[ MAXSIZE_SE ];
		
		//grafico para el patron espectro ( serie diferencial )
		double[] diffPlotx = new double[ MAXSIZE_SE ];
		double[] diffPloty = new double[ MAXSIZE_SE ];
			
		System.out.println("volumeOrig :" + volumeOrig );
		
		int aux=0 ; 
		
		//imagen anterior en la serie diferencial 
		ImageProcessor copyback = orig.duplicate(); 
		
		for(int size = 0; size < listSizes.length ; size++ ){
			
			int sizeSE = listSizes[size];
			//duplicar la imagen para no alterar la imagen original 
			ImageProcessor copy = orig.duplicate();
				
			//crear el elemento estructurante
			int[][] se =  StructElement.SEDisk( sizeSE );
			
			//apertura 
			Operator.erode(copy, se);		
			Operator.dilate(copy, se);	
			
			double volumen = StatisticalMoments.calcVolumen( copy  );
			
			//muestra la imagen actual de la serie
			/*String title = "im_copy: " + size ;
			ImagePlus imgse = new ImagePlus(title, copy);
			imgse.show();
			*/
			System.out.println(size + "\t" + volumen ) ;
			
			//todos los ejes x correponden al tamanho del ES 
			fprobPlotx[aux] = size; 
			granPlotx[aux] = size;
			diffPlotx[aux] = size;
			
			//para la funcion de probabilidad 
			fprobPloty[aux] =  1 - ( volumen / volumeOrig ) ;
			
			//definicion de granulometria 
			granPloty [aux] = volumen / volumeOrig ;
			
			// patron espectro normalizado para que sea independiente al tamanho de la imagen
			copyback.copyBits(copy, 0, 0, Blitter.DIFFERENCE); 
			volumen = StatisticalMoments.calcVolumen( copyback  ); 
			diffPloty[aux] = volumen  /  volumeOrig ;
			
			aux++;	
			
			//almacenar la imagen actual, 
			FilesMngIJ.saveBMP(copy, "imgranul/", imp.getShortTitle() +"_SIZE_" + sizeSE );
			
			copyback = copy.duplicate();
	
		}
		
		
		if ( macroOptions == null ) {
			//grafica de granulometria 
			Plot granPlot ;
			granPlot = new Plot("Granulometria","size SE","vol",granPlotx, granPloty); 
			granPlot.show() ;	
			
			//grafica de funcion de probabilidad 
			Plot fptobPlot ;
			fptobPlot = new Plot("funcion de probabilidad acumulativa","size SE","vol",fprobPlotx, fprobPloty); 
			fptobPlot.show() ;	
		
			//grafica de patron espectro 
			Plot diffPlot ;
			diffPlot = new Plot("patron espectro","size SE","vol",diffPlotx, diffPloty); 
			diffPlot.show() ;
		}
			
		System.out.println("imp.getShortTitle() : "  + imp.getShortTitle() );	
		
		System.out.println( "Macro.getOptions():" + macroOptions );

		
		//if ( macroOptions != null ) {
			String params = "cantsize=" + MAXSIZE_SE + ",stepsize=" + STEPSIZE_SE;
			/*Insercion */
			DatabaseMng dbmg = new DatabaseMng();
			dbmg.typedata= TYPEDATA.TRAIN;
			dbmg.inserTextureProp(imp.getTitle() ,  granPloty ,macroOptions,-1, params);			
		//}
		
	}
	
	
	
	
	
	
}
