
//macro que concatena experimentos por su id 

run( "RunDBtoARFAndConcat " , "O14tl84_CovNorm_VolumenRGB_2 O14tl84_CovNorm_Variance2");
run( "RunDBtoARFAndConcat " , "O14tl84_CovNorm_VolumenRGB_2 O14tl84_CovNorm_Energy");
run( "RunDBtoARFAndConcat " , "O14tl84_CovNorm_VolumenRGB_2 O14tl84_CovNorm_Entropy");
run( "RunDBtoARFAndConcat " , "O14tl84_CovNorm_VolumenRGB_2 O14tl84_CovNorm_VolumenX3");
run( "RunDBtoARFAndConcat " , "O14tl84_CovNorm_VolumenRGB_2 O14tl84_CovNorm_VectorRGBDistEuclid2");
run( "RunDBtoARFAndConcat " , "O14tl84_CovNorm_VolumenRGB_2 O14tl84_CovNorm_VectorRGBDistMahaK1");
run( "RunDBtoARFAndConcat " , "O14tl84_CovNorm_VolumenRGB_2 O14tl84_CovNorm_VectorRGBPorcent2");

//-------------------
run( "RunDBtoARFAndConcat " , "O14tl84_AntiGran_VolumenRGB_S30 O14tl84_AntiGran_VolumenRGB_S30_Variance2");
run( "RunDBtoARFAndConcat " , "O14tl84_AntiGran_VolumenRGB_S30 O14tl84_AntiGran_VolumenRGB_S30_Energy");
run( "RunDBtoARFAndConcat " , "O14tl84_AntiGran_VolumenRGB_S30 O14tl84_AntiGran_VolumenRGB_S30_Entropy");
run( "RunDBtoARFAndConcat " , "O14tl84_AntiGran_VolumenRGB_S30 O14tl84_AntiGran_VolumenRGB_S30_VolumenX3");
run( "RunDBtoARFAndConcat " , "O14tl84_AntiGran_VolumenRGB_S30 O14tl84_AntiGran_VectorRGBDistMahaK1");

