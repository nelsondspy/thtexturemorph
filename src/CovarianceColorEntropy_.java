import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import morpho.color.jvazqnog.profe.plugins.RGBEntropyMin;

/**
 * Covarianza morgologica usando pesos ENTROPIA CON VENTANAS 
 * */
public class CovarianceColorEntropy_ extends CovarianceBasic {
	
	
	/**
	 * Entropia con ventanas  
	 * */
	public ImageProcessor applyMorphoOperation(int distance, int orientation , Pixel [] se  ){
	
		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 
		String imgErod = imp.getShortTitle()+ "_M=covarEntro" + "_dist="+distance  + "_angl="+ orientation+ "_ROI=" + PRM.ROISIZE ;
	
		//erosion  (minimo)  
		RGBEntropyMin  entropyMin  = new RGBEntropyMin(imgErod , dirOutput,
				"bmp", (ColorProcessor)  this.imp.getProcessor(), se, PRM.ROISIZE, PRM.ROISIZE);
		entropyMin.run();

		//
		ImagePlus impFinal = IJ.openImage( dirOutput + imgErod + ".bmp" ); 
		return impFinal.getProcessor();
		
	}	

	
	
}
