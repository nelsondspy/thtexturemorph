import java.util.Arrays;
import java.util.List;

import ij.IJ;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.implespesoventana.TesisRGBEnergy;
import morpho.color.jvazqnog.models.Pixel;


/**
 * Granulometria color, procesamiento vectorial fijando pesos con la moda 
 * 
 * */
public class GranulometryColorMultiWEnergy_ extends GranulometryBasic {
	
	
	/**
	 * Aplica las operaciones morfologicas 
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		
			//----------------------------------
			
			List<Integer> windowList = Arrays.asList(2, 4, 8);
			
			//List<Integer> windowList = Arrays.asList( 2 );

				
			String pathwork= "granulcolor_mw/images/";
				
			String nameImageinput =  pathwork + imp.getTitle() ;
			String nameImageOuput =  "_M=granMultWinEnergy" +  "_s" + sizeSE  ;
				
			//erosion  (minimo)  
			TesisRGBEnergy energyMin = new TesisRGBEnergy("Min", nameImageinput , 
		               "bmp", (ColorProcessor)  this.imp.getProcessor(), se, windowList );
				
			energyMin.setFilterName(nameImageOuput);
			energyMin.run();
				
			//dilatacion 
			String iminput2 = pathwork + imp.getTitle() + nameImageOuput   +"Min" +  ".bmp" ;
			//abrir la imagen erosionada 
			ImagePlus impcurrent = IJ.openImage( iminput2 );
			
			TesisRGBEnergy energyMax = new  TesisRGBEnergy("Max", iminput2 , 
		                "bmp", (ColorProcessor)  impcurrent.getProcessor(), se, windowList );
				
			energyMax.setFilterName( "" );
				
			energyMax.run();
				
			ImagePlus impFinal = IJ.openImage( iminput2  +   "Max.bmp"  ); 
			
			return impFinal.getProcessor();
				
	}

}
