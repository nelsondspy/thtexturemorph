
import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;
import order.Marginal;

/**
 * Covarianza morgologica usando pesos ENTROPIA CON VENTANAS 
 * */
public class CovarianceColorMarginal_ extends CovarianceBasic {
	
	
	/**
	 * covarianza M. usando Bitmixing  
	 * */
	public ImageProcessor applyMorphoOperation(int distance, int orientation , Pixel [] se  ){

		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 

		//String imgErod = imp.getShortTitle()+ "_M=covarMarginal" + "_dist="+distance  + "_angl="+ orientation+ "_ROI=" + PRM.ROISIZE ;

		//nombre de archivo de longitud fija tal que al ordenar queda ordenada por orientacion y distancia
		String imgErod = imp.getShortTitle() + "_" + String.format("%04d",orientation) + "_"+ String.format("%04d",distance)   ;

		Marginal marginal = new Marginal();
		marginal.erode(this.imp.getProcessor(),se, dirOutput , imgErod );
		
		//
		ImagePlus impFinal = IJ.openImage( dirOutput + imgErod + ".bmp" ); 
		return impFinal.getProcessor();
		
	}	

	
	
}
