import order.NormalEuclid;
import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import morpho.color.jvazqnog.models.Pixel;


/**
 * Granulometria color, procesamiento vectorial fijando pesos con ENTROPIA CON VENTANAS
 * 
 * */
public class GranulometryColorNormalEuclid_ extends GranulometryBasic {
	
	
	/**
	 * Aplica las operaciones morfologicas 
	 * 
	 * */
	public ImageProcessor applyMorphoOperation(int sizeSE , Pixel [] se ){
		//nombres de archivo de salida de la imagen erosionada y dilatada , sin la extension 
				String currentcolorimg =  imp.getShortTitle() + "_granEuclid" + "_s" + sizeSE  ;
					
				NormalEuclid normeuclid = new NormalEuclid();
				
				//erosion  (minimo) 
				normeuclid.erode(this.imp.getProcessor(),se, dirOutput , currentcolorimg  );
				
				//dilatacion maximo, 
				ImagePlus impEro = IJ.openImage( dirOutput + currentcolorimg + ".bmp" );
				normeuclid.dilate(impEro.getProcessor(),se, dirOutput , currentcolorimg  );
				
				//cargar en memoria la imagen sobreescrita 
				ImagePlus impFinal = IJ.openImage( dirOutput + currentcolorimg + ".bmp" ); 
				return impFinal.getProcessor();
	
	}

}



