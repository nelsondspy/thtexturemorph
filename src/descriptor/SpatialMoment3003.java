package descriptor;

import ij.process.ImageProcessor;

/**
 * Momento espacial con los parametros preestablecidos segun aptoula con los que se consiguen
 * mejores resultados en la clasificacion
 * para cada canal se calcula el momento espacial con 0,3 3,0  
 * 
 * */
public class SpatialMoment3003 extends Descriptor{
	
		public SpatialMoment3003(){
			super();
			
			//3 valores para cada elemento de la serie
			this.setVALUES_F_ELEMENT( 9 );
			
		}

				
		@Override
		public double[] calculate(ImageProcessor ip ) {
			
			
			//vector resultado 
			double [] result_RGB = new double[ VALUES_F_ELEMENT ]; 

		
			//asegura que sea de tres canales 
			if(ip.getNChannels() <  3 ) {
				new Exception("Error , la imagen debe ser RGB");
			}
			
			SpatialMomentBasic spatialMoment = new SpatialMomentBasic();
			
		
			int index = 0 ;
			//realiza el procesamiento por canal 
			
			//0,1
			spatialMoment.setI( 0 );
			spatialMoment.setJ( 3 );
			double [] momentRGB_A03 =  spatialMoment.calculate( ip );

			result_RGB[ index++ ] = momentRGB_A03[0];
			result_RGB[ index++ ] = momentRGB_A03[1];
			result_RGB[ index++ ] = momentRGB_A03[2];

			// 3,0
			spatialMoment.setI( 3 );
			spatialMoment.setJ( 0 );
			double [] momentRGB_B30 = spatialMoment.calculate( ip );

			result_RGB[index++] = momentRGB_B30[0];
			result_RGB[index++] = momentRGB_B30[1];
			result_RGB[index++] = momentRGB_B30[2];

			//0,0
			spatialMoment.setI( 0 );
			spatialMoment.setJ( 0 );
			double [] momentRGB_B00 = spatialMoment.calculate( ip );

			result_RGB[index++] = momentRGB_B00[0];
			result_RGB[index++] = momentRGB_B00[1];
			result_RGB[index++] = momentRGB_B00[2];
						
			return result_RGB ;
			
		}


		@Override
		public void setLocalParameters() {
			// TODO Auto-generated method stub
			
		}


		@Override
		public void postProcess() {
			// TODO Auto-generated method stub
			double [] originalValues = new double[VALUES_F_ELEMENT ]; 
			
			
			for (int i = 0 ; i < VALUES_F_ELEMENT; i++ ){
				originalValues[ i ] = vectorDescriptor[ i ];
			}
			
			//normalizar con los primeros 9 elementos 
			int i = 0 ;
			
			while(i  <  vectorDescriptor.length ){
				
				vectorDescriptor[ i ] = vectorDescriptor[ i ] / originalValues[0]; 
				i++ ;
				
				vectorDescriptor[ i  ] = vectorDescriptor[ i ] / originalValues[1]   ;
				i++ ;
				
				vectorDescriptor[ i  ] = vectorDescriptor[ i ] / originalValues[2]  ;
				i++ ;
	
				vectorDescriptor[ i  ] = vectorDescriptor[ i ] / originalValues[3]  ;
				i++ ;
				
				vectorDescriptor[ i  ] = vectorDescriptor[ i ] / originalValues[4]  ;
				i++ ;
	
				vectorDescriptor[ i  ] = vectorDescriptor[ i ] / originalValues[5]  ;
				i++ ;
	
				vectorDescriptor[ i  ] = vectorDescriptor[ i ] / originalValues[6]  ;
				i++ ;
	
				vectorDescriptor[ i  ] = vectorDescriptor[ i ] / originalValues[7]  ;
				i++ ;
				
				vectorDescriptor[ i  ] = vectorDescriptor[ i ] / originalValues[8]  ;
				i++ ;
			}
			
			
		}



		

}
