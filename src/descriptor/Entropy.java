package descriptor;

import ij.ImagePlus;
import ij.plugin.ChannelSplitter;
import ij.process.ByteProcessor;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import ij.process.ImageStatistics;

/**
 * Requiere que la imagen sea RGB , sino lanza una excepcion 
 * @author nelsond
 * @param image procesor RGB 32 bits 
 * @return entropia en cada canal, no requiere normalizacion
 * */


public class Entropy extends Descriptor{

		
	public Entropy(){
		
		super();
		
		//3 valores para cada elemento de la serie, uno para cada canal 
		this.setVALUES_F_ELEMENT( 3 );
		
	}


	@Override
	public double[] calculate(ImageProcessor ip ) {
		
		
		//asegura que NO sea de un solo canal 
		if(ip.getNChannels() <  3 ) {
			new Exception("Error , la imagen debe ser RGB");
		}
	
	
		double [] result_entr_RGB = new double[ 3 ];
		
	    double [] rgbentrop = entropyCalc((ColorProcessor) ip);
		
		result_entr_RGB[0 ] = rgbentrop[ 0 ];
		result_entr_RGB[1 ] = rgbentrop[ 1 ] ;	
		result_entr_RGB[2 ] = rgbentrop[ 2 ]; 
		
		return result_entr_RGB ;

	}

	
	/**
	 * Normalizacion
	 * 
	 * */
	@Override
	public void postProcess() {
		
	//cargar los valores originales en un array (volumen y energia originales )
	/*
		double []originalValues = new double[ 3  ];
		
		for(int k = 0 ; k< 3  ; k++  ){
			originalValues[k] = vectorDescriptor[k];
		}
		
		//normalizacion y calculo de distancia  		
		for(int i = 0 ; i < vectorDescriptor.length-3 ; i+=3 ){	
			for(int j = 0 ; j < 3 ; j++  ){
				vectorDescriptor[ i + j  ] /= originalValues[ j ];
			}	
		}	
     */
		
	}
	
	public static double [] entropyCalc(ColorProcessor ip){
		ByteProcessor[] channels = new ByteProcessor[3];
		  channels[0] = ip.getChannel( 1, channels[0]);
		  channels[1] = ip.getChannel( 2, channels[1]);
		  channels[2] = ip.getChannel( 3, channels[2]);
		   
		  
		  double[] xFxSum;
		  int cSize = channels.length;
		  int hSize = channels[0].getHistogramSize();
		  int [][]channelHistogram = new int[cSize][hSize];
		  int totalPixels = ip.getWidth() * ip.getHeight() ;
		  int k;
		  xFxSum = new double[channels.length];

		 for (int i = 0; i < cSize; i++) {
		            channelHistogram[i] = channels[i].getHistogram();
		 }

		for (int i = 0; i < hSize; i++) {
		       for (k = 0; k < cSize; k++) {
		                //se calcula el numerador de (6)
		          double p = (double)channelHistogram[k][i]/(double)(totalPixels);
		             	if(channelHistogram[k][i] != 0){
		                    xFxSum[k] = xFxSum[k] - (p * (Math.log(p)/Math.log(2)));
		                }
		            }
		}
		return xFxSum;
		    
	}


	@Override
	public void setLocalParameters() {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	


}