package descriptor;

import ij.process.ImageProcessor ;


/**
 * Volumen estandar: sumatoria de todos valores de los pixeles 
 * Requiere que la imagen sea RGB , sino lanza una excepcion 
 * @author nelsond
 * @param image procesor RGB 32 bits 
 * @return sumatoria de los valores de intensidad de los pixeles
 * */


public class VolumenRGBCircNorm extends Descriptor{


			
	public VolumenRGBCircNorm(){
		
		super();
		
		//3 valores para cada elemento de la serie
		this.setVALUES_F_ELEMENT( 3 );
		
	}


	@Override
	public double[] calculate(ImageProcessor ip ) {
		
		
		//asegura que NO sea de un solo canal 
		if(ip.getNChannels() <  3 ) {
			new Exception("Error , la imagen debe ser RGB");
		}
	
	
		double [] result_sum_RGBGray = new double[ 3 ];
		double sum_R = 0 ;
		double sum_G = 0 ;
		double sum_B = 0 ;
		
		
		int h = ip.getHeight();
		int w = ip.getWidth();
		
		for(int v = 0 ; v< h ; v++){
			for(int u = 0 ; u< w ; u++){
							
				int c = ip.get(u,v);
	
				//extraer el componente RGB de cada pixel
				int r = (c & 0xff0000 ) >>16;
			    int g = (c & 0x00ff00 ) >>8;
			    int b = (c & 0x0000ff ) ;
			    
			    
			    
			    sum_R = sum_R +  r   ;
				sum_G = sum_G +  g   ;
				sum_B = sum_B +  b   ;
			
			}	
			result_sum_RGBGray[ 0 ] = sum_R;
			result_sum_RGBGray[ 1 ] = sum_G;
			result_sum_RGBGray[ 2 ] = sum_B;
			
		}
		
		return result_sum_RGBGray ;

	}

	
	/**
	 * Normalizacion de cada sumatoria de componente
	 * */
	@Override
	public void postProcess() {
		
		//en las tres primeras posiciones se encuentran los volumenes de la imagen original
		
				
		double []  volOriginals = new double [3];
		
		/*lista que posiciones a accedar que permite simular una lista circular*/
		int [] desplazamientos = {0,1,2 ,   2,0,1 ,   1,2,0 };
		
		
		/*agregar los valores de volumen iniciales R0,G0,B0 , 
		osea los primeros elementos de la serie */
		
		volOriginals[ 0 ] =  vectorDescriptor[ 0 ] ;
		volOriginals[ 1 ] =  vectorDescriptor[ 1 ] ;
		volOriginals[ 2 ] =  vectorDescriptor[ 2 ] ;
		
		
		int pos = 0 ;
		for(int i = 0 ; i < vectorDescriptor.length ; i++ ){
			if (pos >= desplazamientos.length){
				pos = 0; 
			}
			
			int currentVolPos = desplazamientos[ pos ];
			vectorDescriptor[ i ] = vectorDescriptor[ i ] / volOriginals[currentVolPos]  ;
			pos ++ ;
			
		}
		
	}


	@Override
	public void setLocalParameters() {
		// TODO Auto-generated method stub
		
	}
	
	


}