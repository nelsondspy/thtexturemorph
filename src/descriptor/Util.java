package descriptor;

/**
 * 
 * 
 * */
public class Util {
	
	/**
	 * @author nelsond
	 * Metodo de carga una clase de tipo descriptor 
	 * */
	public static Descriptor getDescriptorInstance(String className ){
		Descriptor loadedDescriptor = null ;

		try{
			
		//Class<?> clazz = Class.forName( className );
		Class c = Class.forName( className );
		
		loadedDescriptor = (Descriptor)c.newInstance();
		
		}
		catch(ClassNotFoundException e){
			System.out.println ( "error con ClassNotFoundException : " + e.getMessage());
		}
		catch(Exception e) {
			System.out.println ( "error con reflect : " + e.getMessage());
		}
		
		return loadedDescriptor;
	}
	
	/**
	 * @author nelsond
	 * Metodo de carga una clase de tipo DescriptorStackAbstract  
	 * */
	public static DescriptorStackAbstract  getDescriptorStackInstance(String className ){
		DescriptorStackAbstract loadedDescriptor = null ;

		try{
			
		//Class<?> clazz = Class.forName( className );
		Class c = Class.forName( className );
		
		loadedDescriptor = (DescriptorStackAbstract)c.newInstance();
		
		}
		catch(ClassNotFoundException e){
			System.out.println ( "error con ClassNotFoundException : " + e.getMessage());
		}
		catch(Exception e) {
			System.out.println ( "error con reflect : " + e.getMessage());
		}
		
		return loadedDescriptor;
	}
	
}
