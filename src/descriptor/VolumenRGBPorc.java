package descriptor;

import ij.process.ImageProcessor;

/**
 * Volumen estandar: sumatoria de todos valores de los pixeles 
 * Requiere que la imagen sea RGB , sino lanza una excepcion 
 * @author nelsond
 * @param image procesor RGB 32 bits 
 * @return sumatoria de los valores de intensidad de los pixeles
 * */


public class VolumenRGBPorc extends Descriptor{

			
	public VolumenRGBPorc(){
		
		super();
		
		//3 valores para cada elemento de la serie
		this.setVALUES_F_ELEMENT( 9 );
		
	}


	@Override
	public double[] calculate(ImageProcessor ip ) {
		
		
		//asegura que NO sea de un solo canal 
		if(ip.getNChannels() <  3 ) {
			new Exception("Error , la imagen debe ser RGB");
		}
	
		double [] result_element_RGB = new double[ 9 ];
		
		int h = ip.getHeight();
		int w = ip.getWidth();
		
		for(int v = 0 ; v< h ; v++){
			for(int u = 0 ; u< w ; u++){
							
				int c = ip.get(u,v);
	
				//extraer el componente RGB de cada pixel
				int r = (c & 0xff0000 ) >>16;
			    int g = (c & 0x00ff00 ) >>8;
			    int b = (c & 0x0000ff ) ;
			    
			    
			    result_element_RGB[ 0 ] += r;
			    result_element_RGB[ 1 ] += g;
			    result_element_RGB[ 2 ] += b;	
				
			    result_element_RGB[ 3 ]+= (r * r ) ;
			    result_element_RGB[ 4 ]+= (g * g ) ;
			    result_element_RGB[ 5 ]+= (b * b );
				
			}		
		}
		
		/*
		 *una vez calculados la energia y el volumen se calcula la varianza de cada canal  
		 * */
		
		return result_element_RGB  ;

	}

	
	/**
	 * Normalizacion de cada sumatoria de componente o algun tipo de calculo que requiera dispones 
	 * de todos los elementos para ser efectuado 
	 * */
	@Override
	public void postProcess() {
		
		//cargar los valores originales en un array (volumen y energia originales )
		double []originalValues = new double[ 9  ];
		for(int k = 0 ; k< 9  ; k++  ){
			originalValues[k] = vectorDescriptor[k];
		}
		
		
		
		double [] volumenMedia = new double[ 3  ];
		double [] energyMedia =  new double[ 3  ];
		double [] volumenVarianza = new double[ 3  ];
		double [] energyVarianza =  new double[ 3  ];
		
		//calcula la media para cada canal 		
		int lengthserie = 0 ;
	    for(int i = 0 ; i< vectorDescriptor.length-9 ; i+=9 ){
	    	volumenMedia[ 0 ] += vectorDescriptor[i+0] ;
	    	volumenMedia[ 1 ] += vectorDescriptor[i+1] ;
	    	volumenMedia[ 2 ] += vectorDescriptor[i+2] ;
	    	energyMedia[ 0 ] += vectorDescriptor[i+3];
	    	energyMedia[ 1 ] += vectorDescriptor[i+4];
	    	energyMedia[ 2 ] += vectorDescriptor[i+5] ;
	    			
	    	lengthserie ++;
	    }
	    volumenMedia[0] /= lengthserie ;
    	volumenMedia[1] /= lengthserie  ;
    	volumenMedia[2] /= lengthserie  ;
    	
    	energyMedia[ 0 ] /= lengthserie ;
    	energyMedia[ 1 ] /= lengthserie ;
    	energyMedia[ 2 ] /= lengthserie  ;
    
 
    	
    	//calculo de la varianza  
    	for(int i = 0 ; i< vectorDescriptor.length-9 ; i+=9 ){
    		
    		System.out.println("i : " + i );
    		
    		volumenVarianza[ 0 ] += Math.pow( volumenMedia[0] - vectorDescriptor[i+0] , 2 ) ;
    		volumenVarianza[ 1 ] += Math.pow( volumenMedia[1] - vectorDescriptor[i+1] , 2 ) ;
    		volumenVarianza[ 2 ] += Math.pow( volumenMedia[2] - vectorDescriptor[i+2] , 2 ) ;
 	    	
    		energyVarianza[ 0 ] += Math.pow( energyMedia[ 0 ] - vectorDescriptor[i+3], 2 );
    		energyVarianza[ 1 ] += Math.pow( energyMedia[ 1 ] - vectorDescriptor[i+4], 2 );
    		energyVarianza[ 2 ] += Math.pow( energyMedia[ 2 ] - vectorDescriptor[i+5], 2 ) ;
 	    }
    	
    	//
    	
    	volumenVarianza[ 0 ] /= lengthserie;
		volumenVarianza[ 1 ] /= lengthserie ;
		volumenVarianza[ 2 ] /= lengthserie;
	    	
		energyVarianza[ 0 ] /= lengthserie ;
		energyVarianza[ 1 ] /=  lengthserie ;
		energyVarianza[ 2 ] /=  lengthserie ; 
		
		
		//normalizar con los primeros 9 elementos 
		
		//normalizacion 		
		for(int i = 0 ; i < vectorDescriptor.length-9 ; i+=9 ){
			
			double volR = vectorDescriptor[ i + 0 ]  ;
			double volG = vectorDescriptor[ i + 1 ]  ;
			double volB = vectorDescriptor[ i + 2 ] ;
			
			double enerR =vectorDescriptor[ i + 3 ] ;
			double enerG =vectorDescriptor[ i + 4 ] ;
			double enerB =vectorDescriptor[ i + 5 ] ;
			
			//distancia R-G de volumen y energia 
			
			vectorDescriptor[ i + 6] = Math.sqrt (Math.pow( (volR - volG)/ (volR + volG)/2  , 2)  + Math.pow(enerR - enerG,2) ) ;
			//distancia G-B
			vectorDescriptor[ i + 7] =  Math.sqrt (Math.pow( (volG - volB) / (volG + volB)/2 , 2)  + Math.pow(enerG - enerB ,2)) ;
			//distancia B-R 
			vectorDescriptor[ i + 8] =  Math.sqrt (Math.pow( (volB - volR)  / (volB + volR)/2, 2)  + Math.pow(enerB - enerR,2) ) ;
 			
			//la normalizacion comun y corriente
			for(int j = 0 ; j < 6 ; j++  ){
				vectorDescriptor[ i + j  ] /= originalValues[ j ];
				System.out.print(", i + j: " + (i + j) );
			}
		
			
			//para probar que la energia no es tan importante
			vectorDescriptor[ i + 3 ] = 0  ;
			vectorDescriptor[ i + 4 ] = 0  ;
			vectorDescriptor[ i + 5 ] = 0 ;
			
			
		}
		
		
		
		
		/*
		 * 
		while(i  <  vectorDescriptor.length ){
					
		//volumen RGB 
				vectorDescriptor[ i ] = vectorDescriptor[ i ] / originalValues[0]; 
				i++ ;
						
				vectorDescriptor[ i  ] = vectorDescriptor[ i ] / originalValues[1]   ;
				i++ ;	
				vectorDescriptor[ i  ] = vectorDescriptor[ i ] / originalValues[2]  ;
				i++ ;
				
				//calculo de distancias 			
			}
			*/
					
	}


	@Override
	public void setLocalParameters() {
		// TODO Auto-generated method stub
		
	}
	
	


}