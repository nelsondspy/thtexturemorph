package descriptor;

import ij.process.ImageProcessor;

/**
 * Volumen Bitmix , utiliza el valor reducido por bitmixing
 * Requiere que la imagen sea RGB , sino lanza una excepcion 
 * @author nelsond
 * @param image procesor RGB 32 bits 
 * @return sumatoria de los valores de intensidad de los pixeles
 * */


public class VolumenPorPares extends Descriptor{

			
	public VolumenPorPares(){
		
		super();
		
		//3 valores para cada elemento de la serie
		this.setVALUES_F_ELEMENT( 3 );
		
	}


	@Override
	public double[] calculate(ImageProcessor ip ) {
		
		
		//asegura que NO sea de un solo canal 
		if(ip.getNChannels() <  3 ) {
			new Exception("Error , la imagen debe ser RGB");
		}
	
		double [] result_element_RGB = new double[ 3 ];
		
		int h = ip.getHeight();
		int w = ip.getWidth();
		
		for(int v = 0 ; v< h ; v++){
			for(int u = 0 ; u< w ; u++){
							
				int c = ip.get(u,v);
	
				//extraer el componente RGB de cada pixel
				int r = (c & 0xff0000 ) >>16;
			    int g = (c & 0x00ff00 ) >>8;
			    int b = (c & 0x0000ff ) ;
			    
			    
			    result_element_RGB[ 0 ] += r;
			    result_element_RGB[ 1 ] += g;
			    result_element_RGB[ 2 ] += b;	
			    
	
			}		
		}
		
		/*
		 *una vez calculados la energia y el volumen se calcula la varianza de cada canal  
		 * */
		
		return result_element_RGB  ;

	}

	
	/**
	 * Normalizacion de cada sumatoria de componente o algun tipo de calculo que requiera dispones 
	 * de todos los elementos para ser efectuado 
	 * */
	@Override
	public void postProcess() {
		
		//normalizacion y calculo de distancia  		
				for(int i = 0 ; i < vectorDescriptor.length ; i+=3 ){
					
					double volR = vectorDescriptor[ i + 0 ]  ;
					double volG = vectorDescriptor[ i + 1 ]  ;
					double volB = vectorDescriptor[ i + 2 ] ;
					
					//suma RG de volumen
					vectorDescriptor[ i + 0] = (volR + volG)   ;
					//suma GB
					vectorDescriptor[ i + 1] =  (volG + volB) ;
					//suma BR 
					vectorDescriptor[ i + 2] =  (volB + volR)   ;
		 			
				}	
				
		//cargar los valores originales en un array (los tres primeros volumenes )
		double []originalValues = new double[ 3 ];
		for(int k = 0 ; k< 3  ; k++  ){
			originalValues[k] = vectorDescriptor[k];
		}
		
		
	
		//normalizar con los primeros 3 elementos 
		
		//normalizacion 		
		for(int i = 0 ; i < vectorDescriptor.length ; i+=3 ){
						
			//la normalizacion comun y corriente
				vectorDescriptor[ i + 0  ] /= originalValues[  0 ];
				vectorDescriptor[ i + 1  ] /= originalValues[  1 ];
				vectorDescriptor[ i + 2  ] /= originalValues[  2 ];
				
		}
	
	}


	@Override
	public void setLocalParameters() {
		// TODO Auto-generated method stub
		
	}
	
	


}