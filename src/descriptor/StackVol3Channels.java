package descriptor;

import ij.ImagePlus;
import ij.ImageStack;
import ij.process.ImageProcessor;

/**
 *  calculo de volumen consistente en la sumatoria de todos los valores de pixeles
 *  para cada canal, para un stack de 3 canales , utilizando el valor de tipo float 
 * 	del pixel 
 * @author nelsond
 * @param ImagePlus
 * @return 
 * */


public class StackVol3Channels extends DescriptorStackAbstract{

			
	public StackVol3Channels(){
		
		super();
		
		//3 valores para cada elemento de la serie
		this.setVALUES_F_ELEMENT( 3 );
		
	}


	@Override
	public double[] calculate(ImagePlus imlplus ) {
		
	  ImageStack stack = imlplus.getStack();
		

	   ImageProcessor c1= stack.getProcessor(1);
	   ImageProcessor c2= stack.getProcessor(2);
	   ImageProcessor c3= stack.getProcessor(3);
		

	
		double [] result_element = new double[ 3 ];
		
		int h = imlplus.getHeight();
		int w = imlplus.getWidth();
		
		/*primero se calcula el volumen para cada canal*/
		for(int v = 0 ; v< h ; v++){
			for(int u = 0 ; u< w ; u++){
				
		
				result_element[ 0 ] += c1.getf(u,v) ;
				result_element[ 1 ] += c2.getf(u,v) ;
				result_element[ 2 ] += c3.getf(u,v);
			   
			}		
		}
		
		return result_element ;

	}

	
	/**
	 * Normalizacion de cada sumatoria de componente o algun tipo de calculo que requiera dispones 
	 * de todos los elementos para ser efectuado 
	 * */
	@Override
	public void postProcess() {
		
		double origv1 =  vectorDescriptor[ 0 ];
		double origv2 =  vectorDescriptor[ 1 ];
		double origv3 =  vectorDescriptor[ 2 ];
		
		//normalizacion de los volumenes de Saturacion e Intensidad 
		for(int i = 0 ; i < vectorDescriptor.length ; i+=3 ){
			
			vectorDescriptor[ i + 0 ] = vectorDescriptor[ i + 0 ] / origv1 ;
			vectorDescriptor[ i + 1 ] = vectorDescriptor[ i + 1 ] / origv2 ;
			vectorDescriptor[ i + 2 ] = vectorDescriptor[ i + 2 ] / origv3 ;
		}	
	}


	@Override
	public void setLocalParameters() {
		// TODO Auto-generated method stub
		
	}
	
	


}