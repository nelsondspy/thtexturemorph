package descriptor;

import ij.process.ImageProcessor;

/**
 * Solo Vector de distancias mahalanobis intercanales 
 * Requiere que la imagen sea RGB , sino lanza una excepcion 
 * @author nelsond
 * @param image procesor RGB 32 bits 
 * @return sumatoria de los valores de intensidad de los pixeles
 * */


public class VectorRGBDistMaha extends Descriptor{

			
	public VectorRGBDistMaha(){
		
		super();
		
		//3 valores para cada elemento de la serie
		this.setVALUES_F_ELEMENT( 3 );
		
	}


	@Override
	public double[] calculate(ImageProcessor ip ) {
		
		
		//asegura que NO sea de un solo canal 
		if(ip.getNChannels() <  3 ) {
			new Exception("Error , la imagen debe ser RGB");
		}
	
		double [] result_element_RGB = new double[ 3 ];
		
		int h = ip.getHeight();
		int w = ip.getWidth();
		
		/*primero se calcula el volumen para cada canal*/
		for(int v = 0 ; v< h ; v++){
			for(int u = 0 ; u< w ; u++){
							
				int c = ip.get(u,v);
	
				//extraer el componente RGB de cada pixel
				int r = (c & 0xff0000 ) >>16;
			    int g = (c & 0x00ff00 ) >>8;
			    int b = (c & 0x0000ff ) ;
			    
			    
			    result_element_RGB[ 0 ] += r;
			    result_element_RGB[ 1 ] += g;
			    result_element_RGB[ 2 ] += b;	
				
			   
			}		
		}
		
		/*
		 *una vez calculados la energia y el volumen se calcula la varianza de cada canal  
		 * */
		
		return result_element_RGB  ;

	}

	
	/**
	 * Normalizacion de cada sumatoria de componente o algun tipo de calculo que requiera dispones 
	 * de todos los elementos para ser efectuado 
	 * */
	@Override
	public void postProcess() {
		double medR =0 ;
		double medG =0 ;
		double medB =0 ;
		int n = 0 ;
		//calculo de la media para cada canal 
		for(int i = 0 ; i < vectorDescriptor.length ; i+=3 ){
			double volR = vectorDescriptor[ i + 0 ]  ;
			double volG = vectorDescriptor[ i + 1 ]  ;
			double volB = vectorDescriptor[ i + 2 ] ;
			medR += volR ;
			medG += volG ;
			medB += volB ;
			n++ ;
		}
		//finalmente las medias son :
		medR = medR/n ;
		medG = medG/n ;
		medB = medB/n ;
		
		//calculo de la covarianza de cada par de componentes
		double covarRG = 0;
		double covarGB = 0;
		double covarBR = 0;
		
		for(int i = 0 ; i < vectorDescriptor.length-3 ; i+=3 ){
			double volR = vectorDescriptor[ i + 0 ]  ;
			double volG = vectorDescriptor[ i + 1 ]  ;
			double volB = vectorDescriptor[ i + 2 ] ;
			covarRG += (volR - medR) * (volG - medG);
			covarGB += (volG - medG) * (volB - medB) ;
			covarBR += (volB - medB) * (volR - medR) ;	
		}
		
		covarRG /= n; 
		covarGB /= n; 
		covarBR /= n;
		
		//System.out.println("covarRG:" + covarRG);
		//System.out.println("covarGB:"+covarGB);
		//System.out.println("covarBR:" + covarBR);
		
		
		// calculo de distancia usando las covarianzas calculadas 
		for(int i = 0 ; i < vectorDescriptor.length-3 ; i+=3 ){
			
			double volR = vectorDescriptor[ i + 0 ]  ;
			double volG = vectorDescriptor[ i + 1 ]  ;
			double volB = vectorDescriptor[ i + 2 ] ;
		
			
			//distancia R-G de volumen y energia 
			
			vectorDescriptor[ i + 0] = Math.sqrt (Math.pow( (volR - volG)/ covarRG  , 2)  ) ;
			//distancia G-B
			vectorDescriptor[ i + 1] =  Math.sqrt (Math.pow( (volG - volB) / covarGB , 2) ) ;
			//distancia B-R 
			vectorDescriptor[ i + 2] =  Math.sqrt (Math.pow( (volB - volR)  / covarBR , 2)  ) ;
 			
		//	System.out.println("vectorDescriptor[ i + 0]:" + vectorDescriptor[ i + 0]);
		//	System.out.println("vectorDescriptor[ i + 1]:"+vectorDescriptor[ i + 1]);
		//	System.out.println("vectorDescriptor[ i + 2]:" + vectorDescriptor[ i + 2]);
		
			
			
		//normalizacion 
			double originalRGB[] = new double [3];
			originalRGB[ 0 ] = vectorDescriptor[0] ;
			originalRGB[ 1 ] = vectorDescriptor[1] ;
			originalRGB[ 2 ] = vectorDescriptor[2] ;
			
			for(int k = 0 ; k < vectorDescriptor.length-3 ; k+=3 ){
				vectorDescriptor[ k + 0] = vectorDescriptor[ k + 0] / originalRGB[ 0 ];
				//distancia G-B
				vectorDescriptor[ k + 1] = vectorDescriptor[ k + 1 ] / originalRGB[ 1 ];
				//distancia B-R 
				vectorDescriptor[ k + 2] = vectorDescriptor[ k + 2] / originalRGB[ 2 ] ;
			
			}		
			
			
		}	
	}


	@Override
	public void setLocalParameters() {
		// TODO Auto-generated method stub
		
	}
	
	


}