package descriptor;

import ij.process.ImageProcessor;

/**
 * Volumen estandar: sumatoria de todos valores de los pixeles 
 * Requiere que la imagen sea RGB , sino lanza una excepcion 
 * @author nelsond
 * @param image procesor RGB 32 bits 
 * @return sumatoria de los valores de intensidad de los pixeles
 * */


public class RGBPorcentual extends Descriptor{

			
	public RGBPorcentual(){
		
		super();
		
		//3 valores para cada elemento de la serie
		this.setVALUES_F_ELEMENT( 3 );
		
	}


	@Override
	public double[] calculate(ImageProcessor ip ) {
		
		
		//asegura que NO sea de un solo canal 
		if(ip.getNChannels() <  3 ) {
			new Exception("Error , la imagen debe ser RGB");
		}
	
		double [] result_element_RGB = new double[ 3 ];
		
		int h = ip.getHeight();
		int w = ip.getWidth();
		
		for(int v = 0 ; v< h ; v++){
			for(int u = 0 ; u< w ; u++){
							
				int c = ip.get(u,v);
	
				//extraer el componente RGB de cada pixel
				int r = (c & 0xff0000 ) >>16;
			    int g = (c & 0x00ff00 ) >>8;
			    int b = (c & 0x0000ff ) ;
			    
			    
			    result_element_RGB[ 0 ] += r;
			    result_element_RGB[ 1 ] += g;
			    result_element_RGB[ 2 ] += b;	
				
			   
			}		
		}
		
		/*
		 *una vez calculados la energia y el volumen se calcula la varianza de cada canal  
		 * */
		
		return result_element_RGB  ;

	}

	
	/**
	 * Normalizacion de cada sumatoria de componente o algun tipo de calculo que requiera dispones 
	 * de todos los elementos para ser efectuado 
	 * */
	@Override
	public void postProcess() {
		
		//cargar los valores originales en un array (volumen y energia originales )
		double []originalValues = new double[ 3  ];
		for(int k = 0 ; k< 3  ; k++  ){
			originalValues[k] = vectorDescriptor[k];
		}
		
		

    	
		//normalizacion y calculo de distancia  		
		for(int i = 0 ; i < vectorDescriptor.length-3 ; i+=3 ){
			
			double volR = vectorDescriptor[ i + 0 ]  ;
			double volG = vectorDescriptor[ i + 1 ]  ;
			double volB = vectorDescriptor[ i + 2 ] ;
			
		
			
			//distancia R-G de volumen y energia 
			
			vectorDescriptor[ i + 0] =  (volR / volG) ;
			//distancia G-B
			vectorDescriptor[ i + 1] =  (volG / volB)  ;
			//distancia B-R 
			vectorDescriptor[ i + 2] =  (volB / volR)  ;
 			
			
		}	
	}


	@Override
	public void setLocalParameters() {
		// TODO Auto-generated method stub
		
	}
	
	


}