package descriptor;

import ij.ImagePlus;
import ij.ImageStack;
import ij.process.ImageProcessor;

/**
 * Vector de caracteristica volumen(S). volumen(I)
 * Requiere que la imagen sea un stack de 3 canales ,
 * 	c1 = "H";  c2 = "S";   c3 = "I";  
 * @author nelsond
 * @param ImagePlus
 * @return 
 * */


public class HSIVolonlyIntensity extends DescriptorStackAbstract{

			
	public HSIVolonlyIntensity(){
		
		super();
		
		//3 valores para cada elemento de la serie
		this.setVALUES_F_ELEMENT( 1 );
		
	}


	@Override
	public double[] calculate(ImagePlus imlplus ) {
		
	  ImageStack stack = imlplus.getStack();
		

	   //sin utilizar por el momento 
	   ImageProcessor cH= stack.getProcessor(1);
	   
	   //canales utilizados 
	   ImageProcessor cS= stack.getProcessor(2);
	   ImageProcessor cI= stack.getProcessor(3);
		

	
		double [] result_element = new double[ 1 ];
		
		int h = imlplus.getHeight();
		int w = imlplus.getWidth();
		
		/*primero se calcula el volumen para cada canal*/
		for(int v = 0 ; v< h ; v++){
			for(int u = 0 ; u< w ; u++){
				
				//extraer la saturacion y la intensidad
				//int s = 255 * cS.get(u,v);
				int i = 1 * cI.get(u,v);
	
				//result_element[ 0 ] += s;
				result_element[ 0 ] += i;
			   
			}		
		}
		
		/*
		 *una vez calculados la energia y el volumen se calcula la varianza de cada canal  
		 * */
		
		return result_element ;

	}

	
	/**
	 * Normalizacion de cada sumatoria de componente o algun tipo de calculo que requiera dispones 
	 * de todos los elementos para ser efectuado 
	 * */
	@Override
	public void postProcess() {
		
		//double origS =  vectorDescriptor[ 0 ];
		double origI =  vectorDescriptor[ 0 ];
		
		//normalizacion de los volumenes de Saturacion e Intensidad 
		for(int i = 0 ; i < vectorDescriptor.length ; i++ ){
			
			vectorDescriptor[ i ] = vectorDescriptor[ i ] /origI ;
			
			//vectorDescriptor[ i + 1] = vectorDescriptor[ i + 1] / origI ;	
		}	
	}


	@Override
	public void setLocalParameters() {
		// TODO Auto-generated method stub
		
	}
	
	


}