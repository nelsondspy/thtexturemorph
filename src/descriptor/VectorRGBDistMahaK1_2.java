package descriptor;

import ij.process.ImageProcessor;

/**
 * Solo Vector de distancias mahalanobis intercanales 
 * Requiere que la imagen sea RGB , sino lanza una excepcion 
 * @author nelsond
 * @param image procesor RGB 32 bits 
 * @return sumatoria de los valores de intensidad de los pixeles
 * */


public class VectorRGBDistMahaK1_2 extends Descriptor{

			
	public VectorRGBDistMahaK1_2(){
		
		super();
		
		//3 valores para cada elemento de la serie
		this.setVALUES_F_ELEMENT( 3 );
		
	}


	@Override
	public double[] calculate(ImageProcessor ip ) {
		
		
		//asegura que NO sea de un solo canal 
		if(ip.getNChannels() <  3 ) {
			new Exception("Error , la imagen debe ser RGB");
		}
	
		double [] result_element_RGB = new double[ 3 ];
		
		int h = ip.getHeight();
		int w = ip.getWidth();
		
		/*primero se calcula el volumen para cada canal*/
		for(int v = 0 ; v< h ; v++){
			for(int u = 0 ; u< w ; u++){
							
				int c = ip.get(u,v);
	
				//extraer el componente RGB de cada pixel
				int r = (c & 0xff0000 ) >>16;
			    int g = (c & 0x00ff00 ) >>8;
			    int b = (c & 0x0000ff ) ;
			    
			    
			    result_element_RGB[ 0 ] += r;
			    result_element_RGB[ 1 ] += g;
			    result_element_RGB[ 2 ] += b;	
				
			   
			}		
		}
		
		/*
		 *una vez calculados la energia y el volumen se calcula la varianza de cada canal  
		 * */
		
		return result_element_RGB  ;

	}

	
	/**
	 * Normalizacion de cada sumatoria de componente o algun tipo de calculo que requiera dispones 
	 * de todos los elementos para ser efectuado 
	 * */
	@Override
	public void postProcess() {
		
	
		// calculo de distancia usando las covarianzas calculadas 
		for(int i = 0 ; i < vectorDescriptor.length ; i+=3 ){
			
			double volR = vectorDescriptor[ i + 0 ]  ;
			double volG = vectorDescriptor[ i + 1 ]  ;
			double volB = vectorDescriptor[ i + 2 ] ;
		
			double vmRG = (volR + volG)/2;
			double vmGB = (volG + volB)/2;
			double vmBR = (volB + volR)/2;
			
			//  1  / n-1  =  1/ 2-1   =  1   
			double stdvRG =  Math.sqrt ( Math.pow(vmRG  -volR , 2) + Math.pow(vmRG  -volG , 2) ) ;
			double stdvGB =  Math.sqrt ( Math.pow(vmGB  -volG , 2) + Math.pow(vmGB  -volB , 2) ) ;
			double stdvBR =  Math.sqrt ( Math.pow(vmBR  -volB , 2) + Math.pow(vmBR  -volR , 2) ) ;

			//distancia R-G de volumen y energia 	
			vectorDescriptor[ i + 0] = Math.abs(volR - volG) / stdvRG  ;
			//distancia G-B
			vectorDescriptor[ i + 1] =  Math.abs(volG - volB)/ stdvGB ;
			//distancia B-R 
			vectorDescriptor[ i + 2] =  Math.abs(volB - volR)/ stdvBR ;
 			
		//	System.out.println("vectorDescriptor[ i + 0]:" + vectorDescriptor[ i + 0]);
		//	System.out.println("vectorDescriptor[ i + 1]:"+vectorDescriptor[ i + 1]);
		//	System.out.println("vectorDescriptor[ i + 2]:" + vectorDescriptor[ i + 2]);
		
			
			
		//normalizacion 
			double originalRGB[] = new double [3];
			originalRGB[ 0 ] = vectorDescriptor[0] ;
			originalRGB[ 1 ] = vectorDescriptor[1] ;
			originalRGB[ 2 ] = vectorDescriptor[2] ;
		
			/* 
			 * Son propiedades de esta distancia:
1) Es invariante por cambios de escala.
2) Es una distancia normalizada expresada en unidades de desviaci ́on t ́ıpica. Para
una variable con distribuci ́on normal, el campo de variabilidad de esta distancia
estar ́a pr ́acticamente comprendido entre 0 y 4
			 * 
			for(int k = 0 ; k < vectorDescriptor.length-3 ; k+=3 ){
				vectorDescriptor[ k + 0] = vectorDescriptor[ k + 0] / originalRGB[ 0 ];
				//distancia G-B
				vectorDescriptor[ k + 1] = vectorDescriptor[ k + 1 ] / originalRGB[ 1 ];
				//distancia B-R 
				vectorDescriptor[ k + 2] = vectorDescriptor[ k + 2] / originalRGB[ 2 ] ;
			
			}		
			
			*/
		}	
	}


	@Override
	public void setLocalParameters() {
		// TODO Auto-generated method stub
		
	}
	
	


}