package descriptor;

import ij.process.ImageProcessor;

/**
 * Volumen estandar: sumatoria de todos valores de los pixeles 
 * si la imagen es RGB , convierte la imagen a escala de grises 
 * @author nelsond
 * @param image procesor en escala de grises
 * @return sumatoria de los valores de intensidad de los pixeles
 * */


public class Volumen extends Descriptor{

	//grafico para el patron espectro ( serie diferencial )
	double[] diffPlotx = new double[ 1] ;
	double[] diffPloty = new double[ 1] ;
			
	public Volumen(){
		
		super();
		
		//el volumen consiste en un solo valor por cada elemento de la serie 
		this.setVALUES_F_ELEMENT( 1 );
		
	}


	@Override
	public double[] calculate(ImageProcessor ipparam ) {
		
		ImageProcessor ip ;
		
		//asegura que sea de un solo canal 
		if(ipparam.getNChannels() > 1 ) {
			ip = ipparam.convertToShortProcessor();
		}
		else{
			ip = ipparam;
		}
		
		double [] sum = new double[1];
		sum[0] = 0 ;
		
		int h = ip.getHeight();
		int w = ip.getWidth();
		
		for(int v = 0 ; v< h ; v++){
			for(int u = 0 ; u< w ; u++){
				sum[0] = sum[0] + ( double )( ip.get(u,v) );
			}	
		}
		return sum;
	
	}

	
	/**
	 * 
	 * En el caso del volumen , se normaliza 
	 * 
	 * */
	@Override
	public void postProcess() {
		double volumenorig = vectorDescriptor[ 0 ];
		for(int i = 0 ; i < vectorDescriptor.length ; i++ ){
			vectorDescriptor[ i ] = vectorDescriptor[ i ] / volumenorig  ;
		}
		
	}
	
	public void  setLocalParameters(){
		
	}


}