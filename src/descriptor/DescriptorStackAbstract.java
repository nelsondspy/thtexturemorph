package descriptor;
import ij.ImagePlus;

/**
 * @author nelsond
 * Clase que permite administrar de manera uniforme los descriptores utilizando imagenes en forma de Stack.
 * Permite la implementacion de nuevos descriptores independiente a la generacion de 
 * series morfologicas  
 * Fecha de creacion: 2016-07-22
 * 
 * */
public abstract class DescriptorStackAbstract {
	
	/** separador interno de parametros del descriptor por ejemplo  "1.05,0.1"  */
	public static String PARAM_INTERNAL_SEPARATOR = ","; 
	
	/** valores por elemento */
	int VALUES_F_ELEMENT;
	
	/** tamanho o dimension de la serie */ 
	int SERIE_DIM ;
	
	/**vector de descripcion */ 
	double [] vectorDescriptor ;
	
	/**indice actual del vector */
	private int index = -1 ;
	
	
	/** cadena que representa los parametros que recibio el descriptor a nivel de macro  */
	public String DESCRIPTOR_PRM ;
	
	public String getDESCRIPTOR_PRM() {
		return DESCRIPTOR_PRM;
	}


	public void setDESCRIPTOR_PRM(String dESCRIPTOR_PRM) {
		DESCRIPTOR_PRM = dESCRIPTOR_PRM;
	}


	/** implementar el metodo de calculo */
	public abstract double [] calculate( ImagePlus ip);
	
	

	/**
	 * Metodo que se debe implementar en los descriptores que requieran parametros. 
	 * Internamente estos descriptores implementan esos parametros como propiedades 
	 * que luego son mapeadas a valores concretros 
	 * */
	public abstract void setLocalParameters();
	
	
	
	/**implementar algun tipo de postprocesamiento como algun promedio o acumulacion 
	 * es decir alguna operacion especifica descriptor
	 * */
	public abstract void postProcess();
	
	
	/**
	 * agregar al vector de caracteristicas 
	 * */
	public void add(double [] caracteristics )
	{
		for(int i = 0 ; i < caracteristics.length ; i++ ){
			index ++;
			vectorDescriptor[ index ] =  caracteristics[ i ];			
		}
	}	
	
	/***
	 * Calcular un elemento del descriptor y agregar al vector de caracteristicas
	 * */
	public void calculateAndAdd(ImagePlus imp  )
	{
		double [] aux = calculate( imp ) ;
		add( aux );
	}
	
	
	/**
	 * Instancia el vector de caracteristicas 
	 * @param tamanho de la serie morfologica 
	 * @param numero de valores por elemento  
	 * 
	 * */
	public void setDim( int serieSize ){
		SERIE_DIM =  serieSize  ; 
		vectorDescriptor = new double[ serieSize *  VALUES_F_ELEMENT  ];
		
	}	
	
	
	public void setVALUES_F_ELEMENT(int VALUES_F_ELEMENT  ){
		this.VALUES_F_ELEMENT  = VALUES_F_ELEMENT  ;
	}
	
	
	public void setSERIE_DIM(int SERIE_DIM  ){
		this.SERIE_DIM  = SERIE_DIM  ;
	}
	
	public double [] getVectorDescriptor(){
		return vectorDescriptor;
	}
	
	
	/**
	 * Convierte la cadena de parametros DESCRIPTOR_PRM , en un array de doubles 
	 * el significado y el orden depende de particularmente de cada tipo de descriptor
	 * si es que necesita algun parametro
	 * */
	public double []paramsStringToDouble(){
		
		String[] listTokens = DESCRIPTOR_PRM.split( DescriptorStackAbstract.PARAM_INTERNAL_SEPARATOR );
		double [] lparam = new  double [ listTokens.length ];
		
		for (int i = 0 ; i < listTokens.length ; i++ ) {
			lparam [ i ]  = Double.parseDouble( listTokens[ i ] ) ;
		}
		return lparam;
	}
	

}