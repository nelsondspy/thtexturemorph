package descriptor;

import ij.process.ImageProcessor;

/**
 * Momento espacial con los parametros preestablecidos segun aptoula con los que se consiguen
 * mejores resultados en la clasificacion
 * para cada canal se calcula el momento espacial con 0,3 3,0  
 * 
 * */
public class VolumenRGBComb extends Descriptor{
	
		public VolumenRGBComb(){
			super();
			
			//9 valores para cada elemento de la serie, osea 3 para cada canal 
			this.setVALUES_F_ELEMENT( 9 );
			
		}

				
		@Override
		public double[] calculate(ImageProcessor ip ) {
			
			
			//asegura que NO sea de un solo canal 
			if(ip.getNChannels() <  3 ) {
				new Exception("Error , la imagen debe ser RGB");
			}
		
		
			double [] result_sum_RGBGray = new double[ 9 ];
			double sum_R = 0 ;
			double sum_G = 0 ;
			double sum_B = 0 ;
			
			
			int h = ip.getHeight();
			int w = ip.getWidth();
			
			for(int v = 0 ; v< h ; v++){
				for(int u = 0 ; u< w ; u++){
								
					int c = ip.get(u,v);
		
					//extraer el componente RGB de cada pixel
					int r = (c & 0xff0000 ) >>16;
				    int g = (c & 0x00ff00 ) >>8;
				    int b = (c & 0x0000ff ) ;
				    
				    
				    sum_R = sum_R +  r   ;
					sum_G = sum_G +  g   ;
					sum_B = sum_B +  b   ;
				
				}
				
				//volumen R con sus relaciones mismo canal e intercanal
				result_sum_RGBGray[ 0 ] = sum_R; // R_i/ R_Original
				result_sum_RGBGray[ 1 ] = sum_R; // R_i/ G_Original
				result_sum_RGBGray[ 2 ] = sum_R; // R_i/ B_Original
				
				//volumen G con sus relaciones del mismo mismo canal e intercanal
				result_sum_RGBGray[ 3 ] = sum_G; // G_i/ R_Original
				result_sum_RGBGray[ 4 ] = sum_G; // G_i/ G_Original
				result_sum_RGBGray[ 5 ] = sum_G; // G_i/ B_Original
				
				//volumen b con sus relaciones mismo canal e intercanal
				result_sum_RGBGray[ 6 ] = sum_B; // B_i/ R_Original
				result_sum_RGBGray[ 7 ] = sum_B; // B_i/ G_Original
				result_sum_RGBGray[ 8 ] = sum_B; // B_i/ B_Original
				
				
			}

		
		return 	result_sum_RGBGray;
			
		}


		@Override
		public void setLocalParameters() {
			// TODO Auto-generated method stub
			
		}


		@Override
		public void postProcess() {
			// TODO Auto-generated method stub
			double [] originalValues = new double[ 3  ]; 
			
			originalValues[0] = vectorDescriptor[0];
			originalValues[1] = vectorDescriptor[3];
			originalValues[2] = vectorDescriptor[6];
					
			//normalizar con los primeros 9 elementos 
			int i = 0 ;
			
			while(i  <  vectorDescriptor.length ){
				
				vectorDescriptor[ i ] = vectorDescriptor[ i ] / originalValues[0]; 
				i++ ;
				
				vectorDescriptor[ i  ] = vectorDescriptor[ i ] / originalValues[1]   ;
				i++ ;
				
				vectorDescriptor[ i  ] = vectorDescriptor[ i ] / originalValues[2]  ;
				i++ ;
	
				
			}
			
			
		}



		

}
