package descriptor;

import ij.process.ImageProcessor;

/**
 * Volumen probando : sumatoria de todos valores de los pixeles 
 * Requiere que la imagen sea RGB , sino lanza una excepcion 
 * @author nelsond
 * @param image procesor RGB 32 bits 
 * @return 
 * */


public class VolumenX3 extends Descriptor{

	
			
	public VolumenX3(){
		
		super();
		
		//3 valores para cada elemento de la serie
		this.setVALUES_F_ELEMENT( 1 );
		
	}


	@Override
	public double[] calculate(ImageProcessor ip ) {
		
		
		//asegura que NO sea de un solo canal 
		if(ip.getNChannels() <  3 ) {
			new Exception("Error , la imagen debe ser RGB");
		}
	
		double [] result_sum_RGB = new double[ 1 ];

		double sum_R = 0 ;
		
		
		int h = ip.getHeight();
		int w = ip.getWidth();
		
		for(int v = 0 ; v< h ; v++){
			for(int u = 0 ; u< w ; u++){
							
				int c = ip.get(u,v);
	
				//extraer el componente RGB de cada pixel
				int r = (c & 0xff0000 ) >>16;
			    int g = (c & 0x00ff00 ) >>8;
			    int b = (c & 0x0000ff ) ;
			    
			    int bitmVal = r *  g *   b   ;
			    
			    sum_R = sum_R + bitmVal ;
			
			}	
			result_sum_RGB[ 0 ] = sum_R;
		}
		
		return result_sum_RGB ;

	}

	
	/**
	 * Normalizacion de cada sumatoria de componente
	 * */
	@Override
	public void postProcess() {
		
		//en las tres primeras posiciones se encuentran los volumenes de la imagen original
		double volumenorigR = vectorDescriptor[ 0 ];
		
		
		for(int i = 0 ; i < vectorDescriptor.length ; i++ ){
			vectorDescriptor[ i ] = vectorDescriptor[ i ] / volumenorigR  ;	
		}
		
	}


	@Override
	public void setLocalParameters() {
		// TODO Auto-generated method stub
		
	}
	
	


}