package descriptor;

import ij.process.ImageProcessor;

/**
 * Volumen estandar: sumatoria de todos valores de los pixeles 
 * Requiere que la imagen sea RGB , sino lanza una excepcion 
 * @author nelsond
 * @param image procesor RGB 32 bits 
 * @return sumatoria de los valores de intensidad de los pixeles
 * */


public class VolumenRGBGray extends Descriptor{


			
	public VolumenRGBGray(){
		
		super();
		
		//3 valores para cada elemento de la serie
		this.setVALUES_F_ELEMENT( 4 );
		
	}


	@Override
	public double[] calculate(ImageProcessor ip ) {
		
		
		//asegura que NO sea de un solo canal 
		if(ip.getNChannels() <  3 ) {
			new Exception("Error , la imagen debe ser RGB");
		}
	
	
		double [] result_sum_RGBGray = new double[ 4 ];
		double sum_R = 0 ;
		double sum_G = 0 ;
		double sum_B = 0 ;
		double sum_Gray = 0 ;
		
		
		int h = ip.getHeight();
		int w = ip.getWidth();
		
		for(int v = 0 ; v< h ; v++){
			for(int u = 0 ; u< w ; u++){
							
				int c = ip.get(u,v);
	
				//extraer el componente RGB de cada pixel
				int r = (c & 0xff0000 ) >>16;
			    int g = (c & 0x00ff00 ) >>8;
			    int b = (c & 0x0000ff ) ;
			    
			    
			    
			    sum_R = sum_R +  r   ;
				sum_G = sum_G +  g   ;
				sum_B = sum_B +  b   ;
				sum_Gray = sum_Gray + (r + g + b  / 3 ) ;
			
			}	
			result_sum_RGBGray[ 0 ] = sum_R;
			result_sum_RGBGray[ 1 ] = sum_G;
			result_sum_RGBGray[ 2 ] = sum_B;
			result_sum_RGBGray[ 3 ] = sum_Gray ;
		}
		
		return result_sum_RGBGray ;

	}

	
	/**
	 * Normalizacion de cada sumatoria de componente
	 * */
	@Override
	public void postProcess() {
		
		//en las tres primeras posiciones se encuentran los volumenes de la imagen original
		double volumenorigR = vectorDescriptor[ 0 ];
		double volumenorigG = vectorDescriptor[ 1 ];
		double volumenorigB = vectorDescriptor[ 2 ];
		//double volumenorigGray = vectorDescriptor[ 3 ];
		
		byte actual = 'R';
		
		for(int i = 0 ; i < vectorDescriptor.length ; i++ ){
			
			double totalchannel = 0;
			switch( actual) {
			case 'R' :
				totalchannel = volumenorigR ;
				actual = 'G';
				break;
				
			case 'G' :
				totalchannel = volumenorigG ;
				actual = 'B';
				break;
				
			case 'B' :	
				totalchannel = volumenorigB ;
				//actual = 'L';
				actual = 'R';
				break;
			
		 /* case 'L' :	
			totalchannel = volumenorigGray ;
			actual = 'R';
			break;*/
				
		}
			
			vectorDescriptor[ i ] = vectorDescriptor[ i ] / totalchannel  ;	
		}
		
	}


	@Override
	public void setLocalParameters() {
		// TODO Auto-generated method stub
		
	}
	
	


}