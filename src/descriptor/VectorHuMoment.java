package descriptor;

import ij.ImagePlus;
import ij.plugin.ChannelSplitter;
import ij.process.ImageProcessor;

/**
 * 
 * @author nelsond
 * @param image procesor RGB 32 bits 
 * */


public class VectorHuMoment extends Descriptor{

			
	public VectorHuMoment(){
		
		super();
		
		//3 valores para cada elemento de la serie
		this.setVALUES_F_ELEMENT( 9 );
		
	}


	@Override
	public double[] calculate(ImageProcessor ip ) {
		
		
		//asegura que NO sea de un solo canal 
		if(ip.getNChannels() <  3 ) {
			new Exception("Error , la imagen debe ser RGB");
		}
	
		double [] result_element_RGB = new double[ 9];
		
		ImagePlus implus = new ImagePlus(  "proc_split", ip );
		ImagePlus[] implusRGB =  ChannelSplitter.split( implus) ;
		
		//R
		ImageMoments_  Rimgm = new ImageMoments_();
		Rimgm.setup("",  implusRGB[0] );
		Rimgm.run( implusRGB[0].getProcessor() ); 
		result_element_RGB[0]= ( Double )Rimgm.hashmapOutput.get("m[3][0]");
		result_element_RGB[1]= ( Double )Rimgm.hashmapOutput.get("m[0][0]");
		result_element_RGB[2]= ( Double )Rimgm.hashmapOutput.get("m[0][3]");
		
		//G
		ImageMoments_  Gimgm = new ImageMoments_();
		Gimgm.setup("",  implusRGB[1] );
		Gimgm.run( implusRGB[1].getProcessor() ); 
		result_element_RGB[3]= ( Double )Gimgm.hashmapOutput.get("m[3][0]");
		result_element_RGB[4]= ( Double )Gimgm.hashmapOutput.get("m[0][0]");
		result_element_RGB[5]= ( Double )Gimgm.hashmapOutput.get("m[0][3]");
		
		//B
		ImageMoments_  Bimgm = new ImageMoments_();
		Bimgm.setup("",  implusRGB[2] );
		Bimgm.run( implusRGB[2].getProcessor() ); 
		result_element_RGB[6]= ( Double )Bimgm.hashmapOutput.get("m[3][0]");
		result_element_RGB[7]= ( Double )Bimgm.hashmapOutput.get("m[0][0]");
		result_element_RGB[8]= ( Double )Bimgm.hashmapOutput.get("m[0][3]");
		
		
		/*
		 *una vez calculados la energia y el volumen se calcula la varianza de cada canal  
		 * */
		
		return result_element_RGB  ;

	}

	
	/**
	 * algun tipo de calculo que requiera disponer 
	 * de todos los elementos para ser efectuado 
	 * */
	@Override
	public void postProcess() {
		
		double [] originals = new double[9];
		
		for(int i = 0 ; i < originals.length ; i++ ){
			originals[i] = vectorDescriptor[ i ] ;
		}
		
		for(int k = 0 ; k < vectorDescriptor.length ; k+=9 ){
			for(int l = 0 ; l < originals.length ; l++ ){
				vectorDescriptor[ k + l] = vectorDescriptor[ k + l]  / originals[ l ]  ;
			}
			
		}
	
	}


	@Override
	public void setLocalParameters() {
		// TODO Auto-generated method stub
		
	}
	
	


}