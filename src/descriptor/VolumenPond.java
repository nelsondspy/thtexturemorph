package descriptor;

import ij.process.ImageProcessor;

/**
 * Volumen estandar: sumatoria de todos valores de los pixeles 
 * Requiere que la imagen sea RGB , sino lanza una excepcion 
 * @author nelsond
 * @param image procesor RGB 32 bits 
 * @return sumatoria de los valores de intensidad de los pixeles
 * */


public class VolumenPond extends Descriptor{

	static final double GRAY_POND = 0.2 ;
	static final double CHANNEL_POND = 0.8 ;
			
	public VolumenPond(){
		
		super();
		
		//3 valores para cada elemento de la serie
		this.setVALUES_F_ELEMENT( 3 );
		
	}


	@Override
	public double[] calculate(ImageProcessor ip ) {
		
		
		//asegura que NO sea de un solo canal 
		if(ip.getNChannels() <  3 ) {
			new Exception("Error , la imagen debe ser RGB");
		}
	
	
		double [] result_sum_RGB = new double[ 3 ];
		double sum_R = 0 ;
		double sum_G = 0 ;
		double sum_B = 0 ;
		
		
		int h = ip.getHeight();
		int w = ip.getWidth();
		
		for(int v = 0 ; v< h ; v++){
			for(int u = 0 ; u< w ; u++){
							
				int c = ip.get(u,v);
	
				//extraer el componente RGB de cada pixel
				int r = (c & 0xff0000 ) >>16;
			    int g = (c & 0x00ff00 ) >>8;
			    int b = (c & 0x0000ff ) ;
			    
			    final double GRAY_VAL =  GRAY_POND * (r + g + b  / 3 ) ;
			    
			    sum_R = sum_R + (CHANNEL_POND * r ) + GRAY_VAL ;
				sum_G = sum_G + (CHANNEL_POND * g ) + GRAY_VAL ;
				sum_B = sum_B + (CHANNEL_POND * b)  + GRAY_VAL ;
			
			}	
			result_sum_RGB[ 0 ] = sum_R;
			result_sum_RGB[ 1 ] = sum_G;
			result_sum_RGB[ 2 ] = sum_B;	
		}
		
		return result_sum_RGB ;

	}

	
	/**
	 * Normalizacion de cada sumatoria de componente
	 * */
	@Override
	public void postProcess() {
		
		//en las tres primeras posiciones se encuentran los volumenes de la imagen original
		double volumenorigR = vectorDescriptor[ 0 ];
		double volumenorigG = vectorDescriptor[ 1 ];
		double volumenorigB = vectorDescriptor[ 2 ];
		byte actual = 'R';
		
		for(int i = 0 ; i < vectorDescriptor.length ; i++ ){
			
			double totalchannel = 0;
			switch( actual) {
			case 'R' :
				totalchannel = volumenorigR ;
				actual = 'G';
				break;
				
			case 'G' :
				totalchannel = volumenorigG ;
				actual = 'B';
				break;
				
			case 'B' :	
				totalchannel = volumenorigB ;
				actual = 'R';
				break;
			}
			
			vectorDescriptor[ i ] = vectorDescriptor[ i ] / totalchannel  ;	
		}
		
	}


	@Override
	public void setLocalParameters() {
		// TODO Auto-generated method stub
		
	}
	
	


}