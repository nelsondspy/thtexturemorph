package descriptor;

import ij.ImagePlus;
import ij.plugin.ChannelSplitter;
import ij.process.ImageProcessor;
import ij.process.ImageStatistics;

/**
 * Volumen estandar: sumatoria de todos valores de los pixeles 
 * Requiere que la imagen sea RGB , sino lanza una excepcion 
 * @author nelsond
 * @param image procesor RGB 32 bits 
 * @return sumatoria de los valores de intensidad de los pixeles
 * */


public class Variance extends Descriptor{

		
	public Variance(){
		
		super();
		
		//3 valores para cada elemento de la serie, uno para cada canal 
		this.setVALUES_F_ELEMENT( 3 );
		
	}


	@Override
	public double[] calculate(ImageProcessor ip ) {
		
		
		//asegura que NO sea de un solo canal 
		if(ip.getNChannels() <  3 ) {
			new Exception("Error , la imagen debe ser RGB");
		}
	
	
		double [] result_dev_RGB = new double[ 3 ];
		
		ImagePlus implus = new ImagePlus(  "proc_split", ip );
		ImagePlus[] implusRGB =  ChannelSplitter.split( implus) ;
	
		
		//desviacion canal R
		result_dev_RGB[0 ] = Math.pow( implusRGB[ 0 ].getStatistics(ImageStatistics.STD_DEV).stdDev ,2 ) ;
		//desviacion canal G
		result_dev_RGB[1 ] = Math.pow(implusRGB[ 1 ].getStatistics(ImageStatistics.STD_DEV).stdDev, 2 );	
		//desviacion canal B
		result_dev_RGB[2 ] = Math.pow( implusRGB[ 2 ].getStatistics(ImageStatistics.STD_DEV).stdDev ,2 ) ; 
		
		return result_dev_RGB ;

	}

	
	/**
	 * Normalizacion
	 * 
	 * */
	@Override
	public void postProcess() {
			//cargar los valores originales en un array (volumen y energia originales )
		double []originalValues = new double[ 3  ];
		
		for(int k = 0 ; k< 3  ; k++  ){
			originalValues[k] = vectorDescriptor[k];
		}
		
		//normalizacion y calculo de distancia  		
		for(int i = 0 ; i < vectorDescriptor.length-3 ; i+=3 ){	
			for(int j = 0 ; j < 3 ; j++  ){
				vectorDescriptor[ i + j  ] /= originalValues[ j ];
			}	
		}	

	}


	@Override
	public void setLocalParameters() {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	


}