package descriptor;

import ij.ImagePlus;
import ij.plugin.ChannelSplitter;
import ij.process.ImageProcessor;


public class SpatialMomentAptoula extends Descriptor{

		
		double m_I = 3    ; 
		double m_J = 3 ; 
		
		public SpatialMomentAptoula(){
			super();
			
			//9 valores para cada elemento de la serie
			this.setVALUES_F_ELEMENT( 9 );
			
		}

		public void setI( int i  ){
			m_I = i ;
		}

		public void setJ( int j  ){
			m_J = j ;
		}
		
		
		@Override
		public double[] calculate(ImageProcessor ip ) {
			
			int countChannels = ip.getNChannels() ;
			
			//vector resultado 
			double [] result_RGB = new double[ 9  ]; 

		
			//asegura que sea de tres canales 
			if(ip.getNChannels() <  3 ) {
				new Exception("Error , la imagen debe ser RGB");
			}
			
			ImagePlus implus = new ImagePlus(  "proc_split", ip );
			ImagePlus[] implusRGB =  ChannelSplitter.split( implus) ;
			
			
			int h = ip.getHeight();
			int w = ip.getWidth();
			int index= 0 ; 
			//realiza el procesamiento por canal 
			for(int c= 0 ; c < countChannels ; c++ ){
				
				
				ImageProcessor currentChProc  = implusRGB[ c  ].getProcessor();
				
				double C_m00 = BasicSpatialCalc( currentChProc , 0, 0 );
				double C_m10 = BasicSpatialCalc( currentChProc , 1, 0 );	
				double C_m01 = BasicSpatialCalc( currentChProc , 0, 1 );
				
				
				double Xmed_C = C_m10 / C_m00 ;
				double Ymed_C = C_m01 / C_m00 ;
				
				System.out.println( "C_m00:" + C_m00 + ",C_m10:" + C_m10 + ",C_m01:" + C_m01  );
				System.out.println( "Xmed_C:" + Xmed_C + ", Ymed_C:" + Ymed_C );
				
				double U_0_0 = 0  ;
				double U_i_0 = 0  ;
				double U_0_j = 0  ;
				
				double N_0_0 = 0 ;
				double N_i_0 = 0 ;
				double N_0_j = 0 ;
				
				for(int v = 0 ; v< h ; v++){
					for(int u = 0 ; u< w ; u++){
						
						int pixelval = currentChProc.get(u,v)	;
						
						U_0_0 +=  (  Math.pow( u  - Xmed_C , 0 ) *  
								 Math.pow( v - Ymed_C , 0 )  * pixelval  );

						
						U_i_0 +=  (  Math.pow( u  - Xmed_C , m_I ) *  
								 Math.pow( v - Ymed_C , 0 )  * pixelval  );

						U_0_j +=  (  Math.pow( u  - Xmed_C , 0 ) *  
								 Math.pow( v - Ymed_C , m_J )  * pixelval  );
						
					}	
				}
			
				double alpha_00  = ( (  0 + 0  ) /  2 ) +  1 ; //intencionalmente 0+0 para que se entienda que no es un caso particular				
				double alpha_i0  = ( ( m_I + 0 ) /  2 ) +  1 ;
				double alpha_0j  = ( ( 0 + m_J ) /  2 ) +  1 ;
				
				N_0_0 = U_0_0   /  Math.pow( C_m00 , alpha_00 ) ;
				N_i_0 = U_i_0   /  Math.pow( C_m00 , alpha_i0 ) ;
				N_0_j = U_0_j   /  Math.pow( C_m00 , alpha_0j ) ;
				
				result_RGB[ index ]      = N_0_0;
				result_RGB[ index + 1 ]  = N_i_0 ;
				result_RGB[ index + 2 ]  = N_0_j  ;
				
				index +=3 ;
				
			}
			
			
			System.out.println("result_RGB :( " +  result_RGB[0]  + "," + 
					result_RGB[1] + "," + result_RGB[2] + ")" );
			
			
			return result_RGB ;
			
		}


		/**
		 * Calcula el momento espacial basico 
		 * @param imagen de entrada 
		 * @param peso para las coordenadas X 
		 * @param peso para las coordenadas Y 
		 * */
		public static double  BasicSpatialCalc(ImageProcessor ip, int m_I, int m_J  ){
			int h = ip.getHeight();
			int w = ip.getWidth();
			
			double  m_i_j = 0  ;  
			for(int v = 0 ; v< h ; v++){
				for(int u = 0 ; u< w ; u++){
					//sum[0] = sum[0] + ( double )( ip.get(u,v) );
					
					m_i_j +=  ( Math.pow(u, m_I ) *  Math.pow(v, m_J )  *    ip.get(u,v) ); 
					
				}	
			}
			
			return m_i_j ;
		}
		
		
		/**
		 * primer parametro debe ser el peso para I 
		 * segundo parametro peso para J
		 * */
		public void  setLocalParameters(){
			
			/*
			if( DESCRIPTOR_PRM == null ) return ; 
			
			double [] p = paramsStringToDouble();
			m_I = p[0]  ; 
			m_J = p[1]  ; 
			
			System.out.println("m_I: "  + m_I  + "," + "m_J : " + m_J);
			*/
			
		}
	
		/**
		 * 
		 * requerido para normalizar
		 * para cada elemento de la serie : Rnij, Gnij, Bij, Rn00, Gn00, B00  
		 * 
		 * */
		@Override
		public void postProcess() {
		
			double []originalValues = new double[ 9  ];
			
			for(int k = 0 ; k< 9  ; k++  ){
				originalValues[k] = vectorDescriptor[k];
			}
			
			//normalizacion 	
			for(int i = 0 ; i < vectorDescriptor.length-9 ; i+=9 ){	
				for(int j = 0 ; j < 9 ; j++  ){
					vectorDescriptor[ i + j  ] /= originalValues[ j ];
				}	
			}
	
		}
		

}
