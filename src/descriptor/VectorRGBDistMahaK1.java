package descriptor;

import ij.process.ImageProcessor;

/**
 * Solo Vector de distancias mahalanobis intercanales 
 * Requiere que la imagen sea RGB , sino lanza una excepcion 
 * @author nelsond
 * @param image procesor RGB 32 bits 
 * @return sumatoria de los valores de intensidad de los pixeles
 * */


public class VectorRGBDistMahaK1 extends Descriptor{

			
	public VectorRGBDistMahaK1(){
		
		super();
		
		//3 valores para cada elemento de la serie
		this.setVALUES_F_ELEMENT( 3 );
		
	}


	@Override
	public double[] calculate(ImageProcessor ip ) {
		
		
		//asegura que NO sea de un solo canal 
		if(ip.getNChannels() <  3 ) {
			new Exception("Error , la imagen debe ser RGB");
		}
	
		double [] result_element_RGB = new double[ 3 ];
		
		int h = ip.getHeight();
		int w = ip.getWidth();
		
		/*primero se calcula el volumen para cada canal*/
		for(int v = 0 ; v< h ; v++){
			for(int u = 0 ; u< w ; u++){
							
				int c = ip.get(u,v);
	
				//extraer el componente RGB de cada pixel
				int r = (c & 0xff0000 ) >>16;
			    int g = (c & 0x00ff00 ) >>8;
			    int b = (c & 0x0000ff ) ;
			    
			    
			    result_element_RGB[ 0 ] += r;
			    result_element_RGB[ 1 ] += g;
			    result_element_RGB[ 2 ] += b;	
				
			   
			}		
		}
		
		/*
		 *una vez calculados la energia y el volumen se calcula la varianza de cada canal  
		 * */
		
		return result_element_RGB  ;

	}

	
	/**
	 * Normalizacion de cada sumatoria de componente o algun tipo de calculo que requiera dispones 
	 * de todos los elementos para ser efectuado 
	 * */
	@Override
	public void postProcess() {
		
	
		// calculo de distancia usando las covarianzas calculadas 
		for(int i = 0 ; i < vectorDescriptor.length ; i+=3 ){
			
			double volR = vectorDescriptor[ i + 0 ]  ;
			double volG = vectorDescriptor[ i + 1 ]  ;
			double volB = vectorDescriptor[ i + 2 ] ;
		
			
			double valorMedio = (volR + volG + volB ) / 3 ;
			double desviacion = Math.pow(volR - valorMedio, 2) +
						Math.pow( volG - valorMedio, 2) +
						Math.pow(volB - valorMedio, 2);
			desviacion =Math.sqrt (0.5 * desviacion );
			
			
			//distancia R-G de volumen y energia 
			
			vectorDescriptor[ i + 0] = Math.sqrt ( Math.pow(volR - volG,2 ) / desviacion )  ;
			//distancia G-B
			vectorDescriptor[ i + 1] = Math.sqrt ( Math.pow( volG - volB, 2)/ desviacion );
			//distancia B-R 
			vectorDescriptor[ i + 2] = Math.sqrt ( Math.pow( volB - volR,2) /desviacion );
 	
			
			
		//normalizacion 
			double originalRGB[] = new double [3];
			originalRGB[ 0 ] = vectorDescriptor[0] ;
			originalRGB[ 1 ] = vectorDescriptor[1] ;
			originalRGB[ 2 ] = vectorDescriptor[2] ;
		
			/* 
			 * Son propiedades de esta distancia:
1) Es invariante por cambios de escala.
2) Es una distancia normalizada expresada en unidades de desviaci ́on t ́ıpica. Para
una variable con distribuci ́on normal, el campo de variabilidad de esta distancia
estar ́a pr ́acticamente comprendido entre 0 y 4
			 * 
			for(int k = 0 ; k < vectorDescriptor.length-3 ; k+=3 ){
				vectorDescriptor[ k + 0] = vectorDescriptor[ k + 0] / originalRGB[ 0 ];
				//distancia G-B
				vectorDescriptor[ k + 1] = vectorDescriptor[ k + 1 ] / originalRGB[ 1 ];
				//distancia B-R 
				vectorDescriptor[ k + 2] = vectorDescriptor[ k + 2] / originalRGB[ 2 ] ;
			
			}		
			
			*/
		}	
	}


	@Override
	public void setLocalParameters() {
		// TODO Auto-generated method stub
		
	}
	
	


}