package descriptor;

import ij.ImagePlus;
import ij.plugin.ChannelSplitter;
import ij.process.ImageProcessor;


public class SpatialMomentBasic extends Descriptor{

		
		double m_I   ; 
		double m_J ; 
		
		public SpatialMomentBasic(){
			super();
			
			//3 valores para cada elemento de la serie
			this.setVALUES_F_ELEMENT( 3 );
			
		}

		public void setI( int i  ){
			m_I = i ;
		}

		public void setJ( int j  ){
			m_J = j ;
		}
		
		
		@Override
		public double[] calculate(ImageProcessor ip ) {
			
			int countChannels = ip.getNChannels() ;
			
			//vector resultado 
			double [] result_RGB = new double[ countChannels ]; 

		
			//asegura que sea de tres canales 
			if(ip.getNChannels() <  3 ) {
				new Exception("Error , la imagen debe ser RGB");
			}
			
			ImagePlus implus = new ImagePlus(  "proc_split", ip );
			ImagePlus[] implusRGB =  ChannelSplitter.split( implus) ;
			
		
			//realiza el procesamiento por canal 
			for(int c= 0 ; c < countChannels ; c++ ){
				
				ImageProcessor currentChProc  = implusRGB[ c  ].getProcessor();
				
				double momentChannel = BasicSpatialCalc( currentChProc , (int)m_I, (int)m_J  );
			
				result_RGB[ c ] = momentChannel ;
				
			}
			
			
			
			return result_RGB ;
			
		}


		/**
		 * Calcula el momento espacial basico 
		 * @param imagen de entrada 
		 * @param peso para las coordenadas X 
		 * @param peso para las coordenadas Y 
		 * */
		public static double  BasicSpatialCalc(ImageProcessor ip, int m_I, int m_J  ){
			int h = ip.getHeight();
			int w = ip.getWidth();
			
			double  m_i_j = 0  ;  
			for(int v = 0 ; v< h ; v++){
				for(int u = 0 ; u< w ; u++){
					
					m_i_j +=  ( Math.pow(u, m_I ) *  Math.pow(v, m_J )  *    ip.get(u,v) ); 
					
				}	
			}
			
			return m_i_j ;
		}
		
		
		/**
		 * primer parametro debe ser el peso para I 
		 * segundo parametro peso para J
		 * */
		public void  setLocalParameters(){
			
			if( DESCRIPTOR_PRM == null ) return ; 
			
			double [] p = paramsStringToDouble();
			m_I = p[0]  ; 
			m_J = p[1]  ; 
			
			System.out.println("m_I: "  + m_I  + "," + "m_J : " + m_J);
			
		}
	
		
		
		/**
		 * 
		 * requerido para normalizar
		 * para cada elemento de la serie : Rnij, Gnij, Bij, Rn00, Gn00, B00  
		 * 
		 * */
		@Override
		public void postProcess() {
		
			
			double mOrigR = vectorDescriptor[ 0 ];
			double mOrigG = vectorDescriptor[ 1 ];
			double mOrigB = vectorDescriptor[ 2 ];
			
			int i = 0 ;
			while(i  <  vectorDescriptor.length ){
				
				vectorDescriptor[ i ] = vectorDescriptor[ i ] / mOrigR  ;
				i++ ;
				vectorDescriptor[ i  ] = vectorDescriptor[ i ] / mOrigG  ;
				i++ ;
				vectorDescriptor[ i  ] = vectorDescriptor[ i ] / mOrigB  ;
				
				i++ ;
	
			}
			
		}
		

}
