package descriptor;

import ij.ImagePlus;
import ij.plugin.ChannelSplitter;
import ij.process.ImageProcessor;
import ij.process.ImageStatistics;

/**
 * Volumen estandar: sumatoria de todos valores de los pixeles 
 * Requiere que la imagen sea RGB , sino lanza una excepcion 
 * @author nelsond
 * @param image procesor RGB 32 bits 
 * @return sumatoria de los valores de intensidad de los pixeles
 * */


public class Kurtosis extends Descriptor{

		
	public Kurtosis(){
		
		super();
		
		//3 valores para cada elemento de la serie, uno para cada canal 
		this.setVALUES_F_ELEMENT( 3 );
		
	}


	@Override
	public double[] calculate(ImageProcessor ip ) {
		
		
		//asegura que NO sea de un solo canal 
		if(ip.getNChannels() <  3 ) {
			new Exception("Error , la imagen debe ser RGB");
		}
	
	
		double [] result_entrop_RGB = new double[ 3 ];
		
		ImagePlus implus = new ImagePlus(  "proc_split", ip );
		ImagePlus[] implusRGB =  ChannelSplitter.split( implus) ;
	
		
		//desviacion canal R
		result_entrop_RGB[ 0 ] = implusRGB[ 0 ].getStatistics(ImageStatistics.KURTOSIS).kurtosis ;
		//desviacion canal G
		result_entrop_RGB[ 1 ] = implusRGB[ 1 ].getStatistics(ImageStatistics.KURTOSIS).kurtosis ;	
		//desviacion canal B
		result_entrop_RGB[ 2 ] = implusRGB[ 2 ].getStatistics(ImageStatistics.KURTOSIS).kurtosis ; 
		
		return result_entrop_RGB ;

	}

	
	/**
	 * Normalizacion de la desviacion con respecto a la desviacion de la imagen original 
	 * 
	 * */
	@Override
	public void postProcess() {
	/*	
		//en las tres primeras posiciones se encuentran los volumenes de la imagen original
		double volumenorigR = vectorDescriptor[ 0 ];
		double volumenorigG = vectorDescriptor[ 1 ];
		double volumenorigB = vectorDescriptor[ 2 ];
		byte actual = 'R';
		
		for(int i = 0 ; i < vectorDescriptor.length ; i++ ){
			
			double totalchannel = 0;
			switch( actual) {
			case 'R' :
				totalchannel = volumenorigR ;
				actual = 'G';
				break;
				
			case 'G' :
				totalchannel = volumenorigG ;
				actual = 'B';
				break;
				
			case 'B' :	
				totalchannel = volumenorigB ;
				actual = 'R';
				break;
			}
			
			vectorDescriptor[ i ] = vectorDescriptor[ i ] / totalchannel  ;	
		}
		*/
		
	}


	@Override
	public void setLocalParameters() {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	


}