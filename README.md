# README #
Análisis de textura utilizando morfología matemática, usando api de imagej.
  

### Cuestiones ###

* Basado principalmente en granulometría, covarianza morfológica y el operador híbrido de Aptoula. 

### Dependencias ###
* Requiere Weka 
* Requiere Imagej version1.49
